using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;


namespace GSAssetManagement.Repository
{
    public class StaticDataMasterRepository : RepositoryBase<StaticDataMaster, StaticDataMasterDTO>, IStaticDataMasterRepository
    {
        public StaticDataMasterRepository(IDatabaseFactory databaseFactory, IAuthenticationHelper authenticationHelper)
            : base(databaseFactory, authenticationHelper)
        {
    
        }
    }
    
    public interface IStaticDataMasterRepository : IRepository<StaticDataMaster, StaticDataMasterDTO> { }
}