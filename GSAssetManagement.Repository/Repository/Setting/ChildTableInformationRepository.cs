using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;


namespace GSAssetManagement.Repository
{
    public class ChildTableInformationRepository : RepositoryBase<ChildTableInformation, ChildTableInformationDTO>, IChildTableInformationRepository
    {
        public ChildTableInformationRepository(IDatabaseFactory databaseFactory, IAuthenticationHelper authenticationHelper)
            : base(databaseFactory, authenticationHelper)
        {
    
        }
    }
    
    public interface IChildTableInformationRepository : IRepository<ChildTableInformation, ChildTableInformationDTO> { }
}