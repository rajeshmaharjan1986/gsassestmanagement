using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;


namespace GSAssetManagement.Repository
{
    public class StaticDataDetailsRepository : RepositoryBase<StaticDataDetails, StaticDataDetailsDTO>, IStaticDataDetailsRepository
    {
        public StaticDataDetailsRepository(IDatabaseFactory databaseFactory, IAuthenticationHelper authenticationHelper)
            : base(databaseFactory, authenticationHelper)
        {
    
        }

        public async Task<StaticDataDetailsDTO> GetStaticDataByValue(string columnName, string dataValue)
        {
            try {
                Expression<Func<StaticDataDetails, bool>> getData = s => s.ColumnName == columnName && s.Record_Status == RecordStatus.Active;
                if (!string.IsNullOrEmpty(dataValue))
                {
                    getData = s => s.ColumnName == columnName && s.Parameter3 == dataValue && s.Record_Status == RecordStatus.Active;
                }

                var dto = await this.GetManyDTO(getData);
                if (dto.Count() > 0)
                {
                    return dto.FirstOrDefault();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public async Task<List<StaticDataDetailsDTO>> GetStaticDataList(string columnName, string parameterData)
        {

            try
            {
                try
                {
                    Expression<Func<StaticDataDetails, bool>> getData = s => s.ColumnName == columnName && s.Record_Status == RecordStatus.Active;
                    Expression<Func<StaticDataDetails, bool>> getData2 = s => s.ColumnName == columnName && s.Record_Status == RecordStatus.Active;
                    if (!string.IsNullOrEmpty(parameterData))
                    {
                        getData2 = s => s.ColumnName == columnName && s.Parameter1 == parameterData && s.Record_Status == RecordStatus.Active;
                    }

                    var dto = await this.GetManyDTO(getData2);

                    if (dto.Count() == 0) {
                        dto = await this.GetManyDTO(getData);
                    }

                    return dto.ToList<StaticDataDetailsDTO>();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    
    public interface IStaticDataDetailsRepository : IRepository<StaticDataDetails, StaticDataDetailsDTO> {
        Task<StaticDataDetailsDTO> GetStaticDataByValue(string columnName,string dataValue);

        Task<List<StaticDataDetailsDTO>> GetStaticDataList(string columnName, string parameterData);
    }
}