using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks; 
using GSAssetManagement.Entity; 
using GSAssetManagement.Entity.DTO;         
using GSAssetManagement.Infrastructure;     


namespace GSAssetManagement.Repository
{
    public class AssetDetailRepository : RepositoryBase<AssetDetail, AssetDetailDTO>, IAssetDetailRepository
    {
        public AssetDetailRepository(IDatabaseFactory databaseFactory, IAuthenticationHelper authenticationHelper)
            : base(databaseFactory, authenticationHelper)
        {
    
        }

        public Guid AddAssetDetail(AssetDetailDTO dto, bool Autoauthorise)
        {
            try
            {
                Guid Id = this.Add(dto, Autoauthorise);

                foreach (AssetExtraDetailDTO assetExtraDetailDTO in dto.assetExtraDetailDTOs) {
                    AssetExtraDetail entity = new AssetExtraDetail();
                    entity.Id = Guid.NewGuid();
                    entity.Location = assetExtraDetailDTO.Location;
                    entity.AssetDetailId = Id;
                    entity.PurchaseDate = assetExtraDetailDTO.PurchaseDate;
                    entity.ExpireDate = assetExtraDetailDTO.ExpireDate;

                    this.DataContext.AssetExtraDetails.Add(entity);
                }

                return Id;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        public async Task UpdateAssetDetail(AssetDetailDTO dto, bool Autoauthorise)
        {
            try
            {
                await this.Update(dto, Autoauthorise);

                var listAssetExtraDetail = this.DataContext.AssetExtraDetails.Where(x => x.AssetDetailId == dto.Id).ToList<AssetExtraDetail>();
                this.DataContext.AssetExtraDetails.RemoveRange(listAssetExtraDetail);

                foreach (AssetExtraDetailDTO assetExtraDetailDTO in dto.assetExtraDetailDTOs)
                {
                    AssetExtraDetail entity = new AssetExtraDetail();
                    entity.Id = Guid.NewGuid();
                    entity.Location = assetExtraDetailDTO.Location;
                    entity.AssetDetailId = dto.Id;
                    entity.PurchaseDate = assetExtraDetailDTO.PurchaseDate;
                    entity.ExpireDate = assetExtraDetailDTO.ExpireDate;

                    this.DataContext.AssetExtraDetails.Add(entity);
                }

                return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<AssetDetailDTO> GetAssetDetailDTOById(Guid Id)
        {
            try {
                AssetDetailDTO dto = await this.GetDTOById(Id);

                Expression<Func<AssetExtraDetail, bool>> getData = s => s.AssetDetailId == Id;

                List<AssetExtraDetail> list = this.DataContext.AssetExtraDetails.Where(getData).ToList<AssetExtraDetail>();

                List<AssetExtraDetailDTO> AssetExtraDetailDtos = new List<AssetExtraDetailDTO>();

                list.ForEach(entity =>
                {
                    AssetExtraDetailDTO assetExtraDetailDTO = MapperHelper.Get<AssetExtraDetailDTO, AssetExtraDetail>(Activator.CreateInstance<AssetExtraDetailDTO>(), entity);
                    AssetExtraDetailDtos.Add(assetExtraDetailDTO);
                });
                dto.assetExtraDetailDTOs.AddRange(AssetExtraDetailDtos);

                return dto;
            }
            catch (Exception ex) {
                throw ex;
            }
        }
    }
    
    public interface IAssetDetailRepository : IRepository<AssetDetail, AssetDetailDTO> 
    {
        Guid AddAssetDetail(AssetDetailDTO dto, bool Autoauthorise);
        Task UpdateAssetDetail(AssetDetailDTO dto, bool Autoauthorise);
        Task<AssetDetailDTO> GetAssetDetailDTOById(Guid Id);
    }
}
