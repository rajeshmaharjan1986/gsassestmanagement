﻿using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace GSAssetManagement.Repository
{
    public class CommonRepository : RepositoryBase<StaticDataDetails, StaticDataDetailsDTO>, ICommonRepository
    {

        private readonly IAuthenticationHelper _authenticationHelper;

        public CommonRepository(IDatabaseFactory dataBaseFactory, IAuthenticationHelper authenticationHelper) :
            base(dataBaseFactory, authenticationHelper)
        {
            _authenticationHelper = authenticationHelper;
        }

        public async Task GetSchemaInformationList(string FileName)
        {
            try
            {
                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _commnd = new SqlCommand("EXEC [Setting].[GetModuleSetupSummary]", _connection))
                    {

                        using (XmlReader _reader = await _commnd.ExecuteXmlReaderAsync())
                        {
                            using (XmlWriter writer = XmlWriter.Create(FileName))
                            {
                                writer.WriteNode(_reader, true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task GetAccountOpeningFlowSetupList(string FileName)
        {
            try
            {
                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _commnd = new SqlCommand("EXEC KYCManagement.GetAccountOpeningFlowSetup", _connection))
                    {

                        using (XmlReader _reader = await _commnd.ExecuteXmlReaderAsync())
                        {
                            using (XmlWriter writer = XmlWriter.Create(FileName))
                            {
                                writer.WriteNode(_reader, true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task GetDropdownList(string FileName)
        {
            try
            {
                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _commnd = new SqlCommand("EXEC Setting.GetStaticDataDetails", _connection))
                    {

                        using (XmlReader _reader = await _commnd.ExecuteXmlReaderAsync())
                        {
                            using (XmlWriter writer = XmlWriter.Create(FileName))
                            {
                                writer.WriteNode(_reader, true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task GetTableInformationList(string FileName)
        {
            try
            {
                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _commnd = new SqlCommand("EXEC Setting.GetTableInformationList", _connection))
                    {

                        using (XmlReader _reader = await _commnd.ExecuteXmlReaderAsync())
                        {
                            using (XmlWriter writer = XmlWriter.Create(FileName))
                            {
                                writer.WriteNode(_reader, true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task GetRolesPriority(string FileName)
        {
            try
            {
                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _commnd = new SqlCommand("EXEC Setting.GetRolesPriority", _connection))
                    {

                        using (XmlReader _reader = await _commnd.ExecuteXmlReaderAsync())
                        {
                            using (XmlWriter writer = XmlWriter.Create(FileName))
                            {
                                writer.WriteNode(_reader, true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Select>> GetSelectList(params string[] ColumnNames)
        {
            try
            {
                string ColumnName = string.Join(",", ColumnNames.Select(c => string.Format("'{0}'", c)));

                List<SelectListCustom> _returnselectList = new List<SelectListCustom>();

                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                string _CommandString = string.Format(@"SELECT [ColumnName]
      ,[Title]
      ,[Value]     
  FROM [Setting].[StaticDataDetails]
  WHERE ColumnName IN({0})", ColumnName);



                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _command = new SqlCommand(_CommandString, _connection))
                    {
                        using (SqlDataReader _reader = await _command.ExecuteReaderAsync())
                        {
                            using (DataTable _selectValues = new DataTable())
                            {
                                _selectValues.Load(_reader);

                                List<Select> _selectValuesList = _selectValues.ToList<Select>().ToList();

                                return _selectValuesList;
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<ParameterisedDatasourceDTO>> GetParameterisedDatasource(List<ParameterisedDatasourceDTO> ParameterList)
        {
            try
            {
                string Statement = string.Empty;

                List<string> _dataSourcesList = new List<string>();


                string ColumnName = string.Join(",", ParameterList.Select(c => string.Format("'{0}'", c.ColumnName)));

                List<SelectListCustom> _returnselectList = new List<SelectListCustom>();

                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                string _CommandString = string.Format(@"SELECT * FROM vDataSourceInformation
                WHERE ColumnName IN({0}) AND ParameterisedSource = 1 ", ColumnName);



                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _command = new SqlCommand(_CommandString, _connection))
                    {
                        using (SqlDataReader _reader = await _command.ExecuteReaderAsync())
                        {
                            using (DataTable _dataSources = new DataTable())
                            {
                                _dataSources.Load(_reader);
                                IList<vDataSourceInformation> _information = _dataSources.ToList<vDataSourceInformation>();

                                ParameterList.ForEach(s =>
                                {
                                    string _dataSource = _information.Where(x => x.ColumnName == s.ColumnName && x.DatabaseTable == s.TableName).Select(v => v.Datasource).FirstOrDefault();

                                    if (!string.IsNullOrEmpty(_dataSource))
                                    {

                                        s.Parameters.ForEach(p =>
                                        {
                                            if (p.Name == "@ApplicationUserId")
                                            {
                                                _dataSource = string.Format("SELECT * FROM ({0}) {1} WHERE {1}.Value = '{2}'", _dataSource.Replace(string.Format("{0}", p.Name), string.Format("'{0}'", _authenticationHelper.GetUserId())), s.ColumnName, s.SelectedValue);
                                            }
                                            else
                                            {
                                                if (p.Value == null)
                                                {
                                                    _dataSource = string.Format("SELECT * FROM ({0}) {1} WHERE {1}.Value = '{2}'", _dataSource.Replace(string.Format("{0}", p.Name), "NULL"), s.ColumnName, s.SelectedValue);
                                                }
                                                else
                                                {
                                                    _dataSource = string.Format("SELECT * FROM ({0}) {1} WHERE {1}.Value = '{2}'", _dataSource.Replace(string.Format("{0}", p.Name), string.Format("'{0}'", p.Value)), s.ColumnName, s.SelectedValue);

                                                }


                                            }




                                        });

                                        _dataSourcesList.Add(_dataSource);


                                    }



                                });


                                Statement = string.Join(" UNION ALL ", _dataSourcesList);

                                using (SqlCommand _listcommand = new SqlCommand(Statement, _connection))
                                {
                                    using (SqlDataReader _listreader = await _listcommand.ExecuteReaderAsync())
                                    {
                                        using (DataTable _seelctLists = new DataTable())
                                        {
                                            _seelctLists.Load(_listreader);

                                            List<Select> selects = _seelctLists.ToList<Select>().ToList();

                                            ParameterList.ForEach(p =>
                                            {
                                                p.SelectList.AddRange(selects.Where(k => k.ColumnName == p.ColumnName).ToList());

                                            });


                                        }
                                    }
                                }



                            }
                        }
                    }

                }

                return ParameterList;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<SelectListCustom>> GetSelectList(string TableName, List<SelectListParameter> SelectListParameters)
        {
            try
            {
                List<SelectListCustom> _returnselectList = new List<SelectListCustom>();

                var groupParameters = SelectListParameters.GroupBy(x => new { x.ModuleTableSetupId, x.FieldName }).ToList();
                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                foreach (var item in groupParameters)
                {


                    string dataSourceQuery = await (DataContext.ModuleBussinesLogicSetups.Where(v => v.ModuleSetupId == DataContext.ModuleSetups.Where(m => m.DatabaseTable == TableName).Select(y => y.Id).FirstOrDefault()
                    && v.ColumnName == item.Key.FieldName
                    && v.DataSource != null
                    && v.ParameterisedDataSorce).Select(z => z.DataSource).FirstOrDefaultAsync());

                    using (SqlConnection _connection = new SqlConnection(_connectionString))
                    {
                        await _connection.OpenAsync();

                        using (SqlCommand _command = new SqlCommand(string.Format("SELECT * FROM ({0}) Z ORDER BY ColumnName,LEN(OrderValue),OrderValue ", dataSourceQuery), _connection))
                        {
                            foreach (Parameter parameter in SelectListParameters.Where(x => x.FieldName == item.Key.FieldName && x.ModuleTableSetupId == item.Key.ModuleTableSetupId).Select(c => c.Parameters).FirstOrDefault())
                            {
                                if (parameter.Name == "@ApplicationUserId")
                                {
                                    _command.Parameters.Add(new SqlParameter() { ParameterName = parameter.Name, Value = _authenticationHelper.GetUserId() });
                                }
                                else if (parameter.Name == "@CurrentBranch")
                                {
                                    _command.Parameters.Add(new SqlParameter() { ParameterName = parameter.Name, Value = _authenticationHelper.GetBranch() });
                                }
                                else
                                {
                                    _command.Parameters.Add(new SqlParameter() { ParameterName = parameter.Name, Value = parameter.Value ?? DBNull.Value });
                                }

                            }

                            using (SqlDataReader _reader = await _command.ExecuteReaderAsync())
                            {
                                using (DataTable _selectValues = new DataTable())
                                {
                                    _selectValues.Load(_reader);

                                    SelectListCustom _selectList = new SelectListCustom
                                    {
                                        FieldName = item.Key.FieldName,
                                        ModuleTableSetupId = item.Key.ModuleTableSetupId
                                    };

                                    foreach (DataRow _select in _selectValues.Rows)
                                    {
                                        Select _dtoSelect = new Select
                                        {
                                            Title = _select["Title"].ToString(),
                                            Value = _select["Value"].ToString()
                                        };


                                        if (item.Where(x => x.SelectedValue == _select["Value"].ToString()).Count() > 0)
                                        {
                                            _dtoSelect.SelectedValue = true;
                                        }

                                        _selectList.SelectValues.Add(_dtoSelect);

                                    }

                                    _returnselectList.Add(_selectList);
                                }
                            }
                        }
                    }



                }



                return _returnselectList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<D> GetModuleInformationRuntime<D>(Guid? Id, string Schema, string ModuleTable, bool flag1, bool flag2, string record, params Parameter[] parameters)
        {
            throw new NotImplementedException();
        }


        public async Task<string> GetStaffVehicleNo(string staffNo)
        {
            string query = "SELECT VehicleNo FROM VehicleSetting.Vehicle ";
            query += "WHERE Id in (SELECT VehicleId FROM VehicleSetting.StaffVehicleList WHERE StaffId = '" + staffNo + "')";
            try
            {
                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _commnd = new SqlCommand(query, _connection))
                    {
                        using (SqlDataReader _reader = await _commnd.ExecuteReaderAsync())
                        {
                            DataTable _records = new DataTable();
                            _records.Load(_reader);
                            if (_records.Rows.Count > 0)
                            {
                                return _records.Rows[0]["VehicleNo"].ToString();
                            }
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Guid> GetStaffVehicleInsurance(string staffNo)
        {
            string query = "SELECT  VehicleInsurance.[Id] VehicleInsuranceId FROM[InsuranceAndRenew].[VehicleInsurance](NOLOCK) AS VehicleInsurance ";
            query += "INNER JOIN[VehicleSetting].[VW_VehicleDetail] (NOLOCK)AS VW_VehicleDetail ON VehicleInsurance.VehicleId = VW_VehicleDetail.Id ";
            query += "WHERE VW_VehicleDetail.StaffId = '" + staffNo + "'";

            try
            {
                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _commnd = new SqlCommand(query, _connection))
                    {
                        using (SqlDataReader _reader = await _commnd.ExecuteReaderAsync())
                        {
                            DataTable _records = new DataTable();
                            _records.Load(_reader);
                            if (_records.Rows.Count > 0)
                            {
                                return (Guid)_records.Rows[0]["VehicleInsuranceId"];
                            }
                        }
                    }
                }
                return Guid.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }

    public interface ICommonRepository : IRepository<StaticDataDetails, StaticDataDetailsDTO>
    {
        Task GetSchemaInformationList(string FileName);
        Task GetAccountOpeningFlowSetupList(string FileName);
        Task GetDropdownList(string FileName);
        Task GetTableInformationList(string FileName);
        Task GetRolesPriority(string dropdownPath);
        Task<List<ParameterisedDatasourceDTO>> GetParameterisedDatasource(List<ParameterisedDatasourceDTO> ParameterList);
        Task<List<Select>> GetSelectList(params string[] ColumnNames);
        Task<List<SelectListCustom>> GetSelectList(string TableName, List<SelectListParameter> SelectListParameters);
        Task<D> GetModuleInformationRuntime<D>(Guid? Id, string Schema, string ModuleTable, bool flag1, bool flag2, string record, params Parameter[] parameters);

        Task<string> GetStaffVehicleNo(string staffNo);
        Task<Guid> GetStaffVehicleInsurance(string staffNo);
    }
}
