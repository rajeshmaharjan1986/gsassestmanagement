﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Configuration;
using System.Linq.Expressions;
using System.Reflection;

namespace GSAssetManagement.Repository
{
    public class ApplicationUserRepository : IApplicationUserRepository
    {
        private IAuthenticationHelper _authenticationHelper;
        private Data.ApplicationDbContext DataContext;
        private readonly IUnitOfWork _unitOfWork;
        public ApplicationUserRepository(IDatabaseFactory databaseFactory, IAuthenticationHelper authenticationHelper, IUnitOfWork unitOfWork)
        {
            _authenticationHelper = authenticationHelper;
            _unitOfWork = unitOfWork;
            DataContext = databaseFactory.Get();
        }

        public async Task<Guid> InsertUpdateRecords(ApplicationUserDTO applicationuserDTO, bool Autoauthorized)
        {
            try
            {
                ApplicationUser application = new ApplicationUser();
                application = MapperHelper.Get<ApplicationUser, ApplicationUserDTO>(application, applicationuserDTO, CurrentAction.Create);
                application.Id = application.Id == Guid.Empty ? Guid.NewGuid() : application.Id;
                application.UserName = string.IsNullOrEmpty(application.UserName) ? applicationuserDTO.Email : application.UserName;


                application.CreatedBy = application.CreatedBy == null ? _authenticationHelper.GetFullname() : application.CreatedBy;
                application.ModifiedBy = application.ModifiedBy == null ? _authenticationHelper.GetFullname() : application.ModifiedBy;

                application.CreatedByStaffNo = application.CreatedByStaffNo == null ? _authenticationHelper.GetStaffNo() : application.CreatedByStaffNo;
                application.ModifiedByStaffNo = application.ModifiedByStaffNo == null ? _authenticationHelper.GetStaffNo() : application.ModifiedByStaffNo;

                application.CreatedDate = DateTime.Now;
                application.ModifiedDate = DateTime.Now;

                application.CreatedById = application.CreatedById == null || application.CreatedById == Guid.Empty ? _authenticationHelper.GetUserId() : application.CreatedById;
                application.ModifiedById = application.ModifiedById == null || application.ModifiedById == Guid.Empty ? _authenticationHelper.GetUserId() : application.ModifiedById;

                if (Autoauthorized)
                {
                    application.AuthorisedBy = application.AuthorisedBy == null ? _authenticationHelper.GetFullname() : application.AuthorisedBy;
                    application.AuthorisedByStaffNo = application.AuthorisedByStaffNo == null ? _authenticationHelper.GetStaffNo() : application.AuthorisedByStaffNo;
                    application.AuthorisedById = application.AuthorisedById == null || application.AuthorisedById == Guid.Empty ? _authenticationHelper.GetUserId() : application.AuthorisedById;
                    application.AuthorisedDate = DateTime.Now;
                }

                application.Record_Status = RecordStatus.Active;

                var userstore = new UserStore<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>(this.DataContext);
                var usermanager = new UserManager<ApplicationUser, Guid>(userstore);

                //DataContext.Users.Add(application);

                var identity = await usermanager.CreateAsync(application, applicationuserDTO.PasswordHash);

                ApplicationUserRole roleEntity = new ApplicationUserRole()
                {
                    UserId = application.Id,
                    RoleId = Guid.Parse("E0085AEC-589C-4CE4-93D2-2F3245262CFA"),
                };

                DataContext.ApplicationUserRoles.Add(roleEntity);

                await DataContext.SaveChangesAsync();

                return application.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Guid> InsertUpdateRecordWithOutPassword(ApplicationUserDTO applicationuserDTO, bool Autoauthorized)
        {
            try
            {

                ApplicationUser application = new ApplicationUser();
                application = MapperHelper.Get<ApplicationUser, ApplicationUserDTO>(application, applicationuserDTO, CurrentAction.Create);
                application.Id = application.Id == Guid.Empty ? Guid.NewGuid() : application.Id;
                application.UserName = string.IsNullOrEmpty(application.UserName) ? applicationuserDTO.Email : application.UserName;


                application.CreatedBy = application.CreatedBy == null ? _authenticationHelper.GetFullname() : application.CreatedBy;
                application.ModifiedBy = application.ModifiedBy == null ? _authenticationHelper.GetFullname() : application.ModifiedBy;

                application.CreatedByStaffNo = application.CreatedByStaffNo == null ? _authenticationHelper.GetStaffNo() : application.CreatedByStaffNo;
                application.ModifiedByStaffNo = application.ModifiedByStaffNo == null ? _authenticationHelper.GetStaffNo() : application.ModifiedByStaffNo;

                application.CreatedDate = DateTime.Now;
                application.ModifiedDate = DateTime.Now;

                application.CreatedById = application.CreatedById == null || application.CreatedById == Guid.Empty ? _authenticationHelper.GetUserId() : application.CreatedById;
                application.ModifiedById = application.ModifiedById == null || application.ModifiedById == Guid.Empty ? _authenticationHelper.GetUserId() : application.ModifiedById;

                if (Autoauthorized)
                {
                    application.AuthorisedBy = application.AuthorisedBy == null ? _authenticationHelper.GetFullname() : application.AuthorisedBy;
                    application.AuthorisedByStaffNo = application.AuthorisedByStaffNo == null ? _authenticationHelper.GetStaffNo() : application.AuthorisedByStaffNo;
                    application.AuthorisedById = application.AuthorisedById == null || application.AuthorisedById == Guid.Empty ? _authenticationHelper.GetUserId() : application.AuthorisedById;
                    application.AuthorisedDate = DateTime.Now;
                }

                application.Record_Status = RecordStatus.Active;

                var userstore = new UserStore<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>(this.DataContext);
                var usermanager = new UserManager<ApplicationUser, Guid>(userstore);
                
                var user = await usermanager.FindByNameAsync(application.UserName);
                if (user == null)
                {
                    var identity = await usermanager.CreateAsync(application);

                    ApplicationUserRole roleEntity = new ApplicationUserRole()
                    {
                        UserId = application.Id,
                        RoleId = Guid.Parse("E0085AEC-589C-4CE4-93D2-2F3245262CFA"),
                    };

                    DataContext.ApplicationUserRoles.Add(roleEntity);

                    await DataContext.SaveChangesAsync();

                    return application.Id;
                }
                else {
                    return Guid.Empty;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> InsertUserAndRole()
        {
            try
            {

                const string name = "administrator@sanimabank.com";
                const string email = "administrator@sanimabank.com";
                const string password = "P@ssw0rd#123456";
                const string roleName = "Administrator";


                //if (!this.DataContext.Users.Any(user => string.Compare(user.UserName, name, StringComparison.CurrentCultureIgnoreCase) == 0))
                //{
                var userstore = new UserStore<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>(this.DataContext);
                var usermanager = new UserManager<ApplicationUser, Guid>(userstore);

                var administrator = new ApplicationUser
                {
                    Id = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                    FullName = "administrator",
                    Branch = "000",
                    UserName = name,
                    Email = email,
                    EmailConfirmed = true,
                    CreatedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                    ModifiedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                    AuthorisedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                    CreatedByStaffNo = "000",
                    ModifiedByStaffNo = "000",
                    AuthorisedByStaffNo = "000",
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    AuthorisedDate = DateTime.Now,
                    Record_Status = RecordStatus.Active,
                    CreatedBy = "administrator",
                    ModifiedBy = "administrator",
                    AuthorisedBy = "administrator"

                };

                usermanager.Create(administrator, password);
                //usermanager.AddToRole(administrator.Id, roleName);


                var rolestore = new RoleStore<ApplicationRole, Guid, ApplicationUserRole>(this.DataContext);
                var rolemanager = new RoleManager<ApplicationRole, Guid>(rolestore);

                ApplicationRole _role = new ApplicationRole
                {
                    Id = Guid.NewGuid(),
                    Name = roleName,
                    RoleCode = "001-SUP-RO",
                    Remarks = "Superadmin role for development.",
                    CreatedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                    ModifiedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                    AuthorisedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                    CreatedByStaffNo = "000",
                    ModifiedByStaffNo = "000",
                    AuthorisedByStaffNo = "000",
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    AuthorisedDate = DateTime.Now,
                    Record_Status = RecordStatus.Active,
                    CreatedBy = "administrator",
                    ModifiedBy = "administrator",
                    AuthorisedBy = "administrator"
                };

                rolemanager.Create(_role);




                this.DataContext.ApplicationUserRoles.Add(new ApplicationUserRole()
                {
                    UserId = administrator.Id,
                    RoleId = _role.Id,
                });

                this.DataContext.SaveChanges();
                return true;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ApplicationUser> InsertUpdateUserWithRole(ApplicationUserDTO applicationuserDTO, string roleName, UserLoginInfo userlogin, string tokenKey)
        {
            try
            {
                ApplicationUser application = new ApplicationUser();
                application = MapperHelper.Get<ApplicationUser, ApplicationUserDTO>(application, applicationuserDTO);

                application.Id = application.Id == Guid.Empty ? Guid.NewGuid() : application.Id;
                application.UserName = string.IsNullOrEmpty(application.UserName) ? applicationuserDTO.Email : application.UserName;


                application.CreatedBy = application.CreatedBy == null ? _authenticationHelper.GetFullname() : application.CreatedBy;
                application.ModifiedBy = application.ModifiedBy == null ? _authenticationHelper.GetFullname() : application.ModifiedBy;

                application.CreatedByStaffNo = application.CreatedByStaffNo == null ? _authenticationHelper.GetStaffNo() : application.CreatedByStaffNo;
                application.ModifiedByStaffNo = application.ModifiedByStaffNo == null ? _authenticationHelper.GetStaffNo() : application.ModifiedByStaffNo;

                application.CreatedDate = DateTime.Now;
                application.ModifiedDate = DateTime.Now;

                application.CreatedById = application.CreatedById == null || application.CreatedById == Guid.Empty ? _authenticationHelper.GetUserId() : application.CreatedById;
                application.ModifiedById = application.ModifiedById == null || application.ModifiedById == Guid.Empty ? _authenticationHelper.GetUserId() : application.ModifiedById;

                application.AuthorisedBy = application.AuthorisedBy == null ? _authenticationHelper.GetFullname() : application.AuthorisedBy;
                application.AuthorisedByStaffNo = application.AuthorisedByStaffNo == null ? _authenticationHelper.GetStaffNo() : application.AuthorisedByStaffNo;
                application.AuthorisedById = application.AuthorisedById == null || application.AuthorisedById == Guid.Empty ? _authenticationHelper.GetUserId() : application.AuthorisedById;
                application.AuthorisedDate = DateTime.Now;

                application.Record_Status = RecordStatus.Active;
                application.LastLoginDate = DateTime.Now;

                var userstore = new UserStore<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>(this.DataContext);
                var usermanager = new UserManager<ApplicationUser, Guid>(userstore);

                var user = await usermanager.FindByNameAsync(application.UserName);

                var rolestore = new RoleStore<ApplicationRole, Guid, ApplicationUserRole>(this.DataContext);
                var rolemanager = new RoleManager<ApplicationRole, Guid>(rolestore);

                var role = rolemanager.FindByName(roleName);
                if (role == null && (roleName == "BranchMaker"))
                {
                    ApplicationRole _role = new ApplicationRole
                    {
                        Id = Guid.Parse("E9841CC4-7E55-45A5-9202-709EDB7008F5"),
                        Name = roleName,
                        RoleCode = "GS_001",
                        Remarks = "Branch Maker",
                        CreatedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                        ModifiedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                        AuthorisedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                        CreatedByStaffNo = "000",
                        ModifiedByStaffNo = "000",
                        AuthorisedByStaffNo = "000",
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        AuthorisedDate = DateTime.Now,
                        Record_Status = RecordStatus.Active,
                        CreatedBy = "administrator",
                        ModifiedBy = "administrator",
                        AuthorisedBy = "administrator"
                    };

                    rolemanager.Create(_role);
                }
                else if (role == null)
                {
                    role = rolemanager.FindByName("BranchMaker");
                }

                Guid roleId = role == null ? Guid.Parse("E9841CC4-7E55-45A5-9202-709EDB7008F5") : role.Id;

                if (user == null)
                {
                    //DataContext.Users.Add(application);

                    var result = await usermanager.CreateAsync(application);
                    if (result.Succeeded)
                    {
                        await usermanager.AddClaimAsync(application.Id, new System.Security.Claims.Claim("SanimaAPIToken", tokenKey));

                        await usermanager.AddLoginAsync(application.Id, userlogin);

                        ApplicationUserRole roleEntity = new ApplicationUserRole()
                        {
                            UserId = application.Id,
                            RoleId = roleId
                        };

                        DataContext.ApplicationUserRoles.Add(roleEntity);


                        await DataContext.SaveChangesAsync();
                    }

                }
                else
                {
                    try
                    {
                        user.DeptId = applicationuserDTO.DeptId;
                        user.DeptName = applicationuserDTO.DeptName;
                        user.BranchName = applicationuserDTO.BranchName;
                        user.Designation = applicationuserDTO.Designation;
                        user.Branch = applicationuserDTO.Branch;
                        user.currentFiscalYear = applicationuserDTO.currentFiscalYear;
                        user.JobID = applicationuserDTO.JobID;
                        user.FunctionalTitle = applicationuserDTO.FunctionalTitle;
                        user.LastLoginDate = DateTime.Now;

                        await usermanager.UpdateAsync(user);


                        foreach (var claimKeg in await usermanager.GetClaimsAsync(user.Id))
                        {
                            if (claimKeg.Type == "SanimaAPIToken")
                            {
                                await usermanager.RemoveClaimAsync(user.Id, claimKeg);
                            }
                        }

                        await usermanager.AddClaimAsync(user.Id, new System.Security.Claims.Claim("SanimaAPIToken", tokenKey));
                    }
                    catch
                    {

                    }

                    var roles = await usermanager.GetRolesAsync(user.Id);
                    if (!roles.Contains(roleName))
                    {

                        ApplicationUserRole roleEntity = new ApplicationUserRole()
                        {
                            UserId = user.Id,
                            RoleId = roleId
                        };

                        DataContext.ApplicationUserRoles.Add(roleEntity);


                        await DataContext.SaveChangesAsync();
                    }
                }

                return application;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<ApplicationRole>> GetUserRole(Guid userId)
        {
            var userstore = new UserStore<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>(this.DataContext);
            var usermanager = new UserManager<ApplicationUser, Guid>(userstore);

            var user = await usermanager.FindByIdAsync(userId);

            var listRoleName = await usermanager.GetRolesAsync(userId);

            var rolestore = new RoleStore<ApplicationRole, Guid, ApplicationUserRole>(this.DataContext);
            var rolemanager = new RoleManager<ApplicationRole, Guid>(rolestore);

            List<ApplicationRole> applicationRoles = new List<ApplicationRole>();
            foreach (string roleName in listRoleName)
            {
                ApplicationRole role = await rolemanager.FindByNameAsync(roleName);
                applicationRoles.Add(role);
            }

            return applicationRoles;
        }

        public async Task<ApplicationUser> GetUserByUsername(string userName)
        {
            try
            {
                var userstore = new UserStore<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>(this.DataContext);
                var usermanager = new UserManager<ApplicationUser, Guid>(userstore);

                var user = await usermanager.FindByNameAsync(userName);

                return user;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<PagedResultDataTable> GetAllByProcedure(string Schema, string EntityName, params SqlParameter[] parameters)
        {
            try
            {
                var pageList = new PagedResult<DataTable>();

                int PageSize = int.Parse(parameters.Where(x => x.ParameterName == "PageSize").Select(x => x.Value).FirstOrDefault().ToString());
                int PageNumber = int.Parse(parameters.Where(x => x.ParameterName == "PageNumber").Select(x => x.Value).FirstOrDefault().ToString());

                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _commnd = new SqlCommand(string.Format("[{0}].[SP_Get{1}List]", Schema, EntityName), _connection))
                    {
                        _commnd.CommandType = CommandType.StoredProcedure;

                        _commnd.Parameters.AddRange(parameters);


                        using (SqlDataReader _reader = await _commnd.ExecuteReaderAsync())
                        {
                            DataTable _records = new DataTable();
                            _records.Load(_reader);

                            if (_records.Rows.Count > 0)
                            {

                                var _result = _records.AsEnumerable().GroupBy(x => new { PageCount = x["PageCount"], TotalRecords = x["TotalRecords"] }).Select(z => new PagedResultDataTable()
                                {
                                    PageCount = int.Parse(z.Key.PageCount.ToString()),
                                    RowCount = int.Parse(z.Key.TotalRecords.ToString()),
                                    PageSize = PageSize,
                                    PageNumber = PageNumber,
                                    Results = z.CopyToDataTable()

                                }).FirstOrDefault();

                                return _result;
                            }
                            else
                            {
                                return new PagedResultDataTable()
                                {
                                    PageCount = 0,
                                    RowCount = 0,
                                    PageNumber = 1,
                                    PageSize = PageSize,
                                    Results = _records

                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ModuleSummary> GetModuleBussinesLogicSetup(Guid? Id, Guid? ParentPrimaryRecordId, bool IsSummaryRequest, bool DropdownRequired, List<AdditionalDropdownParameter> DropdownAdditionalParameters = null)
        {
            try
            {
                Dictionary<string, object> RecordToDictionary = new Dictionary<string, object>();
                Guid? CurrentApplicationUserId = this._authenticationHelper.GetUserId();

                var ModuleSetup = ModuleHelper.GetModuleSetup<ApplicationUser>();

                ModuleSummary moduleSummary = new ModuleSummary()
                {
                    SchemaName = ModuleSetup.ModuleTypeSetup.ModuleName,
                    ModuleSummaryName = ModuleSetup.ApplicationClass,
                    ModuleSummaryTitle = ModuleSetup.Name,
                    EntryType = ModuleSetup.EntryType,
                    PrimaryRecordId = Id,
                    IsParent = ModuleSetup.IsParent,
                    ParentModule = ModuleSetup.ParentModule,
                    ParentPrimaryRecordId = ParentPrimaryRecordId
                };

                if (Id != null || (ModuleSetup.EntryType == "S" && ParentPrimaryRecordId != null))
                {
                    string ParentColumn = ModuleSetup.ModuleBussinesLogicSetups.Where(x => x.IsParentColumn).Select(x => x.ColumnName).FirstOrDefault();

                    Expression<Func<ApplicationUser, bool>> linqCondition = null;

                    if (!string.IsNullOrEmpty(ParentColumn) && ParentPrimaryRecordId != null)
                    {
                        linqCondition = DynamicLinqBuilder.CreateExpression<ApplicationUser, bool>(string.Format("{0} == @0", ParentColumn), ParentPrimaryRecordId);

                    }

                    var Record = Id != null ? this.DataContext.Users.Where(x => x.Id == Id.Value).FirstOrDefault() :
                        ParentPrimaryRecordId != null && !moduleSummary.IsParent ? this.DataContext.Users.Where(linqCondition).FirstOrDefault() :
                        ParentPrimaryRecordId != null ? this.DataContext.Users.Where(linqCondition).FirstOrDefault() : null;
                    if (Record != null)
                    {
                        moduleSummary.TotalModification = 0;
                        moduleSummary.CreatedBy = Record.CreatedBy;
                        moduleSummary.ModifiedBy = Record.ModifiedBy;
                        moduleSummary.AuthorisedBy = Record.AuthorisedBy;
                        moduleSummary.CreatedById = Record.CreatedById;
                        moduleSummary.ModifiedById = Record.ModifiedById;
                        moduleSummary.AuthorisedById = Record.AuthorisedById;
                        moduleSummary.CreatedDate = Record.CreatedDate;
                        moduleSummary.ModifiedDate = Record.ModifiedDate;
                        moduleSummary.AuthorisedDate = Record.AuthorisedDate;
                        moduleSummary.EntityState = Record.EntityState;
                        moduleSummary.RecordStatus = Record.Record_Status;
                        moduleSummary.DoRecordExists = Record != null ? true : false;
                        moduleSummary.PrimaryRecordId = Record.Id;

                        moduleSummary.RecordChangeLog = Record.EntityState == DataEntityState.Modified && Record.Record_Status == RecordStatus.UnVerified ? ChangeLogHelper.GetLatestRecordChangeLogs(Record.ChangeLog) : null;

                        RecordToDictionary = Record.GetType()
        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
            .ToDictionary(prop => prop.Name, prop => prop.GetValue(Record, null));
                    }

                }

                ModuleSetup.ChildTableInformations.ForEach(c =>
                {
                    moduleSummary.ChildInformations.Add(new ChildModuleSummary()
                    {
                        ChildModuleSummaryName = c.Name,
                        ChildModuleSummaryTitle = c.Title,
                        Url = c.Url,
                        OrderValue = c.OrderValue

                    });


                });


                foreach (var ModuleBussinesLogic in ModuleSetup.ModuleBussinesLogicSetups.Where(x => IsSummaryRequest ? x.SummaryHeader || x.ParameterForSummaryHeader || (ParentPrimaryRecordId != null && x.IsParentColumn) : true).ToList())
                {
                    try
                    {
                        Dictionary<string, object> Attributes = new Dictionary<string, object>();

                        var moduleBussinesLogicSummary = new ModuleBussinesLogicSummary()
                        {
                            Name = ModuleBussinesLogic.Name,
                            ColumnName = ModuleBussinesLogic.ColumnName,
                            Description = ModuleBussinesLogic.Description,
                            DataType = ModuleBussinesLogic.DataType,
                            Required = ModuleBussinesLogic.Required,
                            HtmlDataType = ModuleBussinesLogic.HtmlDataType,
                            HtmlSize = ModuleBussinesLogic.HtmlSize,
                            Position = ModuleBussinesLogic.Position,
                            DefaultValue = ModuleBussinesLogic.DefaultValue,
                            CanUpdate = ModuleBussinesLogic.CanUpdate,
                            HelpMessage = ModuleBussinesLogic.HelpMessage,
                            IsParentColumn = ModuleBussinesLogic.IsParentColumn || !string.IsNullOrEmpty(moduleSummary.ParentModule) && string.Format("{0}Id", moduleSummary.ParentModule) == ModuleBussinesLogic.ColumnName,
                            SummaryHeader = ModuleBussinesLogic.SummaryHeader,
                            ParameterForSummaryHeader = ModuleBussinesLogic.ParameterForSummaryHeader,
                            IsForeignKey = ModuleBussinesLogic.IsForeignKey,
                            ForeignTable = ModuleBussinesLogic.ForeignTable,
                            DataSource = ModuleBussinesLogic.DataSource,
                            ParameterisedDataSorce = ModuleBussinesLogic.ParameterisedDataSorce,
                            Parameters = ModuleBussinesLogic.Parameters,
                            CurrentValue = RecordToDictionary.ContainsKey(ModuleBussinesLogic.ColumnName) ? RecordToDictionary[ModuleBussinesLogic.ColumnName] : ModuleBussinesLogic.IsParentColumn || !string.IsNullOrEmpty(moduleSummary.ParentModule) && string.Format("{0}Id", moduleSummary.ParentModule) == ModuleBussinesLogic.ColumnName ? ParentPrimaryRecordId : null,
                            SelectList = DropdownRequired && (ModuleBussinesLogic.HtmlDataType.ToLower() == "select" || ModuleBussinesLogic.HtmlDataType.ToLower() == "datalist") ? DropdownHelper.GetDropdownInformation
                            (ModuleBussinesLogic.ColumnName,
                            Id == null && moduleSummary.PrimaryRecordId == null ? ModuleBussinesLogic.DefaultValue : RecordToDictionary.ContainsKey(ModuleBussinesLogic.ColumnName) ? RecordToDictionary[ModuleBussinesLogic.ColumnName] : null,
                            ModuleBussinesLogic.DataSource,
                            ModuleBussinesLogic.IsStaticDropDown,
                            ModuleBussinesLogic.ParameterisedDataSorce,
                            ModuleBussinesLogic.Parameters,
                            RecordToDictionary,
                            CurrentApplicationUserId,
                            DropdownAdditionalParameters) : null

                        };

                        if (!IsSummaryRequest)
                            ModuleBussinesLogic.ModuleValidationAttributeSetups.Distinct().ToList().HtmlValidationAttributes(ref Attributes);

                        moduleBussinesLogicSummary.Attributes = Attributes;

                        ModuleBussinesLogic.ModuleHtmlAttributeSetups.ToList().ForEach(f =>
                        {
                            if (moduleBussinesLogicSummary.Attributes.Where(a => a.Key == f.AttributeType).Count() == 0)
                            {
                                moduleBussinesLogicSummary.Attributes.Add(f.AttributeType, f.Value);
                            }

                        });

                        moduleSummary.moduleBussinesLogicSummaries.Add(moduleBussinesLogicSummary);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                return moduleSummary;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }

    public interface IApplicationUserRepository
    {
        Task<Guid> InsertUpdateRecords(ApplicationUserDTO applicationuserDTO, bool Autoauthorized);

        Task<Guid> InsertUpdateRecordWithOutPassword(ApplicationUserDTO applicationuserDTO, bool Autoauthorized);

        Task<ApplicationUser> InsertUpdateUserWithRole(ApplicationUserDTO applicationuserDTO, string roleName, UserLoginInfo userlogin, string tokenKey);

        Task<bool> InsertUserAndRole();

        Task<List<ApplicationRole>> GetUserRole(Guid userId);

        Task<ApplicationUser> GetUserByUsername(string userName);

        Task<PagedResultDataTable> GetAllByProcedure(string Schema, string EntityName, params SqlParameter[] parameters);

        Task<ModuleSummary> GetModuleBussinesLogicSetup(Guid? Id, Guid? ParentPrimaryRecordId, bool IsSummaryRequest, bool DropdownRequired, List<AdditionalDropdownParameter> DropdownAdditionalParameters = null);
    }
}
