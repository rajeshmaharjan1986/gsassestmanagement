﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;

namespace GSAssetManagement.Repository
{
    public class ApplicationRoleRepository : IApplicationRoleRepository
    {
        private IAuthenticationHelper _authenticationHelper;
        private Data.ApplicationDbContext DataContext;
        private readonly IUnitOfWork _unitOfWork;
        public ApplicationRoleRepository(IDatabaseFactory databaseFactory, IAuthenticationHelper authenticationHelper, IUnitOfWork unitOfWork)
        {
            _authenticationHelper = authenticationHelper;
            _unitOfWork = unitOfWork;
            DataContext = databaseFactory.Get();
        }

        public async Task<PagedResultDataTable> GetAllByProcedure(string Schema, string EntityName, params SqlParameter[] parameters)
        {
            try
            {
                var pageList = new PagedResult<DataTable>();

                int PageSize = int.Parse(parameters.Where(x => x.ParameterName == "PageSize").Select(x => x.Value).FirstOrDefault().ToString());
                int PageNumber = int.Parse(parameters.Where(x => x.ParameterName == "PageNumber").Select(x => x.Value).FirstOrDefault().ToString());

                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _commnd = new SqlCommand(string.Format("[{0}].[SP_Get{1}List]", Schema, EntityName), _connection))
                    {
                        _commnd.CommandType = CommandType.StoredProcedure;

                        _commnd.Parameters.AddRange(parameters);


                        using (SqlDataReader _reader = await _commnd.ExecuteReaderAsync())
                        {
                            DataTable _records = new DataTable();
                            _records.Load(_reader);

                            if (_records.Rows.Count > 0)
                            {

                                var _result = _records.AsEnumerable().GroupBy(x => new { PageCount = x["PageCount"], TotalRecords = x["TotalRecords"] }).Select(z => new PagedResultDataTable()
                                {
                                    PageCount = int.Parse(z.Key.PageCount.ToString()),
                                    RowCount = int.Parse(z.Key.TotalRecords.ToString()),
                                    PageSize = PageSize,
                                    PageNumber = PageNumber,
                                    Results = z.CopyToDataTable()

                                }).FirstOrDefault();

                                return _result;
                            }
                            else
                            {
                                return new PagedResultDataTable()
                                {
                                    PageCount = 0,
                                    RowCount = 0,
                                    PageNumber = 1,
                                    PageSize = PageSize,
                                    Results = _records

                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<ModuleSummary> GetModuleBussinesLogicSetup(Guid? Id, Guid? ParentPrimaryRecordId, bool IsSummaryRequest, bool DropdownRequired, List<AdditionalDropdownParameter> DropdownAdditionalParameters = null)
        {
            try
            {
                Dictionary<string, object> RecordToDictionary = new Dictionary<string, object>();
                Guid? CurrentApplicationUserId = this._authenticationHelper.GetUserId();

                var ModuleSetup = ModuleHelper.GetModuleSetup<ApplicationRole>();

                ModuleSummary moduleSummary = new ModuleSummary()
                {
                    SchemaName = ModuleSetup.ModuleName,
                    ModuleSummaryName = ModuleSetup.ApplicationClass,
                    ModuleSummaryTitle = ModuleSetup.Name,
                    EntryType = ModuleSetup.EntryType,
                    PrimaryRecordId = Id,
                    IsParent = ModuleSetup.IsParent,
                    ParentModule = ModuleSetup.ParentModule,
                    ParentPrimaryRecordId = ParentPrimaryRecordId
                };

                if (Id != null || (ModuleSetup.EntryType == "S" && ParentPrimaryRecordId != null))
                {
                    string ParentColumn = ModuleSetup.ModuleBussinesLogicSetups.Where(x => x.IsParentColumn).Select(x => x.ColumnName).FirstOrDefault();

                    Expression<Func<ApplicationRole, bool>> linqCondition = null;

                    if (!string.IsNullOrEmpty(ParentColumn) && ParentPrimaryRecordId != null)
                    {
                        linqCondition = DynamicLinqBuilder.CreateExpression<ApplicationRole, bool>(string.Format("{0} == @0", ParentColumn), ParentPrimaryRecordId);

                    }

                    var Record = Id != null ? this.DataContext.Roles.Where(x => x.Id == Id.Value).FirstOrDefault() :
                        ParentPrimaryRecordId != null && !moduleSummary.IsParent ? this.DataContext.Roles.Where(linqCondition).FirstOrDefault() :
                        ParentPrimaryRecordId != null ? this.DataContext.Roles.Where(linqCondition).FirstOrDefault() : null;
                    if (Record != null)
                    {
                        moduleSummary.TotalModification = 0;
                        moduleSummary.CreatedBy = Record.CreatedBy;
                        moduleSummary.ModifiedBy = Record.ModifiedBy;
                        moduleSummary.AuthorisedBy = Record.AuthorisedBy;
                        moduleSummary.CreatedById = Record.CreatedById;
                        moduleSummary.ModifiedById = Record.ModifiedById;
                        moduleSummary.AuthorisedById = Record.AuthorisedById;
                        moduleSummary.CreatedDate = Record.CreatedDate;
                        moduleSummary.ModifiedDate = Record.ModifiedDate;
                        moduleSummary.AuthorisedDate = Record.AuthorisedDate;
                        moduleSummary.EntityState = Record.EntityState;
                        moduleSummary.RecordStatus = Record.Record_Status;
                        moduleSummary.DoRecordExists = Record != null ? true : false;
                        moduleSummary.PrimaryRecordId = Record.Id;

                        moduleSummary.RecordChangeLog = Record.EntityState == DataEntityState.Modified && Record.Record_Status == RecordStatus.UnVerified ? ChangeLogHelper.GetLatestRecordChangeLogs(Record.ChangeLog) : null;

                        RecordToDictionary = Record.GetType()
        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
            .ToDictionary(prop => prop.Name, prop => prop.GetValue(Record, null));
                    }

                }

                ModuleSetup.ChildTableInformations.ForEach(c =>
                {
                    moduleSummary.ChildInformations.Add(new ChildModuleSummary()
                    {
                        ChildModuleSummaryName = c.Name,
                        ChildModuleSummaryTitle = c.Title,
                        Url = c.Url,
                        OrderValue = c.OrderValue

                    });


                });


                foreach (var ModuleBussinesLogic in ModuleSetup.ModuleBussinesLogicSetups.Where(x => IsSummaryRequest ? x.SummaryHeader || x.ParameterForSummaryHeader || (ParentPrimaryRecordId != null && x.IsParentColumn) : true).ToList())
                {
                    try
                    {
                        Dictionary<string, object> Attributes = new Dictionary<string, object>();

                        var moduleBussinesLogicSummary = new ModuleBussinesLogicSummary()
                        {
                            Name = ModuleBussinesLogic.Name,
                            ColumnName = ModuleBussinesLogic.ColumnName,
                            Description = ModuleBussinesLogic.Description,
                            DataType = ModuleBussinesLogic.DataType,
                            Required = ModuleBussinesLogic.Required,
                            HtmlDataType = ModuleBussinesLogic.HtmlDataType,
                            HtmlSize = ModuleBussinesLogic.HtmlSize,
                            Position = ModuleBussinesLogic.Position,
                            DefaultValue = ModuleBussinesLogic.DefaultValue,
                            CanUpdate = ModuleBussinesLogic.CanUpdate,
                            HelpMessage = ModuleBussinesLogic.HelpMessage,
                            IsParentColumn = ModuleBussinesLogic.IsParentColumn || !string.IsNullOrEmpty(moduleSummary.ParentModule) && string.Format("{0}Id", moduleSummary.ParentModule) == ModuleBussinesLogic.ColumnName,
                            SummaryHeader = ModuleBussinesLogic.SummaryHeader,
                            ParameterForSummaryHeader = ModuleBussinesLogic.ParameterForSummaryHeader,
                            IsForeignKey = ModuleBussinesLogic.IsForeignKey,
                            ForeignTable = ModuleBussinesLogic.ForeignTable,
                            DataSource = ModuleBussinesLogic.DataSource,
                            ParameterisedDataSorce = ModuleBussinesLogic.ParameterisedDataSorce,
                            Parameters = ModuleBussinesLogic.Parameters,
                            CurrentValue = RecordToDictionary.ContainsKey(ModuleBussinesLogic.ColumnName) ? RecordToDictionary[ModuleBussinesLogic.ColumnName] : ModuleBussinesLogic.IsParentColumn || !string.IsNullOrEmpty(moduleSummary.ParentModule) && string.Format("{0}Id", moduleSummary.ParentModule) == ModuleBussinesLogic.ColumnName ? ParentPrimaryRecordId : null,
                            SelectList = DropdownRequired && (ModuleBussinesLogic.HtmlDataType.ToLower() == "select" || ModuleBussinesLogic.HtmlDataType.ToLower() == "datalist") ? DropdownHelper.GetDropdownInformation
                            (ModuleBussinesLogic.ColumnName,
                            Id == null && moduleSummary.PrimaryRecordId == null ? ModuleBussinesLogic.DefaultValue : RecordToDictionary.ContainsKey(ModuleBussinesLogic.ColumnName) ? RecordToDictionary[ModuleBussinesLogic.ColumnName] : null,
                            ModuleBussinesLogic.DataSource,
                            ModuleBussinesLogic.IsStaticDropDown,
                            ModuleBussinesLogic.ParameterisedDataSorce,
                            ModuleBussinesLogic.Parameters,
                            RecordToDictionary,
                            CurrentApplicationUserId,
                            DropdownAdditionalParameters) : null

                        };

                        if (!IsSummaryRequest)
                            ModuleBussinesLogic.ModuleValidationAttributeSetups.Distinct().ToList().HtmlValidationAttributes(ref Attributes);

                        moduleBussinesLogicSummary.Attributes = Attributes;

                        ModuleBussinesLogic.ModuleHtmlAttributeSetups.ToList().ForEach(f =>
                        {
                            if (moduleBussinesLogicSummary.Attributes.Where(a => a.Key == f.AttributeType).Count() == 0)
                            {
                                moduleBussinesLogicSummary.Attributes.Add(f.AttributeType, f.Value);
                            }

                        });

                        moduleSummary.moduleBussinesLogicSummaries.Add(moduleBussinesLogicSummary);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                return moduleSummary;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public Guid InsertRecords(ApplicationRoleDTO applicationRoleDTO, bool Autoauthorized)
        {
            try
            {
                var rolestore = new RoleStore<ApplicationRole, Guid, ApplicationUserRole>(this.DataContext);
                var rolemanager = new RoleManager<ApplicationRole, Guid>(rolestore);

                ApplicationRole _role = new ApplicationRole
                {
                    Id = Guid.NewGuid(),
                    Name = applicationRoleDTO.Name,
                    RoleCode = applicationRoleDTO.RoleCode,
                    Remarks = applicationRoleDTO.Remarks
                };

                _role.CreatedBy = _role.CreatedBy == null ? _authenticationHelper.GetFullname() : _role.CreatedBy;
                _role.ModifiedBy = _role.ModifiedBy == null ? _authenticationHelper.GetFullname() : _role.ModifiedBy;

                _role.CreatedByStaffNo = _role.CreatedByStaffNo == null ? _authenticationHelper.GetStaffNo() : _role.CreatedByStaffNo;
                _role.ModifiedByStaffNo = _role.ModifiedByStaffNo == null ? _authenticationHelper.GetStaffNo() : _role.ModifiedByStaffNo;

                _role.CreatedDate = DateTime.Now;
                _role.ModifiedDate = DateTime.Now;

                _role.CreatedById = _role.CreatedById == null || _role.CreatedById == Guid.Empty ? _authenticationHelper.GetUserId() : _role.CreatedById;
                _role.ModifiedById = _role.ModifiedById == null || _role.ModifiedById == Guid.Empty ? _authenticationHelper.GetUserId() : _role.ModifiedById;

                _role.Record_Status = RecordStatus.UnVerified;
                if (Autoauthorized)
                {
                    _role.AuthorisedBy = _role.AuthorisedBy == null ? _authenticationHelper.GetFullname() : _role.AuthorisedBy;
                    _role.AuthorisedByStaffNo = _role.AuthorisedByStaffNo == null ? _authenticationHelper.GetStaffNo() : _role.AuthorisedByStaffNo;
                    _role.AuthorisedById = _role.AuthorisedById == null || _role.AuthorisedById == Guid.Empty ? _authenticationHelper.GetUserId() : _role.AuthorisedById;
                    _role.AuthorisedDate = DateTime.Now;
                    _role.Record_Status = RecordStatus.Active;
                }

                rolemanager.Create(_role);

                var role = rolemanager.FindByName(_role.Name);

                return role.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> UpdateRecords(ApplicationRoleDTO applicationRoleDTO, bool Autoauthorized)
        {
            try
            {
                var rolestore = new RoleStore<ApplicationRole, Guid, ApplicationUserRole>(this.DataContext);
                var rolemanager = new RoleManager<ApplicationRole, Guid>(rolestore);

                ApplicationRole _role = new ApplicationRole
                {
                    Id = Guid.NewGuid(),
                    Name = applicationRoleDTO.Name,
                    RoleCode = applicationRoleDTO.RoleCode,
                    Remarks = applicationRoleDTO.Remarks
                };

                _role.ModifiedBy = _role.ModifiedBy == null ? _authenticationHelper.GetFullname() : _role.ModifiedBy;
                _role.ModifiedByStaffNo = _role.ModifiedByStaffNo == null ? _authenticationHelper.GetStaffNo() : _role.ModifiedByStaffNo;
                _role.ModifiedDate = DateTime.Now;
                _role.ModifiedById = _role.ModifiedById == null || _role.ModifiedById == Guid.Empty ? _authenticationHelper.GetUserId() : _role.ModifiedById;

                _role.Record_Status = RecordStatus.UnVerified;

                if (Autoauthorized)
                {
                    _role.AuthorisedBy = _role.AuthorisedBy == null ? _authenticationHelper.GetFullname() : _role.AuthorisedBy;
                    _role.AuthorisedByStaffNo = _role.AuthorisedByStaffNo == null ? _authenticationHelper.GetStaffNo() : _role.AuthorisedByStaffNo;
                    _role.AuthorisedById = _role.AuthorisedById == null || _role.AuthorisedById == Guid.Empty ? _authenticationHelper.GetUserId() : _role.AuthorisedById;
                    _role.AuthorisedDate = DateTime.Now;
                    _role.Record_Status = RecordStatus.Active;
                }

                rolemanager.Update(_role);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> AuthorizedRecords(ApplicationRoleDTO applicationRoleDTO)
        {
            throw new NotImplementedException();
        }
    }

    public interface IApplicationRoleRepository
    {
        Guid InsertRecords(ApplicationRoleDTO applicationRoleDTO, bool Autoauthorized);
        Task<bool> UpdateRecords(ApplicationRoleDTO applicationRoleDTO, bool Autoauthorized);
        Task<bool> AuthorizedRecords(ApplicationRoleDTO applicationRoleDTO);
        Task<PagedResultDataTable> GetAllByProcedure(string Schema, string EntityName, params SqlParameter[] parameters);
        Task<ModuleSummary> GetModuleBussinesLogicSetup(Guid? Id, Guid? ParentPrimaryRecordId, bool IsSummaryRequest, bool DropdownRequired, List<AdditionalDropdownParameter> DropdownAdditionalParameters = null);
    }
}
