using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;


namespace GSAssetManagement.Repository
{
    public class BranchDrillingReportRepository : RepositoryBase<BranchDrillingReport, BranchDrillingReportDTO>, IBranchDrillingReportRepository
    {
        public BranchDrillingReportRepository(IDatabaseFactory databaseFactory, IAuthenticationHelper authenticationHelper)
            : base(databaseFactory, authenticationHelper)
        {

        }

        public async Task<ModuleSummaryForDetails> GetModuleBussinesLogicSetupWithChild(Guid? Id, string branchCode)
        {
            try
            {
                string BranchId = branchCode == "000" ? "1" : branchCode;

                Dictionary<string, object> RecordToDictionary = new Dictionary<string, object>();
                Guid? CurrentApplicationUserId = this._authenticationHelper.GetUserId();

                var ModuleSetup = ModuleHelper.GetModuleSetup<BranchDrillingReport>();

                ModuleSummaryForDetails moduleSummary = new ModuleSummaryForDetails()
                {
                    ModuleSummaryName = ModuleSetup.ApplicationClass,
                    ModuleSummaryTitle = ModuleSetup.Name,
                    EntryType = ModuleSetup.EntryType,
                    PrimaryRecordId = null,
                    IsParent = ModuleSetup.IsParent,
                    ParentModule = ModuleSetup.ParentModule,
                    ParentPrimaryRecordId = null
                };

                BranchDrillingReportDTO dto = null;

                List<AssetWorkingStatusDTO> dtos = new List<AssetWorkingStatusDTO>();
                if (Id != null)
                {
                    var assestStatusDTOs = this.DataContext.AssetWorkingStatuses.Where(a => a.BranchDrillingReportId == Id).ToList<AssetWorkingStatus>();
                    assestStatusDTOs.ForEach(entity =>
                    {
                        AssetWorkingStatusDTO assetWorkingStatusDTO = MapperHelper.Get<AssetWorkingStatusDTO, AssetWorkingStatus>(Activator.CreateInstance<AssetWorkingStatusDTO>(), entity);

                        AssetDetail assetDetail = this.DataContext.AssetDetails.Where(ad => ad.Id == assetWorkingStatusDTO.AssetDetailId).FirstOrDefault();
                        AssetCategory assetCategory = this.DataContext.AssetCategorys.Where(ac => ac.Id == assetDetail.AssetCategoryId).FirstOrDefault();
                        AssetExtraDetail assetExtraDetail = this.DataContext.AssetExtraDetails.Where(ac => ac.Id == assetWorkingStatusDTO.AssetExtraDetailId).FirstOrDefault();

                        assetWorkingStatusDTO.AssetCode = assetDetail.AssetCode;
                        assetWorkingStatusDTO.AssetName = assetDetail.AssetName;
                        assetWorkingStatusDTO.Location = assetExtraDetail.Location;
                        assetWorkingStatusDTO.ExpireDate = assetWorkingStatusDTO.ExpireDate.HasValue ? assetWorkingStatusDTO.ExpireDate.Value : assetExtraDetail.ExpireDate;
                        assetWorkingStatusDTO.PurchaseDate = assetExtraDetail.PurchaseDate;
                        assetWorkingStatusDTO.CategoryCode = assetCategory.CategoryCode;
                        assetWorkingStatusDTO.CategoryName = assetCategory.CategoryName;

                        dtos.Add(assetWorkingStatusDTO);
                    });

                    var drillingEntity = this.DataContext.BranchDrillingReports.Where(a => a.Id == Id).FirstOrDefault();
                    dto = MapperHelper.Get<BranchDrillingReportDTO, BranchDrillingReport>(Activator.CreateInstance<BranchDrillingReportDTO>(), drillingEntity);

                }
                else
                {

                    var result = this.DataContext.AssetDetails.Where(ad => ad.BranchCode == BranchId && ad.Record_Status == RecordStatus.Active).ToList<AssetDetail>();
                    foreach (var singleDTO in result)
                    {
                        var childResult = this.DataContext.AssetExtraDetails.Where(ae => ae.AssetDetailId == singleDTO.Id).ToList<AssetExtraDetail>();
                        foreach (var childDto in childResult)
                        {
                            AssetWorkingStatusDTO assetWorkingStatusDTO = new AssetWorkingStatusDTO();
                            assetWorkingStatusDTO.BranchDrillingReportId = Guid.Empty;
                            assetWorkingStatusDTO.AssetDetailId = singleDTO.Id;
                            assetWorkingStatusDTO.AssetCode = singleDTO.AssetCode;
                            assetWorkingStatusDTO.AssetName = singleDTO.AssetName;

                            assetWorkingStatusDTO.AssetExtraDetailId = childDto.Id;
                            assetWorkingStatusDTO.Location = childDto.Location;
                            assetWorkingStatusDTO.ExpireDate = childDto.ExpireDate;
                            assetWorkingStatusDTO.PurchaseDate = childDto.PurchaseDate;

                            var category = this.DataContext.AssetCategorys.Where(ac => ac.Id == singleDTO.AssetCategoryId).FirstOrDefault();

                            assetWorkingStatusDTO.CategoryCode = category.CategoryCode;
                            assetWorkingStatusDTO.CategoryName = category.CategoryName;

                            dtos.Add(assetWorkingStatusDTO);
                        }
                    }
                }

                moduleSummary.AssetWorkingStatusDTOs = dtos;


                if (dto != null)
                {
                    moduleSummary.TotalModification = dto.TotalModification;
                    moduleSummary.CreatedBy = dto.CreatedBy;
                    moduleSummary.ModifiedBy = dto.ModifiedBy;
                    moduleSummary.AuthorisedBy = dto.AuthorisedBy;
                    moduleSummary.CreatedById = dto.CreatedById;
                    moduleSummary.ModifiedById = dto.ModifiedById;
                    moduleSummary.AuthorisedById = dto.AuthorisedById;
                    moduleSummary.CreatedDate = dto.CreatedDate;
                    moduleSummary.ModifiedDate = dto.ModifiedDate;
                    moduleSummary.AuthorisedDate = dto.AuthorisedDate;
                    moduleSummary.EntityState = dto.EntityState;
                    moduleSummary.RecordStatus = dto.Record_Status;
                    moduleSummary.DoRecordExists = true;

                    RecordToDictionary = dto.GetType()
    .GetProperties(BindingFlags.Instance | BindingFlags.Public)
        .ToDictionary(prop => prop.Name, prop => prop.GetValue(dto, null));
                }


                ModuleSetup.ChildTableInformations.ForEach(c =>
                {
                    moduleSummary.ChildInformations.Add(new ChildModuleSummary()
                    {
                        ChildModuleSummaryName = c.Name,
                        ChildModuleSummaryTitle = c.Title,
                        Url = c.Url,
                        OrderValue = c.OrderValue

                    });
                });


                foreach (var ModuleBussinesLogic in ModuleSetup.ModuleBussinesLogicSetups.ToList())
                {
                    try
                    {
                        Dictionary<string, object> Attributes = new Dictionary<string, object>();

                        var moduleBussinesLogicSummary = new ModuleBussinesLogicSummary()
                        {
                            Name = ModuleBussinesLogic.Name,
                            ColumnName = ModuleBussinesLogic.ColumnName,
                            Description = ModuleBussinesLogic.Description,
                            DataType = ModuleBussinesLogic.DataType,
                            Required = ModuleBussinesLogic.Required,
                            HtmlDataType = ModuleBussinesLogic.HtmlDataType,
                            HtmlSize = ModuleBussinesLogic.HtmlSize,
                            Position = ModuleBussinesLogic.Position,
                            DefaultValue = ModuleBussinesLogic.DefaultValue,
                            CanUpdate = ModuleBussinesLogic.CanUpdate,
                            HelpMessage = ModuleBussinesLogic.HelpMessage,
                            IsParentColumn = ModuleBussinesLogic.IsParentColumn || !string.IsNullOrEmpty(moduleSummary.ParentModule) && string.Format("{0}Id", moduleSummary.ParentModule) == ModuleBussinesLogic.ColumnName,
                            SummaryHeader = ModuleBussinesLogic.SummaryHeader,
                            ParameterForSummaryHeader = ModuleBussinesLogic.ParameterForSummaryHeader,
                            IsForeignKey = ModuleBussinesLogic.IsForeignKey,
                            ForeignTable = ModuleBussinesLogic.ForeignTable,
                            DataSource = ModuleBussinesLogic.DataSource,
                            ParameterisedDataSorce = ModuleBussinesLogic.ParameterisedDataSorce,
                            Parameters = ModuleBussinesLogic.Parameters,
                            CurrentValue = RecordToDictionary.ContainsKey(ModuleBussinesLogic.ColumnName) ? RecordToDictionary[ModuleBussinesLogic.ColumnName] : null,
                            SelectList = ModuleBussinesLogic.HtmlDataType.ToLower() == "select" ? DropdownHelper.GetDropdownInformation
                            (ModuleBussinesLogic.ColumnName, RecordToDictionary.ContainsKey(ModuleBussinesLogic.ColumnName) ? RecordToDictionary[ModuleBussinesLogic.ColumnName] : ModuleBussinesLogic.DefaultValue, ModuleBussinesLogic.DataSource,
                            ModuleBussinesLogic.IsStaticDropDown) : null
                        };

                        moduleBussinesLogicSummary.Attributes = Attributes;

                        ModuleBussinesLogic.ModuleHtmlAttributeSetups.ToList().ForEach(f =>
                        {
                            if (moduleBussinesLogicSummary.Attributes.Where(a => a.Key == f.AttributeType).Count() == 0)
                            {
                                moduleBussinesLogicSummary.Attributes.Add(f.AttributeType, f.Value);
                            }

                        });

                        moduleSummary.moduleBussinesLogicSummaries.Add(moduleBussinesLogicSummary);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                return moduleSummary;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public Guid AddBranchDrillingReport(BranchDrillingReportDTO dto, bool Autoauthorise)
        {
            try
            {
                Guid Id = this.Add(dto, Autoauthorise);

                foreach (AssetWorkingStatusDTO assetStatusDTO in dto.AssetWorkingStatusDTOs)
                {
                    assetStatusDTO.BranchDrillingReportId = Id;

                    AssetWorkingStatus entity = new AssetWorkingStatus();
                    entity = MapperHelper.Get<AssetWorkingStatus, AssetWorkingStatusDTO>(entity, assetStatusDTO, CurrentAction.Create);

                    entity.Id = entity.Id == Guid.Empty ? Guid.NewGuid() : entity.Id;

                    this.DataContext.AssetWorkingStatuses.Add(entity);
                }
                return Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task UpdateBranchDrillingReport(BranchDrillingReportDTO dto, bool Autoauthorise)
        {
            try
            {
                await this.UpdateDrillingReport(dto, Autoauthorise);

                foreach (AssetWorkingStatusDTO assetStatusDTO in dto.AssetWorkingStatusDTOs)
                {
                    AssetWorkingStatus entity = this.DataContext.AssetWorkingStatuses.Where(e => e.Id == assetStatusDTO.Id).FirstOrDefault();

                    entity.ChangeLog = ChangeLogHelper.CreateChangeLog<AssetWorkingStatus, AssetWorkingStatusDTO>(entity, assetStatusDTO, entity.ChangeLog);

                    entity = MapperHelper.Get<AssetWorkingStatus, AssetWorkingStatusDTO>(entity, assetStatusDTO, CurrentAction.Edit);

                    this.DataContext.AssetWorkingStatuses.Attach(entity);
                    this.DataContext.Entry(entity).State = EntityState.Modified;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task UpdateDrillingReport(BranchDrillingReportDTO dto, bool Autoauthorise)
        {
            try
            {
                BranchDrillingReport entity = this.DataContext.BranchDrillingReports.Where(e => e.Id == dto.Id).FirstOrDefault();

                entity.ChangeLog = ChangeLogHelper.CreateChangeLog<BranchDrillingReport, BranchDrillingReportDTO>(entity, dto, entity.ChangeLog);

                entity = MapperHelper.Get<BranchDrillingReport, BranchDrillingReportDTO>(entity, dto, CurrentAction.Edit);

                if (Autoauthorise)
                {
                    entity.AuthorisedBy = _authenticationHelper.GetFullname();
                    entity.AuthorisedByStaffNo = _authenticationHelper.GetStaffNo();
                    entity.AuthorisedById = _authenticationHelper.GetUserId();
                    entity.AuthorisedDate = DateTime.Now;
                    entity.Record_Status = RecordStatus.Active;
                    entity.EntityState = DataEntityState.Modified;
                }
                else
                {
                    entity.AuthorisedBy = null;
                    entity.AuthorisedByStaffNo = null;
                    entity.AuthorisedById = null;
                    entity.AuthorisedDate = null;
                    entity.Record_Status = RecordStatus.UnVerified;
                    entity.EntityState = DataEntityState.Modified;
                }

                if (entity != null)
                {
                    entity.ModifiedBy = _authenticationHelper.GetFullname();
                    entity.ModifiedByStaffNo = _authenticationHelper.GetStaffNo();
                    entity.ModifiedById = _authenticationHelper.GetUserId();
                    entity.ModifiedDate = DateTime.Now;

                    this.DbSet.Attach(entity);
                    this.DataContext.Entry(entity).State = EntityState.Modified;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<GetDrillingReportByNotWorkingStatusDTO>> GetDrillingReportByNotWorkingStatus(params SqlParameter[] parameters)
        {
            try
            {
                List<GetDrillingReportByNotWorkingStatusDTO> listGetDrillingReportByNotWorkingStatusDTO = new List<GetDrillingReportByNotWorkingStatusDTO>();
                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _commnd = new SqlCommand("DrillingReport.SP_GetDrillingReportByNotWorkingStatus", _connection))
                    {
                        _commnd.CommandType = CommandType.StoredProcedure;

                        _commnd.Parameters.AddRange(parameters);


                        using (SqlDataReader _reader = await _commnd.ExecuteReaderAsync())
                        {
                            DataTable _records = new DataTable();
                            _records.Load(_reader);

                            var list = _records.ToList<GetDrillingReportByNotWorkingStatusDTO>();

                            listGetDrillingReportByNotWorkingStatusDTO.AddRange(list);
                        }
                    }
                }
                return listGetDrillingReportByNotWorkingStatusDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<GetBranchWiseReportStatusDTO>> GetBranchWiseReportStatus(params SqlParameter[] parameters)
        {
            try
            {
                List<GetBranchWiseReportStatusDTO> listGetBranchWiseReportStatusDTO = new List<GetBranchWiseReportStatusDTO>();
                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _commnd = new SqlCommand("DrillingReport.SP_GetBranchWiseReportStatus", _connection))
                    {
                        _commnd.CommandType = CommandType.StoredProcedure;

                        _commnd.Parameters.AddRange(parameters);


                        using (SqlDataReader _reader = await _commnd.ExecuteReaderAsync())
                        {
                            DataTable _records = new DataTable();
                            _records.Load(_reader);

                            var list = _records.ToList<GetBranchWiseReportStatusDTO>();

                            listGetBranchWiseReportStatusDTO.AddRange(list);
                        }
                    }
                }
                return listGetBranchWiseReportStatusDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<GetDrillingReportListDTO>> GetDrillingReportList(params SqlParameter[] parameters)
        {
            try
            {
                List<GetDrillingReportListDTO> listGetDrillingReportListDTO = new List<GetDrillingReportListDTO>();
                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _commnd = new SqlCommand("DrillingReport.SP_GetDrillingReportList", _connection))
                    {
                        _commnd.CommandType = CommandType.StoredProcedure;

                        _commnd.Parameters.AddRange(parameters);


                        using (SqlDataReader _reader = await _commnd.ExecuteReaderAsync())
                        {
                            DataTable _records = new DataTable();
                            _records.Load(_reader);

                            var list = _records.ToList<GetDrillingReportListDTO>();

                            listGetDrillingReportListDTO.AddRange(list);
                        }
                    }
                }
                return listGetDrillingReportListDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ModuleSummaryReport> GetModuleSummaryReportSetup()
        {
            try
            {
                Dictionary<string, object> RecordToDictionary = new Dictionary<string, object>();
                Guid? CurrentApplicationUserId = this._authenticationHelper.GetUserId();

                var ModuleSetup = ModuleHelper.GetModuleSetup<BranchDrillingReport>();

                ModuleSummaryReport moduleSummary = new ModuleSummaryReport()
                {
                    ModuleSummaryName = ModuleSetup.ApplicationClass,
                    ModuleSummaryTitle = ModuleSetup.Name,
                    EntryType = ModuleSetup.EntryType,
                    PrimaryRecordId = null,
                    IsParent = ModuleSetup.IsParent,
                    ParentModule = ModuleSetup.ParentModule,
                    ParentPrimaryRecordId = null
                };

                foreach (var ModuleBussinesLogic in ModuleSetup.ModuleBussinesLogicSetups.ToList())
                {
                    try
                    {
                        Dictionary<string, object> Attributes = new Dictionary<string, object>();

                        var moduleBussinesLogicSummary = new ModuleBussinesLogicSummary()
                        {
                            Name = ModuleBussinesLogic.Name,
                            ColumnName = ModuleBussinesLogic.ColumnName,
                            Description = ModuleBussinesLogic.Description,
                            DataType = ModuleBussinesLogic.DataType,
                            Required = ModuleBussinesLogic.Required,
                            HtmlDataType = ModuleBussinesLogic.HtmlDataType,
                            HtmlSize = ModuleBussinesLogic.HtmlSize,
                            Position = ModuleBussinesLogic.Position,
                            DefaultValue = ModuleBussinesLogic.DefaultValue,
                            CanUpdate = ModuleBussinesLogic.CanUpdate,
                            HelpMessage = ModuleBussinesLogic.HelpMessage,
                            IsParentColumn = ModuleBussinesLogic.IsParentColumn || !string.IsNullOrEmpty(moduleSummary.ParentModule) && string.Format("{0}Id", moduleSummary.ParentModule) == ModuleBussinesLogic.ColumnName,
                            SummaryHeader = ModuleBussinesLogic.SummaryHeader,
                            ParameterForSummaryHeader = ModuleBussinesLogic.ParameterForSummaryHeader,
                            IsForeignKey = ModuleBussinesLogic.IsForeignKey,
                            ForeignTable = ModuleBussinesLogic.ForeignTable,
                            DataSource = ModuleBussinesLogic.DataSource,
                            ParameterisedDataSorce = ModuleBussinesLogic.ParameterisedDataSorce,
                            Parameters = ModuleBussinesLogic.Parameters,
                            CurrentValue = RecordToDictionary.ContainsKey(ModuleBussinesLogic.ColumnName) ? RecordToDictionary[ModuleBussinesLogic.ColumnName] : null,
                            SelectList = ModuleBussinesLogic.HtmlDataType.ToLower() == "select" ? DropdownHelper.GetDropdownInformation
                            (ModuleBussinesLogic.ColumnName, ModuleBussinesLogic.DefaultValue, ModuleBussinesLogic.DataSource,
                            ModuleBussinesLogic.IsStaticDropDown) : null
                        };

                        moduleBussinesLogicSummary.Attributes = Attributes;

                        ModuleBussinesLogic.ModuleHtmlAttributeSetups.ToList().ForEach(f =>
                        {
                            if (moduleBussinesLogicSummary.Attributes.Where(a => a.Key == f.AttributeType).Count() == 0)
                            {
                                moduleBussinesLogicSummary.Attributes.Add(f.AttributeType, f.Value);
                            }

                        });

                        moduleSummary.moduleBussinesLogicSummaries.Add(moduleBussinesLogicSummary);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                return moduleSummary;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<List<ModuleBussinesLogicSummary>> GetReportFilterParameter()
        {
            List<ModuleBussinesLogicSummary> moduleBussinesLogicSummaries = new List<ModuleBussinesLogicSummary>();

            var moduleBussinesLogic = new ModuleBussinesLogicSummary()
            {
                Name = "Branch / EC",
                ColumnName = "BranchCode",
                Description = "Branch / EC",
                DataType = "string",
                Required = true,
                HtmlDataType = "select",
                HtmlSize = 3,
                Position = 1,
                HelpMessage = "Branch / EC",
                ParameterForSummaryHeader = true,
                DataSource = "Branch",
                SelectList = DropdownHelper.GetDropdownInformation
                           ("Branch", null, "Branch",
                           false)

            };
            Dictionary<string, object> Attributes = new Dictionary<string, object>();
            Attributes.Add("class", "form-control dropdown");
            Attributes.Add("placeholder", "Branch / EC");
            moduleBussinesLogic.Attributes = Attributes;

            moduleBussinesLogicSummaries.Add(moduleBussinesLogic);

            moduleBussinesLogic = new ModuleBussinesLogicSummary()
            {
                Name = "Year (Calendar Year)",
                ColumnName = "ReportYear",
                Description = "Year (Calendar Year)",
                DataType = "string",
                Required = true,
                HtmlDataType = "select",
                HtmlSize = 3,
                Position = 3,
                HelpMessage = "Year (Calendar Year)",
                ParameterForSummaryHeader = true,
                DataSource = "ReportYear",
                SelectList = DropdownHelper.GetDropdownInformation("ReportYear", null, "ReportYear", true)
            };
            Attributes = new Dictionary<string, object>();
            Attributes.Add("class", "form-control dropdown");
            Attributes.Add("placeholder", "Year (Calendar Year)");
            moduleBussinesLogic.Attributes = Attributes;

            moduleBussinesLogicSummaries.Add(moduleBussinesLogic);

            moduleBussinesLogic = new ModuleBussinesLogicSummary()
            {
                Name = "Reported Quarter",
                ColumnName = "ReportPeriod",
                Description = "Reported Quarter",
                DataType = "string",
                Required = true,
                HtmlDataType = "select",
                HtmlSize = 3,
                Position = 4,
                HelpMessage = "Reported Quarter",
                ParameterForSummaryHeader = true,
                DataSource = "ReportPeriod",
                SelectList = DropdownHelper.GetDropdownInformation("ReportPeriod", null, "ReportPeriod", true)
            };
            Attributes = new Dictionary<string, object>();
            Attributes.Add("class", "form-control dropdown");
            Attributes.Add("placeholder", "Reported Quarter");
            moduleBussinesLogic.Attributes = Attributes;

            moduleBussinesLogicSummaries.Add(moduleBussinesLogic);
            //moduleBussinesLogic = new ModuleBussinesLogicSummary()
            //{
            //    Name = "From Date",
            //    ColumnName = "FROMReportDate",
            //    Description = "From Date",
            //    DataType = "DateTime",
            //    Required = true,
            //    HtmlDataType = "input",
            //    HtmlSize = 3,
            //    Position = 2,
            //    HelpMessage = "From Date",
            //    DefaultValue = DateTime.Now.ToString("MM/dd/yyyy"),
            //    ParameterForSummaryHeader = true,
            //};
            //Attributes = new Dictionary<string, object>();
            //Attributes.Add("class", "form-control datepicker");
            //Attributes.Add("placeholder", "From Date");
            //moduleBussinesLogic.Attributes = Attributes;

            //moduleBussinesLogicSummaries.Add(moduleBussinesLogic);

            //moduleBussinesLogic = new ModuleBussinesLogicSummary()
            //{
            //    Name = "To Date",
            //    ColumnName = "ToReportDate",
            //    Description = "To Date",
            //    DataType = "DateTime",
            //    Required = false,
            //    HtmlDataType = "input",
            //    HtmlSize = 3,
            //    Position = 3,
            //    HelpMessage = "To Date",
            //    DefaultValue = DateTime.Now.ToString("MM/dd/yyyy"),
            //    ParameterForSummaryHeader = true,
            //};
            //Attributes = new Dictionary<string, object>();
            //Attributes.Add("class", "form-control datepicker");
            //Attributes.Add("placeholder", "To Date");
            //moduleBussinesLogic.Attributes = Attributes;

            //moduleBussinesLogicSummaries.Add(moduleBussinesLogic);

            return moduleBussinesLogicSummaries;
        }

        public async Task<List<ModuleBussinesLogicSummary>> GetReportFilterParameterNew()
        {
            List<ModuleBussinesLogicSummary> moduleBussinesLogicSummaries = new List<ModuleBussinesLogicSummary>();

            var moduleBussinesLogic = new ModuleBussinesLogicSummary()
            {
                Name = "Branch / EC",
                ColumnName = "BranchCode",
                Description = "Branch / EC",
                DataType = "string",
                Required = true,
                HtmlDataType = "select",
                HtmlSize = 3,
                Position = 1,
                HelpMessage = "Branch / EC",
                ParameterForSummaryHeader = true,
                DataSource = "Branch",
                SelectList = DropdownHelper.GetDropdownInformation
                           ("Branch", null, "Branch",
                           false)

            };
            Dictionary<string, object> Attributes = new Dictionary<string, object>();
            Attributes.Add("class", "form-control dropdown");
            Attributes.Add("placeholder", "Branch / EC");
            moduleBussinesLogic.Attributes = Attributes;

            moduleBussinesLogicSummaries.Add(moduleBussinesLogic);

            moduleBussinesLogic = new ModuleBussinesLogicSummary()
            {
                Name = "Asset Category",
                ColumnName = "AssetCategoryId",
                Description = "Asset Category",
                DataType = "string",
                Required = true,
                HtmlDataType = "select",
                HtmlSize = 3,
                Position = 2,
                HelpMessage = "Asset Category",
                ParameterForSummaryHeader = true,
                DataSource = "SELECT CategoryName [Text],CAST(Id AS nvarchar(50)) [Value] FROM AssetTypeSetting.AssetCategory WHERE Record_Status=2",
                SelectList = DropdownHelper.GetDropdownInformation
                           ("AssetCategoryId", null, "SELECT CategoryName [Text],CAST(Id AS nvarchar(50)) [Value] FROM AssetTypeSetting.AssetCategory WHERE Record_Status=2",
                           false)

            };
            Attributes = new Dictionary<string, object>();
            Attributes.Add("class", "form-control dropdown");
            Attributes.Add("placeholder", "Asset Category");
            moduleBussinesLogic.Attributes = Attributes;

            moduleBussinesLogicSummaries.Add(moduleBussinesLogic);

            moduleBussinesLogic = new ModuleBussinesLogicSummary()
            {
                Name = "Year (Calendar Year)",
                ColumnName = "ReportYear",
                Description = "Year (Calendar Year)",
                DataType = "string",
                Required = true,
                HtmlDataType = "select",
                HtmlSize = 3,
                Position = 3,
                HelpMessage = "Year (Calendar Year)",
                ParameterForSummaryHeader = true,
                DataSource = "ReportYear",
                SelectList = DropdownHelper.GetDropdownInformation("ReportYear", null, "ReportYear", true)
            };
            Attributes = new Dictionary<string, object>();
            Attributes.Add("class", "form-control dropdown");
            Attributes.Add("placeholder", "Year (Calendar Year)");
            moduleBussinesLogic.Attributes = Attributes;

            moduleBussinesLogicSummaries.Add(moduleBussinesLogic);

            moduleBussinesLogic = new ModuleBussinesLogicSummary()
            {
                Name = "Reported Quarter",
                ColumnName = "ReportPeriod",
                Description = "Reported Quarter",
                DataType = "string",
                Required = true,
                HtmlDataType = "select",
                HtmlSize = 3,
                Position = 4,
                HelpMessage = "Reported Quarter",
                ParameterForSummaryHeader = true,
                DataSource = "ReportPeriod",
                SelectList = DropdownHelper.GetDropdownInformation("ReportPeriod", null, "ReportPeriod", true)
            };
            Attributes = new Dictionary<string, object>();
            Attributes.Add("class", "form-control dropdown");
            Attributes.Add("placeholder", "Reported Quarter");
            moduleBussinesLogic.Attributes = Attributes;

            moduleBussinesLogicSummaries.Add(moduleBussinesLogic);

            //moduleBussinesLogic = new ModuleBussinesLogicSummary()
            //{
            //    Name = "From Date",
            //    ColumnName = "FROMReportDate",
            //    Description = "From Date",
            //    DataType = "DateTime",
            //    Required = true,
            //    HtmlDataType = "hidden",
            //    HtmlSize = 3,
            //    Position = 4,
            //    HelpMessage = "From Date",
            //    DefaultValue = DateTime.Now.ToString("MM/dd/yyyy"),
            //    ParameterForSummaryHeader = true,
            //};
            //Attributes = new Dictionary<string, object>();
            //Attributes.Add("class", "form-control datepicker");
            //Attributes.Add("placeholder", "From Date");
            //moduleBussinesLogic.Attributes = Attributes;

            //moduleBussinesLogicSummaries.Add(moduleBussinesLogic);

            //moduleBussinesLogic = new ModuleBussinesLogicSummary()
            //{
            //    Name = "To Date",
            //    ColumnName = "ToReportDate",
            //    Description = "To Date",
            //    DataType = "DateTime",
            //    Required = false,
            //    HtmlDataType = "hidden",
            //    HtmlSize = 3,
            //    Position = 5,
            //    HelpMessage = "To Date",
            //    DefaultValue = DateTime.Now.ToString("MM/dd/yyyy"),
            //    ParameterForSummaryHeader = true,
            //};
            //Attributes = new Dictionary<string, object>();
            //Attributes.Add("class", "form-control datepicker");
            //Attributes.Add("placeholder", "To Date");
            //moduleBussinesLogic.Attributes = Attributes;

            //moduleBussinesLogicSummaries.Add(moduleBussinesLogic);

            return moduleBussinesLogicSummaries;
        }
        public async Task<List<BranchDrillingReportDTO>> CheckBranchDrillingReportDTOs(string branchCode, string year, string reportPeriod)
        {
            try
            {
                Expression<Func<BranchDrillingReport, bool>> checkDupData = s => s.BranchCode == branchCode && s.ReportYear == year && s.ReportPeriod == reportPeriod && s.Record_Status != RecordStatus.Inactive;

                var dto = await this.GetManyDTO(checkDupData);

                List<BranchDrillingReportDTO> list = new List<BranchDrillingReportDTO>();
                list.AddRange(dto);

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public interface IBranchDrillingReportRepository : IRepository<BranchDrillingReport, BranchDrillingReportDTO>
    {
        Task<ModuleSummaryForDetails> GetModuleBussinesLogicSetupWithChild(Guid? Id, string branchCode);
        Guid AddBranchDrillingReport(BranchDrillingReportDTO dto, bool Autoauthorise);
        Task UpdateBranchDrillingReport(BranchDrillingReportDTO dto, bool Autoauthorise);
        Task<List<GetDrillingReportByNotWorkingStatusDTO>> GetDrillingReportByNotWorkingStatus(params SqlParameter[] parameters);
        Task<List<GetBranchWiseReportStatusDTO>> GetBranchWiseReportStatus(params SqlParameter[] parameters);
        Task<List<GetDrillingReportListDTO>> GetDrillingReportList(params SqlParameter[] parameters);
        Task<ModuleSummaryReport> GetModuleSummaryReportSetup();
        Task<List<ModuleBussinesLogicSummary>> GetReportFilterParameter();
        Task<List<ModuleBussinesLogicSummary>> GetReportFilterParameterNew();
        Task<List<BranchDrillingReportDTO>> CheckBranchDrillingReportDTOs(string branchCode, string year, string reportPeriod);
    }
}
