﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;

namespace GSAssetManagement.Infrastructure
{
    public interface IRepository<T, D> where T : class where D : class
    {
        Guid Add(D dto, bool Autoauthorise);
        Task Update(D dto, bool Autoauthorise);
        Task DiscardChanges(Guid Id);
        Task Authorise(Guid Id);
        Task Revert(Guid Id);
        Task Delete(Guid Id, bool Autoauthorise);
        Task Close(Guid Id, bool Autoauthorise);
        Task<D> GetDTOById(Guid Id);
        Task<T> GetEntityById(Guid Id);
        Task<D> GetDTO(Expression<Func<T, bool>> where);
        Task<IEnumerable<D>> GetManyDTO(Expression<Func<T, bool>> where);
        //Task<PagedResultDataTable> GetAllByProcedure(string Schema, string EntityName, params SqlParameter[] parameters);
        Task<PagedResultDataTable> GetAllByProcedure(string StoreProcedure, params SqlParameter[] parameters);
        //Task<ModuleSummary> GetModuleBussinesLogicSetup(Guid? Id, Guid? ParentPrimaryRecordId, bool IsSummaryRequest, bool DropdownRequired, List<AdditionalDropdownParameter> DropdownAdditionalParameters = null);
        Task<ModuleSummary> GetModuleBussinesLogicSetup();
        Task<ModuleSummary> GetModuleBussinesLogicSetup(D dto,Guid? ParentPrimaryRecordId);

    }
}
