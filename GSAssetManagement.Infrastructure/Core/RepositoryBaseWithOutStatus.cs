﻿using GSAssetManagement.Data;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Infrastructure
{
    public abstract class RepositoryBaseWithOutStatus<T, D> where T : BaseWithOutStatus<Guid>, new() where D : BaseWithOutStatusDTO<Guid>
    {
        private ApplicationDbContext dataContext;
        private readonly DbSet<T> dbset;
        protected readonly IAuthenticationHelper _authenticationHelper;

        protected RepositoryBaseWithOutStatus(
            IDatabaseFactory databaseFactory,
            IAuthenticationHelper authenticationHelper
            )
        {
            DatabaseFactory = databaseFactory;
            dbset = DataContext.Set<T>();
            this._authenticationHelper = authenticationHelper;
        }

        protected IDatabaseFactory DatabaseFactory
        {
            get;
            private set;
        }

        protected ApplicationDbContext DataContext
        {
            get { return dataContext ?? (dataContext = DatabaseFactory.Get()); }
        }

        protected IAuthenticationHelper AuthenticationHelper
        {
            get { return _authenticationHelper; }
        }
        public virtual Guid Add(D DTO)
        {
            T entity = new T();


            entity = MapperHelper.Get<T, D>(entity, DTO);

            entity.Id = entity.Id == Guid.Empty ? Guid.NewGuid() : entity.Id;

            dbset.Add(entity);

            return entity.Id;
        }
        public virtual async Task Update(D DTO)
        {

            try
            {
                T entity = await this.dbset.Where(e => e.Id == DTO.Id).FirstOrDefaultAsync();

                entity.ChangeLog = ChangeLogHelper.CreateChangeLog<T, D>(entity, DTO, entity.ChangeLog);

                entity = MapperHelper.Get<T, D>(entity, DTO);

                dbset.Attach(entity);
                dataContext.Entry(entity).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<D> GetDTOById(Guid Id)
        {
            T entity = await dbset.FindAsync(Id);
            if (entity != null)
                return MapperHelper.Get<D, T>(Activator.CreateInstance<D>(), entity);
            else
                return null;
        }
        public virtual async Task<D> GetDTO(Expression<Func<T, bool>> where)
        {
            T entity = await dbset.Where(where).FirstOrDefaultAsync();
            if (entity != null)
                return MapperHelper.Get<D, T>(Activator.CreateInstance<D>(), entity);
            else
                return null;
        }
        public async Task<T> GetEntityById(Guid Id)
        {
            return await dbset.FindAsync(Id);
        }
        public virtual async Task<IEnumerable<D>> GetManyDTO(Expression<Func<T, bool>> where)
        {
            List<T> entities = await dbset.Where(where).ToListAsync();
            List<D> dtos = new List<D>();

            entities.ForEach(entity =>
            {
                D dto = MapperHelper.Get<D, T>(Activator.CreateInstance<D>(), entity);
                dtos.Add(dto);

            });

            return dtos;
        }
        //public async Task<PagedResultDataTable> GetAllByProcedure(string Schema, string EntityName, params SqlParameter[] parameters)
        //{
        //    try
        //    {
        //        var pageList = new PagedResult<DataTable>();

        //        int PageSize = int.Parse(parameters.Where(x => x.ParameterName == "PageSize").Select(x => x.Value).FirstOrDefault().ToString());
        //        int PageNumber = int.Parse(parameters.Where(x => x.ParameterName == "PageNumber").Select(x => x.Value).FirstOrDefault().ToString());

        //        string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        //        using (SqlConnection _connection = new SqlConnection(_connectionString))
        //        {
        //            await _connection.OpenAsync();

        //            using (SqlCommand _commnd = new SqlCommand(string.Format("[{0}].[SP_Get{1}List]", Schema, EntityName), _connection))
        //            {
        //                _commnd.CommandType = CommandType.StoredProcedure;

        //                _commnd.Parameters.AddRange(parameters);


        //                using (SqlDataReader _reader = await _commnd.ExecuteReaderAsync())
        //                {
        //                    DataTable _records = new DataTable();
        //                    _records.Load(_reader);

        //                    if (_records.Rows.Count > 0)
        //                    {

        //                        var _result = _records.AsEnumerable().GroupBy(x => new { PageCount = x["PageCount"], TotalRecords = x["TotalRecords"] }).Select(z => new PagedResultDataTable()
        //                        {
        //                            PageCount = int.Parse(z.Key.PageCount.ToString()),
        //                            RowCount = int.Parse(z.Key.TotalRecords.ToString()),
        //                            PageSize = PageSize,
        //                            PageNumber = PageNumber,
        //                            Results = z.CopyToDataTable()

        //                        }).FirstOrDefault();

        //                        return _result;
        //                    }
        //                    else
        //                    {
        //                        return new PagedResultDataTable()
        //                        {
        //                            PageCount = 0,
        //                            RowCount = 0,
        //                            PageNumber = 1,
        //                            PageSize = PageSize,
        //                            Results = _records

        //                        };
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        public async Task<PagedResultDataTable> GetAllByProcedure(string StoreProcedure, params SqlParameter[] parameters)
        {
            try
            {
                var pageList = new PagedResult<DataTable>();

                int PageSize = int.Parse(parameters.Where(x => x.ParameterName == "PageSize").Select(x => x.Value).FirstOrDefault().ToString());
                int PageNumber = int.Parse(parameters.Where(x => x.ParameterName == "PageNumber").Select(x => x.Value).FirstOrDefault().ToString());

                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _commnd = new SqlCommand(StoreProcedure, _connection))
                    {
                        _commnd.CommandType = CommandType.StoredProcedure;

                        _commnd.Parameters.AddRange(parameters);


                        using (SqlDataReader _reader = await _commnd.ExecuteReaderAsync())
                        {
                            DataTable _records = new DataTable();
                            _records.Load(_reader);

                            if (_records.Rows.Count > 0)
                            {

                                var _result = _records.AsEnumerable().GroupBy(x => new { PageCount = x["PageCount"], TotalRecords = x["TotalRecords"] }).Select(z => new PagedResultDataTable()
                                {
                                    PageCount = int.Parse(z.Key.PageCount.ToString()),
                                    RowCount = int.Parse(z.Key.TotalRecords.ToString()),
                                    PageSize = PageSize,
                                    PageNumber = PageNumber,
                                    Results = z.CopyToDataTable()

                                }).FirstOrDefault();

                                return _result;
                            }
                            else
                            {
                                return new PagedResultDataTable()
                                {
                                    PageCount = 0,
                                    RowCount = 0,
                                    PageNumber = 1,
                                    PageSize = PageSize,
                                    Results = _records

                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ModuleSummary> GetModuleBussinesLogicSetup()
        {
            try
            {
                Dictionary<string, object> RecordToDictionary = new Dictionary<string, object>();
                Guid? CurrentApplicationUserId = this._authenticationHelper.GetUserId();

                var ModuleSetup = ModuleHelper.GetModuleSetup<T>();

                ModuleSummary moduleSummary = new ModuleSummary()
                {
                    ModuleSummaryName = ModuleSetup.ApplicationClass,
                    ModuleSummaryTitle = ModuleSetup.Name,
                    EntryType = ModuleSetup.EntryType,
                    PrimaryRecordId = null,
                    IsParent = ModuleSetup.IsParent,
                    ParentModule = ModuleSetup.ParentModule,
                    ParentPrimaryRecordId = null
                };

                ModuleSetup.ChildTableInformations.ForEach(c =>
                {
                    moduleSummary.ChildInformations.Add(new ChildModuleSummary()
                    {
                        ChildModuleSummaryName = c.Name,
                        ChildModuleSummaryTitle = c.Title,
                        Url = c.Url,
                        OrderValue = c.OrderValue

                    });


                });


                foreach (var ModuleBussinesLogic in ModuleSetup.ModuleBussinesLogicSetups.ToList())
                {
                    try
                    {
                        Dictionary<string, object> Attributes = new Dictionary<string, object>();

                        var moduleBussinesLogicSummary = new ModuleBussinesLogicSummary()
                        {
                            Name = ModuleBussinesLogic.Name,
                            ColumnName = ModuleBussinesLogic.ColumnName,
                            Description = ModuleBussinesLogic.Description,
                            DataType = ModuleBussinesLogic.DataType,
                            Required = ModuleBussinesLogic.Required,
                            HtmlDataType = ModuleBussinesLogic.HtmlDataType,
                            HtmlSize = ModuleBussinesLogic.HtmlSize,
                            Position = ModuleBussinesLogic.Position,
                            DefaultValue = ModuleBussinesLogic.DefaultValue,
                            CanUpdate = ModuleBussinesLogic.CanUpdate,
                            HelpMessage = ModuleBussinesLogic.HelpMessage,
                            IsParentColumn = ModuleBussinesLogic.IsParentColumn || !string.IsNullOrEmpty(moduleSummary.ParentModule) && string.Format("{0}Id", moduleSummary.ParentModule) == ModuleBussinesLogic.ColumnName,
                            SummaryHeader = ModuleBussinesLogic.SummaryHeader,
                            ParameterForSummaryHeader = ModuleBussinesLogic.ParameterForSummaryHeader,
                            IsForeignKey = ModuleBussinesLogic.IsForeignKey,
                            ForeignTable = ModuleBussinesLogic.ForeignTable,
                            DataSource = ModuleBussinesLogic.DataSource,
                            ParameterisedDataSorce = ModuleBussinesLogic.ParameterisedDataSorce,
                            Parameters = ModuleBussinesLogic.Parameters,
                            CurrentValue = RecordToDictionary.ContainsKey(ModuleBussinesLogic.ColumnName) ? RecordToDictionary[ModuleBussinesLogic.ColumnName] : null,
                            SelectList = ModuleBussinesLogic.HtmlDataType.ToLower() == "select" ? DropdownHelper.GetDropdownInformation
                            (ModuleBussinesLogic.ColumnName, ModuleBussinesLogic.DefaultValue, ModuleBussinesLogic.DataSource,
                            ModuleBussinesLogic.IsStaticDropDown) : null
                        };

                        moduleBussinesLogicSummary.Attributes = Attributes;

                        ModuleBussinesLogic.ModuleHtmlAttributeSetups.ToList().ForEach(f =>
                        {
                            if (moduleBussinesLogicSummary.Attributes.Where(a => a.Key == f.AttributeType).Count() == 0)
                            {
                                moduleBussinesLogicSummary.Attributes.Add(f.AttributeType, f.Value);
                            }

                        });

                        moduleSummary.moduleBussinesLogicSummaries.Add(moduleBussinesLogicSummary);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                return moduleSummary;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task<ModuleSummary> GetModuleBussinesLogicSetup(D dto, Guid? ParentPrimaryRecordId)
        {
            try
            {
                Dictionary<string, object> RecordToDictionary = new Dictionary<string, object>();
                Guid? CurrentApplicationUserId = this._authenticationHelper.GetUserId();

                var ModuleSetup = ModuleHelper.GetModuleSetup<T>();

                ModuleSummary moduleSummary = new ModuleSummary()
                {
                    ModuleSummaryName = ModuleSetup.ApplicationClass,
                    ModuleSummaryTitle = ModuleSetup.Name,
                    EntryType = ModuleSetup.EntryType,
                    PrimaryRecordId = dto.Id,
                    IsParent = ModuleSetup.IsParent,
                    ParentModule = ModuleSetup.ParentModule,
                    ParentPrimaryRecordId = ParentPrimaryRecordId
                };

                if (dto != null)
                {
                    moduleSummary.TotalModification = dto.TotalModification;
                    moduleSummary.DoRecordExists = true;


                    //moduleSummary.RecordChangeLog = Record.EntityState == DataEntityState.Modified && Record.RecordStatus == RecordStatus.UnVerified ? ChangeLogHelper.GetLatestRecordChangeLogs(Record.ChangeLog) : null;

                    RecordToDictionary = dto.GetType()
    .GetProperties(BindingFlags.Instance | BindingFlags.Public)
        .ToDictionary(prop => prop.Name, prop => prop.GetValue(dto, null));
                }

                ModuleSetup.ChildTableInformations.ForEach(c =>
                {
                    moduleSummary.ChildInformations.Add(new ChildModuleSummary()
                    {
                        ChildModuleSummaryName = c.Name,
                        ChildModuleSummaryTitle = c.Title,
                        Url = c.Url,
                        OrderValue = c.OrderValue

                    });


                });


                foreach (var ModuleBussinesLogic in ModuleSetup.ModuleBussinesLogicSetups.ToList())
                {
                    try
                    {
                        Dictionary<string, object> Attributes = new Dictionary<string, object>();

                        var moduleBussinesLogicSummary = new ModuleBussinesLogicSummary()
                        {
                            Name = ModuleBussinesLogic.Name,
                            ColumnName = ModuleBussinesLogic.ColumnName,
                            Description = ModuleBussinesLogic.Description,
                            DataType = ModuleBussinesLogic.DataType,
                            Required = ModuleBussinesLogic.Required,
                            HtmlDataType = ModuleBussinesLogic.HtmlDataType,
                            HtmlSize = ModuleBussinesLogic.HtmlSize,
                            Position = ModuleBussinesLogic.Position,
                            DefaultValue = ModuleBussinesLogic.DefaultValue,
                            CanUpdate = ModuleBussinesLogic.CanUpdate,
                            HelpMessage = ModuleBussinesLogic.HelpMessage,
                            IsParentColumn = ModuleBussinesLogic.IsParentColumn || !string.IsNullOrEmpty(moduleSummary.ParentModule) && string.Format("{0}Id", moduleSummary.ParentModule) == ModuleBussinesLogic.ColumnName,
                            SummaryHeader = ModuleBussinesLogic.SummaryHeader,
                            ParameterForSummaryHeader = ModuleBussinesLogic.ParameterForSummaryHeader,
                            IsForeignKey = ModuleBussinesLogic.IsForeignKey,
                            ForeignTable = ModuleBussinesLogic.ForeignTable,
                            DataSource = ModuleBussinesLogic.DataSource,
                            ParameterisedDataSorce = ModuleBussinesLogic.ParameterisedDataSorce,
                            Parameters = ModuleBussinesLogic.Parameters,
                            CurrentValue = RecordToDictionary.ContainsKey(ModuleBussinesLogic.ColumnName) ? RecordToDictionary[ModuleBussinesLogic.ColumnName] : null,
                            SelectList = ModuleBussinesLogic.HtmlDataType.ToLower() == "select" ? DropdownHelper.GetDropdownInformation
                            (ModuleBussinesLogic.ColumnName, RecordToDictionary.ContainsKey(ModuleBussinesLogic.ColumnName) ? RecordToDictionary[ModuleBussinesLogic.ColumnName] : ModuleBussinesLogic.DefaultValue, ModuleBussinesLogic.DataSource,
                            ModuleBussinesLogic.IsStaticDropDown) : null

                        };

                        moduleBussinesLogicSummary.Attributes = Attributes;

                        ModuleBussinesLogic.ModuleHtmlAttributeSetups.ToList().ForEach(f =>
                        {
                            if (moduleBussinesLogicSummary.Attributes.Where(a => a.Key == f.AttributeType).Count() == 0)
                            {
                                moduleBussinesLogicSummary.Attributes.Add(f.AttributeType, f.Value);
                            }

                        });

                        moduleSummary.moduleBussinesLogicSummaries.Add(moduleBussinesLogicSummary);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                return moduleSummary;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
