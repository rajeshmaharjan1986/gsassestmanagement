﻿using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Infrastructure
{
    public interface IRepositoryBaseWithOutStatus<T, D> where T : BaseWithOutStatus<Guid>, new() where D : BaseWithOutStatusDTO<Guid>
    {
        Guid Add(D dto);
        Task Update(D dto);
        Task<D> GetDTOById(Guid Id);
        Task<T> GetEntityById(Guid Id);
        Task<D> GetDTO(Expression<Func<T, bool>> where);
        Task<IEnumerable<D>> GetManyDTO(Expression<Func<T, bool>> where);
        //Task<PagedResultDataTable> GetAllByProcedure(string Schema, string EntityName, params SqlParameter[] parameters);
        Task<PagedResultDataTable> GetAllByProcedure(string StoreProcedure, params SqlParameter[] parameters);
        //Task<ModuleSummary> GetModuleBussinesLogicSetup(Guid? Id, Guid? ParentPrimaryRecordId, bool IsSummaryRequest, bool DropdownRequired, List<AdditionalDropdownParameter> DropdownAdditionalParameters = null);
        Task<ModuleSummary> GetModuleBussinesLogicSetup();
        Task<ModuleSummary> GetModuleBussinesLogicSetup(D dto, Guid? ParentPrimaryRecordId);
    }
}
