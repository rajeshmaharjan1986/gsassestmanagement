﻿using GSAssetManagement.Data;
using System;
using System.Data.SqlClient;


namespace GSAssetManagement.Infrastructure
{
    public interface IDatabaseFactory //: IDisposable
    {
        ApplicationDbContext Get();
    }
}



