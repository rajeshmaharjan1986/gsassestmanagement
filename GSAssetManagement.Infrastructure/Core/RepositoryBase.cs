﻿using GSAssetManagement.Data;
using GSAssetManagement.Entity;
using GSAssetManagement.Infrastructure.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using GSAssetManagement.Entity.DTO;
using System.IO;

namespace GSAssetManagement.Infrastructure
{
    public abstract class RepositoryBase<T, D> where T : OfficeBaseEntity<Guid>, new() where D : OfficeBaseEntityDTO<Guid>
    {
        private ApplicationDbContext dataContext;
        private readonly DbSet<T> dbset;
        protected readonly IAuthenticationHelper _authenticationHelper;

        protected RepositoryBase(
            IDatabaseFactory databaseFactory,
            IAuthenticationHelper authenticationHelper
            )
        {
            DatabaseFactory = databaseFactory;
            dbset = DataContext.Set<T>();
            this._authenticationHelper = authenticationHelper;
        }

        protected IDatabaseFactory DatabaseFactory
        {
            get;
            private set;
        }

        protected ApplicationDbContext DataContext
        {
            get { return dataContext ?? (dataContext = DatabaseFactory.Get()); }
        }

        protected DbSet<T> DbSet
        {
            get { return dbset; }
        }

        public virtual Guid Add(D DTO, bool Autoauthorise)
        {
            T entity = new T();


            entity = MapperHelper.Get<T, D>(entity, DTO, CurrentAction.Create);

            entity.Id = entity.Id == Guid.Empty ? Guid.NewGuid() : entity.Id;

            entity.EntityState = DataEntityState.Added;

            entity.CreatedBy = entity.CreatedBy == null ? _authenticationHelper.GetFullname() : entity.CreatedBy;
            entity.ModifiedBy = entity.ModifiedBy == null ? _authenticationHelper.GetFullname() : entity.ModifiedBy;

            entity.CreatedById = entity.CreatedById == null || entity.CreatedById == Guid.Empty ? _authenticationHelper.GetUserId() : entity.CreatedById;
            entity.ModifiedById = entity.ModifiedById == null || entity.ModifiedById == Guid.Empty ? _authenticationHelper.GetUserId() : entity.ModifiedById;

            entity.CreatedByStaffNo = entity.CreatedByStaffNo == null || string.IsNullOrEmpty(entity.CreatedByStaffNo) ? _authenticationHelper.GetStaffNo() : entity.CreatedByStaffNo;
            entity.ModifiedByStaffNo = entity.ModifiedByStaffNo == null || string.IsNullOrEmpty(entity.ModifiedByStaffNo) ? _authenticationHelper.GetStaffNo() : entity.CreatedByStaffNo;

            entity.CreatedDate = DateTime.Now;
            entity.ModifiedDate = DateTime.Now;

            if (Autoauthorise)
            {
                entity.AuthorisedBy = entity.AuthorisedBy == null ? _authenticationHelper.GetFullname() : entity.AuthorisedBy;
                entity.AuthorisedById = entity.AuthorisedById == null || entity.AuthorisedById == Guid.Empty ? _authenticationHelper.GetUserId() : entity.AuthorisedById;
                entity.AuthorisedByStaffNo = entity.AuthorisedByStaffNo == null || string.IsNullOrEmpty(entity.AuthorisedByStaffNo) ? _authenticationHelper.GetStaffNo() : entity.AuthorisedByStaffNo;

                entity.AuthorisedDate = DateTime.Now;
            }

            entity.Record_Status = RecordStatus.Active;

            if (!Autoauthorise)
                entity.Record_Status = RecordStatus.UnVerified;


            dbset.Add(entity);

            return entity.Id;
        }
        public virtual async Task Update(D DTO, bool Autoauthorise)
        {

            try
            {
                T entity = await this.dbset.Where(e => e.Id == DTO.Id).FirstOrDefaultAsync();

                entity.ChangeLog = ChangeLogHelper.CreateChangeLog<T, D>(entity, DTO, entity.ChangeLog);

                entity = MapperHelper.Get<T, D>(entity, DTO, CurrentAction.Edit);
                //entity = MapperHelper.Get<T, D>(entity, DTO);

                if (!string.IsNullOrEmpty(entity.ChangeLog))
                {
                    if (Autoauthorise)
                    {
                        //entity = MapperHelper.GetEntityForAutoAuthoriser<T, D>(entity, DTO, true);
                        entity.AuthorisedBy = _authenticationHelper.GetFullname();
                        entity.AuthorisedByStaffNo = _authenticationHelper.GetStaffNo();
                        entity.AuthorisedById = _authenticationHelper.GetUserId();
                        entity.AuthorisedDate = DateTime.Now;
                        entity.Record_Status = RecordStatus.Active;
                        entity.EntityState = DataEntityState.Modified;
                    }
                    else
                    {
                        entity.AuthorisedBy = null;
                        entity.AuthorisedByStaffNo = null;
                        entity.AuthorisedById = null;
                        entity.AuthorisedDate = null;
                        entity.Record_Status = RecordStatus.UnVerified;
                        entity.EntityState = DataEntityState.Modified;
                    }

                    if (entity != null)
                    {
                        entity.ModifiedBy = _authenticationHelper.GetFullname();
                        entity.ModifiedByStaffNo = _authenticationHelper.GetStaffNo();
                        entity.ModifiedById = _authenticationHelper.GetUserId();
                        entity.ModifiedDate = DateTime.Now;

                        dbset.Attach(entity);
                        dataContext.Entry(entity).State = EntityState.Modified;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public virtual async Task DiscardChanges(Guid Id)
        {
            try
            {
                T entity = await this.dbset.Where(e => e.Id == Id && e.Record_Status == RecordStatus.UnVerified).FirstOrDefaultAsync();

                if (entity != null)
                {
                    entity.Record_Status = RecordStatus.Active;

                    entity.ChangeLog = ChangeLogHelper.DiscardChangeLog(entity.ChangeLog);

                    entity.AuthorisedBy = _authenticationHelper.GetFullname();
                    entity.AuthorisedByStaffNo = _authenticationHelper.GetStaffNo();
                    entity.AuthorisedById = _authenticationHelper.GetUserId();
                    entity.AuthorisedDate = DateTime.Now;


                    dbset.Attach(entity);
                    dataContext.Entry(entity).State = EntityState.Modified;
                }
                else
                {
                    throw new Exception("No record to revert.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public virtual async Task Authorise(Guid Id)
        {
            try
            {
                T entity = await this.dbset.Where(e => e.Id == Id && e.Record_Status == RecordStatus.UnVerified).FirstOrDefaultAsync();

                if (entity != null)
                {
                    entity.AuthorisedBy = _authenticationHelper.GetFullname();
                    entity.AuthorisedByStaffNo = _authenticationHelper.GetStaffNo();
                    entity.AuthorisedById = _authenticationHelper.GetUserId();
                    entity.AuthorisedDate = DateTime.Now;

                    if (entity.EntityState == DataEntityState.Deleted)
                    {
                        entity.EntityState = DataEntityState.Deleted;
                        entity.Record_Status = RecordStatus.Inactive;
                    }
                    else if (entity.EntityState == DataEntityState.Closed)
                    {
                        entity.EntityState = DataEntityState.Closed;
                        entity.Record_Status = RecordStatus.Inactive;
                    }
                    else if (entity.EntityState == DataEntityState.Modified)
                    {
                        //var result = ChangeLogHelper.GetTByChangeApplied<T>(entity, entity.ChangeLog);
                        //entity = result.Item1;
                        //entity.ChangeLog = result.Item2;
                        entity.Record_Status = RecordStatus.Active;
                        entity.TotalModification = entity.TotalModification + 1;
                    }
                    else
                    {
                        entity.Record_Status = RecordStatus.Active;
                    }

                    dbset.Attach(entity);
                    dataContext.Entry(entity).State = EntityState.Modified;
                }
                else
                {
                    throw new Exception("No record to authorise.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public virtual async Task Revert(Guid Id)
        {

            try
            {
                T entity = await this.dbset.Where(e => e.Id == Id && e.Record_Status == RecordStatus.UnVerified).FirstOrDefaultAsync();

                if (entity != null)
                {
                    entity.Record_Status = RecordStatus.Reverted;

                    if (entity.ChangeLog != null)
                    {
                        entity.ChangeLog = ChangeLogHelper.RevertChangeLog(entity.ChangeLog);
                    }

                    entity.AuthorisedBy = _authenticationHelper.GetFullname();
                    entity.AuthorisedByStaffNo = _authenticationHelper.GetStaffNo();
                    entity.AuthorisedById = _authenticationHelper.GetUserId();
                    entity.AuthorisedDate = DateTime.Now;


                    dbset.Attach(entity);
                    dataContext.Entry(entity).State = EntityState.Modified;
                }
                else
                {
                    throw new Exception("No record to revert.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public virtual async Task Delete(Guid Id, bool Autoauthorise)
        {
            try
            {
                T entity = await this.dbset.Where(e => (e.Id == Id && e.ChangeLog == null) || (e.Id == Id && e.Record_Status == RecordStatus.Active)).FirstOrDefaultAsync();

                if (entity != null)
                {
                    entity.EntityState = DataEntityState.Deleted;

                    entity.ModifiedBy = _authenticationHelper.GetFullname();
                    entity.ModifiedDate = DateTime.Now;
                    entity.ModifiedById = _authenticationHelper.GetUserId();
                    entity.ModifiedByStaffNo = _authenticationHelper.GetStaffNo();


                    if (Autoauthorise)
                    {
                        entity.AuthorisedBy = _authenticationHelper.GetFullname();
                        entity.AuthorisedById = _authenticationHelper.GetUserId();
                        entity.AuthorisedByStaffNo = _authenticationHelper.GetStaffNo();
                        entity.AuthorisedDate = DateTime.Now;

                        entity.Record_Status = RecordStatus.Inactive;
                    }
                    else
                    {
                        entity.AuthorisedBy = null;
                        entity.AuthorisedById = null;
                        entity.AuthorisedByStaffNo = null;
                        entity.AuthorisedDate = null;

                        entity.Record_Status = RecordStatus.UnVerified;
                    }

                    dbset.Attach(entity);
                    dataContext.Entry(entity).State = EntityState.Modified;
                }
                else
                {
                    throw new Exception("No record to delete.");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public virtual async Task Close(Guid Id, bool Autoauthorise)
        {
            try
            {
                T entity = await this.dbset.Where(e => (e.Id == Id && e.ChangeLog == null) || (e.Id == Id && e.Record_Status == RecordStatus.Active)).FirstOrDefaultAsync();

                if (entity != null)
                {

                    entity.EntityState = DataEntityState.Closed;

                    entity.ModifiedBy = _authenticationHelper.GetFullname();
                    entity.ModifiedDate = DateTime.Now;
                    entity.ModifiedById = _authenticationHelper.GetUserId();
                    entity.ModifiedByStaffNo = _authenticationHelper.GetStaffNo();


                    if (Autoauthorise)
                    {
                        entity.AuthorisedBy = _authenticationHelper.GetFullname();
                        entity.AuthorisedById = _authenticationHelper.GetUserId();
                        entity.AuthorisedByStaffNo = _authenticationHelper.GetStaffNo();
                        entity.AuthorisedDate = DateTime.Now;

                        entity.Record_Status = RecordStatus.Inactive;
                    }
                    else
                    {
                        entity.AuthorisedBy = null;
                        entity.AuthorisedById = null;
                        entity.AuthorisedByStaffNo = null;
                        entity.AuthorisedDate = null;

                        entity.Record_Status = RecordStatus.UnVerified;
                    }

                    dbset.Attach(entity);
                    dataContext.Entry(entity).State = EntityState.Modified;
                }
                else
                {
                    throw new Exception("No record to delete.");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<D> GetDTOById(Guid Id)
        {
            T entity = await dbset.FindAsync(Id);
            if (entity != null)
                return MapperHelper.Get<D, T>(Activator.CreateInstance<D>(), entity);
            else
                return null;
        }
        public virtual async Task<D> GetDTO(Expression<Func<T, bool>> where)
        {
            T entity = await dbset.Where(where).FirstOrDefaultAsync();
            if (entity != null)
                return MapperHelper.Get<D, T>(Activator.CreateInstance<D>(), entity);
            else
                return null;
        }
        public async Task<T> GetEntityById(Guid Id)
        {
            return await dbset.FindAsync(Id);
        }
        public virtual async Task<IEnumerable<D>> GetManyDTO(Expression<Func<T, bool>> where)
        {
            List<T> entities = await dbset.Where(where).ToListAsync();
            List<D> dtos = new List<D>();

            entities.ForEach(entity =>
                {
                    D dto = MapperHelper.Get<D, T>(Activator.CreateInstance<D>(), entity);
                    dtos.Add(dto);

                });

            return dtos;
        }
        //public async Task<PagedResultDataTable> GetAllByProcedure(string Schema, string EntityName, params SqlParameter[] parameters)
        //{
        //    try
        //    {
        //        var pageList = new PagedResult<DataTable>();

        //        int PageSize = int.Parse(parameters.Where(x => x.ParameterName == "PageSize").Select(x => x.Value).FirstOrDefault().ToString());
        //        int PageNumber = int.Parse(parameters.Where(x => x.ParameterName == "PageNumber").Select(x => x.Value).FirstOrDefault().ToString());

        //        string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        //        using (SqlConnection _connection = new SqlConnection(_connectionString))
        //        {
        //            await _connection.OpenAsync();

        //            using (SqlCommand _commnd = new SqlCommand(string.Format("[{0}].[OnlineAccountOpening_SP_Get{1}List]", Schema, EntityName), _connection))
        //            {
        //                _commnd.CommandType = CommandType.StoredProcedure;

        //                _commnd.Parameters.AddRange(parameters);


        //                using (SqlDataReader _reader = await _commnd.ExecuteReaderAsync())
        //                {
        //                    DataTable _records = new DataTable();
        //                    _records.Load(_reader);

        //                    if (_records.Rows.Count > 0)
        //                    {

        //                        var _result = _records.AsEnumerable().GroupBy(x => new { PageCount = x["PageCount"], TotalRecords = x["TotalRecords"] }).Select(z => new PagedResultDataTable()
        //                        {
        //                            PageCount = int.Parse(z.Key.PageCount.ToString()),
        //                            RowCount = int.Parse(z.Key.TotalRecords.ToString()),
        //                            PageSize = PageSize,
        //                            PageNumber = PageNumber,
        //                            Results = z.CopyToDataTable()

        //                        }).FirstOrDefault();

        //                        return _result;
        //                    }
        //                    else
        //                    {
        //                        return new PagedResultDataTable()
        //                        {
        //                            PageCount = 0,
        //                            RowCount = 0,
        //                            PageNumber = 1,
        //                            PageSize = PageSize,
        //                            Results = _records

        //                        };
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        public async Task<PagedResultDataTable> GetAllByProcedure(string StoreProcedure, params SqlParameter[] parameters)
        {
            try
            {
                var pageList = new PagedResult<DataTable>();

                int PageSize = int.Parse(parameters.Where(x => x.ParameterName == "PageSize").Select(x => x.Value).FirstOrDefault().ToString());
                int PageNumber = int.Parse(parameters.Where(x => x.ParameterName == "PageNumber").Select(x => x.Value).FirstOrDefault().ToString());

                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();

                    using (SqlCommand _commnd = new SqlCommand(StoreProcedure, _connection))
                    {
                        _commnd.CommandType = CommandType.StoredProcedure;

                        _commnd.Parameters.AddRange(parameters);


                        using (SqlDataReader _reader = await _commnd.ExecuteReaderAsync())
                        {
                            DataTable _records = new DataTable();
                            _records.Load(_reader);

                            if (_records.Rows.Count > 0)
                            {

                                var _result = _records.AsEnumerable().GroupBy(x => new { PageCount = x["PageCount"], TotalRecords = x["TotalRecords"] }).Select(z => new PagedResultDataTable()
                                {
                                    PageCount = int.Parse(z.Key.PageCount.ToString()),
                                    RowCount = int.Parse(z.Key.TotalRecords.ToString()),
                                    PageSize = PageSize,
                                    PageNumber = PageNumber,
                                    Results = z.CopyToDataTable()

                                }).FirstOrDefault();

                                return _result;
                            }
                            else
                            {
                                return new PagedResultDataTable()
                                {
                                    PageCount = 0,
                                    RowCount = 0,
                                    PageNumber = 1,
                                    PageSize = PageSize,
                                    Results = _records

                                };
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public async Task<ModuleSummary> GetModuleBussinesLogicSetup(Guid? Id, Guid? ParentPrimaryRecordId, bool IsSummaryRequest, bool DropdownRequired, List<AdditionalDropdownParameter> DropdownAdditionalParameters = null)
        //{
        //    try
        //    {
        //        Dictionary<string, object> RecordToDictionary = new Dictionary<string, object>();
        //        Guid? CurrentApplicationUserId = this._authenticationHelper.GetUserId();

        //        var ModuleSetup = ModuleHelper.GetModuleSetup<T>();

        //        ModuleSummary moduleSummary = new ModuleSummary()
        //        {
        //            ModuleSummaryName = ModuleSetup.ApplicationClass,
        //            ModuleSummaryTitle = ModuleSetup.Name,
        //            EntryType = ModuleSetup.EntryType,
        //            PrimaryRecordId = Id,
        //            IsParent = ModuleSetup.IsParent,
        //            ParentModule = ModuleSetup.ParentModule,
        //            ParentPrimaryRecordId = ParentPrimaryRecordId
        //        };

        //        if (Id != null || (ModuleSetup.EntryType == "S" && ParentPrimaryRecordId != null))
        //        {
        //            string ParentColumn = ModuleSetup.ModuleBussinesLogicSetups.Where(x => x.IsParentColumn).Select(x => x.ColumnName).FirstOrDefault();

        //            Expression<Func<T, bool>> linqCondition = null;

        //            if (!string.IsNullOrEmpty(ParentColumn) && ParentPrimaryRecordId != null)
        //            {
        //                linqCondition = DynamicLinqBuilder.CreateExpression<T, bool>(string.Format("{0} == @0", ParentColumn), ParentPrimaryRecordId);

        //            }

        //            var Record = Id != null ? await this.dbset.Where(x => x.Id == Id.Value).FirstOrDefaultAsync() :
        //                ParentPrimaryRecordId != null && !moduleSummary.IsParent ? await this.dbset.Where(linqCondition).FirstOrDefaultAsync() : null;
        //            if (Record != null)
        //            {
        //                moduleSummary.TotalModification = Record.TotalModification;
        //                moduleSummary.CreatedBy = Record.CreatedBy;
        //                moduleSummary.ModifiedBy = Record.ModifiedBy;
        //                moduleSummary.AuthorisedBy = Record.AuthorisedBy;
        //                moduleSummary.CreatedById = Record.CreatedById;
        //                moduleSummary.ModifiedById = Record.ModifiedById;
        //                moduleSummary.AuthorisedById = Record.AuthorisedById;
        //                moduleSummary.CreatedDate = Record.CreatedDate;
        //                moduleSummary.ModifiedDate = Record.ModifiedDate;
        //                moduleSummary.AuthorisedDate = Record.AuthorisedDate;
        //                moduleSummary.EntityState = Record.EntityState;
        //                moduleSummary.RecordStatus = Record.Record_Status;
        //                moduleSummary.DoRecordExists = Record != null ? true : false;


        //                moduleSummary.RecordChangeLog = Record.EntityState == DataEntityState.Modified && Record.Record_Status == RecordStatus.UnVerified ? ChangeLogHelper.GetLatestRecordChangeLogs(Record.ChangeLog) : null;

        //                RecordToDictionary = Record.GetType()
        //.GetProperties(BindingFlags.Instance | BindingFlags.Public)
        //    .ToDictionary(prop => prop.Name, prop => prop.GetValue(Record, null));
        //            }

        //        }

        //        ModuleSetup.ChildTableInformations.ForEach(c =>
        //        {
        //            moduleSummary.ChildInformations.Add(new ChildModuleSummary()
        //            {
        //                ChildModuleSummaryName = c.Name,
        //                ChildModuleSummaryTitle = c.Title,
        //                Url = c.Url,
        //                OrderValue = c.OrderValue

        //            });


        //        });


        //        foreach (var ModuleBussinesLogic in ModuleSetup.ModuleBussinesLogicSetups.Where(x => IsSummaryRequest ? x.SummaryHeader || x.ParameterForSummaryHeader || (ParentPrimaryRecordId != null && x.IsParentColumn) : true).ToList())
        //        {
        //            try
        //            {
        //                Dictionary<string, object> Attributes = new Dictionary<string, object>();

        //                var moduleBussinesLogicSummary = new ModuleBussinesLogicSummary()
        //                {
        //                    Name = ModuleBussinesLogic.Name,
        //                    ColumnName = ModuleBussinesLogic.ColumnName,
        //                    Description = ModuleBussinesLogic.Description,
        //                    DataType = ModuleBussinesLogic.DataType,
        //                    Required = ModuleBussinesLogic.Required,
        //                    HtmlDataType = ModuleBussinesLogic.HtmlDataType,
        //                    HtmlSize = ModuleBussinesLogic.HtmlSize,
        //                    Position = ModuleBussinesLogic.Position,
        //                    DefaultValue = ModuleBussinesLogic.DefaultValue,
        //                    CanUpdate = ModuleBussinesLogic.CanUpdate,
        //                    HelpMessage = ModuleBussinesLogic.HelpMessage,
        //                    IsParentColumn = ModuleBussinesLogic.IsParentColumn || !string.IsNullOrEmpty(moduleSummary.ParentModule) && string.Format("{0}Id", moduleSummary.ParentModule) == ModuleBussinesLogic.ColumnName,
        //                    SummaryHeader = ModuleBussinesLogic.SummaryHeader,
        //                    ParameterForSummaryHeader = ModuleBussinesLogic.ParameterForSummaryHeader,
        //                    IsForeignKey = ModuleBussinesLogic.IsForeignKey,
        //                    ForeignTable = ModuleBussinesLogic.ForeignTable,
        //                    DataSource = ModuleBussinesLogic.DataSource,
        //                    ParameterisedDataSorce = ModuleBussinesLogic.ParameterisedDataSorce,
        //                    Parameters = ModuleBussinesLogic.Parameters,
        //                    CurrentValue = RecordToDictionary.ContainsKey(ModuleBussinesLogic.ColumnName) ? RecordToDictionary[ModuleBussinesLogic.ColumnName] : ModuleBussinesLogic.IsParentColumn || !string.IsNullOrEmpty(moduleSummary.ParentModule) && string.Format("{0}Id", moduleSummary.ParentModule) == ModuleBussinesLogic.ColumnName ? ParentPrimaryRecordId : null,
        //                    SelectList = DropdownRequired && (ModuleBussinesLogic.HtmlDataType.ToLower() == "select" || ModuleBussinesLogic.HtmlDataType.ToLower() == "datalist") ? DropdownHelper.GetDropdownInformation
        //                    (ModuleBussinesLogic.ColumnName,
        //                    Id == null ? ModuleBussinesLogic.DefaultValue : RecordToDictionary.ContainsKey(ModuleBussinesLogic.ColumnName) ? RecordToDictionary[ModuleBussinesLogic.ColumnName] : null,
        //                    ModuleBussinesLogic.DataSource,
        //                    ModuleBussinesLogic.IsStaticDropDown,
        //                    ModuleBussinesLogic.ParameterisedDataSorce,
        //                    ModuleBussinesLogic.Parameters,
        //                    RecordToDictionary,
        //                    CurrentApplicationUserId,
        //                    DropdownAdditionalParameters) : null

        //                };

        //                if (!IsSummaryRequest)
        //                    ModuleBussinesLogic.ModuleValidationAttributeSetups.Distinct().ToList().HtmlValidationAttributes(ref Attributes);

        //                moduleBussinesLogicSummary.Attributes = Attributes;

        //                ModuleBussinesLogic.ModuleHtmlAttributeSetups.ToList().ForEach(f =>
        //                {
        //                    if (moduleBussinesLogicSummary.Attributes.Where(a => a.Key == f.AttributeType).Count() == 0)
        //                    {
        //                        moduleBussinesLogicSummary.Attributes.Add(f.AttributeType, f.Value);
        //                    }

        //                });

        //                moduleSummary.moduleBussinesLogicSummaries.Add(moduleBussinesLogicSummary);
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }
        //        }

        //        return moduleSummary;


        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}
        public async Task<ModuleSummary> GetModuleBussinesLogicSetup()
        {
            try
            {
                Dictionary<string, object> RecordToDictionary = new Dictionary<string, object>();
                Guid? CurrentApplicationUserId = this._authenticationHelper.GetUserId();

                var ModuleSetup = ModuleHelper.GetModuleSetup<T>();

                ModuleSummary moduleSummary = new ModuleSummary()
                {
                    ModuleSummaryName = ModuleSetup.ApplicationClass,
                    ModuleSummaryTitle = ModuleSetup.Name,
                    EntryType = ModuleSetup.EntryType,
                    PrimaryRecordId = null,
                    IsParent = ModuleSetup.IsParent,
                    ParentModule = ModuleSetup.ParentModule,
                    ParentPrimaryRecordId = null
                };

                ModuleSetup.ChildTableInformations.ForEach(c =>
                {
                    moduleSummary.ChildInformations.Add(new ChildModuleSummary()
                    {
                        ChildModuleSummaryName = c.Name,
                        ChildModuleSummaryTitle = c.Title,
                        Url = c.Url,
                        OrderValue = c.OrderValue

                    });


                });


                foreach (var ModuleBussinesLogic in ModuleSetup.ModuleBussinesLogicSetups.ToList())
                {
                    try
                    {
                        Dictionary<string, object> Attributes = new Dictionary<string, object>();

                        var moduleBussinesLogicSummary = new ModuleBussinesLogicSummary()
                        {
                            Name = ModuleBussinesLogic.Name,
                            ColumnName = ModuleBussinesLogic.ColumnName,
                            Description = ModuleBussinesLogic.Description,
                            DataType = ModuleBussinesLogic.DataType,
                            Required = ModuleBussinesLogic.Required,
                            HtmlDataType = ModuleBussinesLogic.HtmlDataType,
                            HtmlSize = ModuleBussinesLogic.HtmlSize,
                            Position = ModuleBussinesLogic.Position,
                            DefaultValue = ModuleBussinesLogic.DefaultValue,
                            CanUpdate = ModuleBussinesLogic.CanUpdate,
                            HelpMessage = ModuleBussinesLogic.HelpMessage,
                            IsParentColumn = ModuleBussinesLogic.IsParentColumn || !string.IsNullOrEmpty(moduleSummary.ParentModule) && string.Format("{0}Id", moduleSummary.ParentModule) == ModuleBussinesLogic.ColumnName,
                            SummaryHeader = ModuleBussinesLogic.SummaryHeader,
                            ParameterForSummaryHeader = ModuleBussinesLogic.ParameterForSummaryHeader,
                            IsForeignKey = ModuleBussinesLogic.IsForeignKey,
                            ForeignTable = ModuleBussinesLogic.ForeignTable,
                            DataSource = ModuleBussinesLogic.DataSource,
                            ParameterisedDataSorce = ModuleBussinesLogic.ParameterisedDataSorce,
                            Parameters = ModuleBussinesLogic.Parameters,
                            CurrentValue = RecordToDictionary.ContainsKey(ModuleBussinesLogic.ColumnName) ? RecordToDictionary[ModuleBussinesLogic.ColumnName] : null,
                            SelectList = ModuleBussinesLogic.HtmlDataType.ToLower() == "select" ? DropdownHelper.GetDropdownInformation
                            (ModuleBussinesLogic.ColumnName, ModuleBussinesLogic.DefaultValue, ModuleBussinesLogic.DataSource,
                            ModuleBussinesLogic.IsStaticDropDown) : null
                        };

                        moduleBussinesLogicSummary.Attributes = Attributes;

                        ModuleBussinesLogic.ModuleHtmlAttributeSetups.ToList().ForEach(f =>
                        {
                            if (moduleBussinesLogicSummary.Attributes.Where(a => a.Key == f.AttributeType).Count() == 0)
                            {
                                moduleBussinesLogicSummary.Attributes.Add(f.AttributeType, f.Value);
                            }

                        });

                        moduleSummary.moduleBussinesLogicSummaries.Add(moduleBussinesLogicSummary);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                return moduleSummary;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task<ModuleSummary> GetModuleBussinesLogicSetup(D dto, Guid? ParentPrimaryRecordId)
        {
            try
            {
                Dictionary<string, object> RecordToDictionary = new Dictionary<string, object>();
                Guid? CurrentApplicationUserId = this._authenticationHelper.GetUserId();

                var ModuleSetup = ModuleHelper.GetModuleSetup<T>();

                ModuleSummary moduleSummary = new ModuleSummary()
                {
                    ModuleSummaryName = ModuleSetup.ApplicationClass,
                    ModuleSummaryTitle = ModuleSetup.Name,
                    EntryType = ModuleSetup.EntryType,
                    PrimaryRecordId = dto.Id,
                    IsParent = ModuleSetup.IsParent,
                    ParentModule = ModuleSetup.ParentModule,
                    ParentPrimaryRecordId = ParentPrimaryRecordId
                };

                if (dto != null)
                {
                    moduleSummary.TotalModification = dto.TotalModification;
                    moduleSummary.CreatedBy = dto.CreatedBy;
                    moduleSummary.ModifiedBy = dto.ModifiedBy;
                    moduleSummary.AuthorisedBy = dto.AuthorisedBy;
                    moduleSummary.CreatedById = dto.CreatedById;
                    moduleSummary.ModifiedById = dto.ModifiedById;
                    moduleSummary.AuthorisedById = dto.AuthorisedById;
                    moduleSummary.CreatedDate = dto.CreatedDate;
                    moduleSummary.ModifiedDate = dto.ModifiedDate;
                    moduleSummary.AuthorisedDate = dto.AuthorisedDate;
                    moduleSummary.EntityState = dto.EntityState;
                    moduleSummary.RecordStatus = dto.Record_Status;
                    moduleSummary.DoRecordExists = true;


                    //moduleSummary.RecordChangeLog = Record.EntityState == DataEntityState.Modified && Record.RecordStatus == RecordStatus.UnVerified ? ChangeLogHelper.GetLatestRecordChangeLogs(Record.ChangeLog) : null;

                    RecordToDictionary = dto.GetType()
    .GetProperties(BindingFlags.Instance | BindingFlags.Public)
        .ToDictionary(prop => prop.Name, prop => prop.GetValue(dto, null));
                }

                ModuleSetup.ChildTableInformations.ForEach(c =>
                {
                    moduleSummary.ChildInformations.Add(new ChildModuleSummary()
                    {
                        ChildModuleSummaryName = c.Name,
                        ChildModuleSummaryTitle = c.Title,
                        Url = c.Url,
                        OrderValue = c.OrderValue

                    });


                });


                foreach (var ModuleBussinesLogic in ModuleSetup.ModuleBussinesLogicSetups.ToList())
                {
                    try
                    {
                        Dictionary<string, object> Attributes = new Dictionary<string, object>();

                        var moduleBussinesLogicSummary = new ModuleBussinesLogicSummary()
                        {
                            Name = ModuleBussinesLogic.Name,
                            ColumnName = ModuleBussinesLogic.ColumnName,
                            Description = ModuleBussinesLogic.Description,
                            DataType = ModuleBussinesLogic.DataType,
                            Required = ModuleBussinesLogic.Required,
                            HtmlDataType = ModuleBussinesLogic.HtmlDataType,
                            HtmlSize = ModuleBussinesLogic.HtmlSize,
                            Position = ModuleBussinesLogic.Position,
                            DefaultValue = ModuleBussinesLogic.DefaultValue,
                            CanUpdate = ModuleBussinesLogic.CanUpdate,
                            HelpMessage = ModuleBussinesLogic.HelpMessage,
                            IsParentColumn = ModuleBussinesLogic.IsParentColumn || !string.IsNullOrEmpty(moduleSummary.ParentModule) && string.Format("{0}Id", moduleSummary.ParentModule) == ModuleBussinesLogic.ColumnName,
                            SummaryHeader = ModuleBussinesLogic.SummaryHeader,
                            ParameterForSummaryHeader = ModuleBussinesLogic.ParameterForSummaryHeader,
                            IsForeignKey = ModuleBussinesLogic.IsForeignKey,
                            ForeignTable = ModuleBussinesLogic.ForeignTable,
                            DataSource = ModuleBussinesLogic.DataSource,
                            ParameterisedDataSorce = ModuleBussinesLogic.ParameterisedDataSorce,
                            Parameters = ModuleBussinesLogic.Parameters,
                            CurrentValue = RecordToDictionary.ContainsKey(ModuleBussinesLogic.ColumnName) ? RecordToDictionary[ModuleBussinesLogic.ColumnName] : null,
                            SelectList = ModuleBussinesLogic.HtmlDataType.ToLower() == "select" ? DropdownHelper.GetDropdownInformation
                            (ModuleBussinesLogic.ColumnName, RecordToDictionary.ContainsKey(ModuleBussinesLogic.ColumnName) ? RecordToDictionary[ModuleBussinesLogic.ColumnName] : ModuleBussinesLogic.DefaultValue, ModuleBussinesLogic.DataSource,
                            ModuleBussinesLogic.IsStaticDropDown) : null

                        };

                        moduleBussinesLogicSummary.Attributes = Attributes;

                        ModuleBussinesLogic.ModuleHtmlAttributeSetups.ToList().ForEach(f =>
                        {
                            if (moduleBussinesLogicSummary.Attributes.Where(a => a.Key == f.AttributeType).Count() == 0)
                            {
                                moduleBussinesLogicSummary.Attributes.Add(f.AttributeType, f.Value);
                            }

                        });

                        moduleSummary.moduleBussinesLogicSummaries.Add(moduleBussinesLogicSummary);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                return moduleSummary;


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
