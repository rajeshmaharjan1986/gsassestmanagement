﻿using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Infrastructure
{
    public static class IEnumerableExtensions
    {
        public static DataTable AsDataTable<T>(this IEnumerable<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public static T WithChangeLog<T>(this List<ChangeLogDTO> _changeLogs, T obj, bool AutoAuthorised)
        {
            try
            {
                Type temp = typeof(T);

                foreach (PropertyInfo pro in temp.GetProperties())
                {


                    var _changeLog = _changeLogs.Where(x => x.PropertyName == pro.Name).Select(m => m.ModifiedValue).FirstOrDefault();
                    if (_changeLog != null)
                    {

                        var t = Nullable.GetUnderlyingType(pro.PropertyType) ?? pro.PropertyType;

                        if (!string.IsNullOrEmpty(_changeLog.ToString()))
                        {
                            switch (t.Name.ToLower())
                            {
                                case "byte":
                                    pro.SetValue(obj, byte.Parse(_changeLog.ToString()), null);
                                    break;
                                case "sbyte":
                                    pro.SetValue(obj, sbyte.Parse(_changeLog.ToString()), null);
                                    break;
                                case "int32":
                                    pro.SetValue(obj, Int32.Parse(_changeLog.ToString()), null);
                                    break;
                                case "uint32":
                                    pro.SetValue(obj, UInt32.Parse(_changeLog.ToString()), null);
                                    break;
                                case "int16":
                                    pro.SetValue(obj, Int16.Parse(_changeLog.ToString()), null);
                                    break;
                                case "uint16":
                                    pro.SetValue(obj, UInt16.Parse(_changeLog.ToString()), null);
                                    break;
                                case "int64":
                                    pro.SetValue(obj, Int64.Parse(_changeLog.ToString()), null);
                                    break;
                                case "uint64":
                                    pro.SetValue(obj, UInt64.Parse(_changeLog.ToString()), null);
                                    break;
                                case "single":
                                    pro.SetValue(obj, Single.Parse(_changeLog.ToString()), null);
                                    break;
                                case "double":
                                    pro.SetValue(obj, double.Parse(_changeLog.ToString()), null);
                                    break;
                                case "char":
                                    pro.SetValue(obj, char.Parse(_changeLog.ToString()), null);
                                    break;
                                case "boolean":
                                    pro.SetValue(obj, Boolean.Parse(_changeLog.ToString()), null);
                                    break;
                                case "string":
                                    pro.SetValue(obj, _changeLog, null);
                                    break;
                                case "decimal":
                                    pro.SetValue(obj, decimal.Parse(_changeLog.ToString()), null);
                                    break;
                                case "datetime":
                                    pro.SetValue(obj, DateTime.Parse(_changeLog.ToString()), null);
                                    break;
                                case "guid":
                                    pro.SetValue(obj, Guid.Parse(_changeLog.ToString()), null);
                                    break;
                                    //default:
                                    //    pro.SetValue(obj, Enum.Parse column.FormFieldValue.ToString(), null);
                                    //    break;

                            }

                            if (t.BaseType.Name == "Enum")
                            {

                                object safeValue = (_changeLog == null || DBNull.Value.Equals(_changeLog)) ? null : Enum.ToObject(t, _changeLog);// 
                                pro.SetValue(obj, safeValue, null);

                            }
                        }


                    }


                }

                return obj;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static T WithChangeLogAndDataRow<T>(this List<ChangeLogDTO> _changeLogs, T obj, DataRow Row, bool AutoAuthorised)
        {
            try
            {
                Type temp = typeof(T);

                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    try
                    {
                        var _changeLog = _changeLogs.Where(x => x.PropertyName == pro.Name).Select(m => m.ModifiedValue).FirstOrDefault();

                        _changeLog = Row.Table.Columns.Contains(pro.Name) ? Row[pro.Name] : null;// _changeLog == null &&  : _changeLog;

                        if (pro.Name == "RowVersion")
                        {
                            pro.SetValue(obj, Convert.FromBase64String(Row[pro.Name].ToString()), null);

                        }

                        if (_changeLog != null)
                        {

                            var t = Nullable.GetUnderlyingType(pro.PropertyType) ?? pro.PropertyType;

                            if (!string.IsNullOrEmpty(_changeLog.ToString()))
                            {
                                switch (t.Name.ToLower())
                                {
                                    case "byte":
                                        pro.SetValue(obj, byte.Parse(_changeLog.ToString()), null);
                                        break;
                                    case "sbyte":
                                        pro.SetValue(obj, sbyte.Parse(_changeLog.ToString()), null);
                                        break;
                                    case "int32":
                                        pro.SetValue(obj, Int32.Parse(_changeLog.ToString()), null);
                                        break;
                                    case "uint32":
                                        pro.SetValue(obj, UInt32.Parse(_changeLog.ToString()), null);
                                        break;
                                    case "int16":
                                        pro.SetValue(obj, Int16.Parse(_changeLog.ToString()), null);
                                        break;
                                    case "uint16":
                                        pro.SetValue(obj, UInt16.Parse(_changeLog.ToString()), null);
                                        break;
                                    case "int64":
                                        pro.SetValue(obj, Int64.Parse(_changeLog.ToString()), null);
                                        break;
                                    case "uint64":
                                        pro.SetValue(obj, UInt64.Parse(_changeLog.ToString()), null);
                                        break;
                                    case "single":
                                        pro.SetValue(obj, Single.Parse(_changeLog.ToString()), null);
                                        break;
                                    case "double":
                                        pro.SetValue(obj, double.Parse(_changeLog.ToString()), null);
                                        break;
                                    case "char":
                                        pro.SetValue(obj, char.Parse(_changeLog.ToString()), null);
                                        break;
                                    case "boolean":
                                        if (_changeLog.ToString() == "0")
                                            pro.SetValue(obj, false, null);
                                        else if (_changeLog.ToString() == "1")
                                            pro.SetValue(obj, true, null);
                                        else
                                            pro.SetValue(obj, Boolean.Parse(_changeLog.ToString()), null);
                                        break;
                                    case "string":
                                        pro.SetValue(obj, _changeLog, null);
                                        break;
                                    case "decimal":
                                        pro.SetValue(obj, decimal.Parse(_changeLog.ToString()), null);
                                        break;
                                    case "datetime":
                                        pro.SetValue(obj, DateTime.Parse(_changeLog.ToString()), null);
                                        break;
                                    case "guid":
                                        pro.SetValue(obj, Guid.Parse(_changeLog.ToString()), null);
                                        break;
                                        //default:
                                        //    pro.SetValue(obj, Enum.Parse column.FormFieldValue.ToString(), null);
                                        //    break;

                                }

                                if (t.BaseType.Name == "Enum")
                                {

                                    int value;

                                    if (int.TryParse(_changeLog.ToString(), out value))
                                    {
                                        object safeValue = (_changeLog == null || DBNull.Value.Equals(_changeLog)) ? null : Enum.ToObject(t, value);// 
                                        pro.SetValue(obj, safeValue, null);
                                    }
                                    else
                                    {
                                        object safeValue = (_changeLog == null || DBNull.Value.Equals(_changeLog)) ? null : Enum.ToObject(t, _changeLog);// 
                                        pro.SetValue(obj, safeValue, null);
                                    }



                                }
                            }


                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }


                }

                return obj;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static Dictionary<Type, IList<PropertyInfo>> typeDictionary = new Dictionary<Type, IList<PropertyInfo>>();
        public static IList<PropertyInfo> GetPropertiesForType<T>()
        {
            var type = typeof(T);
            if (!typeDictionary.ContainsKey(typeof(T)))
            {
                var types = type.GetProperties().ToList();
                if (types != null)
                {
                    typeDictionary.Add(type, types);
                }
            }
            return typeDictionary[type];
        }

        public static IList<T> ToList<T>(this DataTable table) where T : new()
        {
            try
            {
                T tempT = new T();
                var tType = tempT.GetType();
                List<T> list = new List<T>();
                foreach (var row in table.Rows.Cast<DataRow>())
                {
                    try
                    {
                        T obj = new T();
                        foreach (var prop in obj.GetType().GetProperties())
                        {
                            var propertyInfo = tType.GetProperty(prop.Name);
                            if (table.Columns.Contains(prop.Name))
                            {
                                var rowValue = row[prop.Name];
                                var t = Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType;

                                try
                                {
                                    if (t.BaseType != null)
                                    {
                                        if (t.BaseType.Name == "Enum")
                                        {

                                            object safeValue = (rowValue == null || DBNull.Value.Equals(rowValue)) ? null : Enum.ToObject(t, rowValue);// 
                                            propertyInfo.SetValue(obj, safeValue, null);

                                        }
                                        else
                                        {
                                            switch (t.Name)
                                            {
                                                case "Guid":
                                                    object _guid = Guid.Parse(rowValue.ToString());
                                                    propertyInfo.SetValue(obj, _guid, null);
                                                    break;
                                                case "DateTime":
                                                    object _dateTime = DateTime.Parse(rowValue.ToString());
                                                    propertyInfo.SetValue(obj, _dateTime, null);
                                                    break;
                                                case "Boolean":
                                                    object Boolean = bool.Parse(rowValue.ToString());
                                                    propertyInfo.SetValue(obj, Boolean, null);
                                                    break;
                                                case "Int32":
                                                    object intValue = int.Parse(rowValue.ToString());
                                                    propertyInfo.SetValue(obj, intValue, null);
                                                    break;
                                                default:
                                                    object safeValue = (rowValue == null || DBNull.Value.Equals(rowValue) || rowValue.ToString().ToLower() == "null") ? null : Convert.ChangeType(rowValue, t);
                                                    propertyInfo.SetValue(obj, safeValue, null);
                                                    break;

                                            }

                                            //if (t.Name == "Guid")
                                            //{



                                            //}
                                            //else
                                            //{


                                            //    object safeValue = (rowValue == null || DBNull.Value.Equals(rowValue) || rowValue.ToString().ToLower() == "null") ? null : Convert.ChangeType(rowValue, t);
                                            //    propertyInfo.SetValue(obj, rowValue, null);

                                            //}
                                        }
                                    }
                                    else
                                    {
                                        object safeValue = (rowValue == null || DBNull.Value.Equals(rowValue) || rowValue.ToString().ToLower() == "null") ? null : Convert.ChangeType(rowValue, t);
                                        propertyInfo.SetValue(obj, safeValue, null);
                                    }


                                }
                                catch (Exception ex)
                                {//this write exception to my logger
                                    continue;
                                }
                            }

                        }
                        list.Add(obj);
                    }
                    catch
                    {
                        continue;

                    }

                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static T CreateItemFromRow<T>(DataRow row, IList<PropertyInfo> properties) where T : new()
        {
            T item = new T();
            foreach (var property in properties)
            {
                if (row[property.Name] == null || row[property.Name] is DBNull)
                {
                    property.SetValue(item, null, null);
                }
                else
                {

                    property.SetValue(item, row[property.Name], null);


                }
            }
            return item;
        }

        private static bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
        }

        public static Dictionary<string, object> ToDictionary(this DataTable dt)
        {
            return dt.AsEnumerable().Select(
        row => dt.Columns.Cast<DataColumn>().ToDictionary(
            column => column.ColumnName,
            column => row[column]
        )).FirstOrDefault();
        }
    }
}
