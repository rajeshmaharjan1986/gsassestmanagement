﻿using System.Threading.Tasks;


namespace GSAssetManagement.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();

        Task<bool> CommitAsync();

    }
}
