﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using GSAssetManagement.Entity.APIRequestResponse;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure.Core;
using GSAssetManagement.Service;

namespace GSAssetManagement.Infrastructure
{
    public static class DropdownHelper
    {
        public static List<SelectListItem> GetDropdownInformation(string ColumnName,
            object SelectedValue,
            string DataSource,
            bool IsStaticDropDown,
            bool ParameterisedDataSorce,
            string Parameters,
            Dictionary<string, object> CurrentRecord,
            Guid? CurrentApplicationUserId,
            List<AdditionalDropdownParameter> DropdownAdditionalParameters = null)
        {
            try
            {
                List<SelectListItem> selectList = new List<SelectListItem>();

                if (ColumnName == "Branch" || ColumnName == "DeptId" || ColumnName == "StaffId" || DataSource == "StaffId" || DataSource == "FinacleBranch"
                    || DataSource == "Branch" || DataSource == "DeptId" || DataSource == "JobTitle" || DataSource == "WorkClass" || DataSource == "Role")
                {
                    if (DataSource == "StaffId" || DataSource == "FinacleBranch"
                    || DataSource == "Branch" || DataSource == "DeptId" || DataSource == "JobTitle" || DataSource == "WorkClass" || DataSource == "Role")
                    {
                        return GetDropdownInforFromApi(DataSource, SelectedValue);
                    }
                    else
                    {
                        return GetDropdownInforFromApi(ColumnName, SelectedValue);
                    }
                }
                else if (IsStaticDropDown)
                {
                    string SchemaInformationListPath = ConfigurationManager.AppSettings["ApplicationRootPath"].ToString() + @"\ApplicationDataRule\DropdownList.xml";

                    XmlRootAttribute xRoot = new XmlRootAttribute();
                    xRoot.ElementName = "DropdownList";
                    xRoot.IsNullable = false;

                    XmlSerializer SchemaInformationSerializer = CachingXmlSerializerFactory.Create(typeof(List<SelectListItem>), xRoot);

                    using (StreamReader reader = new StreamReader(SchemaInformationListPath))
                    {
                        var DropdownList = (List<SelectListItem>)SchemaInformationSerializer.Deserialize(reader);

                        List<object> ParametersValues = new List<object>();

                        string _condition = "ColumnName.ToLower() = @0 ";

                        ParametersValues.Add(DataSource.ToLower());

                        if (!string.IsNullOrEmpty(Parameters))
                        {
                            List<string> ParameterList = Parameters.Split(',').ToList();

                            int i = 1;

                            ParameterList.OrderBy(o => o).ToList().ForEach(Parameter =>
                              {
                                  List<string> ParameterInformation = Parameter.Split('-').ToList();

                                  _condition += string.Format(" and {0}.ToLower() = @{1} ", ParameterInformation.First(), i);


                                  if (DropdownAdditionalParameters != null && DropdownAdditionalParameters.Where(w => DropdownAdditionalParameters != null && w.ColumnName == ColumnName && DropdownAdditionalParameters.Any(a => a.ParameterName == ParameterInformation.Last())).ToList().Count > 0)
                                  {
                                      AdditionalDropdownParameter additionalDropdownParameter = DropdownAdditionalParameters.Where(w => DropdownAdditionalParameters != null && w.ColumnName == ColumnName && DropdownAdditionalParameters.Any(a => a.ParameterName == ParameterInformation.Last())).FirstOrDefault();
                                      ParametersValues.Add(additionalDropdownParameter.ParameterValue);
                                  }
                                  else
                                  {
                                      if (CurrentRecord != null)
                                      {
                                          ParametersValues.Add(CurrentRecord.ContainsKey(ParameterInformation.Last()) ? CurrentRecord[ParameterInformation.Last()].ToString().ToLower() : null);
                                      }
                                      else
                                      {
                                          ParametersValues.Add(null);
                                      }
                                  }


                                  i = i + 1;
                              });
                        }

                        var where = DynamicLinqBuilder.CreateExpression<SelectListItem, bool>(_condition, ParametersValues.ToArray());


                        foreach (var x in DropdownList.AsQueryable().Where(where).ToList())
                        {
                            if (SelectedValue != null)
                            {
                                if (x.Value.ToString().ToLower() == SelectedValue.ToString().ToLower())
                                {
                                    x.Selected = true;
                                }
                            }

                            SelectListItem onlineAccountOpeningSelectListItem = new SelectListItem()
                            {
                                ColumnName = x.ColumnName,
                                Text = x.Text,
                                Value = x.Value,
                                OrderValue = x.OrderValue,
                                Parameter1 = x.Parameter1,
                                Parameter2 = x.Parameter2,
                                Parameter3 = x.Parameter3,
                                Parameter4 = x.Parameter4,
                                Parameter5 = x.Parameter5,
                                Selected = x.Selected

                            };

                            selectList.Add(onlineAccountOpeningSelectListItem);
                        }

                        return selectList.OrderBy(o => o.OrderValue).ToList();

                    }
                }
                else if (!string.IsNullOrEmpty(DataSource))
                {
                    string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                    using (SqlConnection _connection = new SqlConnection(_connectionString))
                    {
                        _connection.Open();

                        string _command = DataSource;

                        using (SqlCommand _commnd = new SqlCommand(_command, _connection))
                        {
                            _commnd.CommandType = CommandType.Text;

                            if (ParameterisedDataSorce)
                            {
                                List<string> ParameterList = Parameters.Split(',').ToList();
                                List<SqlParameter> sqlParameters = new List<SqlParameter>();

                                ParameterList.ForEach(Parameter =>
                                {
                                    if (Parameter == "CurrentApplicationUserId")
                                        sqlParameters.Add(new SqlParameter("CurrentApplicationUserId", CurrentApplicationUserId));

                                    else if (DropdownAdditionalParameters != null && DropdownAdditionalParameters.Where(w => DropdownAdditionalParameters != null && w.ColumnName == ColumnName && DropdownAdditionalParameters.Any(a => a.ParameterName == Parameter)).ToList().Count > 0)
                                    {
                                        AdditionalDropdownParameter additionalDropdownParameter = DropdownAdditionalParameters.Where(w => DropdownAdditionalParameters != null && w.ColumnName == ColumnName && DropdownAdditionalParameters.Any(a => a.ParameterName == Parameter)).FirstOrDefault();

                                        sqlParameters.Add(new SqlParameter(additionalDropdownParameter.ParameterName, additionalDropdownParameter.ParameterValue ?? DBNull.Value));


                                    }
                                    else
                                        sqlParameters.Add(new SqlParameter(Parameter, CurrentRecord.ContainsKey(Parameter) ? CurrentRecord[Parameter] : DBNull.Value));

                                });

                                _commnd.Parameters.AddRange(sqlParameters.ToArray());

                            }

                            using (var _reader = _commnd.ExecuteReader())
                            {
                                DataTable Result = new DataTable();
                                Result.Load(_reader);
                                var _selectList = Result.ToList<SelectListItem>().ToList();

                                foreach (var x in _selectList)
                                {
                                    if (SelectedValue != null)
                                    {
                                        if (x.Value.ToString().ToLower() == SelectedValue.ToString().ToLower())
                                        {
                                            x.Selected = true;
                                        }
                                    }

                                    SelectListItem onlineAccountOpeningSelectListItem = new SelectListItem()
                                    {
                                        ColumnName = x.ColumnName,
                                        Text = x.Text,
                                        Value = x.Value,
                                        OrderValue = x.OrderValue,
                                        Parameter1 = x.Parameter1,
                                        Parameter2 = x.Parameter2,
                                        Parameter3 = x.Parameter3,
                                        Parameter4 = x.Parameter4,
                                        Parameter5 = x.Parameter5,
                                        Selected = x.Selected

                                    };

                                    selectList.Add(onlineAccountOpeningSelectListItem);
                                }

                                return selectList.OrderBy(o => o.OrderValue).ToList();

                            }
                        }
                    }
                }
                else
                {
                    return new List<SelectListItem>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool ColumnExists(string ColumnName)
        {
            try
            {
                string SchemaInformationListPath = ConfigurationManager.AppSettings["ApplicationRootPath"].ToString() + @"\ApplicationDataRule\DropdownList.xml";

                XmlRootAttribute xRoot = new XmlRootAttribute();
                xRoot.ElementName = "DropdownList";
                xRoot.IsNullable = false;

                XmlSerializer SchemaInformationSerializer = CachingXmlSerializerFactory.Create(typeof(List<SelectListItem>), xRoot);

                using (StreamReader reader = new StreamReader(SchemaInformationListPath))
                {
                    var DropdownList = (List<SelectListItem>)SchemaInformationSerializer.Deserialize(reader);

                    if (DropdownList.Any(d => d.ColumnName.ToLower() == ColumnName.ToLower()))
                        return true;
                    else
                        return false;



                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetDropdownInformationTitle(string ColumnName, object SelectedValue)
        {
            try
            {
                if (SelectedValue == null)
                    return string.Empty;
                string SchemaInformationListPath = ConfigurationManager.AppSettings["ApplicationRootPath"].ToString() + @"\ApplicationDataRule\DropdownList.xml";

                XmlRootAttribute xRoot = new XmlRootAttribute();
                xRoot.ElementName = "DropdownList";
                xRoot.IsNullable = false;

                XmlSerializer SchemaInformationSerializer = CachingXmlSerializerFactory.Create(typeof(List<SelectListItem>), xRoot);

                using (StreamReader reader = new StreamReader(SchemaInformationListPath))
                {
                    var DropdownList = (List<SelectListItem>)SchemaInformationSerializer.Deserialize(reader);

                    List<SelectListItem> selectList = DropdownList.Where(s => s.ColumnName.ToLower() == ColumnName.ToLower()).ToList();

                    return selectList.Where(s => s.ColumnName.ToLower() == ColumnName.ToLower() && s.Value == SelectedValue.ToString()).Select(sv => sv.Text).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetDropdownList()
        {
            try
            {

                string SchemaInformationListPath = ConfigurationManager.AppSettings["ApplicationRootPath"].ToString() + @"\ApplicationDataRule\DropdownList.xml";

                XmlRootAttribute xRoot = new XmlRootAttribute();
                xRoot.ElementName = "DropdownList";
                xRoot.IsNullable = false;

                XmlSerializer SchemaInformationSerializer = CachingXmlSerializerFactory.Create(typeof(List<SelectListItem>), xRoot);

                using (StreamReader reader = new StreamReader(SchemaInformationListPath))
                {
                    var DropdownList = (List<SelectListItem>)SchemaInformationSerializer.Deserialize(reader);

                    List<SelectListItem> selectList = DropdownList.ToList();

                    return selectList;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetDropdownInformation(string ColumnName,
           object SelectedValue,
           string DataSource,
           bool IsStaticDropDown)
        {
            try
            {
                List<SelectListItem> selectList = new List<SelectListItem>();

                if (ColumnName == "Branch" || ColumnName == "DeptId" || ColumnName == "StaffId" || DataSource == "StaffId" || DataSource == "FinacleBranch"
                    || DataSource == "Branch" || DataSource == "DeptId" || DataSource == "JobTitle" || DataSource == "WorkClass" || DataSource == "Role")
                {
                    if (DataSource == "StaffId" || DataSource == "FinacleBranch"
                    || DataSource == "Branch" || DataSource == "DeptId" || DataSource == "JobTitle" || DataSource == "WorkClass" || DataSource == "Role")
                    {
                        return GetDropdownInforFromApi(DataSource, SelectedValue);
                    }
                    else
                    {
                        return GetDropdownInforFromApi(ColumnName, SelectedValue);
                    }
                }
                else if (IsStaticDropDown)
                {
                    string SchemaInformationListPath = ConfigurationManager.AppSettings["ApplicationRootPath"].ToString() + @"\ApplicationDataRule\DropdownList.xml";

                    XmlRootAttribute xRoot = new XmlRootAttribute();
                    xRoot.ElementName = "DropdownList";
                    xRoot.IsNullable = false;

                    XmlSerializer SchemaInformationSerializer = CachingXmlSerializerFactory.Create(typeof(List<SelectListItem>), xRoot);

                    using (StreamReader reader = new StreamReader(SchemaInformationListPath))
                    {
                        var DropdownList = (List<SelectListItem>)SchemaInformationSerializer.Deserialize(reader);

                        List<object> ParametersValues = new List<object>();

                        string _condition = "ColumnName.ToLower() = @0 ";

                        ParametersValues.Add(DataSource.ToLower());

                        var where = DynamicLinqBuilder.CreateExpression<SelectListItem, bool>(_condition, ParametersValues.ToArray());

                        foreach (var x in DropdownList.OrderBy(x => x.OrderValue.Length).ThenBy(y => y.OrderValue).AsQueryable().Where(where).ToList())
                        {
                            if (SelectedValue != null)
                            {
                                if (x.Value.ToString().ToLower() == SelectedValue.ToString().ToLower())
                                {
                                    x.Selected = true;
                                }
                            }

                            SelectListItem onlineAccountOpeningSelectListItem = new SelectListItem()
                            {
                                ColumnName = x.ColumnName,
                                Text = x.Text,
                                Value = x.Value,
                                OrderValue = x.OrderValue,
                                Parameter1 = x.Parameter1,
                                Parameter2 = x.Parameter2,
                                Parameter3 = x.Parameter3,
                                Parameter4 = x.Parameter4,
                                Parameter5 = x.Parameter5,
                                Selected = x.Selected

                            };

                            selectList.Add(onlineAccountOpeningSelectListItem);
                        }

                        return selectList.OrderBy(o => o.OrderValue).ToList();

                    }
                }
                else if (!string.IsNullOrEmpty(DataSource))
                {
                    string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                    using (SqlConnection _connection = new SqlConnection(_connectionString))
                    {
                        _connection.Open();

                        string _command = DataSource;

                        using (SqlCommand _commnd = new SqlCommand(_command, _connection))
                        {
                            _commnd.CommandType = CommandType.Text;

                            using (var _reader = _commnd.ExecuteReader())
                            {
                                DataTable Result = new DataTable();
                                Result.Load(_reader);
                                var _selectList = Result.ToList<SelectListItem>().ToList();

                                foreach (var x in _selectList)
                                {
                                    if (SelectedValue != null)
                                    {
                                        if (x.Value.ToString().ToLower() == SelectedValue.ToString().ToLower())
                                        {
                                            x.Selected = true;
                                        }
                                    }

                                    SelectListItem onlineAccountOpeningSelectListItem = new SelectListItem()
                                    {
                                        ColumnName = x.ColumnName,
                                        Text = x.Text,
                                        Value = x.Value,
                                        OrderValue = x.OrderValue,
                                        Parameter1 = x.Parameter1,
                                        Parameter2 = x.Parameter2,
                                        Parameter3 = x.Parameter3,
                                        Parameter4 = x.Parameter4,
                                        Parameter5 = x.Parameter5,
                                        Selected = x.Selected

                                    };

                                    selectList.Add(onlineAccountOpeningSelectListItem);
                                }

                                return selectList.OrderBy(o => o.OrderValue).ToList();

                            }
                        }
                    }
                }
                else
                {
                    return new List<SelectListItem>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetDropdownInformation(string query, List<SqlParameter> sqlParameters)
        {
            try
            {
                List<SelectListItem> selectList = new List<SelectListItem>();

                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    _connection.Open();

                    using (SqlCommand _commnd = new SqlCommand(query, _connection))
                    {
                        _commnd.CommandType = CommandType.Text;
                        if (sqlParameters.Count > 0)
                        {
                            _commnd.Parameters.AddRange(sqlParameters.ToArray());
                        }


                        using (var _reader = _commnd.ExecuteReader())
                        {
                            DataTable Result = new DataTable();
                            Result.Load(_reader);
                            var _selectList = Result.ToList<SelectListItem>().ToList();

                            foreach (var x in _selectList)
                            {
                                SelectListItem selectListItem = new SelectListItem()
                                {
                                    ColumnName = x.ColumnName,
                                    Text = x.Text,
                                    Value = x.Value,
                                    OrderValue = x.OrderValue,
                                    Parameter1 = x.Parameter1,
                                    Parameter2 = x.Parameter2,
                                    Parameter3 = x.Parameter3,
                                    Parameter4 = x.Parameter4,
                                    Parameter5 = x.Parameter5,
                                    Selected = x.Selected

                                };

                                selectList.Add(selectListItem);
                            }

                            return selectList.OrderBy(o => o.OrderValue).ToList();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetDropdownInforFromApi(string ColumnName, object SelectedValue)
        {
            APIConsumtionService aPIConsumtionService = new APIConsumtionService();
            List<SelectListItem> listItems = new List<SelectListItem>();

            if (ColumnName == "Branch")
            {
                List<Service.BranchList> branchLists = aPIConsumtionService.GetBranchList();
                foreach (var x in branchLists.OrderBy(x => x.BranchName))
                {
                    bool selected = false;
                    if (SelectedValue != null)
                    {
                        if (x.BranchID.ToString().ToLower() == SelectedValue.ToString().ToLower())
                        {
                            selected = true;
                        }
                    }

                    SelectListItem selectListItem = new SelectListItem()
                    {
                        ColumnName = ColumnName,
                        Text = x.BranchName,
                        Value = x.BranchID,
                        OrderValue = "1",
                        Parameter1 = null,
                        Parameter2 = null,
                        Parameter3 = null,
                        Parameter4 = null,
                        Parameter5 = null,
                        Selected = selected

                    };

                    listItems.Add(selectListItem);
                }
            }
            else if (ColumnName == "DeptId")
            {
                List<DepartmentList> departmentLists = aPIConsumtionService.GetDepartmentList("", "", "");

                foreach (var x in departmentLists.OrderBy(x => x.DepartmentName))
                {
                    bool selected = false;
                    if (SelectedValue != null)
                    {
                        if (x.DepartmentID.ToString().ToLower() == SelectedValue.ToString().ToLower())
                        {
                            selected = true;
                        }
                    }

                    SelectListItem selectListItem = new SelectListItem()
                    {
                        ColumnName = ColumnName,
                        Text = x.DepartmentName,
                        Value = x.DepartmentID,
                        OrderValue = "1",
                        Parameter1 = null,
                        Parameter2 = null,
                        Parameter3 = null,
                        Parameter4 = null,
                        Parameter5 = null,
                        Selected = selected

                    };

                    listItems.Add(selectListItem);
                }
            }
            else if (ColumnName == "StaffId")
            {
                List<Service.StaffList> staffLists = aPIConsumtionService.GetStaffList("", "", "", "", "");

                foreach (var x in staffLists.OrderBy(x => x.fullName))
                {
                    bool selected = false;
                    if (SelectedValue != null)
                    {
                        if (x.StaffId.ToString().ToLower() == SelectedValue.ToString().ToLower())
                        {
                            selected = true;
                        }
                    }

                    SelectListItem selectListItem = new SelectListItem()
                    {
                        ColumnName = ColumnName,
                        Text = x.fullName,
                        Value = x.StaffId,
                        OrderValue = "1",
                        Parameter1 = null,
                        Parameter2 = null,
                        Parameter3 = null,
                        Parameter4 = null,
                        Parameter5 = null,
                        Selected = selected

                    };

                    listItems.Add(selectListItem);
                }
            }
            else
            {
                listItems = new List<SelectListItem>();
            }

            return listItems;
        }
    }
}
