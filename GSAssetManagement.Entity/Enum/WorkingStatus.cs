﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public enum WorkingStatus
    {
        [Display(Name = "Not Working")]
        NotWorking = 0,
        [Display(Name = "Working")]
        Working = 1
    }
}
