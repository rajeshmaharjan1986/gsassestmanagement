﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public enum DataEntityState
    {
        Added = 1,
        Deleted = 2,
        Modified = 3,
        Closed = 4,
        Reverted
    }
}
