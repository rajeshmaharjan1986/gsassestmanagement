﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public enum PeriodicType
    {
        Monthly=1, 
        Yearly, 
        MonthRange, 
        YearlyRange
    }
}
