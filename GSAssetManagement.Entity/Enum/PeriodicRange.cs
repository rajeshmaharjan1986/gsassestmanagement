﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public enum PeriodicRange
    {

        [Display(Name = "From 1 to 2 Year")]
        Upto_Two_Year =1,
        [Display(Name = "3to 4 Year")]
        Three_To_4_Year,
        [Display(Name = "5 and above Year")]
        Above_4_year

    }
}
