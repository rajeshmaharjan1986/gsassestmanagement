﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public enum ModuleName
    {
        [Display(Name = "Admin Setting", Description = "fa-gears")]
        AdminSetting = 1,
        [Display(Name = "Setting", Description = "fa-gears")]
        AssetTypeSetting,
        [Display(Name = "User Management", Description = "fa-users")]
        UserManagement,
        [Display(Name = "DrillingReport", Description = "fa-search")]
        DrillingReport,
        [Display(Name = "Report", Description = "fa-search")]
        GSDrillingReport
    }
}
