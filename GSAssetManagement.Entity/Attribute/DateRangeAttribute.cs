﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public class DateRangeAttribute : RangeAttribute
    {
        public DateRangeAttribute(string minimum, string maximum)
            : base(typeof(DateTime), minimum, DateTime.Now.ToShortDateString())
        {

        }



    }

    //[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    //public class ValidateFutureDateAttribute : ValidationAttribute
    //{
    //    private const string DefaultErrorMessage = "Date cannot be greater than today.";

    //    public ValidateFutureDateAttribute()
    //        : base(DefaultErrorMessage)
    //    {

    //    }
    //    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    //    {
    //        if (value != null)
    //        {
    //            DateTime parsedValue = (DateTime)value;

    //            if (parsedValue >= DateTime.Now)
    //            {
    //                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
    //            }
    //        }
    //        return ValidationResult.Success;

    //    }

    //    public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
    //    {
    //        var rule = new ModelClientValidationRule()
    //        {
    //            ValidationType = "validateage",
    //            ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
    //        };

    //        rule.ValidationParameters.Add("minumumdate", MinimumDateProperty.ToShortDateString());
    //        rule.ValidationParameters.Add("maximumdate", MaximumDateProperty.ToShortDateString());

    //        yield return rule;
    //    }
    //}
}
