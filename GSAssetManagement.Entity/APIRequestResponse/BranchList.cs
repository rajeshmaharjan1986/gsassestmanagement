﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity.APIRequestResponse
{
    public class BranchList
    {
        public string BranchName { get; set; }
        public string BranchID { get; set; }
        public string Email { get; set; }
    }
}
