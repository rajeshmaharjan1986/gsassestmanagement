﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity.APIRequestResponse
{
    public class DepartmentList
    {
        public string DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentNameNepali { get; set; }
        public string UnderDepartment { get; set; }
        public string DepartmentCode { get; set; }
        public string HeadEmpCode { get; set; }
        public string IsActive { get; set; }
        public string FunctionalCategoryID { get; set; }
        public string DepartmentForBranchTypes { get; set; }
        public string DepartmentEmail { get; set; }
    }
}
