﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity.APIRequestResponse
{
    public class ResponseResult
    {
        public bool success { get; set; }
        public string token { get; set; }
        public string DeptId { get; set; }
        public string DeptName { get; set; }
        public string StaffId { get; set; }
        public string Username { get; set; }
        public string BranchName { get; set; }
        public string Designation { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Branch { get; set; }
        public string currentFiscalYear { get; set; }

    }
}
