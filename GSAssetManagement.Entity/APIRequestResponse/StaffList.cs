﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity.APIRequestResponse
{
    public class StaffList
    {
        public string fullName { get; set; }
        public string JobID { get; set; }
        public string Username { get; set; }
        public string StaffId { get; set; }
        public string DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string BranchName { get; set; }
        public string FunctionalTitle { get; set; }
        public string BranchID { get; set; }
    }
}
