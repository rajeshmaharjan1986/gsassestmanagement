﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity.APIRequestResponse
{
    public class OfficeRoleResponses {
        public string success { get; set; }
        public List<OfficeRoleList> data { get; set; }
    }
    public class OfficeRoleList
    {
        public string Title { get; set; }
        public string FunctionalTitle { get; set; }
        public string Role { get; set; }
        public string RoleId { get; set; }
        public string ID { get; set; }
    }
}
