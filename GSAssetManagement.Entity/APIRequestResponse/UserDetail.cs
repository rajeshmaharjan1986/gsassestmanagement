﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity.APIRequestResponse
{
    public class UserDetail
    {
        public string applicationcode { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
