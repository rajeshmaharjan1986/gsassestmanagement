﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity.DTO
{
    public class ApplicationUserDTO
    {
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string DeptId { get; set; }
        public string DeptName { get; set; }
        public string StaffId { get; set; }
        public string BranchName { get; set; }
        public string Designation { get; set; }
        public string Branch { get; set; }
        public string currentFiscalYear { get; set; }
        public string JobID { get; set; }
        public string FunctionalTitle { get; set; }
        public Nullable<DateTime> LastLoginDate { get; set; }
        public string CreatedBy { get; set; }
        public Guid CreatedById { get; set; }
        public string CreatedByStaffNo { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Guid ModifiedById { get; set; }
        public string ModifiedByStaffNo { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string AuthorisedBy { get; set; }
        public Nullable<Guid> AuthorisedById { get; set; }
        public string AuthorisedByStaffNo { get; set; }
        public Nullable<DateTime> AuthorisedDate { get; set; }
        public RecordStatus Record_Status { get; set; }
        public DataEntityState EntityState { get; set; }
        public DataEntry DataEntry { get; set; }
        public byte[] RowVersion { get; set; }
        public string ChangeLog { get; set; }
    }
}
