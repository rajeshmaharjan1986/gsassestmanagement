using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;

namespace GSAssetManagement.Entity.DTO
{
    public class ApplicationUserGroupDTO : OfficeBaseEntityDTO<Guid>
    {
        public ApplicationUserGroupDTO()
        {
        }
        [Required(ErrorMessage = "User is required.")]
        public Guid ApplicationUserId { get; set; }
        [Required(ErrorMessage = "Group is required.")]
        public Guid ApplicationGroupId { get; set; }
        [Required(ErrorMessage = "Remarks is required.")]
        [StringLength(300)]
        public string Remarks { get; set; }
        [ForeignKey("ApplicationUserId")]
        public virtual ApplicationUserDTO ApplicationUser { get; set; }
        [ForeignKey("ApplicationGroupId")]
        public virtual ApplicationGroupDTO ApplicationGroup { get; set; }
    }
}
