﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public class ApplicationGroupRoleDTO: OfficeBaseEntityDTO<Guid>
    {
        public Guid GroupId { get; set; }
     
        public Guid RoleId { get; set; }
    }
}
