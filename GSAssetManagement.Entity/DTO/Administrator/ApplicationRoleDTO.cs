using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSAssetManagement.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace GSAssetManagement.Entity
{
    public class ApplicationRoleDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string RoleCode { get; set; }
        public string Remarks { get; set; }
        public string FieldString1 { get; set; }
        public string FieldString2 { get; set; }
        public string FieldString3 { get; set; }
        public string FieldString4 { get; set; }
        public string FieldString5 { get; set; }
        public string CreatedBy { get; set; }
        public Guid CreatedById { get; set; }
        public string CreatedByStaffNo { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Guid ModifiedById { get; set; }
        public string ModifiedByStaffNo { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string AuthorisedBy { get; set; }
        public Nullable<Guid> AuthorisedById { get; set; }
        public string AuthorisedByStaffNo { get; set; }
        public Nullable<DateTime> AuthorisedDate { get; set; }
        public DataEntityState EntityState { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public DataEntry DataEntry { get; set; }
        public byte[] RowVersion { get; set; }
        public string ChangeLog { get; set; }
    }
}
