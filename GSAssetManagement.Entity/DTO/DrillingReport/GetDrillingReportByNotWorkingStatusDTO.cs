﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public class GetDrillingReportByNotWorkingStatusDTO
    {
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string ReportDateInBS { get; set; }
        public DateTime ReportDate { get; set; }
        public string ReportPeriod { get; set; }
        public string ReportMonth { get; set; }
        public string ReportYear { get; set; }
        public string CCTVBackupRemarks { get; set; }
        public string ReportStatus { get; set; }
        public Guid AssetDetailId { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string AssetCode { get; set; }
        public string AssetName { get; set; }
        public string Location { get; set; }
        public DateTime PurchaseDate { get; set; }
        public Nullable<DateTime> ExpireDate { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
    }
}
