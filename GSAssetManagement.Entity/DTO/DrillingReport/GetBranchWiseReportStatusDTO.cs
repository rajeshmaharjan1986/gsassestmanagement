﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public class GetBranchWiseReportStatusDTO
    {
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string BranchReportStatus { get; set; }
        public string ReportDateInBS { get; set; }
        public DateTime ReportDate { get; set; }
        public string ReportPeriod { get; set; }
        public string ReportMonth { get; set; }
        public string ReportYear { get; set; }

        //ReportDate,ReportMonth,ReportDateInBS,ReportYear,ReportPeriod
    }
}
