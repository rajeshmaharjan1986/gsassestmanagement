﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public class GetDrillingReportListDTO
    {
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string ReportMonth { get; set; }
        public string ReportYear { get; set; }
        public string ReportDateInBS { get; set; }
        public DateTime ReportDate { get; set; }
        public string ReportPeriod { get; set; }
        public string CCTVBackupRemarks { get; set; }
        public string ReportStatus { get; set; }
        public int TotalModification { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string AuthorisedBy { get; set; }
        public Guid CreatedById { get; set; }
        public Guid ModifiedById { get; set; }
        public Nullable<Guid> AuthorisedById { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Nullable<DateTime> AuthorisedDate { get; set; }
        public DataEntityState EntityState { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public DataEntry DataEntry { get; set; }
    }
}
