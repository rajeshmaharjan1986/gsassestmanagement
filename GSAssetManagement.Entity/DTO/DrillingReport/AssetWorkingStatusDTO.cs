﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public class AssetWorkingStatusDTO : BaseWithOutStatusDTO<Guid>
    {
        public Guid BranchDrillingReportId { get; set; }
        public Guid AssetDetailId { get; set; }
        public Guid AssetExtraDetailId { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string AssetCode { get; set; }
        public string AssetName { get; set; }
        public string Location { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public Nullable<DateTime> PurchaseDate { get; set; }
        public Nullable<DateTime> ExpireDate { get; set; }
    }
}
