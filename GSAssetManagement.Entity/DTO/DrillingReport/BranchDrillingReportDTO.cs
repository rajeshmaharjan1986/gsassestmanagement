﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public class BranchDrillingReportDTO : OfficeBaseEntityDTO<Guid>
    {
        public BranchDrillingReportDTO() {
            AssetWorkingStatusDTOs = new List<AssetWorkingStatusDTO>();
        }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public string ReportMonth { get; set; }
        public string ReportYear { get; set; }
        public string ReportDateInBS { get; set; }
        public DateTime ReportDate { get; set; }
        public string ReportPeriod { get; set; }
        public string CCTVBackupRemarks { get; set; }
        public string CCTVBackupStatus { get; set; }
        public string ReportStatus { get; set; }
        public List<AssetWorkingStatusDTO> AssetWorkingStatusDTOs { get; set; }
    }
}
