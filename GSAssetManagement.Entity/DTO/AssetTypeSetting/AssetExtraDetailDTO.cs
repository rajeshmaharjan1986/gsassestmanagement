﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public class AssetExtraDetailDTO : BaseWithOutStatusDTO<Guid>
    {
        public Guid AssetDetailId { get; set; }
        public string Location { get; set; }
        public Nullable<DateTime> PurchaseDate { get; set; }
        public Nullable<DateTime> ExpireDate { get; set; }
        
    }
}
