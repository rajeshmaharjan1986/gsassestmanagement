﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public class AssetDetailDTO : OfficeBaseEntityDTO<Guid>
    {
        public AssetDetailDTO()
        {
            assetExtraDetailDTOs = new List<AssetExtraDetailDTO>();
        }
        public Guid AssetCategoryId { get; set; }
        public string AssetCode { get; set; }
        public string AssetName { get; set; }
        public int AssetNumber { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public Nullable<DateTime> PurchaseDate { get; set; }
        public Nullable<DateTime> ExpireDate { get; set; }
        public List<AssetExtraDetailDTO> assetExtraDetailDTOs { get; set; }
    }
}
