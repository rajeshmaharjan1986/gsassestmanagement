﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity.DTO
{
    public class ParameterisedDatasourceDTO
    {
        public ParameterisedDatasourceDTO()
        {
            Parameters = new List<Parameter>();
            SelectList = new List<Select>();
        }


        public string ColumnName { get; set; }
        public string TableName { get; set; }
        public object SelectedValue { get; set; }
        public List<Parameter> Parameters { get; set; }
        public List<Select> SelectList { get; set; }
    }
}
