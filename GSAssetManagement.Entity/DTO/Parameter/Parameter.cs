﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity.DTO
{
    public class Parameter
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public object SelectedValue { get; set; }
    }
}
