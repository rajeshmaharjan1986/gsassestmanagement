﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity.DTO
{
    public class vDataSourceInformation
    {
        public string Module { get; set; }
        public string DatabaseTable { get; set; }
        public string ColumnName { get; set; }
        public bool ParameterisedSource { get; set; }
        public string Datasource { get; set; }
    }
}
