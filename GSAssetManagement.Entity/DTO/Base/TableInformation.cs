﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity.DTO
{
    public class TableInformation
    {
        public string TableName { get; set; }
        public string Parameter { get; set; }
        public string ColumnName { get; set; }
        public string TableDescription { get; set; }
    }
}
