﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity.DTO
{
    public class ViewModuleDataDTO
    {
        public string ModuleName { get; set; }
        public Guid DataId { get; set; }
    }
}
