﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity.DTO
{
    public class SelectListItem
    {
        public string ColumnName { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public string OrderValue { get; set; }
        public int IntOrdervalue { get; set; }
        public string StringOrderByValue { get; set; }
        public DateTime DateTimeOrderByValue { get; set; }
        public bool Selected { get; set; }
        public string Parameter1 { get; set; }
        public string Parameter2 { get; set; }
        public string Parameter3 { get; set; }
        public string Parameter4 { get; set; }
        public string Parameter5 { get; set; }

    }
}
