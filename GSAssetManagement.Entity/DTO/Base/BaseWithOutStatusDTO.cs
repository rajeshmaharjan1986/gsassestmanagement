﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public class BaseWithOutStatusDTO<TKey>
    {
        public TKey Id { get; set; }
        public int TotalModification { get; set; }
        public string FieldString1 { get; set; }
        public string FieldString2 { get; set; }
        public string FieldString3 { get; set; }
        public string FieldString4 { get; set; }
        public string FieldString5 { get; set; }
        public string FieldString6 { get; set; }
        public string FieldString7 { get; set; }
        public string FieldString8 { get; set; }
        public string FieldString9 { get; set; }
        public string FieldString10 { get; set; }
        public string FieldString11 { get; set; }
        public string FieldString12 { get; set; }
        public string FieldString13 { get; set; }
        public string FieldString14 { get; set; }
        public string FieldString15 { get; set; }
        public string FieldString16 { get; set; }
        public string FieldString17 { get; set; }
        public string FieldString18 { get; set; }
        public string FieldString19 { get; set; }
        public string FieldString20 { get; set; }
        public byte[] RowVersion { get; set; }
        public string ChangeLog { get; set; }
    }
}
