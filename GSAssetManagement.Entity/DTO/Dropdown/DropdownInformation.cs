﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity.DTO
{
    public class DropdownInformationDTO
    {
        public DropdownInformationDTO()
        {
            Parameters = new List<DropdownParameterDTO>();
            selectListItems = new List<SelectListItemDTO>();
        }

        public string ColumnName { get; set; }
        public string EntityName { get; set; }
        public List<DropdownParameterDTO> Parameters { get; set; }
        public List<SelectListItemDTO> selectListItems { get; set; }
    }

    public class DropdownParameterDTO
    {
        public string ParameterName { get; set; }
        public object ParameterValue { get; set; }
    }

    public class SelectListItemDTO
    {
        public bool Disabled { get; set; }        
        public bool Selected { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
