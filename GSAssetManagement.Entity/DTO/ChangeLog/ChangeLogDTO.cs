﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GSAssetManagement.Entity.DTO
{
    public class ChangeLogDTO
    {
        public string PropertyName { get; set; }
        public Guid EntityPK { get; set; }
        public object Originalvalue { get; set; }
        public object ModifiedValue { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string LogStatus { get; set; }
    }
    [XmlRoot(ElementName = "ArrayOfChangeLogDTO")]
    public class ArrayOfChangeLogDTO
    {
        [XmlElement(ElementName = "ChangeLogDTO")]
        public List<ChangeLogDTO> ChangeLogDTO { get; set; }
    }

}
