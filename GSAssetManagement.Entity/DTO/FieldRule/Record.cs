﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GSAssetManagement.Entity.DTO
{
    public class RecordDetails<T> where T : new()
    {
        public RecordDetails()
        {
            FieldRuleList = new List<DTO.FieldRule>();
            Record = new T();
        }

        public T Record { get; set; }
        public List<FieldRule> FieldRuleList { get; set; }
    }

    [XmlRoot(ElementName = "RecordPreview")]
    public class RecordPreview
    {

        public List<ArrayOfChangeLogDTO> ChangeLog { get; set; }
        public RecordLog RecordLog { get; set; }

    }

    public class RecordLog
    {
        public FieldRuleList FieldRuleList { get; set; }
    }

    public class FieldRuleList
    {
        public FieldRuleList()
        {
            FieldRule = new List<FieldRule>();
        }

        [XmlElement(ElementName= "FieldRule")]
        public List<FieldRule> FieldRule { get; set; }
    }
}
