﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity.DTO
{
    public class Select
    {
        public string ColumnName { get; set; }
        public string Title { get; set; }
        public string Value { get; set; }
        public bool SelectedValue { get; set; }

    }

    public class SelectListParameter
    {
        public SelectListParameter()
        {
            Parameters = new List<Parameter>();

        }
        public Guid ModuleTableSetupId { get; set; }
        public string FieldName { get; set; }
        public string SelectedValue { get; set; }
        public List<Parameter> Parameters { get; set; }
    }

    public class SelectListCustom
    {
        public SelectListCustom()
        {
            SelectValues = new List<Select>();
        }
        public Guid ModuleTableSetupId { get; set; }
        public string FieldName { get; set; }
        public List<Select> SelectValues { get; set; }
    }
}
