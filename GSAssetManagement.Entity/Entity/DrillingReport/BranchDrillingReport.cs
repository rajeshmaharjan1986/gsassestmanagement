﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    [Table("BranchDrillingReport", Schema = "DrillingReport")]
    public class BranchDrillingReport : OfficeBaseEntity<Guid>
    {
        [Required]
        [StringLength(20)]
        public string BranchCode { get; set; }
        [StringLength(20)]
        public string DepartmentCode { get; set; }
        [StringLength(20)]
        public string ReportMonth { get; set; }
        [StringLength(20)]
        public string ReportYear { get; set; }
        [Required]
        [StringLength(20)]
        public string ReportDateInBS { get; set; }
        public DateTime ReportDate { get; set; }
        [Required]
        [StringLength(200)]
        public string CCTVBackupRemarks { get; set; }
        [StringLength(200)]
        public string CCTVBackupStatus { get; set; }
        [Required]
        [StringLength(50)]
        public string ReportPeriod { get; set; }
        [Required]
        [StringLength(20)]
        public string ReportStatus { get; set; }
    }
}
