﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    [Table("AssetWorkingStatus", Schema = "DrillingReport")]
    public class AssetWorkingStatus : BaseWithOutStatus<Guid>
    {
        public Guid BranchDrillingReportId { get; set; }
        public Guid AssetDetailId { get; set; }
        public Guid AssetExtraDetailId { get; set; }
        [Required]
        [StringLength(20)]
        public string Status { get; set; }
        public Nullable<DateTime> ExpireDate { get; set; }
        [StringLength(1000)]
        public string Remarks { get; set; }
        [ForeignKey("BranchDrillingReportId")]
        public virtual BranchDrillingReport BranchDrillingReport { get; set; }
        [ForeignKey("AssetDetailId")]
        public virtual AssetDetail AssetDetail { get; set; }
    }
}
