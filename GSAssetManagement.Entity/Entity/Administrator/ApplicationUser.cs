using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using GSAssetManagement.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace GSAssetManagement.Entity
{
    [Table("ApplicationUser", Schema = "Administrator")]
    public class ApplicationUser : IdentityUser<Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>, IUser<Guid>
    {
        public ApplicationUser()
        {
            ApplicationUserRoles = new HashSet<ApplicationUserRole>();
        }

        [Required(ErrorMessage = "Fullname is required.")]
        [StringLength(500)]
        public string FullName { get; set; }

        [StringLength(10)]
        public string DeptId { get; set; }
        [StringLength(200)]
        public string DeptName { get; set; }
        [StringLength(20)]
        public string StaffId { get; set; }
        [StringLength(100)]
        public string BranchName { get; set; }
        [StringLength(100)]
        public string Designation { get; set; }
        [Required(ErrorMessage = "Branch is required.")]
        [StringLength(10)]
        public string Branch { get; set; }
        [StringLength(50)]
        public string currentFiscalYear { get; set; }
        [StringLength(10)]
        public string JobID { get; set; }
        [StringLength(100)]
        public string FunctionalTitle { get; set; }
        public Nullable<DateTime> LastLoginDate { get; set; }
        [StringLength(300)]
        public string CreatedBy { get; set; }
        public Guid CreatedById { get; set; }
        [StringLength(20)]
        public string CreatedByStaffNo { get; set; }
        public DateTime CreatedDate { get; set; }
        [StringLength(300)]
        public string ModifiedBy { get; set; }
        public Guid ModifiedById { get; set; }
        [StringLength(20)]
        public string ModifiedByStaffNo { get; set; }
        public DateTime ModifiedDate { get; set; }
        [StringLength(300)]
        public string AuthorisedBy { get; set; }
        public Nullable<Guid> AuthorisedById { get; set; }
        [StringLength(20)]
        public string AuthorisedByStaffNo { get; set; }
        public Nullable<DateTime> AuthorisedDate { get; set; }
        public RecordStatus Record_Status { get; set; }
        public DataEntityState EntityState { get; set; }
        public DataEntry DataEntry { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        [Column(TypeName = "xml")]
        public string ChangeLog { get; set; }

        public virtual ICollection<ApplicationUserRole> ApplicationUserRoles { get; set; }

        public async Task<ClaimsIdentity>
         GenerateUserIdentityAsync(UserManager<ApplicationUser, Guid> manager)
        {
            string functionTitle= string.IsNullOrEmpty(this.FunctionalTitle) ? string.Empty : this.FunctionalTitle;
            string deptId = string.IsNullOrEmpty(this.DeptId) ? string.Empty : this.DeptId;
            string staffId = string.IsNullOrEmpty(this.StaffId) ? "000" : this.StaffId;
            string designation = string.IsNullOrEmpty(this.Designation) ? string.Empty : this.Designation;

            var userIdentity = await manager
               .CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            //List<ApplicationRoleDetailsDTO> _roleInformation= manager.

            userIdentity.AddClaim(new Claim(ClaimTypes.GivenName, this.FullName.ToString()));
            userIdentity.AddClaim(new Claim(ClaimTypes.UserData, string.Empty));
            userIdentity.AddClaim(new Claim(ClaimTypes.Country, this.Branch.ToString()));
            userIdentity.AddClaim(new Claim(ClaimTypes.Actor, functionTitle));
            userIdentity.AddClaim(new Claim(ClaimTypes.GroupSid, deptId));
            userIdentity.AddClaim(new Claim(ClaimTypes.PostalCode, staffId));
            userIdentity.AddClaim(new Claim(ClaimTypes.Gender, designation));

            foreach (var item in this.Claims)
            {
                userIdentity.AddClaim(new Claim(item.ClaimType, item.ClaimValue));
            }


            return userIdentity;
        }



        //[ForeignKey("CustomerId")]
        //public virtual CustomerRegistration CustomerRegistration { get; set; }
        //[ForeignKey("BranchId")]
        //public virtual BranchSetup BranchSetup { get; set; }
    }
}
