﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    [Table("ApplicationGroupRole", Schema = "Administrator")]
    public class ApplicationGroupRole : OfficeBaseEntity<Guid>
    {
        [Required(ErrorMessage = "ApplicationRoleId is required.")]
        public Guid GroupId { get; set; }
        [Required(ErrorMessage = "ApplicationRoleId is required.")]
        public Guid RoleId { get; set; }

        [ForeignKey("GroupId")]
        public virtual ApplicationGroup ApplicationGroup { get; set; }
        [ForeignKey("RoleId")]
        public virtual ApplicationRole ApplicationRole { get; set; }
    }
}
