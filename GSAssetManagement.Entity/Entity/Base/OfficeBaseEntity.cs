﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{ //aDD
    public class OfficeBaseEntity<TKey>
    {
        [Key]
        [Required]
        public TKey Id { get; set; }
        public int TotalModification { get; set; }
        [StringLength(500)]
        public string FieldString1 { get; set; }
        [StringLength(500)]
        public string FieldString2 { get; set; }
        [StringLength(500)]
        public string FieldString3 { get; set; }
        [StringLength(500)]
        public string FieldString4 { get; set; }
        [StringLength(500)]
        public string FieldString5 { get; set; }
        [StringLength(500)]
        public string FieldString6 { get; set; }
        [StringLength(500)]
        public string FieldString7 { get; set; }
        [StringLength(500)]
        public string FieldString8 { get; set; }
        public string FieldString9 { get; set; }
        [StringLength(500)]
        public string FieldString10 { get; set; }
        [StringLength(500)]
        public string FieldString11 { get; set; }
        [StringLength(500)]
        public string FieldString12 { get; set; }
        [StringLength(500)]
        public string FieldString13 { get; set; }
        [StringLength(500)]
        public string FieldString14 { get; set; }
        [StringLength(500)]
        public string FieldString15 { get; set; }
        [StringLength(500)]
        public string FieldString16 { get; set; }
        [StringLength(500)]
        public string FieldString17 { get; set; }
        [StringLength(500)]
        public string FieldString18 { get; set; }
        [StringLength(500)]
        public string FieldString19 { get; set; }
        [StringLength(500)]
        public string FieldString20 { get; set; }
        [Required]
        [StringLength(300)]
        public string CreatedBy { get; set; }
        public TKey CreatedById { get; set; }
        [Required]
        [StringLength(20)]
        public string CreatedByStaffNo { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [StringLength(300)]
        public string ModifiedBy { get; set; }
        public TKey ModifiedById { get; set; }
        [StringLength(20)]
        public string ModifiedByStaffNo { get; set; }
        public DateTime ModifiedDate { get; set; }
        [StringLength(300)]
        public string AuthorisedBy { get; set; }
        public Nullable<Guid> AuthorisedById { get; set; }
        [StringLength(20)]
        public string AuthorisedByStaffNo { get; set; }
        public Nullable<DateTime> AuthorisedDate { get; set; }
        public RecordStatus Record_Status { get; set; }
        public DataEntityState EntityState { get; set; }
        public DataEntry DataEntry { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        [Column(TypeName = "xml")]
        public string ChangeLog { get; set; }
    }
}
