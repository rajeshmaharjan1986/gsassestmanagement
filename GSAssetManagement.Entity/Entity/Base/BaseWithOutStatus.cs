﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public class BaseWithOutStatus<TKey>
    {
        [Key]
        [Required]
        public TKey Id { get; set; }
        public int TotalModification { get; set; }
        [StringLength(500)]
        public string FieldString1 { get; set; }
        [StringLength(500)]
        public string FieldString2 { get; set; }
        [StringLength(500)]
        public string FieldString3 { get; set; }
        [StringLength(500)]
        public string FieldString4 { get; set; }
        [StringLength(500)]
        public string FieldString5 { get; set; }
        [StringLength(500)]
        public string FieldString6 { get; set; }
        [StringLength(500)]
        public string FieldString7 { get; set; }
        [StringLength(500)]
        public string FieldString8 { get; set; }
        public string FieldString9 { get; set; }
        [StringLength(500)]
        public string FieldString10 { get; set; }
        [StringLength(500)]
        public string FieldString11 { get; set; }
        [StringLength(500)]
        public string FieldString12 { get; set; }
        [StringLength(500)]
        public string FieldString13 { get; set; }
        [StringLength(500)]
        public string FieldString14 { get; set; }
        [StringLength(500)]
        public string FieldString15 { get; set; }
        [StringLength(500)]
        public string FieldString16 { get; set; }
        [StringLength(500)]
        public string FieldString17 { get; set; }
        [StringLength(500)]
        public string FieldString18 { get; set; }
        [StringLength(500)]
        public string FieldString19 { get; set; }
        [StringLength(500)]
        public string FieldString20 { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        [Column(TypeName = "xml")]
        public string ChangeLog { get; set; }
    }
}
