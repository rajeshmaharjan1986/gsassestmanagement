using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GSAssetManagement.Entity
{
    [Table("ModuleTypeSetup", Schema = "Setting")]
    public class ModuleTypeSetup : OfficeBaseEntity<Guid>
    {
        public ModuleTypeSetup()
        {
        }
        [Required]
        [StringLength(200)]
        public string ModuleName { get; set; }
        [Required]
        [StringLength(300)]
        public string Description { get; set; }

    }
}
