using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GSAssetManagement.Entity
{
    [Table("ModuleHtmlAttributeSetup", Schema = "Setting")]
    public class ModuleHtmlAttributeSetup : OfficeBaseEntity<Guid>
    {
        public ModuleHtmlAttributeSetup()
        {
        }
        [Required]
        public Guid ModuleBussinesLogicSetupId { get; set; }
        [Required]
        [StringLength(200)]
        public string AttributeType { get; set; }
        [Required]
        public string Value { get; set; }

        [ForeignKey("ModuleBussinesLogicSetupId")]
        public virtual ModuleBussinesLogicSetup ModuleBussinesLogicSetup { get; set; }
    }
}
