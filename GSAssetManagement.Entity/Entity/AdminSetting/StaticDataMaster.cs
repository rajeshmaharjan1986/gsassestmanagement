using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GSAssetManagement.Entity
{
    [Table("StaticDataMaster", Schema = "Setting")]
    public class StaticDataMaster : OfficeBaseEntity<Guid>
    {
        public StaticDataMaster()
        {
            StaticDataDetailss = new HashSet<StaticDataDetails>();
        }
        [Required]
        [StringLength(300)]
        public string Title { get; set; }
        [Required]
        [StringLength(500)]
        public string Description { get; set; }

        public virtual ICollection<StaticDataDetails> StaticDataDetailss { get; set; }
    }
}
