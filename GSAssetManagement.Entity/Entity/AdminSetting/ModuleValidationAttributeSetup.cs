using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GSAssetManagement.Entity
{
    [Table("ModuleValidationAttributeSetup", Schema = "Setting")]
    public class ModuleValidationAttributeSetup : OfficeBaseEntity<Guid>
    {
        public ModuleValidationAttributeSetup()
        {
        }
        [Required]
        public Guid ModuleBussinesLogicSetupId { get; set; }
        [Required]
        [StringLength(200)]
        public string AttributeType { get; set; }
        [Required]
        public string Value { get; set; }
        [Required]
        [StringLength(500)]
        public string ErrorMessage { get; set; }

        [ForeignKey("ModuleBussinesLogicSetupId")]
        public virtual ModuleBussinesLogicSetup ModuleBussinesLogicSetup { get; set; }
    }
}
