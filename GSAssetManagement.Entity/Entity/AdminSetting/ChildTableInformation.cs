using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GSAssetManagement.Entity
{
    [Table("ChildTableInformation", Schema = "Setting")]
    public class ChildTableInformation : OfficeBaseEntity<Guid>
    {
        public ChildTableInformation()
        {
        }
        [Required]
        public Guid ModuleSetupId { get; set; }
        [Required]
        [StringLength(200)]
        public string Title { get; set; }
        [Required]
        [StringLength(200)]
        public string Name { get; set; }
        [Required]
        public string Url { get; set; }
        [Required]
        public int OrderValue { get; set; }

        [ForeignKey("ModuleSetupId")]
        public virtual ModuleSetup ModuleSetup { get; set; }
    }
}
