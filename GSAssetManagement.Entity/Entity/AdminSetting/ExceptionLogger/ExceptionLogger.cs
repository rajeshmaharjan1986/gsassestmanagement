﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    public class ExceptionLogger : OfficeBaseEntity<Guid>
    {
        [Required]
        public string ExceptionMessage { get; set; }
        [StringLength(100)]
        [Required]
        public string ControllerName { get; set; }
        public string ExceptionStackTrace { get; set; }
    }
}
