﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    [Table("AssetDetail", Schema = "AssetTypeSetting")]
    public class AssetDetail : OfficeBaseEntity<Guid>
    {
        public Guid AssetCategoryId { get; set; }
        [StringLength(20)]
        public string AssetCode { get; set; }
        [StringLength(100)]
        public string AssetName { get; set; }
        public int AssetNumber { get; set; }
        [Required]
        [StringLength(20)]
        public string BranchCode { get; set; }
        [StringLength(100)]
        public string BranchName { get; set; }
        [StringLength(20)]
        public string DepartmentCode { get; set; }
        [StringLength(100)]
        public string DepartmentName { get; set; }
        [ForeignKey("AssetCategoryId")]
        public virtual AssetCategory AssetCategory { get; set; }
    }
}
