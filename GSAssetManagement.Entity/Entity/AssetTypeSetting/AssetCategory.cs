﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    [Table("AssetCategory", Schema = "AssetTypeSetting")]
    public class AssetCategory : OfficeBaseEntity<Guid>
    {
        [Required]
        [StringLength(20)]
        public string CategoryCode { get; set; }
        [Required]
        [StringLength(100)]
        public string CategoryName { get; set; }
        [StringLength(1000)]
        public string Description { get; set; }
    }
}
