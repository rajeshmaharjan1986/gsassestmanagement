﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.Entity
{
    [Table("AssetExtraDetail", Schema = "AssetTypeSetting")]
    public class AssetExtraDetail : BaseWithOutStatus<Guid>
    {
        public Guid AssetDetailId { get; set; }
        [Required]
        [StringLength(50)]
        public string Location { get; set; }
        public Nullable<DateTime> PurchaseDate { get; set; }
        public Nullable<DateTime> ExpireDate { get; set; }

        [ForeignKey("AssetDetailId")]
        public virtual AssetDetail AssetDetail { get; set; }
    }
}
