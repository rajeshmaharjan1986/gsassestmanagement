﻿using GSAssetManagement.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GSAssetManagement.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
            // Get the ObjectContext related to this DbContext
            var objectContext = (this as IObjectContextAdapter).ObjectContext;
            // Sets the command timeout for all the commands
            objectContext.CommandTimeout = 180;
            this.Configuration.LazyLoadingEnabled = false;
        }

        static ApplicationDbContext()
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            // If you have deployed application then use it
            Database.SetInitializer<ApplicationDbContext>(null);


            //Enable for firsttime 
            //Database.SetInitializer<ApplicationDbContext>(new ApplicationDbInitializer());

        }

        public new DbEntityEntry Entry(object entity)
        {
            return base.Entry(entity);
        }

        public new DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class
        {
            return base.Entry<TEntity>(entity);
        }

        public override DbSet<TEntity> Set<TEntity>()
        {
            return base.Set<TEntity>();
        }

        public override DbSet Set(Type entityType)
        {
            return base.Set(entityType);
        }


        public Database DataBaseInfo
        {
            get
            {
                return base.Database;
            }
        }

        #region DbSet

        #region Administrator

        public virtual IDbSet<ApplicationGroup> ApplicationGroups { get; set; }
        public override IDbSet<ApplicationRole> Roles { get => base.Roles; set => base.Roles = value; }
        public virtual IDbSet<ApplicationRoleDetails> ApplicationRoleDetails { get; set; }
        public override IDbSet<ApplicationUser> Users { get => base.Users; set => base.Users = value; }
        public virtual IDbSet<ApplicationUserClaim> ApplicationUserClaims { get; set; }
        public virtual IDbSet<ApplicationUserGroup> ApplicationUserGroups { get; set; }
        public virtual IDbSet<ApplicationUserLogin> ApplicationUserLogins { get; set; }
        public virtual IDbSet<ApplicationUserRole> ApplicationUserRoles { get; set; }

        #endregion Administrator     

        #region Setting

        public virtual DbSet<ModuleTypeSetup> ModuleTypeSetup { get; set; }
        public virtual DbSet<ModuleSetup> ModuleSetups { get; set; }
        public virtual DbSet<ModuleBussinesLogicSetup> ModuleBussinesLogicSetups { get; set; }
        public virtual DbSet<ModuleHtmlAttributeSetup> ModuleHtmlAttributeSetups { get; set; }
        public virtual DbSet<ModuleValidationAttributeSetup> ModuleValidationAttributeSetups { get; set; }
        public virtual DbSet<ChildTableInformation> ChildTableInformations { get; set; }
        public virtual DbSet<ExceptionLogger> ExceptionLoggers { get; set; }
        public virtual DbSet<StaticDataDetails> StaticDataDetails { get; set; }
        public virtual DbSet<StaticDataMaster> StaticDataMasters { get; set; }

        #endregion Setting

        #region AssetTypeSetting

        public virtual DbSet<AssetCategory> AssetCategorys { get; set; }
        public virtual DbSet<AssetDetail> AssetDetails { get; set; }
        public virtual DbSet<AssetExtraDetail> AssetExtraDetails { get; set; }

        #endregion AssetTypeSetting

        #region DrillingReport

        public virtual DbSet<AssetWorkingStatus> AssetWorkingStatuses { get; set; }
        public virtual DbSet<BranchDrillingReport> BranchDrillingReports { get; set; }

        #endregion DrillingReport

        #endregion Dbset

        public IEnumerable<T> ExecuteProcedure<T>(string procedureName, params object[] parameters)
        {
            return Database.SqlQuery<T>(procedureName, parameters);

        }

        public Task<int> ExecuteSqlCommandAsync(string sqlQuery, params object[] parameters)
        {
            return Database.ExecuteSqlCommandAsync(sqlQuery, parameters);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); // This needs to go before the other rules!           

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<ApplicationUser>().ToTable("ApplicationUser", "Administrator");
            modelBuilder.Entity<ApplicationRole>().ToTable("ApplicationRole", "Administrator");
            modelBuilder.Entity<ApplicationUserRole>().ToTable("ApplicationUserRole", "Administrator");
            modelBuilder.Entity<ApplicationUserLogin>().ToTable("ApplicationUserLogin", "Administrator");
            modelBuilder.Entity<ApplicationUserClaim>().ToTable("ApplicationUserClaim", "Administrator");


        }

        //commit
        public Task<int> CommitAsync()
        {
            try
            {

                var __ = base.SaveChangesAsync();
                return __;
            }
            catch (DbEntityValidationException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Commit()
        {
            try
            {
                var __ = base.SaveChanges();
                return __;
            }
            catch (DbEntityValidationException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ApplicationDbContext Create()

        {
            return new ApplicationDbContext();
        }
    }
}
