﻿namespace GSAssetManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddcolumninReporttable : DbMigration
    {
        public override void Up()
        {
            AddColumn("DrillingReport.BranchDrillingReport", "ReportYear", c => c.String(maxLength: 20));
            AlterColumn("DrillingReport.BranchDrillingReport", "ReportMonth", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("DrillingReport.BranchDrillingReport", "ReportMonth", c => c.String());
            DropColumn("DrillingReport.BranchDrillingReport", "ReportYear");
        }
    }
}
