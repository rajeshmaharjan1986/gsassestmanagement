﻿namespace GSAssetManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addtableextradetail : DbMigration
    {
        public override void Up()
        {
            AddColumn("AssetTypeSetting.AssetDetail", "AssetNumber", c => c.Int(nullable: false));
            DropColumn("AssetTypeSetting.AssetDetail", "Location");
            DropColumn("AssetTypeSetting.AssetDetail", "PurchaseDate");
            DropColumn("AssetTypeSetting.AssetDetail", "ExpireDate");
        }
        
        public override void Down()
        {
            AddColumn("AssetTypeSetting.AssetDetail", "ExpireDate", c => c.DateTime());
            AddColumn("AssetTypeSetting.AssetDetail", "PurchaseDate", c => c.DateTime());
            AddColumn("AssetTypeSetting.AssetDetail", "Location", c => c.String(nullable: false, maxLength: 50));
            DropColumn("AssetTypeSetting.AssetDetail", "AssetNumber");
        }
    }
}
