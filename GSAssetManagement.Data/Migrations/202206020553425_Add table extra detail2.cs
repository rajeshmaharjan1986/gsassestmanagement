﻿namespace GSAssetManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addtableextradetail2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "AssetTypeSetting.AssetExtraDetail",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AssetDetailId = c.Guid(nullable: false),
                        Location = c.String(nullable: false, maxLength: 50),
                        PurchaseDate = c.DateTime(),
                        ExpireDate = c.DateTime(),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("AssetTypeSetting.AssetDetail", t => t.AssetDetailId)
                .Index(t => t.AssetDetailId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("AssetTypeSetting.AssetExtraDetail", "AssetDetailId", "AssetTypeSetting.AssetDetail");
            DropIndex("AssetTypeSetting.AssetExtraDetail", new[] { "AssetDetailId" });
            DropTable("AssetTypeSetting.AssetExtraDetail");
        }
    }
}
