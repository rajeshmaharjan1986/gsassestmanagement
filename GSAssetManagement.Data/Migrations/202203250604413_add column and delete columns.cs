﻿namespace GSAssetManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcolumnanddeletecolumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("DrillingReport.BranchDrillingReport", "ReportStatus", c => c.String(nullable: false, maxLength: 20));
            DropColumn("DrillingReport.AssetWorkingStatus", "CreatedBy");
            DropColumn("DrillingReport.AssetWorkingStatus", "CreatedById");
            DropColumn("DrillingReport.AssetWorkingStatus", "CreatedByStaffNo");
            DropColumn("DrillingReport.AssetWorkingStatus", "CreatedDate");
            DropColumn("DrillingReport.AssetWorkingStatus", "ModifiedBy");
            DropColumn("DrillingReport.AssetWorkingStatus", "ModifiedById");
            DropColumn("DrillingReport.AssetWorkingStatus", "ModifiedByStaffNo");
            DropColumn("DrillingReport.AssetWorkingStatus", "ModifiedDate");
            DropColumn("DrillingReport.AssetWorkingStatus", "AuthorisedBy");
            DropColumn("DrillingReport.AssetWorkingStatus", "AuthorisedById");
            DropColumn("DrillingReport.AssetWorkingStatus", "AuthorisedByStaffNo");
            DropColumn("DrillingReport.AssetWorkingStatus", "AuthorisedDate");
            DropColumn("DrillingReport.AssetWorkingStatus", "Record_Status");
            DropColumn("DrillingReport.AssetWorkingStatus", "EntityState");
            DropColumn("DrillingReport.AssetWorkingStatus", "DataEntry");
        }
        
        public override void Down()
        {
            AddColumn("DrillingReport.AssetWorkingStatus", "DataEntry", c => c.Int(nullable: false));
            AddColumn("DrillingReport.AssetWorkingStatus", "EntityState", c => c.Int(nullable: false));
            AddColumn("DrillingReport.AssetWorkingStatus", "Record_Status", c => c.Int(nullable: false));
            AddColumn("DrillingReport.AssetWorkingStatus", "AuthorisedDate", c => c.DateTime());
            AddColumn("DrillingReport.AssetWorkingStatus", "AuthorisedByStaffNo", c => c.String(maxLength: 20));
            AddColumn("DrillingReport.AssetWorkingStatus", "AuthorisedById", c => c.Guid());
            AddColumn("DrillingReport.AssetWorkingStatus", "AuthorisedBy", c => c.String(maxLength: 300));
            AddColumn("DrillingReport.AssetWorkingStatus", "ModifiedDate", c => c.DateTime(nullable: false));
            AddColumn("DrillingReport.AssetWorkingStatus", "ModifiedByStaffNo", c => c.String(maxLength: 20));
            AddColumn("DrillingReport.AssetWorkingStatus", "ModifiedById", c => c.Guid(nullable: false));
            AddColumn("DrillingReport.AssetWorkingStatus", "ModifiedBy", c => c.String(maxLength: 300));
            AddColumn("DrillingReport.AssetWorkingStatus", "CreatedDate", c => c.DateTime(nullable: false));
            AddColumn("DrillingReport.AssetWorkingStatus", "CreatedByStaffNo", c => c.String(nullable: false, maxLength: 20));
            AddColumn("DrillingReport.AssetWorkingStatus", "CreatedById", c => c.Guid(nullable: false));
            AddColumn("DrillingReport.AssetWorkingStatus", "CreatedBy", c => c.String(nullable: false, maxLength: 300));
            DropColumn("DrillingReport.BranchDrillingReport", "ReportStatus");
        }
    }
}
