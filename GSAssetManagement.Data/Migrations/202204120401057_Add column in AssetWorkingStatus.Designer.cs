﻿// <auto-generated />
namespace GSAssetManagement.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class AddcolumninAssetWorkingStatus : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddcolumninAssetWorkingStatus));
        
        string IMigrationMetadata.Id
        {
            get { return "202204120401057_Add column in AssetWorkingStatus"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
