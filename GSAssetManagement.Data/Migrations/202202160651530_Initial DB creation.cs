﻿namespace GSAssetManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialDBcreation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Administrator.ApplicationGroup",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                        Email = c.String(nullable: false, maxLength: 256),
                        Remarks = c.String(nullable: false, maxLength: 300),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Administrator.ApplicationUserGroup",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ApplicationUserId = c.Guid(nullable: false),
                        ApplicationGroupId = c.Guid(nullable: false),
                        Remarks = c.String(nullable: false, maxLength: 300),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Administrator.ApplicationGroup", t => t.ApplicationGroupId)
                .ForeignKey("Administrator.ApplicationUser", t => t.ApplicationUserId)
                .Index(t => t.ApplicationUserId)
                .Index(t => t.ApplicationGroupId);
            
            CreateTable(
                "Administrator.ApplicationUser",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FullName = c.String(nullable: false, maxLength: 500),
                        DeptId = c.String(maxLength: 10),
                        DeptName = c.String(maxLength: 200),
                        StaffId = c.String(maxLength: 20),
                        BranchName = c.String(maxLength: 100),
                        Designation = c.String(maxLength: 100),
                        Branch = c.String(nullable: false, maxLength: 10),
                        currentFiscalYear = c.String(maxLength: 50),
                        JobID = c.String(maxLength: 10),
                        FunctionalTitle = c.String(maxLength: 100),
                        CreatedBy = c.String(maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "Administrator.ApplicationUserRole",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("Administrator.ApplicationRole", t => t.RoleId)
                .ForeignKey("Administrator.ApplicationUser", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "Administrator.ApplicationRole",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RoleCode = c.String(nullable: false, maxLength: 20),
                        Remarks = c.String(nullable: false, maxLength: 300),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        CreatedBy = c.String(maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "Administrator.ApplicationRoleDetails",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                        ModuleName = c.String(nullable: false, maxLength: 100),
                        SubModuleName = c.String(nullable: false, maxLength: 100),
                        CanView = c.Boolean(nullable: false),
                        CanCreate = c.Boolean(nullable: false),
                        CanEdit = c.Boolean(nullable: false),
                        CanDelete = c.Boolean(nullable: false),
                        CanAuthorize = c.Boolean(nullable: false),
                        CanDiscard = c.Boolean(nullable: false),
                        CanDownload = c.Boolean(nullable: false),
                        CanAutoAuthorise = c.Boolean(nullable: false),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Administrator.ApplicationRole", t => t.RoleId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "Administrator.ApplicationUserClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Administrator.ApplicationUser", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "Administrator.ApplicationUserLogin",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("Administrator.ApplicationUser", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "AssetTypeSetting.AssetCategory",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CategoryCode = c.String(nullable: false, maxLength: 20),
                        CategoryName = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 1000),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "AssetTypeSetting.AssetDetail",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AssetCategoryId = c.Guid(nullable: false),
                        AssetCode = c.String(nullable: false, maxLength: 20),
                        AssetName = c.String(nullable: false, maxLength: 100),
                        Location = c.String(nullable: false, maxLength: 50),
                        BranchCode = c.String(nullable: false, maxLength: 20),
                        BranchName = c.String(nullable: false, maxLength: 100),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("AssetTypeSetting.AssetCategory", t => t.AssetCategoryId)
                .Index(t => t.AssetCategoryId);
            
            CreateTable(
                "DrillingReport.AssetWorkingStatus",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        BranchDrillingReportId = c.Guid(nullable: false),
                        AssetDetailId = c.Guid(nullable: false),
                        Status = c.String(nullable: false, maxLength: 20),
                        Remarks = c.String(maxLength: 1000),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("AssetTypeSetting.AssetDetail", t => t.AssetDetailId)
                .ForeignKey("DrillingReport.BranchDrillingReport", t => t.BranchDrillingReportId)
                .Index(t => t.BranchDrillingReportId)
                .Index(t => t.AssetDetailId);
            
            CreateTable(
                "DrillingReport.BranchDrillingReport",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        BranchCode = c.String(nullable: false, maxLength: 20),
                        ReportDateInBS = c.String(nullable: false, maxLength: 20),
                        ReportDate = c.DateTime(nullable: false),
                        ReportPeriod = c.String(nullable: false, maxLength: 50),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Setting.ChildTableInformation",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ModuleSetupId = c.Guid(nullable: false),
                        Title = c.String(nullable: false, maxLength: 200),
                        Name = c.String(nullable: false, maxLength: 200),
                        Url = c.String(nullable: false),
                        OrderValue = c.Int(nullable: false),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Setting.ModuleSetup", t => t.ModuleSetupId)
                .Index(t => t.ModuleSetupId);
            
            CreateTable(
                "Setting.ModuleSetup",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ModuleTypeSetupId = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 200),
                        ModuleCode = c.String(nullable: false, maxLength: 20),
                        Description = c.String(nullable: false, maxLength: 300),
                        DatabaseTable = c.String(nullable: false, maxLength: 200),
                        ApplicationClass = c.String(nullable: false, maxLength: 200),
                        EntryType = c.String(nullable: false, maxLength: 5),
                        IsParent = c.Boolean(nullable: false),
                        ParentModule = c.String(maxLength: 200),
                        ChangeLogRequired = c.Boolean(nullable: false),
                        MakerCheckerRequired = c.Boolean(nullable: false),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Setting.ModuleTypeSetup", t => t.ModuleTypeSetupId)
                .Index(t => t.ModuleTypeSetupId);
            
            CreateTable(
                "Setting.ModuleBussinesLogicSetup",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ModuleSetupId = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 300),
                        ColumnName = c.String(nullable: false, maxLength: 200),
                        Description = c.String(nullable: false, maxLength: 300),
                        DataType = c.String(nullable: false, maxLength: 100),
                        StringLength = c.Int(nullable: false),
                        Required = c.Boolean(nullable: false),
                        Position = c.Int(nullable: false),
                        HtmlDataType = c.String(nullable: false, maxLength: 100),
                        HtmlSize = c.Int(nullable: false),
                        LabelIcon = c.String(maxLength: 100),
                        DefaultValue = c.String(),
                        FilePath = c.String(),
                        CanUpdate = c.Boolean(nullable: false),
                        IsParentColumn = c.Boolean(nullable: false),
                        HelpMessage = c.String(nullable: false, maxLength: 300),
                        SummaryHeader = c.Boolean(nullable: false),
                        ParameterForSummaryHeader = c.Boolean(nullable: false),
                        IsForeignKey = c.Boolean(nullable: false),
                        ForeignTable = c.String(maxLength: 200),
                        DataSource = c.String(),
                        IsStaticDropDown = c.Boolean(nullable: false),
                        ParameterisedDataSorce = c.Boolean(nullable: false),
                        Parameters = c.String(),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Setting.ModuleSetup", t => t.ModuleSetupId)
                .Index(t => t.ModuleSetupId);
            
            CreateTable(
                "Setting.ModuleHtmlAttributeSetup",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ModuleBussinesLogicSetupId = c.Guid(nullable: false),
                        AttributeType = c.String(nullable: false, maxLength: 200),
                        Value = c.String(nullable: false),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Setting.ModuleBussinesLogicSetup", t => t.ModuleBussinesLogicSetupId)
                .Index(t => t.ModuleBussinesLogicSetupId);
            
            CreateTable(
                "Setting.ModuleValidationAttributeSetup",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ModuleBussinesLogicSetupId = c.Guid(nullable: false),
                        AttributeType = c.String(nullable: false, maxLength: 200),
                        Value = c.String(nullable: false),
                        ErrorMessage = c.String(nullable: false, maxLength: 500),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Setting.ModuleBussinesLogicSetup", t => t.ModuleBussinesLogicSetupId)
                .Index(t => t.ModuleBussinesLogicSetupId);
            
            CreateTable(
                "Setting.ModuleTypeSetup",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ModuleName = c.String(nullable: false, maxLength: 200),
                        Description = c.String(nullable: false, maxLength: 300),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ExceptionLoggers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ExceptionMessage = c.String(nullable: false),
                        ControllerName = c.String(nullable: false, maxLength: 100),
                        ExceptionStackTrace = c.String(),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Setting.StaticDataDetails",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        StaticDataMasterId = c.Guid(nullable: false),
                        ColumnName = c.String(nullable: false, maxLength: 300),
                        Title = c.String(nullable: false, maxLength: 300),
                        Value = c.String(nullable: false, maxLength: 300),
                        OrderValue = c.String(nullable: false, maxLength: 300),
                        Parameter1 = c.String(maxLength: 300),
                        Parameter2 = c.String(maxLength: 300),
                        Parameter3 = c.String(maxLength: 300),
                        Parameter4 = c.String(maxLength: 300),
                        Parameter5 = c.String(maxLength: 300),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Setting.StaticDataMaster", t => t.StaticDataMasterId)
                .Index(t => t.StaticDataMasterId);
            
            CreateTable(
                "Setting.StaticDataMaster",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(nullable: false, maxLength: 300),
                        Description = c.String(nullable: false, maxLength: 500),
                        TotalModification = c.Int(nullable: false),
                        FieldString1 = c.String(maxLength: 500),
                        FieldString2 = c.String(maxLength: 500),
                        FieldString3 = c.String(maxLength: 500),
                        FieldString4 = c.String(maxLength: 500),
                        FieldString5 = c.String(maxLength: 500),
                        FieldString6 = c.String(maxLength: 500),
                        FieldString7 = c.String(maxLength: 500),
                        FieldString8 = c.String(maxLength: 500),
                        FieldString9 = c.String(),
                        FieldString10 = c.String(maxLength: 500),
                        FieldString11 = c.String(maxLength: 500),
                        FieldString12 = c.String(maxLength: 500),
                        FieldString13 = c.String(maxLength: 500),
                        FieldString14 = c.String(maxLength: 500),
                        FieldString15 = c.String(maxLength: 500),
                        FieldString16 = c.String(maxLength: 500),
                        FieldString17 = c.String(maxLength: 500),
                        FieldString18 = c.String(maxLength: 500),
                        FieldString19 = c.String(maxLength: 500),
                        FieldString20 = c.String(maxLength: 500),
                        CreatedBy = c.String(nullable: false, maxLength: 300),
                        CreatedById = c.Guid(nullable: false),
                        CreatedByStaffNo = c.String(nullable: false, maxLength: 20),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedBy = c.String(maxLength: 300),
                        ModifiedById = c.Guid(nullable: false),
                        ModifiedByStaffNo = c.String(maxLength: 20),
                        ModifiedDate = c.DateTime(nullable: false),
                        AuthorisedBy = c.String(maxLength: 300),
                        AuthorisedById = c.Guid(),
                        AuthorisedByStaffNo = c.String(maxLength: 20),
                        AuthorisedDate = c.DateTime(),
                        Record_Status = c.Int(nullable: false),
                        EntityState = c.Int(nullable: false),
                        DataEntry = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ChangeLog = c.String(storeType: "xml"),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Setting.StaticDataDetails", "StaticDataMasterId", "Setting.StaticDataMaster");
            DropForeignKey("Setting.ModuleSetup", "ModuleTypeSetupId", "Setting.ModuleTypeSetup");
            DropForeignKey("Setting.ModuleValidationAttributeSetup", "ModuleBussinesLogicSetupId", "Setting.ModuleBussinesLogicSetup");
            DropForeignKey("Setting.ModuleBussinesLogicSetup", "ModuleSetupId", "Setting.ModuleSetup");
            DropForeignKey("Setting.ModuleHtmlAttributeSetup", "ModuleBussinesLogicSetupId", "Setting.ModuleBussinesLogicSetup");
            DropForeignKey("Setting.ChildTableInformation", "ModuleSetupId", "Setting.ModuleSetup");
            DropForeignKey("DrillingReport.AssetWorkingStatus", "BranchDrillingReportId", "DrillingReport.BranchDrillingReport");
            DropForeignKey("DrillingReport.AssetWorkingStatus", "AssetDetailId", "AssetTypeSetting.AssetDetail");
            DropForeignKey("AssetTypeSetting.AssetDetail", "AssetCategoryId", "AssetTypeSetting.AssetCategory");
            DropForeignKey("Administrator.ApplicationUserGroup", "ApplicationUserId", "Administrator.ApplicationUser");
            DropForeignKey("Administrator.ApplicationUserLogin", "UserId", "Administrator.ApplicationUser");
            DropForeignKey("Administrator.ApplicationUserClaim", "UserId", "Administrator.ApplicationUser");
            DropForeignKey("Administrator.ApplicationUserRole", "UserId", "Administrator.ApplicationUser");
            DropForeignKey("Administrator.ApplicationUserRole", "RoleId", "Administrator.ApplicationRole");
            DropForeignKey("Administrator.ApplicationRoleDetails", "RoleId", "Administrator.ApplicationRole");
            DropForeignKey("Administrator.ApplicationUserGroup", "ApplicationGroupId", "Administrator.ApplicationGroup");
            DropIndex("Setting.StaticDataDetails", new[] { "StaticDataMasterId" });
            DropIndex("Setting.ModuleValidationAttributeSetup", new[] { "ModuleBussinesLogicSetupId" });
            DropIndex("Setting.ModuleHtmlAttributeSetup", new[] { "ModuleBussinesLogicSetupId" });
            DropIndex("Setting.ModuleBussinesLogicSetup", new[] { "ModuleSetupId" });
            DropIndex("Setting.ModuleSetup", new[] { "ModuleTypeSetupId" });
            DropIndex("Setting.ChildTableInformation", new[] { "ModuleSetupId" });
            DropIndex("DrillingReport.AssetWorkingStatus", new[] { "AssetDetailId" });
            DropIndex("DrillingReport.AssetWorkingStatus", new[] { "BranchDrillingReportId" });
            DropIndex("AssetTypeSetting.AssetDetail", new[] { "AssetCategoryId" });
            DropIndex("Administrator.ApplicationUserLogin", new[] { "UserId" });
            DropIndex("Administrator.ApplicationUserClaim", new[] { "UserId" });
            DropIndex("Administrator.ApplicationRoleDetails", new[] { "RoleId" });
            DropIndex("Administrator.ApplicationRole", "RoleNameIndex");
            DropIndex("Administrator.ApplicationUserRole", new[] { "RoleId" });
            DropIndex("Administrator.ApplicationUserRole", new[] { "UserId" });
            DropIndex("Administrator.ApplicationUser", "UserNameIndex");
            DropIndex("Administrator.ApplicationUserGroup", new[] { "ApplicationGroupId" });
            DropIndex("Administrator.ApplicationUserGroup", new[] { "ApplicationUserId" });
            DropTable("Setting.StaticDataMaster");
            DropTable("Setting.StaticDataDetails");
            DropTable("dbo.ExceptionLoggers");
            DropTable("Setting.ModuleTypeSetup");
            DropTable("Setting.ModuleValidationAttributeSetup");
            DropTable("Setting.ModuleHtmlAttributeSetup");
            DropTable("Setting.ModuleBussinesLogicSetup");
            DropTable("Setting.ModuleSetup");
            DropTable("Setting.ChildTableInformation");
            DropTable("DrillingReport.BranchDrillingReport");
            DropTable("DrillingReport.AssetWorkingStatus");
            DropTable("AssetTypeSetting.AssetDetail");
            DropTable("AssetTypeSetting.AssetCategory");
            DropTable("Administrator.ApplicationUserLogin");
            DropTable("Administrator.ApplicationUserClaim");
            DropTable("Administrator.ApplicationRoleDetails");
            DropTable("Administrator.ApplicationRole");
            DropTable("Administrator.ApplicationUserRole");
            DropTable("Administrator.ApplicationUser");
            DropTable("Administrator.ApplicationUserGroup");
            DropTable("Administrator.ApplicationGroup");
        }
    }
}
