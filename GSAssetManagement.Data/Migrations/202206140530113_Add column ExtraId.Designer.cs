﻿// <auto-generated />
namespace GSAssetManagement.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class AddcolumnExtraId : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddcolumnExtraId));
        
        string IMigrationMetadata.Id
        {
            get { return "202206140530113_Add column ExtraId"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
