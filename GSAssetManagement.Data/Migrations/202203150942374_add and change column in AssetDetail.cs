﻿namespace GSAssetManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addandchangecolumninAssetDetail : DbMigration
    {
        public override void Up()
        {
            AddColumn("AssetTypeSetting.AssetDetail", "PurchaseDate", c => c.DateTime(nullable: false));
            AddColumn("AssetTypeSetting.AssetDetail", "ExpireDate", c => c.DateTime());
            AlterColumn("AssetTypeSetting.AssetDetail", "BranchName", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("AssetTypeSetting.AssetDetail", "BranchName", c => c.String(nullable: false, maxLength: 100));
            DropColumn("AssetTypeSetting.AssetDetail", "ExpireDate");
            DropColumn("AssetTypeSetting.AssetDetail", "PurchaseDate");
        }
    }
}
