﻿namespace GSAssetManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddandchangeColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("Administrator.ApplicationUser", "LastLoginDate", c => c.DateTime());
            AlterColumn("AssetTypeSetting.AssetDetail", "AssetName", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("AssetTypeSetting.AssetDetail", "AssetName", c => c.String(nullable: false, maxLength: 100));
            DropColumn("Administrator.ApplicationUser", "LastLoginDate");
        }
    }
}
