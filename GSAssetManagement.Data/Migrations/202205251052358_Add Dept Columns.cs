﻿namespace GSAssetManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDeptColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("AssetTypeSetting.AssetDetail", "DepartmentCode", c => c.String(maxLength: 20));
            AddColumn("AssetTypeSetting.AssetDetail", "DepartmentName", c => c.String(maxLength: 100));
            AddColumn("DrillingReport.BranchDrillingReport", "DepartmentCode", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            DropColumn("DrillingReport.BranchDrillingReport", "DepartmentCode");
            DropColumn("AssetTypeSetting.AssetDetail", "DepartmentName");
            DropColumn("AssetTypeSetting.AssetDetail", "DepartmentCode");
        }
    }
}
