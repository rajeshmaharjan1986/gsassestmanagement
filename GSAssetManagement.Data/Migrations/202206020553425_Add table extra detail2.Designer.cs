﻿// <auto-generated />
namespace GSAssetManagement.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class Addtableextradetail2 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Addtableextradetail2));
        
        string IMigrationMetadata.Id
        {
            get { return "202206020553425_Add table extra detail2"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
