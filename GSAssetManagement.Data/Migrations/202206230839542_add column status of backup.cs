﻿namespace GSAssetManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcolumnstatusofbackup : DbMigration
    {
        public override void Up()
        {
            AddColumn("DrillingReport.BranchDrillingReport", "CCTVBackupStatus", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("DrillingReport.BranchDrillingReport", "CCTVBackupStatus");
        }
    }
}
