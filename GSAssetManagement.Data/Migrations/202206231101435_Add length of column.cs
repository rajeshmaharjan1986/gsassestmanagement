﻿namespace GSAssetManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addlengthofcolumn : DbMigration
    {
        public override void Up()
        {
            AlterColumn("DrillingReport.BranchDrillingReport", "CCTVBackupStatus", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            AlterColumn("DrillingReport.BranchDrillingReport", "CCTVBackupStatus", c => c.String(nullable: false));
        }
    }
}
