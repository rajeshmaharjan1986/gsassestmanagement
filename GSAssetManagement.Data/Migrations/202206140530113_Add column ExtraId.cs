﻿namespace GSAssetManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddcolumnExtraId : DbMigration
    {
        public override void Up()
        {
            AddColumn("DrillingReport.AssetWorkingStatus", "AssetExtraDetailId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("DrillingReport.AssetWorkingStatus", "AssetExtraDetailId");
        }
    }
}
