﻿namespace GSAssetManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changenullableinAssetDetails : DbMigration
    {
        public override void Up()
        {
            AlterColumn("AssetTypeSetting.AssetDetail", "AssetCode", c => c.String(maxLength: 20));
            AlterColumn("AssetTypeSetting.AssetDetail", "PurchaseDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("AssetTypeSetting.AssetDetail", "PurchaseDate", c => c.DateTime(nullable: false));
            AlterColumn("AssetTypeSetting.AssetDetail", "AssetCode", c => c.String(nullable: false, maxLength: 20));
        }
    }
}
