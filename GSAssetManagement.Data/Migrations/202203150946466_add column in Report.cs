﻿namespace GSAssetManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcolumninReport : DbMigration
    {
        public override void Up()
        {
            AddColumn("DrillingReport.BranchDrillingReport", "ReportMonth", c => c.String());
            AddColumn("DrillingReport.BranchDrillingReport", "CCTVBackupRemarks", c => c.String(nullable: false, maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("DrillingReport.BranchDrillingReport", "CCTVBackupRemarks");
            DropColumn("DrillingReport.BranchDrillingReport", "ReportMonth");
        }
    }
}
