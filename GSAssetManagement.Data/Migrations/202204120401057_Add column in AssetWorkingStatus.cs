﻿namespace GSAssetManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddcolumninAssetWorkingStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("DrillingReport.AssetWorkingStatus", "ExpireDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("DrillingReport.AssetWorkingStatus", "ExpireDate");
        }
    }
}
