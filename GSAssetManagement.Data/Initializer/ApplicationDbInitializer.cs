﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using GSAssetManagement.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSAssetManagement.Data;

namespace GSAssetManagement.Data
{
    public class ApplicationDbInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            InitializeIdentityForEF(context);
            context.Database.Initialize(true);
            base.Seed(context);
        }

        //Create User=Admin@Admin.com with password=Admin@123456 in the Admin role        
        public static void InitializeIdentityForEF(ApplicationDbContext context)
        {
            try
            {

                const string name = "administrator@sanimabank.com";
                const string email = "administrator@sanimabank.com";
                const string password = "Satellite@123456";
                const string roleName = "SuperAdmin";


                if (!context.Users.Any(user => string.Compare(user.UserName, name, StringComparison.CurrentCultureIgnoreCase) == 0))
                {
                    var userstore = new UserStore<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>(context);
                    var usermanager = new UserManager<ApplicationUser, Guid>(userstore);

                    var administrator = new ApplicationUser
                    {
                        Id = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                        FullName = "administrator",
                        Branch = "000",
                        UserName = name,
                        Email = email,
                        EmailConfirmed = true,
                        CreatedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                        ModifiedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                        AuthorisedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        AuthorisedDate = DateTime.Now,
                        Record_Status = RecordStatus.Active,
                        CreatedBy = "administrator",
                        ModifiedBy = "administrator",
                        AuthorisedBy = "administrator"

                    };

                    usermanager.Create(administrator, password);
                    //usermanager.AddToRole(administrator.Id, roleName);


                    var rolestore = new RoleStore<ApplicationRole, Guid, ApplicationUserRole>(context);
                    var rolemanager = new RoleManager<ApplicationRole, Guid>(rolestore);

                    ApplicationRole _role = new ApplicationRole
                    {
                        Id = Guid.NewGuid(),
                        Name = roleName,
                        RoleCode = "001-SUP-RO",
                        Remarks = "Superadmin role for development.",
                        CreatedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                        ModifiedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        AuthorisedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                        AuthorisedDate = DateTime.Now,
                        Record_Status = RecordStatus.Active,
                        CreatedBy = "administrator",
                        ModifiedBy = "administrator",
                        AuthorisedBy = "administrator"
                    };

                    rolemanager.Create(_role);




                    context.ApplicationUserRoles.Add(new ApplicationUserRole()
                    {
                        UserId = administrator.Id,
                        RoleId = _role.Id,
                    });

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
