﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GSAssetManagement.Service
{
    public class APIRequestAndResponseClass
    {
    }

    public class UserDetail
    {
        public string applicationcode { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }

    public class LoginToken
    {
        public string token { get; set; }
    }

    public class BranchList
    {
        public string BranchName { get; set; }
        public string BranchID { get; set; }
        public string Email { get; set; }
    }

    public class ResponseResult {
        public bool success { get; set; }
        public string token { get; set; }
        public string DeptId { get; set; }
        public string DeptName { get; set; }
        public string StaffId { get; set; }
        public string Username { get; set; }
        public string BranchName { get; set; }
        public string Designation { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Branch { get; set; }
        public string currentFiscalYear { get; set; }

    }

    public class StaffList {
        public string fullName { get; set; }
        public string JobID { get; set; }
        public string Username { get; set; }
        public string StaffId { get; set; }
        public string DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string BranchName { get; set; }
        public string FunctionalTitle { get; set; }
        public string BranchID { get; set; }
    }

    public class StaffDetail
    {
        public string staffId { get; set; }
        public StaffBranch branch { get; set; }
        public string department { get; set; }
        public string departmentId { get; set; }
        public string[] units { get; set; }
        public string[] subUnits { get; set; }
        public string jobId { get; set; }
        public string jobName { get; set; }
        public StaffName name { get; set; }
        public object phone { get; set; }
        public string designation { get; set; }
        public string designationId { get; set; }
        public string level { get; set; }
        public string functionalTitle { get; set; }
        public string dateOfBirth { get; set; }
        public string maritalStatus { get; set; }
        public object gender { get; set; }
        public string attendanceCode { get; set; }
        public string email { get; set; }
        public string username { get; set; }
        public string fatherName { get; set; }
        public string motherName { get; set; }
        public string preJoinDate { get; set; }
        public string empPanNo { get; set; }
        public string employmentType { get; set; }
        public string qualification { get; set; }
        public object address { get; set; }
        public object currentAddress { get; set; }
        public object identityDetail { get; set; }
        public string religion { get; set; }
        public string BankAccNo { get; set; }
        public object emergencyInformation { get; set; }
        public object supervisor { get; set; }
        public object secondSupervisor { get; set; }
        public object staffsUnderSupervision { get; set; }
    }
    public class StaffBranch
    {
        public string branchName { get; set; }
        public string branchCode { get; set; }
    }
    public class StaffName
    {
        public string title { get; set; }
        public string fullName { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }

    }
}