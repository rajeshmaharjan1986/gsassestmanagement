﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Script.Serialization;
using GSAssetManagement.Entity.APIRequestResponse;

namespace GSAssetManagement.Service
{
    public class APIConsumtionService
    {
        string bassAddress = ConfigurationManager.AppSettings["BassAddress"].ToString();
        //string bassAddress = @"http://192.168.1.52:8090/";
        public ResponseResult VerifyLogin(string userName,string password)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(bassAddress);
                //var user = new UserDetail();

                UserDetail user = new UserDetail();
                user.username = userName;
                user.password = password;
                user.applicationcode = "VMS";


                var responseTask = client.PostAsync("api/central/login",
                    new StringContent(new JavaScriptSerializer().Serialize(user),
                    Encoding.UTF8, "application/json")).Result;

                var responseResult = responseTask.Content.ReadAsStringAsync().Result;
                var newobj = new JavaScriptSerializer().Deserialize<ResponseResult>(responseResult);

                if (responseTask.IsSuccessStatusCode)
                {
                    return newobj;
                }
                else
                {
                    return null;
                }
            }
            
        }

        public ResponseResult VerifyLogin(string token)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(bassAddress);
                //var user = new UserDetail();

                LoginToken user = new LoginToken();
                user.token = token;

                //HttpContent httpContent = new HttpContent();

                //var responseTask = client.PostAsync("api/dms/verify/token",
                //    new StringContent(new JavaScriptSerializer().Serialize(user),
                //    Encoding.UTF8, "application/json")).Result;


                //var client = new RestClient("http://192.168.1.52:8090/api/verify/token");
                //var request = new RestRequest(Method.POST);
                //request.AddHeader("content-type", "application/x-www-form-urlencoded");
                //request.AddHeader("cache-control", "no-cache");
                //request.AddParameter("Authorization", "Bearer " + loginid, ParameterType.HttpHeader);
                //IRestResponse response = client.Execute(request);

                using (var message = new HttpRequestMessage(HttpMethod.Post, "api/dms/verify/token"))
                {
                    message.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var response = client.SendAsync(message).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var responseResult = response.Content.ReadAsStringAsync().Result;
                        var newobj = new JavaScriptSerializer().Deserialize<ResponseResult>(responseResult);

                        return newobj;
                    }
                }

                return null;


                //if (responseTask.StatusCode == System.Net.HttpStatusCode.OK)
                //{
                //    var responseResult = responseTask.Content.ReadAsStringAsync().Result;
                //    var newobj = new JavaScriptSerializer().Deserialize<ResponseResult>(responseResult);

                //    if (responseTask.IsSuccessStatusCode)
                //    {
                //        return newobj;
                //    }
                //    else
                //    {
                //        return null;
                //    }
                //}
                //else {
                //    return null;
                //}
            }

        }

        public List<BranchList> GetBranchList() {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(bassAddress);

                var responseTask = client.GetAsync("api/central/branches").Result;

                var responseResult = responseTask.Content.ReadAsStringAsync().Result;
                var newobj = new JavaScriptSerializer().Deserialize<List<BranchList>>(responseResult);

                if (responseTask.IsSuccessStatusCode)
                {
                    var branch = newobj.Where(x => x.BranchID == "0").FirstOrDefault();
                    newobj.Remove(branch);
                    return newobj;
                }
                else
                {
                    return null;
                }
            }
        }
        public List<OfficeRoleList> GetRoleList()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(bassAddress);

                var responseTask = client.GetAsync("api/sanimacircle/roles").Result;

                var responseResult = responseTask.Content.ReadAsStringAsync().Result;
                var newobj = new JavaScriptSerializer().Deserialize< OfficeRoleResponses> (responseResult);

                if (responseTask.IsSuccessStatusCode)
                {
                    return newobj.data;
                }
                else
                {
                    return null;
                }
            }
        }

        public BranchList GetBranch(string BranchCode)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(bassAddress);

                var responseTask = client.GetAsync("api/central/branches").Result;

                var responseResult = responseTask.Content.ReadAsStringAsync().Result;
                var newobj = new JavaScriptSerializer().Deserialize<List<BranchList>>(responseResult);

                if (responseTask.IsSuccessStatusCode)
                {
                    BranchList branch = newobj.Where(x => x.BranchID == BranchCode).FirstOrDefault();
                    return branch;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<StaffList> GetStaffList(string departmentId,string branchId,string jobId,string unitId,string subUnitId) {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(bassAddress);

                string url = "api/central/staffs?";
                if (!string.IsNullOrEmpty(departmentId)) {
                    url = url + "DepartmentID=" + departmentId + "&";
                }
                if (!string.IsNullOrEmpty(branchId))
                {
                    url = url + "BranchID=" + branchId + "&";
                }
                if (!string.IsNullOrEmpty(jobId))
                {
                    url = url + "JobID=" + jobId + "&";
                }
                if (!string.IsNullOrEmpty(unitId))
                {
                    url = url + "UnitID=" + unitId + "&";
                }
                if (!string.IsNullOrEmpty(subUnitId))
                {
                    url = url + "SubUnitID=" + subUnitId + "&";
                }

                url = url.Substring(0, url.Length - 1);

                //api/central/staffs?DepartmentID=1&BranchID=1&JobID=office&UnitID=1&SubUnitID=2
                var responseTask = client.GetAsync(url).Result;

                var responseResult = responseTask.Content.ReadAsStringAsync().Result;
                var newobj = new JavaScriptSerializer().Deserialize<List<StaffList>>(responseResult);

                if (responseTask.IsSuccessStatusCode)
                {
                    return newobj;
                }
                else
                {
                    return null;
                }
            }
        }

        //public StaffList GetStaff(string staffId)
        //{
        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(bassAddress);

        //        string url = "api/central/staffs";

        //        //api/central/staffs?DepartmentID=1&BranchID=1&JobID=office&UnitID=1&SubUnitID=2
        //        var responseTask = client.GetAsync(url).Result;

        //        var responseResult = responseTask.Content.ReadAsStringAsync().Result;
        //        var newobj = new JavaScriptSerializer().Deserialize<List<StaffList>>(responseResult);

        //        var staff = newobj.Where(x => x.StaffId == staffId).FirstOrDefault();

        //        if (responseTask.IsSuccessStatusCode)
        //        {
        //            return staff;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}

        public StaffDetail GetStaff(string staffId)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(bassAddress);

                string url = "api/staff/" + staffId;

                //api/central/staffs?DepartmentID=1&BranchID=1&JobID=office&UnitID=1&SubUnitID=2
                var responseTask = client.GetAsync(url).Result;

                var responseResult = responseTask.Content.ReadAsStringAsync().Result;
                if (responseResult.Contains("No Record Found for Provided StaffID"))
                {
                    return null;
                }
                else
                {
                    ////Deserializing Json object from string
                    //DataContractJsonSerializer jsonObjectPersonInfo =
                    //    new DataContractJsonSerializer(typeof(PersonModel));
                    //MemoryStream stream =
                    //    new MemoryStream(Encoding.UTF8.GetBytes(userInfo));
                    //PersonModel personInfoModel =
                    //    (PersonModel)jsonObjectPersonInfo.ReadObject(stream);
                    try
                    {
                        var newobj = new JavaScriptSerializer().Deserialize<StaffDetail>(responseResult);
                        return newobj;
                    }
                    catch (Exception ex) {
                        throw ex;
                    }
                }
            }
        }

        public List<DepartmentList> GetDepartmentList(string departmentId, string unitId, string subUnitId)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(bassAddress);

                string url = "api/central/departments?";
                //department_id=67&unit_id=1&sub_unit_id=1
                if (!string.IsNullOrEmpty(departmentId))
                {
                    url = url + "department_id=" + departmentId + "&";
                }
                if (!string.IsNullOrEmpty(unitId))
                {
                    url = url + "unit_id=" + unitId + "&";
                }
                if (!string.IsNullOrEmpty(subUnitId))
                {
                    url = url + "JobID=" + subUnitId + "&";
                }

                url = url.Substring(0, url.Length - 1);

                var responseTask = client.GetAsync("api/central/departments").Result;

                var responseResult = responseTask.Content.ReadAsStringAsync().Result;
                var newobj = new JavaScriptSerializer().Deserialize<List<DepartmentList>>(responseResult);

                if (responseTask.IsSuccessStatusCode)
                {
                    return newobj;
                }
                else
                {
                    return null;
                }
            }
        }

        //public List<SelectListItem> GetBranchList()
        //{
        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(bassAddress);

        //        var responseTask = client.GetAsync("api/central/branches").Result;

        //        var responseResult = responseTask.Content.ReadAsStringAsync().Result;
        //        var newobj = new JavaScriptSerializer().Deserialize<List<BranchList>>(responseResult);

        //        if (responseTask.IsSuccessStatusCode)
        //        {
        //            return newobj;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}

        //public List<StaffList> GetStaffList(string departmentId, string branchId, string jobId, string unitId, string subUnitId)
        //{
        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(bassAddress);

        //        string url = "api/central/staffs?";
        //        if (!string.IsNullOrEmpty(departmentId))
        //        {
        //            url = url + "DepartmentID=" + departmentId + "&";
        //        }
        //        if (!string.IsNullOrEmpty(branchId))
        //        {
        //            url = url + "BranchID=" + branchId + "&";
        //        }
        //        if (!string.IsNullOrEmpty(jobId))
        //        {
        //            url = url + "JobID=" + jobId + "&";
        //        }
        //        if (!string.IsNullOrEmpty(unitId))
        //        {
        //            url = url + "UnitID=" + unitId + "&";
        //        }
        //        if (!string.IsNullOrEmpty(subUnitId))
        //        {
        //            url = url + "SubUnitID=" + subUnitId + "&";
        //        }

        //        url = url.Substring(0, url.Length - 1);

        //        //api/central/staffs?DepartmentID=1&BranchID=1&JobID=office&UnitID=1&SubUnitID=2
        //        var responseTask = client.GetAsync(url).Result;

        //        var responseResult = responseTask.Content.ReadAsStringAsync().Result;
        //        var newobj = new JavaScriptSerializer().Deserialize<List<StaffList>>(responseResult);

        //        if (responseTask.IsSuccessStatusCode)
        //        {
        //            return newobj;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}

        //public List<DepartmentList> GetDepartmentList(string departmentId, string unitId, string subUnitId)
        //{
        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(bassAddress);

        //        string url = "api/central/departments?";
        //        //department_id=67&unit_id=1&sub_unit_id=1
        //        if (!string.IsNullOrEmpty(departmentId))
        //        {
        //            url = url + "department_id=" + departmentId + "&";
        //        }
        //        if (!string.IsNullOrEmpty(unitId))
        //        {
        //            url = url + "unit_id=" + unitId + "&";
        //        }
        //        if (!string.IsNullOrEmpty(subUnitId))
        //        {
        //            url = url + "JobID=" + subUnitId + "&";
        //        }

        //        url = url.Substring(0, url.Length - 1);

        //        var responseTask = client.GetAsync("api/central/departments").Result;

        //        var responseResult = responseTask.Content.ReadAsStringAsync().Result;
        //        var newobj = new JavaScriptSerializer().Deserialize<List<DepartmentList>>(responseResult);

        //        if (responseTask.IsSuccessStatusCode)
        //        {
        //            return newobj;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}
    }
}