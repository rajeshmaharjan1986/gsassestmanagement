﻿using GSAssetManagement.CodeGenerator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace GSAssetManagement.CodeGenerator
{
    public static class RepositoryGeneratorNew
    {

        public static string Generate(ModuleSetupNew ModuleSetup)
        {
            string projectName = ConfigurationManager.AppSettings["ProjectName"].ToString();
            var _repositoryTemplate = "using System;" + Environment.NewLine +
                                      "using System.Collections.Generic;" + Environment.NewLine +
                                      "using System.Linq;" + Environment.NewLine +
                                      "using System.Text;" + Environment.NewLine +
                                      "using System.Threading.Tasks; " + Environment.NewLine +
                                      string.Format("using {0}.Entity; ", projectName) + Environment.NewLine +
                                      string.Format("using {0}.Entity.DTO;         ", projectName) + Environment.NewLine +
                                      string.Format("using {0}.Infrastructure;     ", projectName) + Environment.NewLine +
                                      "" + Environment.NewLine +
                                      "" + Environment.NewLine +
                                      "namespace " + projectName + ".Repository" + Environment.NewLine +
"{" + Environment.NewLine +
string.Format("    public class {0}Repository : RepositoryBase<{0}, {0}DTO>, I{0}Repository", ModuleSetup.DatabaseTable) + Environment.NewLine +
"    {" + Environment.NewLine +
string.Format("        public {0}Repository(IDatabaseFactory databaseFactory, IAuthenticationHelper authenticationHelper)", ModuleSetup.DatabaseTable) + Environment.NewLine +
"            : base(databaseFactory, authenticationHelper)" + Environment.NewLine +
"        {" + Environment.NewLine +
"    " + Environment.NewLine +
"        }" + Environment.NewLine +
"    }" + Environment.NewLine +
"    " + Environment.NewLine +
string.Format("    public interface I{0}Repository : IRepository<{0}, {0}DTO> ", ModuleSetup.DatabaseTable) + Environment.NewLine +
"    { " + Environment.NewLine +
"    }" + Environment.NewLine +
"}" + Environment.NewLine;

            return _repositoryTemplate; 
        }

    }
}
