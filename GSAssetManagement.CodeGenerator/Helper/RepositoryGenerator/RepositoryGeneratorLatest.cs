﻿using GSAssetManagement.CodeGenerator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.CodeGenerator
{
    public static class RepositoryGeneratorLatest
    {

        public static string Generate(ModuleSetup ModuleSetup)
        {
            var _repositoryTemplate =
             string.Format("using {0}.Entity; ", ModuleSetup.ProjectName) + Environment.NewLine +
             string.Format("using {0}.Entity.DTO;         ", ModuleSetup.ProjectName) + Environment.NewLine +
             string.Format("using {0}.Infrastructure;     ", ModuleSetup.ProjectName) + Environment.NewLine +
             string.Format("using {0}.Infrastructure.Core;", ModuleSetup.ProjectName) + Environment.NewLine +
                "using System;     " + Environment.NewLine +
                "using System.Collections.Generic;  " + Environment.NewLine +
                "using System.Configuration;        " + Environment.NewLine +
                "using System.Data;" + Environment.NewLine +
                "using System.Data.SqlClient;       " + Environment.NewLine +
                "using System.Linq;" + Environment.NewLine +
                "using System.Text;" + Environment.NewLine +
                "using System.Threading.Tasks;      " + Environment.NewLine +
                "using System.Xml; " + Environment.NewLine +
                "using System.Xml.Serialization;    " + Environment.NewLine +
                "using System.Data.Entity;          " + Environment.NewLine +
                "          " + Environment.NewLine +
                string.Format("namespace {0}.Repository           ", ModuleSetup.ProjectName) + Environment.NewLine +
                "         {" + Environment.NewLine +
 string.Format("     public class {0}Repository : RepositoryBase<{0}>, I{0}Repository", ModuleSetup.DatabaseTable) + Environment.NewLine +
   "     {" + Environment.NewLine +
   "         private IAuthenticationHelper _authenticationHelper;" + Environment.NewLine +
   "         private IUnitOfWork _unitOfWork;" + Environment.NewLine +
   string.Format(" private readonly IChangeLogRepository _changelogRepository;" + Environment.NewLine +
   "         public {0}Repository(IDatabaseFactory dataBaseFactory, IAuthenticationHelper authenticationHelper, IUnitOfWork unitOfWork, IChangeLogRepository changelogRepository)", ModuleSetup.DatabaseTable) + Environment.NewLine +
   "             : base(dataBaseFactory)               " + Environment.NewLine +
   "         {       " + Environment.NewLine +
   "             this._authenticationHelper = authenticationHelper;     " + Environment.NewLine +
   "             this._unitOfWork = unitOfWork;        " + Environment.NewLine +
   "             this._changelogRepository = changelogRepository;       " + Environment.NewLine +
   "         }       " + Environment.NewLine +
   "" + Environment.NewLine +
   "" + Environment.NewLine +
 string.Format("public async Task<PagedResultDataTable> Get{0}List(params Parameter[] parameters)", ModuleSetup.DatabaseTable) + Environment.NewLine +
   "         {        " + Environment.NewLine +
   "             try  " + Environment.NewLine +
   "             {    " + Environment.NewLine +
   "var pageList = new PagedResult<DataTable>();        " + Environment.NewLine +
   " " + Environment.NewLine +
   " " + Environment.NewLine +
   " " + Environment.NewLine +
   "int PageSize = int.Parse(parameters.Where(x => x.Name == \"PageSize\").Select(x => x.Value).FirstOrDefault().ToString());      " + Environment.NewLine +
   "int PageNumber = int.Parse(parameters.Where(x => x.Name == \"PageNumber\").Select(x => x.Value).FirstOrDefault().ToString());  " + Environment.NewLine +
   "            " + Environment.NewLine +
   "            " + Environment.NewLine +
   "string _connectionString = ConfigurationManager.ConnectionStrings[\"DefaultConnection\"].ToString();          " + Environment.NewLine +
   "            " + Environment.NewLine +
   "using (SqlConnection _connection = new SqlConnection(_connectionString))    " + Environment.NewLine +
   "{           " + Environment.NewLine +
   "    await _connection.OpenAsync();        " + Environment.NewLine +
   "            " + Environment.NewLine +
 string.Format("    using (SqlCommand _commnd = new SqlCommand(\"[{1}].[IRM_SP_Get{0}List]\", _connection))", ModuleSetup.DatabaseTable, ModuleSetup.Module) + Environment.NewLine +
   "    { " + Environment.NewLine +
   "        _commnd.CommandType = CommandType.StoredProcedure;                " + Environment.NewLine +
   "          " + Environment.NewLine +
   "        parameters.ToList().ForEach(z =>" + Environment.NewLine +
   "        {              " + Environment.NewLine +
   "            _commnd.Parameters.Add(new SqlParameter(string.Format(\"@{0}\", z.Name), z.Value as object ?? DBNull.Value));" + Environment.NewLine +
   "    " + Environment.NewLine +
   "        });      " + Environment.NewLine +
   "    " + Environment.NewLine +
   "    " + Environment.NewLine +
   "        using (SqlDataReader _reader = await _commnd.ExecuteReaderAsync())           " + Environment.NewLine +
   "        {        " + Environment.NewLine +
   "            DataTable _records = new DataTable();  " + Environment.NewLine +
   "            _records.Load(_reader);                " + Environment.NewLine +
   "    " + Environment.NewLine +
   "            if (_records.Rows.Count > 0)           " + Environment.NewLine +
   "            {    " + Environment.NewLine +
   "    " + Environment.NewLine +
   "                var _result = _records.AsEnumerable().GroupBy(x => new { PageCount = x[\"PageCount\"], TotalRecords = x[\"TotalRecords\"] }).Select(z => new PagedResultDataTable()" + Environment.NewLine +
   "                {                " + Environment.NewLine +
   "   PageCount = int.Parse(z.Key.PageCount.ToString()),              " + Environment.NewLine +
   "   RowCount = int.Parse(z.Key.TotalRecords.ToString()),            " + Environment.NewLine +
   "   PageSize = PageSize,          " + Environment.NewLine +
   "   PageNumber = PageNumber,      " + Environment.NewLine +
   "   Results = z.CopyToDataTable() " + Environment.NewLine +
   "                " + Environment.NewLine +
   "                }).FirstOrDefault();              " + Environment.NewLine +
   "                " + Environment.NewLine +
   "                return _result;  " + Environment.NewLine +
   "            }   " + Environment.NewLine +
   "            else" + Environment.NewLine +
   "            {   " + Environment.NewLine +
   "                return new PagedResultDataTable() " + Environment.NewLine +
   "                {                " + Environment.NewLine +
   "   PageCount = 0,                " + Environment.NewLine +
   "   RowCount = 0," + Environment.NewLine +
   "   PageNumber = 1,               " + Environment.NewLine +
   "   PageSize = PageSize,          " + Environment.NewLine +
   "   Results = _records            " + Environment.NewLine +
   "                " + Environment.NewLine +
   "                };               " + Environment.NewLine +
   "            }   " + Environment.NewLine +
   "                " + Environment.NewLine +
   "                " + Environment.NewLine +
   "        }       " + Environment.NewLine +
   "    }           " + Environment.NewLine +
   "}               " + Environment.NewLine +
   "             }  " + Environment.NewLine +
   "             catch (Exception ex)" + Environment.NewLine +
   "             {  " + Environment.NewLine +
   "throw ex;       " + Environment.NewLine +
   "             }  " + Environment.NewLine +
   "         }      " + Environment.NewLine +
   "                " + Environment.NewLine +
   string.Format("public async Task<Tuple<Guid, bool>> InsertUpdateRecords({0}DTO {1}DTO, CurrentAction CurrentAction,string SubmissionRemarks,bool Autoauthorized)", ModuleSetup.DatabaseTable, ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
   "         {          " + Environment.NewLine +
   "             try    " + Environment.NewLine +
   "             {      " + Environment.NewLine +
   "   " + Environment.NewLine +
 string.Format("{0} entity = new {0}(); ", ModuleSetup.DatabaseTable) + Environment.NewLine +
   "   " + Environment.NewLine;

            _repositoryTemplate += string.Format("entity.Id = CurrentAction == CurrentAction.Create ? Guid.NewGuid() : {0}DTO.Id;", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine;

            ModuleSetup.ModuleRuleSetupList.OrderBy(x => x.Order).ToList().ForEach(x =>
              {
                  _repositoryTemplate += string.Format("entity.{0} = {1}DTO.{0};", x.ColumnName, ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine;
              });




            _repositoryTemplate += string.Format("entity.FieldString1 = {0}DTO.FieldString1;", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                string.Format("entity.FieldString2 = {0}DTO.FieldString2;            ",        ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                string.Format("entity.FieldString3 = {0}DTO.FieldString3;            ",        ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                string.Format("entity.FieldString4 = {0}DTO.FieldString4;            ",        ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                string.Format("entity.FieldString5 = {0}DTO.FieldString5;            ", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +

                "   " + Environment.NewLine +
                "   " + Environment.NewLine +
                "   " + Environment.NewLine +
               string.Format("if (!string.IsNullOrEmpty({0}DTO.RowVersion))", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                string.Format("entity.RowVersion = Convert.FromBase64String({0}DTO.RowVersion);", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                "   " + Environment.NewLine +
                "if (CurrentAction == CurrentAction.Create)        " + Environment.NewLine +
                "{  " + Environment.NewLine +
                string.Format("entity.CreatedBy = CurrentAction == CurrentAction.Create ? this._authenticationHelper.GetFullname() : {0}DTO.CreatedBy;       ", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
               string.Format(" entity.CreatedDate = CurrentAction == CurrentAction.Create ? DateTime.Now : {0}DTO.CreatedDate;", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                string.Format(" entity.CreatedById = CurrentAction == CurrentAction.Create ? this._authenticationHelper.GetUserId() : {0}DTO.CreatedById;     ", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                "           " + Environment.NewLine +
                string.Format("entity.ModifiedBy = CurrentAction != CurrentAction.Authorise ? this._authenticationHelper.GetFullname() : {0}DTO.ModifiedBy;  ", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                string.Format("entity.ModifiedById = CurrentAction != CurrentAction.Authorise ? this._authenticationHelper.GetUserId() : {0}DTO.ModifiedById;", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                string.Format("entity.ModifiedDate = CurrentAction != CurrentAction.Authorise ? DateTime.Now : {0}DTO.ModifiedDate;", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                "       " + Environment.NewLine +
                "entity.RecordStatus = RecordStatus.UnVerified;      " + Environment.NewLine +
                "}          " + Environment.NewLine +
                "else       " + Environment.NewLine +
                "{          " + Environment.NewLine +
                string.Format("entity.CreatedBy = {0}DTO.CreatedBy;     ", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                string.Format("entity.CreatedDate = {0}DTO.CreatedDate; ", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                string.Format("entity.CreatedById = {0}DTO.CreatedById; ", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                "" + Environment.NewLine +
                string.Format("entity.ModifiedBy = {0}DTO.ModifiedBy;        ", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                string.Format("entity.ModifiedById = {0}DTO.ModifiedById;    ", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                string.Format("entity.ModifiedDate = {0}DTO.ModifiedDate;    ", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                "}          " + Environment.NewLine +
                "           " + Environment.NewLine +
                "           " + Environment.NewLine +
                "if (CurrentAction == CurrentAction.Authorise || Autoauthorized)             " + Environment.NewLine +
                "{          " + Environment.NewLine +
                "    entity.AuthorisedBy = this._authenticationHelper.GetFullname();        " + Environment.NewLine +
                "    entity.AuthorisedById = this._authenticationHelper.GetUserId();        " + Environment.NewLine +
                "    entity.AuthorisedDate = DateTime.Now;" + Environment.NewLine +
                "" + Environment.NewLine +
                string.Format("if (({0}DTO.DeletedStatus == (Int16)DeletedStatus.Deleted || {0}DTO.DeletedStatus == (Int16)DeletedStatus.Closed)|| (CurrentAction == CurrentAction.Close && Autoauthorized))", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                    "{ " + Environment.NewLine +
                "        entity.RecordStatus = RecordStatus.Inactive;      " + Environment.NewLine +
                "}" + Environment.NewLine +
                "    else   " + Environment.NewLine +
                "        entity.RecordStatus = RecordStatus.Active;        " + Environment.NewLine +
                "}          " + Environment.NewLine +
                "           " + Environment.NewLine +
                "if (CurrentAction == CurrentAction.Close)" + Environment.NewLine +
                "{          " + Environment.NewLine +
              string.Format("entity.ModifiedBy = CurrentAction != CurrentAction.Authorise ? this._authenticationHelper.GetFullname() : {0}DTO.ModifiedBy;  ", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                string.Format("entity.ModifiedById = CurrentAction != CurrentAction.Authorise ? this._authenticationHelper.GetUserId() : {0}DTO.ModifiedById;", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                string.Format("entity.ModifiedDate = CurrentAction != CurrentAction.Authorise ? DateTime.Now : {0}DTO.ModifiedDate;", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                "           " + Environment.NewLine +
                "    entity.RecordStatus = RecordStatus.UnVerified;      " + Environment.NewLine +
                "    entity.DeletedStatus = DeletedStatus.Closed;          " + Environment.NewLine +
                "}          " + Environment.NewLine +
                "           " + Environment.NewLine +
                "if (CurrentAction == CurrentAction.Edit) " + Environment.NewLine +
                "{          " + Environment.NewLine +
                "    entity.ModifiedBy = this._authenticationHelper.GetFullname();          " + Environment.NewLine +
                "    entity.ModifiedById = this._authenticationHelper.GetUserId();          " + Environment.NewLine +
                "    entity.ModifiedDate = DateTime.Now;  " + Environment.NewLine +
                "           " + Environment.NewLine +
                "    entity.RecordStatus = RecordStatus.UnVerified;      " + Environment.NewLine +
                "}          " + Environment.NewLine +
                "           " + Environment.NewLine +
                "if (CurrentAction == CurrentAction.Revert)                " + Environment.NewLine +
                "{          " + Environment.NewLine +
                "    entity.RecordStatus = RecordStatus.Reverted;          " + Environment.NewLine +
                "}          " + Environment.NewLine +
                "           " + Environment.NewLine +
                "           " + Environment.NewLine +
                "entity.DataEntry = DataEntry.User;       " + Environment.NewLine +
                "           " + Environment.NewLine +
               string.Format("if ({0}DTO.ArrayOfChangeLogDTO.Count > 0)", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                "{          " + Environment.NewLine +
                "           " + Environment.NewLine +
                "    var stringwriter = new System.IO.StringWriter();      " + Environment.NewLine +
                "    var serializer = new XmlSerializer(typeof(List<ChangeLogDTO>));        " + Environment.NewLine +
                string.Format("serializer.Serialize(stringwriter, {0}DTO.ArrayOfChangeLogDTO);", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                "    var xmllog = stringwriter.ToString();" + Environment.NewLine +
                "           " + Environment.NewLine +
                "    ChangeLog log = new ChangeLog()      " + Environment.NewLine +
                "    {      " + Environment.NewLine +
                "        Id = Guid.NewGuid()," + Environment.NewLine +
                "        ChangeType = CurrentAction.ToString(),            " + Environment.NewLine +
                "        DateCreated = DateTime.Now,      " + Environment.NewLine +
                "        EntityPK = entity.Id.ToString(), " + Environment.NewLine +
                "        AuditUserId = this._authenticationHelper.GetUserId(),              " + Environment.NewLine +
              string.Format("TableName = \"{0}\",  ", ModuleSetup.DatabaseTable) + Environment.NewLine +
                "        Log = xmllog," + Environment.NewLine +
               "         Remarks = SubmissionRemarks" + Environment.NewLine +
                "    };     " + Environment.NewLine +
                "           " + Environment.NewLine +
                "    this._changelogRepository.Add(log);  " + Environment.NewLine +
                "}          " + Environment.NewLine +
                "           " + Environment.NewLine +
                "           " + Environment.NewLine +
                "           " + Environment.NewLine +
                "           " + Environment.NewLine +
                "if (CurrentAction == CurrentAction.Create)                " + Environment.NewLine +
                "{          " + Environment.NewLine +
                "           " + Environment.NewLine +
                "    this.Add(entity);       " + Environment.NewLine +
                "           " + Environment.NewLine +
                "    if (await this._unitOfWork.CommitAsync())             " + Environment.NewLine +
                "    {      " + Environment.NewLine +
                "        return new Tuple<Guid, bool>(entity.Id, true);    " + Environment.NewLine +
                "    }      " + Environment.NewLine +
                "    else   " + Environment.NewLine +
                "    {      " + Environment.NewLine +
                "        return new Tuple<Guid, bool>(Guid.Empty, true); ; " + Environment.NewLine +
                "    }      " + Environment.NewLine +
                "}          " + Environment.NewLine +
                "else       " + Environment.NewLine +
                "{          " + Environment.NewLine +
              string.Format("if ((CurrentAction == CurrentAction.Authorise && {0}DTO.DeletedStatus == (Int16)DeletedStatus.Deleted) || (Autoauthorized && CurrentAction == CurrentAction.Delete))", ModuleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                "    {      " + Environment.NewLine +
                "        this.Delete(entity);" + Environment.NewLine +
                "    }      " + Environment.NewLine +
                "    else   " + Environment.NewLine +
                "    {      " + Environment.NewLine +
                "        this.Update(entity);" + Environment.NewLine +
                "    }      " + Environment.NewLine +
                "           " + Environment.NewLine +
                "           " + Environment.NewLine +
                "    if (await this._unitOfWork.CommitAsync())             " + Environment.NewLine +
                "    {      " + Environment.NewLine +
                "        return new Tuple<Guid, bool>(entity.Id, true);    " + Environment.NewLine +
                "    }      " + Environment.NewLine +
                "    else   " + Environment.NewLine +
                "    {      " + Environment.NewLine +
                "        return new Tuple<Guid, bool>(Guid.Empty, true); ; " + Environment.NewLine +
                "    }      " + Environment.NewLine +
                "}          " + Environment.NewLine +
                "           " + Environment.NewLine +
                "             }              " + Environment.NewLine +
                "             catch (Exception ex)            " + Environment.NewLine +
                "             {              " + Environment.NewLine +
                "throw ex;  " + Environment.NewLine +
                "             }              " + Environment.NewLine +
                "         } " + Environment.NewLine +
                "     }     " + Environment.NewLine +
                "           " + Environment.NewLine +
               string.Format("     public interface I{0}Repository : IRepository<{0}>           ", ModuleSetup.DatabaseTable) + Environment.NewLine +
                "     {     " + Environment.NewLine +
                string.Format("Task<PagedResultDataTable> Get{0}List(params Parameter[] parameters); ", ModuleSetup.DatabaseTable) + Environment.NewLine +
                string.Format("Task<Tuple<Guid, bool>> InsertUpdateRecords({1}DTO {0}DTO, CurrentAction CurrentAction,string SubmissionRemarks,bool Autoauthorized);", ModuleSetup.DatabaseTable.ToLower(), ModuleSetup.DatabaseTable) + Environment.NewLine +
                "           " + Environment.NewLine +
                "     }     " + Environment.NewLine +
                " }         " + Environment.NewLine;

            return _repositoryTemplate; ;
        }

    }
}
