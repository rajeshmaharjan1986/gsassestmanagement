﻿using GSAssetManagement.CodeGenerator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.CodeGenerator
{
    public static class JavascriptGenerator
    {
        public static string GenerateUIScript(ModuleSetup moduleSetup)
        {
            try
            {
                var _template = "if(!window.app)" + Environment.NewLine +
                                       "{" + Environment.NewLine +
                                          "window.app = {}" + Environment.NewLine +
                                      "}" + Environment.NewLine +
                                      "" + Environment.NewLine +
                                      "" + Environment.NewLine +
                                      "" + Environment.NewLine +
                                      "" + Environment.NewLine +
                        string.Format("app.{0} = (function($) {{", moduleSetup.DatabaseTable) + Environment.NewLine +
                                   "var mainBody = $('#irm-workspace')," + Environment.NewLine +
                                  string.Format("parentId = mainBody.find('#{0}Workspace #ParentId').val(),", moduleSetup.DatabaseTable) + Environment.NewLine +
                                   string.Format("dataSource = app.{0}Datasource,", moduleSetup.DatabaseTable) + Environment.NewLine +
                                   "initializer = appInit.applicationInitializer," + Environment.NewLine +
                                   "validator = {}," + Environment.NewLine +
                                   "" + Environment.NewLine +
                         "init = function() {" + Environment.NewLine +
                         "" + Environment.NewLine +
                             string.Format("mainBody.find('#{0}Workspace').on('click', '.scopeChange', changeScope);", moduleSetup.DatabaseTable) + Environment.NewLine +
                             string.Format("mainBody.find('#{0}Workspace #allLiContentsHere').on('click', 'form#mainForm .saveRecords', confirmation);", moduleSetup.DatabaseTable) + Environment.NewLine +
                             string.Format("mainBody.find('#{0}Workspace #allLiContentsHere').on('click', 'form#mainForm .unlock', unlockRecords);", moduleSetup.DatabaseTable) + Environment.NewLine +
                              string.Format("mainBody.find('#{0}Workspace #allLiContentsHere').on('submit', 'form#mainForm', submitForm);", moduleSetup.DatabaseTable) + Environment.NewLine +
                              "mainBody.on('submit', 'form#parentSearchForm', parentSearchRecords);" + Environment.NewLine +
                              "mainBody.find('#main-workspace #parenttblcontent #RecordsContent').on('click', 'ul.recordSize li a', parentResizeContentRecords);" + Environment.NewLine +
                              "mainBody.find('#main-workspace #parenttblcontent #RecordsContent').on('click', 'ul#paginationUL li a', parentPaginatedContentRecords);" + Environment.NewLine +
                             string.Format("mainBody.find('#main-workspace #{0}Workspace #allLiContentsHere').on ('click','#RecordsContentul.recordSizelia',resizeContentRecords);", moduleSetup.DatabaseTable) + Environment.NewLine +
                              string.Format("mainBody.find('#main-workspace #{0}Workspace #allLiContentsHere').on     ('click','#RecordsContentul#paginationULlia',paginatedContentRecords);", moduleSetup.DatabaseTable) + Environment.NewLine +
                             string.Format("mainBody.find('#{0}Workspace #allLiContentsHere').on('submit', 'form#searchForm', searchRecords);", moduleSetup.DatabaseTable) + Environment.NewLine +
                              string.Format("mainBody.find('#{0}Workspace #allLiContentsHere').on('change', 'form :input', onChangeFormValues);", moduleSetup.DatabaseTable) + Environment.NewLine +
                              string.Format("mainBody.find('#{0}Workspace').on('click', '#addMoreRecords', addMoreRecords);", moduleSetup.DatabaseTable) + Environment.NewLine +
                              string.Format("mainBody.find('#{0}Workspace #allLiContentsHere').on('click', '.childDataSetup', childDataSetup);", moduleSetup.DatabaseTable) + Environment.NewLine +
                              string.Format("mainBody.find('#{0}Workspace #allLiContentsHere').on('click', '.viewRecordDetails', viewRecordDetails);", moduleSetup.DatabaseTable) + Environment.NewLine +
                              "mainBody.find('#cofirmMessgae').on('click', '#confirmOperation', confirmOperation);" + Environment.NewLine +
                              "mainBody.find('#cofirmMessgae').on('click', '#cancelOperation', cancelOperation);" + Environment.NewLine +


                          "}, onChangeFormValues = function() {" + Environment.NewLine +

                              "" + Environment.NewLine +
                             string.Format("if (mainBody.find('#{0}Workspace #allLiContentsHere form#mainForm').valid())", moduleSetup.DatabaseTable) + Environment.NewLine +
                               "{" + Environment.NewLine +
                              string.Format("mainBody.find('#{0}Workspace #allLiContentsHere form .saveRecords').attr('disabled', false);", moduleSetup.DatabaseTable) + Environment.NewLine +
                               "}" + Environment.NewLine +

                              "}, changeScope = function(event) {" + Environment.NewLine +

                                  "event.preventDefault();" + Environment.NewLine +

                                  "var scope = $(this).attr('data-scope');" + Environment.NewLine +
                                  "" + Environment.NewLine +
                                  "if (!parentId) {" + Environment.NewLine +

                                      "mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html('Invalid action attempted.');" + Environment.NewLine +
                                      "mainBody.find('#cofirmMessgae').find('#confirmDialog').hide();" + Environment.NewLine +
                                      "mainBody.find('#cofirmMessgae').find('#alertDialog').show();" + Environment.NewLine +
                                      "mainBody.find('#cofirmMessgae').modal({ backdrop: 'static', keyboard: false });" + Environment.NewLine +
                                      "mainBody.find('#cofirmMessgae').modal('show');" + Environment.NewLine +
                                      "return false;" + Environment.NewLine +

                                  "} else {" + Environment.NewLine +
                                  "" + Environment.NewLine +
                                      "var url = $(this).attr('href');" + Environment.NewLine +
                                      "" + Environment.NewLine +
                                     string.Format("mainBody.find('#{0}Workspace li').removeClass('active');", moduleSetup.DatabaseTable) + Environment.NewLine +
                                      "$(this).closest('li').addClass('active');" + Environment.NewLine +
                                      "" + Environment.NewLine +
                                      "initializer.showloader();" + Environment.NewLine +
                                      "" + Environment.NewLine +
                                      "dataSource.accessLink(url, parentId).done(function(response) {" + Environment.NewLine +
                                      "" + Environment.NewLine +
                                    string.Format("mainBody.find('#{0}Workspace #allLiContentsHere').html(response);", moduleSetup.DatabaseTable) + Environment.NewLine +
                                      "" + Environment.NewLine +
                                      "" + Environment.NewLine +
                                      "    initializer.hideloader();" + Environment.NewLine +
                                      "" + Environment.NewLine +
                                      "    }).fail(function(response) {" + Environment.NewLine +
                                      "" + Environment.NewLine +
                                      "    mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html(response.Summary);" + Environment.NewLine +
                                      "    mainBody.find('#cofirmMessgae').find('#confirmDialog').hide();" + Environment.NewLine +
                                      "    mainBody.find('#cofirmMessgae').find('#confirmresponseContent').show();" + Environment.NewLine +
                                      "    mainBody.find('#cofirmMessgae').modal({ backdrop: 'static', keyboard: false });" + Environment.NewLine +
                                      "    mainBody.find('#cofirmMessgae').modal('show');" + Environment.NewLine +
                                      "    initializer.hideloader();" + Environment.NewLine +
                                      "" + Environment.NewLine +
                                      "});" + Environment.NewLine +
                                  "}" + Environment.NewLine +
                                  "" + Environment.NewLine +
                          "}, confirmation = function(event) {" + Environment.NewLine +
                          "" + Environment.NewLine +
                          "    event.preventDefault();" + Environment.NewLine +
                               "" + Environment.NewLine +
                               "var CurrentAction = $(this).attr('data-currentaction');" + Environment.NewLine +
                              string.Format("mainBody.find('#{0}Workspace #allLiContentsHere form #CurrentAction').val(CurrentAction);", moduleSetup.DatabaseTable) + Environment.NewLine +
                               "" + Environment.NewLine +
                               "mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html('');" + Environment.NewLine +
                               "mainBody.find('#cofirmMessgae').find('#confirmresponseContent').hide();" + Environment.NewLine +
                               "mainBody.find('#cofirmMessgae').find('#confirmDialog').show();" + Environment.NewLine +
                               "" + Environment.NewLine +
                               "mainBody.find('#cofirmMessgae').modal({ backdrop: 'static', keyboard: false });" + Environment.NewLine +
                               "mainBody.find('#cofirmMessgae').modal('show');" + Environment.NewLine +
                               "" + Environment.NewLine +
                               "" + Environment.NewLine +
                               "" + Environment.NewLine +
                               "" + Environment.NewLine +

                          "}, confirmOperation = function(event) {" + Environment.NewLine +

                            string.Format("if (mainBody.find('#{0}Workspace #allLiContentsHere form').valid())  ", moduleSetup.DatabaseTable) + Environment.NewLine +
                              "{" + Environment.NewLine +
                              string.Format("mainBody.find('#{0}Workspace #allLiContentsHere form').submit(); ", moduleSetup.DatabaseTable) + Environment.NewLine +
                              "}" + Environment.NewLine +

                          "}, cancelOperation = function (event) {" + Environment.NewLine +
                          "" + Environment.NewLine +
                              "mainBody.find('#cofirmMessgae').modal('hide');" + Environment.NewLine +
                              "" + Environment.NewLine +
                              "" + Environment.NewLine +

                          "}, submitForm = function (event) {" + Environment.NewLine +
                          "" + Environment.NewLine +
                          "    event.preventDefault();" + Environment.NewLine +
                          "" + Environment.NewLine +
                          "" + Environment.NewLine +
                          "var $form = $(this);" + Environment.NewLine +
                          "" + Environment.NewLine +
                          "initializer.showloader();" + Environment.NewLine +
                          "" + Environment.NewLine +
                          "dataSource.submitForm($form).done(function (response) {" + Environment.NewLine +
                          "" + Environment.NewLine +
                          "if (response.IsSuccess === true)" + Environment.NewLine +
                          "{" + Environment.NewLine +
                          "" + Environment.NewLine +
                         string.Format("if (mainBody.find('#{0}Workspace .nav.nav-pills.nav-stacked li.active a').attr('data-scope') === 'parent')", moduleSetup.DatabaseTable) + Environment.NewLine +
                          "    {" + Environment.NewLine +
                          "        mainBody.find('#ParentId').val(response.Id);" + Environment.NewLine +
                          "        parentId = response.Id;                     " + Environment.NewLine +
                          "    }                                               " + Environment.NewLine +
                          "    mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html(response.Summary);" + Environment.NewLine +
                          "    mainBody.find('#cofirmMessgae').find('#confirmDialog').hide();                         " + Environment.NewLine +
                          "    mainBody.find('#cofirmMessgae').find('#confirmresponseContent').show();                " + Environment.NewLine +
                          "        $('body').find('#loader-wrapper').hide();                                          " + Environment.NewLine +
                          "                                                                                           " + Environment.NewLine +
                          "    initializer.hideloader();                                                              " + Environment.NewLine +
                          "                                                                                           " + Environment.NewLine +
                          "}                                                                                          " + Environment.NewLine +
                          "else                                                                                       " + Environment.NewLine +
                          "{                                                                                          " + Environment.NewLine +
                          "                                                                                           " + Environment.NewLine +
                          "    mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html(response.Summary);" + Environment.NewLine +
                          "    mainBody.find('#cofirmMessgae').find('#confirmDialog').hide();                         " + Environment.NewLine +
                          "    mainBody.find('#cofirmMessgae').find('#confirmresponseContent').show();                " + Environment.NewLine +
                        string.Format("mainBody.find('#{0}Workspace #allLiContentsHere form :button').attr('disabled', false);", moduleSetup.DatabaseTable) + Environment.NewLine +
                          "                               " + Environment.NewLine +
                          "    initializer.hideloader();  " + Environment.NewLine +
                          "}                              " + Environment.NewLine +
                          "                               " + Environment.NewLine +
                          "}).fail(function (response) {" + Environment.NewLine +
                          "" + Environment.NewLine +
                          "mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html(response.Summary);" + Environment.NewLine +
                          "mainBody.find('#cofirmMessgae').find('#confirmDialog').hide();" + Environment.NewLine +
                          "mainBody.find('#cofirmMessgae').find('#confirmresponseContent').show();" + Environment.NewLine +
                         string.Format("mainBody.find('#{0}Workspace #allLiContentsHere form :button').attr('disabled', false);", moduleSetup.DatabaseTable) + Environment.NewLine +
                          "initializer.hideloader();" + Environment.NewLine +
                          "" + Environment.NewLine +
                       "});" + Environment.NewLine +

                   "}, childDataSetup = function(event) {" + Environment.NewLine +
                    "" + Environment.NewLine +
                      "event.preventDefault();" + Environment.NewLine +
                      "" + Environment.NewLine +
                      "var scope = $(this).attr('data-scope');" + Environment.NewLine +
                      "" + Environment.NewLine +
                      "if (parentId === undefined) {" + Environment.NewLine +
                      "" + Environment.NewLine +
                      "mainBody.find('#alertScope').find('.alert').removeClass().addClass('alert alert-warning');" + Environment.NewLine +
                      "mainBody.find('#alertScope').find('.alert p').text('Please fill the main form and proceed');" + Environment.NewLine +
                      "" + Environment.NewLine +
                      "" + Environment.NewLine +
                      "return false;" + Environment.NewLine +

                  "} else {" + Environment.NewLine +


                      "var url = $(this).attr('href');" + Environment.NewLine +
                      "" + Environment.NewLine +
                      "initializer.showloader();" + Environment.NewLine +
                      "" + Environment.NewLine +
                      "dataSource.accessChildLink(url, parentId).done(function(response) {" + Environment.NewLine +
                      "" + Environment.NewLine +
                     string.Format("mainBody.find('#{0}Workspace #allLiContentsHere').html(response);", moduleSetup.DatabaseTable) + Environment.NewLine +
                      "mainBody.find('#alertScope').find('.alert').removeClass().addClass('alert alert-info');" + Environment.NewLine +
                      "mainBody.find('#alertScope').find('.alert p').text('New workspace is loaded. Please fill valid details.');" + Environment.NewLine +
                      "" + Environment.NewLine +
                      "    initializer.hideloader();" + Environment.NewLine +
                      "" + Environment.NewLine +
                      "" + Environment.NewLine +
                      "}).fail(function(response) {" + Environment.NewLine +
                      "" + Environment.NewLine +
                     string.Format("mainBody.find('#{0}Workspace #allLiContentsHere').html(response);", moduleSetup.DatabaseTable) + Environment.NewLine +
                      "mainBody.find('#alertScope').find('.alert').removeClass().addClass('alert alert-danger');" + Environment.NewLine +
                      "mainBody.find('#alertScope').find('.alert p').text('New workspace is loaded. Please fill valid details.');" + Environment.NewLine +
                      "" + Environment.NewLine +
                      "    initializer.hideloader();" + Environment.NewLine +
                      "" + Environment.NewLine +
                      "" + Environment.NewLine +
                      "});" + Environment.NewLine +
                  "}" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "" + Environment.NewLine +

               "}, parentSearchRecords = function(event) {" + Environment.NewLine +
                "" + Environment.NewLine +
                  "event.preventDefault();" + Environment.NewLine +


                   "var $form = $(this);" + Environment.NewLine +
                   "" + Environment.NewLine +
                   "initializer.showloader();" + Environment.NewLine +
                   "" + Environment.NewLine +
                   "dataSource.submitForm($form).done(function (response) {" + Environment.NewLine +
                   "" + Environment.NewLine +
                   "    mainBody.find('#main-workspace #parenttblcontent #RecordsContent').html(response);" + Environment.NewLine +
                   "    mainBody.find('form#parentSearchForm :input').attr('disabled', false);" + Environment.NewLine +
                   "    initializer.hideloader();" + Environment.NewLine +
                   "" + Environment.NewLine +
                   "" + Environment.NewLine +
                   "}).fail(function (response) {" + Environment.NewLine +
                   "" + Environment.NewLine +
                   "    mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html(response.Summary);" + Environment.NewLine +
                   "    mainBody.find('#cofirmMessgae').find('#confirmDialog').hide();                         " + Environment.NewLine +
                   "    mainBody.find('#cofirmMessgae').find('#confirmresponseContent').show();                " + Environment.NewLine +
                   "    initializer.hideloader();                                                              " + Environment.NewLine +
                   "                                                                                           " + Environment.NewLine +
                   "});" + Environment.NewLine +



                  "}, searchRecords = function(event) {" + Environment.NewLine +

                  "event.preventDefault();" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "var $form = $(this);" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "initializer.showloader();" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "dataSource.submitForm($form).done(function (response) {" + Environment.NewLine +
                  "" + Environment.NewLine +
                 string.Format("mainBody.find('#{0}Workspace #allLiContentsHere #RecordsContent').html(response);", moduleSetup.DatabaseTable) + Environment.NewLine +
                 string.Format("mainBody.find('#{0}Workspace #allLiContentsHere form#searchForm :input').attr('disabled', false);", moduleSetup.DatabaseTable) + Environment.NewLine +
                  "initializer.hideloader();" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "}).fail(function (response) {" + Environment.NewLine +

                  "mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html(response.Summary);" + Environment.NewLine +
                  "mainBody.find('#cofirmMessgae').find('#confirmDialog').hide();                         " + Environment.NewLine +
                  "mainBody.find('#cofirmMessgae').find('#confirmresponseContent').show();                " + Environment.NewLine +
                  "initializer.hideloader();                                                              " + Environment.NewLine +
                  "" + Environment.NewLine +
                 "});" + Environment.NewLine +
                 "" + Environment.NewLine +
                 "" + Environment.NewLine +

               "}, parentResizeContentRecords = function(event) {" + Environment.NewLine +

                  "event.preventDefault();" + Environment.NewLine +

                  "var activeSize = $(this).attr('data-pagesize');" + Environment.NewLine +
                  "mainBody.find('form#parentSearchForm #PageSize').val(activeSize);" + Environment.NewLine +
                  "mainBody.find('form#parentSearchForm #PageNumber').val(1);" + Environment.NewLine +
                  "mainBody.find('form#parentSearchForm').submit();" + Environment.NewLine +

              "}, parentPaginatedContentRecords = function()" + Environment.NewLine +
              "{" + Environment.NewLine +

                  "event.preventDefault();" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "var activePage = $(this).attr('data-page');" + Environment.NewLine +
                  "mainBody.find('form#parentSearchForm #PageNumber').val(activePage);" + Environment.NewLine +
                  "mainBody.find('form#parentSearchForm').submit();" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "" + Environment.NewLine +
              "}, resizeContentRecords = function(event) {" + Environment.NewLine +

                  "event.preventDefault();" + Environment.NewLine +

                  "var activeSize = $(this).attr('data-pagesize');" + Environment.NewLine +
                  string.Format("mainBody.find('#{0}Workspace #allLiContentsHere form#searchForm #PageSize').val(activeSize);", moduleSetup.DatabaseTable) + Environment.NewLine +
                  string.Format("mainBody.find('#{0}Workspace #allLiContentsHere form#searchForm #PageNumber').val(1);", moduleSetup.DatabaseTable) + Environment.NewLine +
                  string.Format("mainBody.find('#{0}Workspace #allLiContentsHere form#searchForm').submit();", moduleSetup.DatabaseTable) + Environment.NewLine +
                  "" + Environment.NewLine +
              "}, paginatedContentRecords = function()" + Environment.NewLine +
              "{" + Environment.NewLine +

                  "event.preventDefault();" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "var activePage = $(this).attr('data-page');" + Environment.NewLine +
                  string.Format("mainBody.find('#{0}Workspace #allLiContentsHere form#searchForm #PageNumber').val(activePage);", moduleSetup.DatabaseTable) + Environment.NewLine +
                  string.Format("mainBody.find('#{0}Workspace #allLiContentsHere form#searchForm').submit();", moduleSetup.DatabaseTable) + Environment.NewLine +
                  "" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "" + Environment.NewLine +
              "}, viewRecordDetails = function(event) {" + Environment.NewLine +
               "" + Environment.NewLine +
                  "event.preventDefault();" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "var url = $(this).attr('href');" + Environment.NewLine +
                  "var recordId = $(this).attr('data-id');" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "initializer.showloader();" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "dataSource.accessLink(url, recordId).done(function (response) {" + Environment.NewLine +
                  "" + Environment.NewLine +
                 string.Format("mainBody.find('#{0}Workspace #allLiContentsHere').html(response);", moduleSetup.DatabaseTable) + Environment.NewLine +
                  "mainBody.find('#alertScope').find('.alert').removeClass().addClass('alert alert-info');" + Environment.NewLine +
                  "mainBody.find('#alertScope').find('.alert p').text('New workspace is loaded. Please fill valid details.');" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "    initializer.hideloader();" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "}).fail(function (response) {" + Environment.NewLine +
                  "" + Environment.NewLine +
                 string.Format("mainBody.find('#{0}Workspace #allLiContentsHere').html(response);", moduleSetup.DatabaseTable) + Environment.NewLine +
                  "mainBody.find('#alertScope').find('.alert').removeClass().addClass('alert alert-danger');" + Environment.NewLine +
                  "mainBody.find('#alertScope').find('.alert p').text('New workspace is loaded. Please fill valid details.');" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "    initializer.hideloader();" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "});" + Environment.NewLine +


              "}, addMoreRecords = function(event) {" + Environment.NewLine +
              "    event.preventDefault();" + Environment.NewLine +
              string.Format("mainBody.find('#{0}Workspace #allLiContentsHere form :input').attr('disabled', false);", moduleSetup.DatabaseTable) + Environment.NewLine +
              string.Format("mainBody.find('#{0}Workspace #allLiContentsHere form')[0].reset();", moduleSetup.DatabaseTable) + Environment.NewLine +
              "" + Environment.NewLine +
              "}, unlockRecords = function(event) {" + Environment.NewLine +
              "" + Environment.NewLine +
              string.Format("mainBody.find('#{0}Workspace #allLiContentsHere form :input').attr('disabled', false);", moduleSetup.DatabaseTable) + Environment.NewLine +
              "};" + Environment.NewLine +

              "return {" + Environment.NewLine +

                  "init: init" + Environment.NewLine +
                  "}" + Environment.NewLine +
                  "" + Environment.NewLine +
              "}(jQuery))" + Environment.NewLine +
              "" + Environment.NewLine +
             string.Format("jQuery(app.{0}.init);", moduleSetup.DatabaseTable) + Environment.NewLine;

                return _template;
            }
            catch (Exception ex)
            {
                throw ex;

            }

        }

        public static string GenerateDataSourceScript(ModuleSetup moduleSetup)
        {

            try
            {



                var _template = "if (window.app)" + Environment.NewLine +
                                   "{" + Environment.NewLine +
                                       "window.app = { };" + Environment.NewLine +
                                    "}" + Environment.NewLine +
                                    "" + Environment.NewLine +
                                    "" + Environment.NewLine +
                                    "" + Environment.NewLine +
                   string.Format("app.{0}Datasource = (function($) {{", moduleSetup.DatabaseTable) + Environment.NewLine +
                   "" + Environment.NewLine +
                       "var accessLink = function(url, parentId) {" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "" + Environment.NewLine +
                           "var $d = $.Deferred();" + Environment.NewLine +

                           "$.ajax({" + Environment.NewLine +
                           "    type: \"GET\"," + Environment.NewLine +
                           "    url: url," + Environment.NewLine +
                       "data: { Id: parentId }" + Environment.NewLine +
                           "" + Environment.NewLine +
                           "}).done(function(response) {" + Environment.NewLine +
                               "" + Environment.NewLine +
                               "$d.resolve(response);" + Environment.NewLine +
                               "" + Environment.NewLine +
                           "}).fail(function(response) {" + Environment.NewLine +
                               "" + Environment.NewLine +
                               "$d.reject(response);" + Environment.NewLine +
                           "});" + Environment.NewLine +
                           "" + Environment.NewLine +
                           "" + Environment.NewLine +
                            "return $d.promise();" + Environment.NewLine +
                            "" + Environment.NewLine +
                            "" + Environment.NewLine +
                            "" + Environment.NewLine +
                            "" + Environment.NewLine +
                       "}, submitForm = function($form) {" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "    var $d = $.Deferred();" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "    $form.find(':input').prop(\"disabled\", false);" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "    var data = $form.serialize();" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "    $form.find(':input').prop(\"disabled\", true);" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "    $.ajax({" + Environment.NewLine +
                       "        type: \"POST\"," + Environment.NewLine +
                       "        url: $form.attr('action')," + Environment.NewLine +
                       "        data: data" + Environment.NewLine +
                       "    }).done(function(response) {" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "if(response.IsSuccess == false)" + Environment.NewLine +
                       "{" + Environment.NewLine +
                       " $form.find(':input').prop(\"disabled\", false);" + Environment.NewLine +
                       "}" + Environment.NewLine +
                       "        $d.resolve(response);" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "    }).fail(function(response) {" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "        $d.reject(response);" + Environment.NewLine +
                       "    });" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "    return $d.promise();" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "}, accessChildLink = function(url, parentId) {" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "    var $d = $.Deferred();" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "    $.ajax({" + Environment.NewLine +
                       "        type: \"GET\"," + Environment.NewLine +
                       "        url: url," + Environment.NewLine +
                      "   data: {Id: parentId }" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "    }).done(function(response) {" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "        $d.resolve(response);" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "    }).fail(function(response) {" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "        $d.reject(response);" + Environment.NewLine +
                       "    });" + Environment.NewLine +
                       "" + Environment.NewLine +

                           "return $d.promise();" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "};" + Environment.NewLine +

                       "return {" + Environment.NewLine +
                       "    accessLink: accessLink," + Environment.NewLine +
                       "    submitForm: submitForm," + Environment.NewLine +
                       "    accessChildLink: accessChildLink" + Environment.NewLine +
                       "" + Environment.NewLine +
                       "};" + Environment.NewLine +


                       "} (jQuery));" + Environment.NewLine;


                return _template;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
