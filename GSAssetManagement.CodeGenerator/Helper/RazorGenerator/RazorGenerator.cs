﻿using GSAssetManagement.CodeGenerator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.CodeGenerator
{
    public static class RazorGenerator
    {

        public static string GenerateIndex(ModuleSetup moduleSetup)
        {
            if (moduleSetup.ParentTable == null)
            {

                string _indexTemplate = "@model GSAssetManagement.Entity.DTO.ModuleSummary" + Environment.NewLine +
 "@{" + Environment.NewLine +

 "  Layout = this.Request.IsAjaxRequest() ? \"\" : \"~/Views/Shared/_Layout.cshtml\";" + Environment.NewLine +
 "}" + Environment.NewLine +
string.Format("@using(Html.BeginForm(\"SearchIndex\", \"Model.ModuleSummaryName\", new {{area = \"Model.SchemaName\" }}, FormMethod.Post, new {{ @role = \"form\", @id = \"searchForm\" }}))") + Environment.NewLine +
 "{" + Environment.NewLine +
 "  @Html.Hidden(\"PageSize\", Model.SummaryRecord.PageSize);" + Environment.NewLine +
 "  @Html.Hidden(\"PageNumber\", Model.SummaryRecord.PageNumber);" + Environment.NewLine +
 "  foreach (var item in Model.moduleBussinesLogicSummaries.Where(h => h.HtmlDataType == \"hidden\" && h.SummaryHeader))" + Environment.NewLine +
 "  {" + Environment.NewLine +
 "      @Html.Hidden(item.ColumnName, item.CurrentValue);" + Environment.NewLine +
 "  }" + Environment.NewLine +
 "  @Html.AntiForgeryToken();" + Environment.NewLine +
 "                    <div class=\"card\">" + Environment.NewLine +
 "        <div class=\"form-beautiful\">" + Environment.NewLine +
 "            <div class=\"box-header\" style=\"padding: 5px; \">" + Environment.NewLine +
 string.Format("<h5 class=\"box-title\" id=\"exampleModalLabel\">{0}</h5>", moduleSetup.TableTitle) + Environment.NewLine +
 "            </div>" + Environment.NewLine +
 "            <div class=\"box-body\">" + Environment.NewLine +
 "                <div class=\"shadow\">" + Environment.NewLine +
 "                    <div class=\"preview-box\">" + Environment.NewLine +
 "                        <div class=\"row\">" + Environment.NewLine +
 "                            @foreach(var item in Model.FieldRuleList.Where(z => !z.Hidden && z.SearchField))" + Environment.NewLine +
 "                            {" + Environment.NewLine +
 "                                if (item.HtmlDataType == \"text\")" + Environment.NewLine +
 "                                {" + Environment.NewLine +
 "                                    <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
 "                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle</label>" + Environment.NewLine +
 "                                        @Html.TextBox(item.ColumnName, item.Value, item.Attributes)" + Environment.NewLine +
 "                                    </div>" + Environment.NewLine +
 "                                }" + Environment.NewLine +
 "                                if (item.HtmlDataType == \"textarea\")" + Environment.NewLine +
 "                                {" + Environment.NewLine +
 "                                    <div class=\"form-group has-feedback col-lg-12\">" + Environment.NewLine +
 "                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle</label>" + Environment.NewLine +
 "                                        @Html.TextArea(item.ColumnName, item.Value as string, 5, 20, item.Attributes)" + Environment.NewLine +
 "                                    </div>" + Environment.NewLine +
 "                                }" + Environment.NewLine +
 "                                if (item.HtmlDataType == \"select\")" + Environment.NewLine +
  "                                {" + Environment.NewLine +
"if (item.Attributes.Where(x => x.Key == \"ParameterizedSelect\" && x.Value.ToString() == \"True\").Count() > 0)" + Environment.NewLine +

                                    "    {" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +

                                    "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +

                                    "            <div class=\"input-group\">" + Environment.NewLine +

                                    "                <a href=\"#\" class=\"input-group-addon download-select\" data-entityname=\"@item.ColumnName\">" + Environment.NewLine +

                                    "                    <i class=\"fa fa-download\"></i>" + Environment.NewLine +

                                    "                </a>" + Environment.NewLine +

                                    "" + Environment.NewLine +
                                    "                @Html.DropDownList(item.ColumnName, item.DropdownMasterList, \"--Select--\", item.Attributes)" + Environment.NewLine +

                                    "            </div>" + Environment.NewLine +

                                    "        </div>" + Environment.NewLine +

                                    "    }" + Environment.NewLine +

                                    "    else" + Environment.NewLine +

                                    "    {" + Environment.NewLine +

                                    "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +

                                    "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +

                                    "" + Environment.NewLine +
                                    "            @Html.DropDownList(item.ColumnName, item.DropdownMasterList, \"--Select--\", item.Attributes)" + Environment.NewLine +

                                    "        </div>" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "    }" + Environment.NewLine +
  "                                }         " + Environment.NewLine +
 "                            }" + Environment.NewLine +
 "                            <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
 "                                <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\"></i> Record Status</label>" + Environment.NewLine +
 "                                @Html.DropDownList(\"RecordStatus\", EnumDropdownList.ToSelectList(GSAssetManagement.Entity.RecordStatus.Active), \"--Select--\", new { @class = \"form-control\", placeholder = \"Select Record Status\" })" + Environment.NewLine +
 "                            </div>" + Environment.NewLine +
 "                        </div>    " + Environment.NewLine +
 "                    </div>        " + Environment.NewLine +
 "                </div>            " + Environment.NewLine +
 "                <div class=\"row\">" + Environment.NewLine +
 "                    <button class=\"btn icon-btn btn-success\" type=\"submit\" style=\"float:right;margin-right:15px;\" id=\"searchRecords\">" + Environment.NewLine +
 "                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon-search img-circle text-success\">                          " + Environment.NewLine +
 "                        </span>Search                                                                                                        " + Environment.NewLine +
 "                    </button>                                                                                                                " + Environment.NewLine +
 "                    <button class=\"btn icon-btn btn-primary\" type=\"reset\" style=\"float:right;margin-right:15px;\" id=\"resetForm\">     " + Environment.NewLine +
 "                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon-refresh img-circle text-warning\">                         " + Environment.NewLine +
 "                        </span>Reset                                                                                                         " + Environment.NewLine +
 "                    </button>  " + Environment.NewLine +
 "                </div>         " + Environment.NewLine +
 "            </div>             " + Environment.NewLine +
 "        </div>                 " + Environment.NewLine +
 "    </div>                     " + Environment.NewLine +
 "}                              " + Environment.NewLine +
 "<div class=\"box-footer table-responsive\" id=\"parenttblcontent\">" + Environment.NewLine +
 "    <div style =\"margin-bottom:45px; \">" + Environment.NewLine +
 string.Format("<a class=\"btn icon-btn btn-primary\" href=\"/{0}/{1}/Create\" style=\"float:right;margin-right:15px; \">", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
 "            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon-plus img-circle text-warning\">" + Environment.NewLine +
 "            </span>Add New Record" + Environment.NewLine +
 "        </a>                     " + Environment.NewLine +
 "    </div>                       " + Environment.NewLine +
 "    <div id =\"RecordsContent\"> " + Environment.NewLine +
 "        @Html.Partial(\"PartialIndex\", Model.IndexData)" + Environment.NewLine +
 "    </div>                                              " + Environment.NewLine +
 "</div>                                                  " + Environment.NewLine +
 "                                                        " + Environment.NewLine +

"@if(!this.Request.IsAjaxRequest())" + Environment.NewLine +
"{" + Environment.NewLine +
"@section Scripts" + Environment.NewLine +
 "{                                                       " + Environment.NewLine +
 string.Format("<script src=\"~/Areas/{0}/Scripts/app/{1}/dataSource.js\"></script>", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
 string.Format("<script src=\"~/Areas/{0}/Scripts/app/{1}/{1}.js\"></script>", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
 "}}" + Environment.NewLine;


                return _indexTemplate;
            }


            else
            {
                string _indexTemplate = "@model GSAssetManagement.Web.ViewModel.IndexViewModel" + Environment.NewLine +
                    "@{" + Environment.NewLine +
                    "    Layout = \"\";" + Environment.NewLine +
                    "}" + Environment.NewLine +
                    "" + Environment.NewLine +
"<div class=\"shadow\">" + Environment.NewLine +
"    <div class=\"preview-box\">" + Environment.NewLine +
"        <fieldset class=\"scheduler-border\">" + Environment.NewLine +
"            <legend class=\"scheduler-border\">Search Records</legend>" + Environment.NewLine +
string.Format("@using(Html.BeginForm(\"PartialIndex\", \"{1}\", new {{ area = \"{0}\" }}, FormMethod.Post, new {{ @role = \"form\", @id = \"searchForm\" }}))", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
"            {" + Environment.NewLine +
"" + Environment.NewLine +
"@Html.AntiForgeryToken()" + Environment.NewLine +
string.Format("@Html.Hidden(\"{0}Id\", Model.ParentId);", moduleSetup.ParentTable) + Environment.NewLine +

"" + Environment.NewLine +
"            @Html.Hidden(\"PageSize\", Model.IndexData.PageSize);" + Environment.NewLine +
"" + Environment.NewLine +
"            @Html.Hidden(\"PageNumber\", Model.IndexData.PageNumber);" + Environment.NewLine +
"" + Environment.NewLine +
"            <div class=\"row\">" + Environment.NewLine +
"                    @foreach(var item in Model.FieldRuleList.Where(z => !z.Hidden && z.SearchField))" + Environment.NewLine +
"                {" + Environment.NewLine +
"                    if (item.HtmlDataType == \"text\")" + Environment.NewLine +
"                    {" + Environment.NewLine +
"                            <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
"                                <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle</label>" + Environment.NewLine +
"                                @Html.TextBox(item.ColumnName, item.Value, item.Attributes)" + Environment.NewLine +

"                            </div>" + Environment.NewLine +
"                        }         " + Environment.NewLine +
"                        if (item.HtmlDataType == \"textarea\")" + Environment.NewLine +
"                        {" + Environment.NewLine +
"                            <div class=\"form-group has-feedback col-lg-12\">" + Environment.NewLine +
"                                <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle</label>" + Environment.NewLine +
"                                @Html.TextArea(item.ColumnName, item.Value as string, 5, 20, item.Attributes)" + Environment.NewLine +

"                            </div>" + Environment.NewLine +
"                        }" + Environment.NewLine +
"                         if (item.HtmlDataType == \"select\")" + Environment.NewLine +
  "                                {" + Environment.NewLine +
"if (item.Attributes.Where(x => x.Key == \"ParameterizedSelect\" && x.Value.ToString() == \"True\").Count() > 0)" + Environment.NewLine +

                                    "    {" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +

                                    "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +

                                    "            <div class=\"input-group\">" + Environment.NewLine +

                                    "                <a href=\"#\" class=\"input-group-addon download-select\" data-entityname=\"@item.ColumnName\">" + Environment.NewLine +

                                    "                    <i class=\"fa fa-download\"></i>" + Environment.NewLine +

                                    "                </a>" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "                @Html.DropDownList(item.ColumnName, item.DropdownMasterList, \"--Select--\", item.Attributes)" + Environment.NewLine +



                                    "            </div>" + Environment.NewLine +

                                    "        </div>" + Environment.NewLine +

                                    "    }" + Environment.NewLine +

                                    "    else" + Environment.NewLine +

                                    "    {" + Environment.NewLine +

                                    "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +

                                    "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +

                                    "" + Environment.NewLine +
                                    "            @Html.DropDownList(item.ColumnName, item.DropdownMasterList, \"--Select--\", item.Attributes)" + Environment.NewLine +



                                    "        </div>" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "    }" + Environment.NewLine +
  "                                }         " + Environment.NewLine +
"                    }" + Environment.NewLine +
"                    <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
"                        <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\"></i> Record Status</label>" + Environment.NewLine +
"                        @Html.DropDownList(\"RecordStatus\", EnumDropdownList.ToSelectList(GSAssetManagement.Entity.RecordStatus.Active), \"--Select--\", new { @class = \"form-control\", placeholder = \"Select Record Status\" })" + Environment.NewLine +
"                    </div>" + Environment.NewLine +
"                </div>    " + Environment.NewLine +
"" + Environment.NewLine +
"                <div class=\"row\">" + Environment.NewLine +
"                    <button class=\"btn icon-btn btn-success\" type=\"submit\" style=\"float:right;margin-right:15px;\" id=\"searchRecords\">" + Environment.NewLine +
"                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon-search img-circle text-success\">" + Environment.NewLine +
"                        </span>Search" + Environment.NewLine +
"                    </button>" + Environment.NewLine +
"                    <button class=\"btn icon-btn btn-primary\" type=\"reset\" style=\"float:right;margin-right:15px;\" id=\"resetForm\">" + Environment.NewLine +
"                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon-refresh img-circle text-warning\">" + Environment.NewLine +
"                        </span>Reset" + Environment.NewLine +
"                    </button>                   " + Environment.NewLine +
string.Format("<a class=\"btn icon-btn btn-info childDataSetup\" href=\"/{0}/{1}/Create\" style=\"float:right;margin-right:15px;\">", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
"                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon-plus-sign img-circle text-warning\">" + Environment.NewLine +
"                        </span>New Record" + Environment.NewLine +
"                    </a>" + Environment.NewLine +
"                </div>" + Environment.NewLine +
"" + Environment.NewLine +
"            }" + Environment.NewLine +
"        </fieldset>" + Environment.NewLine +
"" + Environment.NewLine +
"        <fieldset class=\"scheduler-border\">" + Environment.NewLine +
"            <legend class=\"scheduler-border\">Records Details</legend>" + Environment.NewLine +
"            <div class=\"table-responsive\" id=\"RecordsContent\">" + Environment.NewLine +
"                @Html.Partial(\"PartialIndex\", Model.IndexData)" + Environment.NewLine +
"            </div>" + Environment.NewLine +
"        </fieldset>" + Environment.NewLine +
"    </div>" + Environment.NewLine +
"</div>" + Environment.NewLine;


                return _indexTemplate;




            }



        }

        public static string GeneratePartialDetails(ModuleSetup moduleSetup)
        {
            try
            {
                string _partialDetails = "@model GSAssetManagement.Web.Models.Utility.DetailsViewModel" + Environment.NewLine +
    "@{                              " + Environment.NewLine +
    "    Layout = \"\";              " + Environment.NewLine +
    "}" + Environment.NewLine +
    " " + Environment.NewLine +
    " " + Environment.NewLine +
    " " + Environment.NewLine +
   string.Format("@using (Html.BeginForm(\"ModifyRecords\", \"{1}\", new {{ area = \"{0}\" }}, FormMethod.Post, new {{ @role = \"form\", @id = \"mainForm\", @autocomplete = \"off\" }})) ", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
    "            { " + Environment.NewLine +
    "@Html.AntiForgeryToken()" + Environment.NewLine +
    "    @Html.Hidden(\"Id\", Model.FieldRuleList.Where(x => x.ColumnName == \"Id\").Select(z => z.Value).FirstOrDefault()) " + Environment.NewLine +
    "    @Html.Hidden(\"CurrentAction\", GSAssetManagement.Entity.CurrentAction.View)                                                     " + Environment.NewLine +
    "                                                                                                                       " + Environment.NewLine +
    "    <div class=\"shadow\">                                                                                             " + Environment.NewLine +
    "        <div class=\"preview-box\">                                                                                    " + Environment.NewLine +
    "            <fieldset class=\"scheduler-border\">                                                                      " + Environment.NewLine +
    "                <legend class=\"scheduler-border\">Record Details</legend>                                             " + Environment.NewLine +
    "                <div class=\"row\">                                                                                    " + Environment.NewLine +
    "                    @foreach (var item in Model.FieldRuleList.Where(z => !z.Hidden))                                   " + Environment.NewLine +
    "                {                                                                                                      " + Environment.NewLine +
    "                    if (item.HtmlDataType == \"text\")                                                                 " + Environment.NewLine +
    "                    {                                                                                                  " + Environment.NewLine +
    "                            <div class=\"form-group has-feedback col-lg-3\">                                           " + Environment.NewLine +
    "                                <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle</label> " + Environment.NewLine +

    "                                @Html.TextBox(item.ColumnName, item.Value, item.Attributes) " + Environment.NewLine +


    "                            </div> " + Environment.NewLine +
    "                        } " + Environment.NewLine +
    "                        if (item.HtmlDataType == \"textarea\") " + Environment.NewLine +
    "                        {                                      " + Environment.NewLine +
    "                            <div class=\"form-group has-feedback col-lg-12\"> " + Environment.NewLine +
    "                                <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle</label> " + Environment.NewLine +

    "                                @Html.TextArea(item.ColumnName, item.Value as string, 5, 20, item.Attributes) " + Environment.NewLine +


    "                            </div>                                                " + Environment.NewLine +
    "                        }                                                         " + Environment.NewLine +
    "                        if (item.HtmlDataType == \"select\")                      " + Environment.NewLine +
    "                        {                                                         " + Environment.NewLine +
    "if (item.Attributes.Where(x => x.Key == \"ParameterizedSelect\" && x.Value.ToString() == \"True\").Count() > 0)" + Environment.NewLine +

                                    "    {" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +

                                    "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +

                                    "            <div class=\"input-group\">" + Environment.NewLine +

                                    "                <a href=\"#\" class=\"input-group-addon download-select\" data-entityname=\"@item.ColumnName\">" + Environment.NewLine +

                                    "                    <i class=\"fa fa-download\"></i>" + Environment.NewLine +

                                    "                </a>" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "                @Html.DropDownList(item.ColumnName, item.DropdownMasterList, \"--Select--\", item.Attributes)" + Environment.NewLine +




                                    "            </div>" + Environment.NewLine +

                                    "        </div>" + Environment.NewLine +

                                    "    }" + Environment.NewLine +

                                    "    else" + Environment.NewLine +

                                    "    {" + Environment.NewLine +

                                    "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +

                                    "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "            @Html.DropDownList(item.ColumnName, item.DropdownMasterList, \"--Select--\", item.Attributes)" + Environment.NewLine +




                                    "        </div>" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "    }" + Environment.NewLine +
    "                        }          " + Environment.NewLine +
    "                    }              " + Environment.NewLine +
    "                </div>             " + Environment.NewLine +
    "            </fieldset>            " + Environment.NewLine +
    "            <div class=\"row\">    " + Environment.NewLine +
    "                <div class=\"shadow\">              " + Environment.NewLine +
    "                    <div class=\"preview-box\">     " + Environment.NewLine +
    "                        <div class=\"alert alert-success\"> " + Environment.NewLine +
    "                            <span class=\"glyphicon glyphicon-ok\"></span> <strong>Record Log</strong> " + Environment.NewLine +
    "                            <hr class=\"message-inner-separator\">                                     " + Environment.NewLine +
    "                            <div class=\"box-body table-responsive no-padding\">                       " + Environment.NewLine +
    "                                <table class=\"table table-bordered\">                                 " + Environment.NewLine +
    "                                    <thead>                                                            " + Environment.NewLine +
    "                                        <tr>                                                           " + Environment.NewLine +
    "                                            <th>#</th>                                                 " + Environment.NewLine +
    "                                            <th nowrap>Record Status</th>                              " + Environment.NewLine +
    "                                            <th nowrap>Created By</th>                                 " + Environment.NewLine +
    "                                            <th nowrap>Created Date</th>                               " + Environment.NewLine +
    "                                            <th nowrap>Modified By</th>                                " + Environment.NewLine +
    "                                            <th nowrap>Modified Date</th>                              " + Environment.NewLine +
    "                                            <th nowrap>Authorise By</th>                               " + Environment.NewLine +
    "                                            <th nowrap>Authorise Date</th>                             " + Environment.NewLine +
    "                                                                                                       " + Environment.NewLine +
    "                                        </tr>                                                          " + Environment.NewLine +
    "                                    </thead>                                                           " + Environment.NewLine +
    "                                    <tbody>                                                            " + Environment.NewLine +
    "                                        <tr>                                                           " + Environment.NewLine +
    "                                            <td>1</td>                                                 " + Environment.NewLine +
    "                                            <th nowrap>@(((GSAssetManagement.Entity.RecordStatus)Model.FieldRuleList.Where(z => z.ColumnName == \"RecordStatus\").FirstOrDefault().Value).ToString())</th> " + Environment.NewLine +
    "                                            <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"CreatedBy\").FirstOrDefault().Value</th>    " + Environment.NewLine +
    "                                            <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"CreatedDate\").FirstOrDefault().Value</th>  " + Environment.NewLine +
    "                                            <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"ModifiedBy\").FirstOrDefault().Value</th>   " + Environment.NewLine +
    "                                            <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"ModifiedDate\").FirstOrDefault().Value</th> " + Environment.NewLine +
    "                                            <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"AuthorisedBy\").Select(x => x.Value).FirstOrDefault()</th> " + Environment.NewLine +
    "                                            <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"AuthorisedDate\").Select(x => x.Value).FirstOrDefault()</th> " + Environment.NewLine +
    "                                        </tr> " + Environment.NewLine +
    "                                    </tbody>  " + Environment.NewLine +
    "                                </table>      " + Environment.NewLine +
    "                            </div>            " + Environment.NewLine +
    "                        </div>                " + Environment.NewLine +
    "                    </div>                    " + Environment.NewLine +
    "                </div>                        " + Environment.NewLine +
    "            </div>                            " + Environment.NewLine +
    "            @if (Model.ChangeLogList.Count > 0) " + Environment.NewLine +
    "                {                               " + Environment.NewLine +
    "                <div class=\"row\">             " + Environment.NewLine +
    "                    <div class=\"shadow\">      " + Environment.NewLine +
    "                        <div class=\"preview-box\"> " + Environment.NewLine +
    "                            <div class=\"alert alert-danger\"> " + Environment.NewLine +
    "                                <span class=\"glyphicon glyphicon-alert \"></span> <strong>Change Logs</strong> " + Environment.NewLine +
    "                                <hr class=\"message-inner-separator\">                                          " + Environment.NewLine +
    "                                <div class=\"box-body table-responsive no-padding\">                            " + Environment.NewLine +
    "                                    <table class=\"table table-bordered\">                                      " + Environment.NewLine +
    "                                        <thead>                                                                 " + Environment.NewLine +
    "                                            <tr>                                                                " + Environment.NewLine +
    "                                                <th>#</th>                                                      " + Environment.NewLine +
    "                                                <th>Field Name</th>                                             " + Environment.NewLine +
    "                                                <th>Log Status</th>                                             " + Environment.NewLine +
    "                                                <th>Modified Date</th>                                          " + Environment.NewLine +
    "                                                <th>Original Value</th>                                         " + Environment.NewLine +
    "                                                <th>Modified Value</th>                                         " + Environment.NewLine +
    "                                            </tr>                                                               " + Environment.NewLine +
    "                                        </thead>                                                                " + Environment.NewLine +
    "                                        <tbody>                                                                 " + Environment.NewLine +
    "                                            @{                                                                  " + Environment.NewLine +
    "                                                int i = 1;                                                      " + Environment.NewLine +
    "                                                string _class = string.Empty;                                   " + Environment.NewLine +
    "                                                foreach (var item in Model.ChangeLogList)                       " + Environment.NewLine +
    "                                                {                                                               " + Environment.NewLine +
    "                                                    if (i % 2 != 0)                                             " + Environment.NewLine +
    "                                                    {                                                           " + Environment.NewLine +
    "                                                        _class = \"success\";                                   " + Environment.NewLine +
    "                                                    }                                                           " + Environment.NewLine +
    "                                                    else                                                        " + Environment.NewLine +
    "                                                    {                                                           " + Environment.NewLine +
    "                                                        _class = \"warning\";                                   " + Environment.NewLine +
    "                                                    }                                                           " + Environment.NewLine +
    "                                                    <tr class=\"@_class\">                                      " + Environment.NewLine +
    "                                                        <td scope=\"row\"> @i </td>                             " + Environment.NewLine +
    "                                                        <td> @item.PropertyName </td>                           " + Environment.NewLine +
    "                                                        <td> @item.LogStatus </td>                              " + Environment.NewLine +
    "                                                        <td> @item.ModifiedDate </td>                           " + Environment.NewLine +
    "                                                        <td> @item.Originalvalue </td>                          " + Environment.NewLine +
    "                                                        <td> @item.ModifiedValue </td>                          " + Environment.NewLine +
    "                                                                                                                " + Environment.NewLine +
    "                                                    </tr>                                                       " + Environment.NewLine +
    "                                                    i++;                                                        " + Environment.NewLine +
    "                                                }                                                               " + Environment.NewLine +
    "                                            }                                                                   " + Environment.NewLine +
    "                                        </tbody>                                                                " + Environment.NewLine +
    "                                    </table>                                                                    " + Environment.NewLine +
    "                                </div>                                                                          " + Environment.NewLine +
    "                            </div>                                                                              " + Environment.NewLine +
    "                        </div>                                                                                  " + Environment.NewLine +
    "                    </div>                                                                                      " + Environment.NewLine +
    "                                                                                                                " + Environment.NewLine +
    "                                                                                                                " + Environment.NewLine +
    "                                                                                                                " + Environment.NewLine +
    "                                                                                                                " + Environment.NewLine +
    "                </div>                                                                                          " + Environment.NewLine +
    "                                                }                                                               " + Environment.NewLine +
    "            <div class=\"row\">                                                                                 " + Environment.NewLine +
    "                <div class=\"col-sm-12 col-md-12\" style=\"margin-top:20px;\" id=\"responseContent\">           " + Environment.NewLine +
    "                                                                                                                " + Environment.NewLine +
    "                </div>                                                                                          " + Environment.NewLine +
    "            </div>                                                                                              " + Environment.NewLine +
    "            <div class=\"row\">                                                                                 " + Environment.NewLine +
    "                                                                                                                " + Environment.NewLine +
    "                @if ((GSAssetManagement.Entity.RecordStatus)Model.FieldRuleList.Where(x => x.ColumnName == \"RecordStatus\").Select(z => z.Value).FirstOrDefault() == GSAssetManagement.Entity.RecordStatus.Active || ((GSAssetManagement.Entity.RecordStatus)Model.FieldRuleList.Where(x => x.ColumnName == \"RecordStatus\").Select(z => z.Value).FirstOrDefault() == GSAssetManagement.Entity.RecordStatus.Reverted && (Guid)Model.FieldRuleList.Where(x => x.ColumnName == \"ModifiedById\").Select(z => z.Value).FirstOrDefault() == ViewUserInformation.GetCurrentUserId())) " + Environment.NewLine +
    "                { " + Environment.NewLine +
    string.Format("if (GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{0}, \"{1}\", GSAssetManagement.Entity.CurrentAction.Edit)) ", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
    "                    { " + Environment.NewLine +
    "                        <button class=\"btn icon-btn btn-success update saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Edit\"> " + Environment.NewLine +
    "                            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-disk img-circle text-success\"> " + Environment.NewLine +
    "                            </span>Update Record " + Environment.NewLine +
    "                        </button> " + Environment.NewLine +
    "                        <button class=\"btn icon-btn btn-warning unlock\" type=\"button\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Edit\"> " + Environment.NewLine +
    "                            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-lock img-circle text-success\"> " + Environment.NewLine +
    "                            </span>Unlock Record " + Environment.NewLine +
    "                        </button> " + Environment.NewLine +
    "                    }             " + Environment.NewLine +
       string.Format("if (GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{0}, \"{1}\", GSAssetManagement.Entity.CurrentAction.Close))", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
                "                    {" + Environment.NewLine +
                "                        <button class=\"btn icon-btn btn-danger update saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Delete\">" + Environment.NewLine +
                "                            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-trash img-circle text-success\">" + Environment.NewLine +
                "                            </span>Delete Record" + Environment.NewLine +
                "                        </button>" + Environment.NewLine +
                "                    }" + Environment.NewLine +
                string.Format("if (GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{0}, \"{1}\", GSAssetManagement.Entity.CurrentAction.Edit))", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
                "                    {" + Environment.NewLine +
                "                        <button class=\"btn icon-btn btn-warning update saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Close\">" + Environment.NewLine +
                "                            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-remove img-circle text-success\">" + Environment.NewLine +
                "                            </span>Close Record" + Environment.NewLine +
                "                        </button>" + Environment.NewLine +
                "                    }" + Environment.NewLine +
    "                }                 " + Environment.NewLine +
    "                                  " + Environment.NewLine +
    "                else if ((GSAssetManagement.Entity.RecordStatus)Model.FieldRuleList.Where(x => x.ColumnName == \"RecordStatus\").Select(z => z.Value).FirstOrDefault() == GSAssetManagement.Entity.RecordStatus.UnVerified) " + Environment.NewLine +
    "                { " + Environment.NewLine +
    string.Format("if (GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{0}, \"{1}\", GSAssetManagement.Entity.CurrentAction.Revert)) ", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
    "                    { " + Environment.NewLine +
    "                        <button class=\"btn icon-btn btn-warning revert saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Revert\"> " + Environment.NewLine +
    "                            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-remove img-circle text-success\"> " + Environment.NewLine +
    "                            </span>Revert Record                                                                                         " + Environment.NewLine +
    "                        </button>                                                                                                        " + Environment.NewLine +
    "                    }                                                                                                                    " + Environment.NewLine +
    "                                                                                                                                         " + Environment.NewLine +
    string.Format("if (GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{0}, \"{1}\", GSAssetManagement.Entity.CurrentAction.Revert)) ", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
    "                    { " + Environment.NewLine +
    "                        <button class=\"btn icon-btn btn-success authorise saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Authorise\"> " + Environment.NewLine +
    "                            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-saved img-circle text-success\"> " + Environment.NewLine +
    "                            </span>Authorise Record                                                                                     " + Environment.NewLine +
    "                        </button>                                                                                                       " + Environment.NewLine +
    "                    }                                                                                                                   " + Environment.NewLine +
    "                }                                                                                                                       " + Environment.NewLine +
    "                                                                                                                                        " + Environment.NewLine +
    "            </div>                                                                                                                      " + Environment.NewLine +
    "                                                                                                                                        " + Environment.NewLine +
    "                                                                                                                                        " + Environment.NewLine +
    "                                                                                                                                        " + Environment.NewLine +
    "</div>" + Environment.NewLine +
    "</div>" + Environment.NewLine +
"}" + Environment.NewLine;

                return _partialDetails;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GeneratePartialIndex(ModuleSetup moduleSetup)
        {
            var _template = " @model GSAssetManagement.Infrastructure.PagedResultDataTable" + Environment.NewLine +
"@using GSAssetManagement.Web" + Environment.NewLine +
"@{" + Environment.NewLine +
"                Layout = \"\";" + Environment.NewLine +
"}" + Environment.NewLine +
"<table class=\"table table-bordered searchme-table\">" + Environment.NewLine +
"    <thead>                                                                    " + Environment.NewLine +
"        <tr>                                                                   " + Environment.NewLine +
"            <th>#</th>                                                         " + Environment.NewLine +
"            <th>Action</th>                                                    " + Environment.NewLine +
"            @foreach(System.Data.DataColumn column in Model.Results.Columns)   " + Environment.NewLine +
"        {                                                                      " + Environment.NewLine +
"            if (!column.ColumnName.Contains(\"PageCount\") && !column.ColumnName.Contains(\"TotalRecords\") && !column.ColumnName.Contains(\"Id\"))" + Environment.NewLine +
"                {                                                         " + Environment.NewLine +
"                    <th nowrap> @column.ColumnName </th>              " + Environment.NewLine +
"                }                                                         " + Environment.NewLine +
"        }                                                                 " + Environment.NewLine +
"        </tr>                                                             " + Environment.NewLine +
"    </thead>                                                              " + Environment.NewLine +
"    <tbody>                                                               " + Environment.NewLine +
"        @{                                                                " + Environment.NewLine +
"            int i = 1;                                                    " + Environment.NewLine +
"        string _class = string.Empty;                                     " + Environment.NewLine +
"            foreach (System.Data.DataRow item in Model.Results.Rows)      " + Environment.NewLine +
"            {                                                             " + Environment.NewLine +
"                if ((GSAssetManagement.Entity.RecordStatus)Enum.Parse(typeof(GSAssetManagement.Entity.RecordStatus), item[\"Record Status\"].ToString()) == GSAssetManagement.Entity.RecordStatus.UnVerified)" + Environment.NewLine +
"                {" + Environment.NewLine +
"                    _class = \"warning\";" + Environment.NewLine +
"                }                        " + Environment.NewLine +
"                else if ((GSAssetManagement.Entity.RecordStatus)Enum.Parse(typeof(GSAssetManagement.Entity.RecordStatus), item[\"Record Status\"].ToString()) == GSAssetManagement.Entity.RecordStatus.Inactive)" + Environment.NewLine +
"                {" + Environment.NewLine +
"                    _class = \"danger\";" + Environment.NewLine +
"                }                       " + Environment.NewLine +
"                else                    " + Environment.NewLine +
"                {                       " + Environment.NewLine +
"                    _class = \"success\";" + Environment.NewLine +
"                                         " + Environment.NewLine +
"                }                        " + Environment.NewLine +
"                <tr class=\"@_class\">   " + Environment.NewLine +
"                    <td scope =\"row\"> @i </td>" + Environment.NewLine;

            if (string.IsNullOrEmpty(moduleSetup.ParentTable))
            {
                _template += string.Format("<td title =\"Preview Record Detail\"><a href=\"/{0}/{1}/Details/?Id=@(item[\"Id\"].ToString())\" ><i class=\"glyphicon glyphicon-edit\"></i></a></td>", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine;
            }
            else
            {
                _template += string.Format("<td title =\"Preview Record Detail\"><a href=\"/{0}/{1}/Details\" data-id=\"@(item[\"Id\"].ToString())\" class=\"viewRecordDetails\"><i class=\"glyphicon glyphicon-edit\"></i></a></td>", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine;
            }


            _template += "                    @foreach(System.Data.DataColumn column in Model.Results.Columns)" + Environment.NewLine +
            "{                                      " + Environment.NewLine +
            "    if (!column.ColumnName.Contains(\"PageCount\") && !column.ColumnName.Contains(\"TotalRecords\") && !column.ColumnName.Contains(\"Id\")) " + Environment.NewLine +
            "                        {                                                                            " + Environment.NewLine +
            "        if (column.ColumnName == \"Record Status\")                                                  " + Environment.NewLine +
            "                            {                                                                        " + Environment.NewLine +
            "                                <td nowrap> @Enum.Parse(typeof(GSAssetManagement.Entity.RecordStatus), item[column.ColumnName].ToString()).ToString() </td>" + Environment.NewLine +
            "                            }                                                                                                                   " + Environment.NewLine +
            "                            else if (column.ColumnName == \"Deleted Status\" && !string.IsNullOrEmpty(item[column.ColumnName].ToString()))      " + Environment.NewLine +
            "                            {                                                                                                                   " + Environment.NewLine +
            "                                <td nowrap> @Enum.Parse(typeof(GSAssetManagement.Entity.DeletedStatus), item[column.ColumnName].ToString()).ToString() </td>" + Environment.NewLine +
            "                            }              " + Environment.NewLine +
            "                            else           " + Environment.NewLine +
            "                            {              " + Environment.NewLine +
            "                                <td nowrap> @item[column.ColumnName].ToString() </td>   " + Environment.NewLine +
            "                            }                                                             " + Environment.NewLine +
            "    }                                                                                     " + Environment.NewLine +
            "}                                                                                         " + Environment.NewLine +
            "                </tr>                                                                     " + Environment.NewLine +
            "                i++;                                                                      " + Environment.NewLine +
            "            }                                                                             " + Environment.NewLine +
            "        }                                                                                 " + Environment.NewLine +
            "    </tbody>                                                                              " + Environment.NewLine +
            "</table>                                                                                  " + Environment.NewLine +
            "<div class=\"form-group\">                                                                " + Environment.NewLine +
            "    <div class=\"col-md-6 col-sm-3 col-xs-12\">                                           " + Environment.NewLine +
            "        <nav aria-label=\"Page navigation\" style=\"float:left;\">                        " + Environment.NewLine +
            "            <ul class=\"pagination recordSize\">                                          " + Environment.NewLine +
            "                <li>                                                                      " + Environment.NewLine +
            "                    <a href =\"#\" aria-label=\"Previous\">                               " + Environment.NewLine +
            "                        <span aria-hidden=\"true\">&larr;</span> Records Per Page         " + Environment.NewLine +
            "                    </a>                                                                  " + Environment.NewLine +
            "                </li>                                                                     " + Environment.NewLine +
            "                <li><a href =\"#\" data-pagesize=\"10\">10</a></li>                       " + Environment.NewLine +
            "                <li><a href =\"#\" data-pagesize=\"20\">20</a></li>                       " + Environment.NewLine +
            "                <li><a href =\"#\" data-pagesize=\"40\">40</a></li>                       " + Environment.NewLine +
            "                <li><a href =\"#\" data-pagesize=\"50\">50</a></li>                       " + Environment.NewLine +
            "                <li><a href =\"#\" data-pagesize=\"100\">100</a></li>                     " + Environment.NewLine +
            "            </ul>                                                                         " + Environment.NewLine +
            "        </nav>                                                                            " + Environment.NewLine +
            "    </div>                                                                                " + Environment.NewLine +
            "    <div class=\"col-md-6 col-sm-3 col-xs-12\">                                           " + Environment.NewLine +
            "        <nav aria-label=\"Page navigation\" style=\"float:right;\">   " + Environment.NewLine +
            string.Format("@Html.RenderPager(\"{0}\", \"{1}\", \"PartialIndex\", (int)Math.Ceiling((decimal)Model.RowCount / ((decimal)Model.PageSize == 0 ? 1 : (decimal)Model.PageSize)), Model.PageSize, Model.PageNumber)", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
            "        </nav>   " + Environment.NewLine +
            "    </div>       " + Environment.NewLine +
            "</div>           " + Environment.NewLine;


            return _template;
        }   
        public static string GenerateCreate(ModuleSetup moduleSetup)
        {


            if (moduleSetup.ParentTable == null)
            {
                string _createTemplate = "@model List<GSAssetManagement.Web.Models.Utility.FieldRuleViewModel>" + Environment.NewLine +
                  "@{" + Environment.NewLine +
                  "    Layout = this.Request.IsAjaxRequest() ? \"\" : \"~/Views/Shared/_Layout.cshtml\";" + Environment.NewLine +
                  "}" + Environment.NewLine +

 string.Format("<div class=\"row\" id=\"{0}Workspace\">", moduleSetup.DatabaseTable) + Environment.NewLine +
  "    @Html.Hidden(\"ParentId\", null)" + Environment.NewLine +
  "    <div class=\"col-md-2\">" + Environment.NewLine +
  string.Format("<a href = \"/{0}/{1}/Create\" class=\"btn btn-success btn-flat btn-block margin-bottom\">Add More</a>", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
  "        <div class=\"box box-solid\">" + Environment.NewLine +
  "            <div class=\"box-body no-padding\">" + Environment.NewLine +
  "                <ul class=\"nav nav-pills nav-stacked\">" + Environment.NewLine;

                _createTemplate += string.Format("<li class=\"active\"><a href = \"/{0}/{2}/Details\" class=\"scopeChange\" data-scope=\"parent\"><i class=\"fa fa-hand-o-right\"></i> {1}</a></li>", moduleSetup.Module, moduleSetup.TableTitle, moduleSetup.DatabaseTable) + Environment.NewLine;

                moduleSetup.ChildLists.ForEach(c =>
                {

                    _createTemplate += string.Format("<li><a href=\"/{0}/{1}\" class=\"scopeChange\" data-scope=\"child\"><i class=\"fa fa-hand-o-right\"></i> {2}</a></li>", moduleSetup.Module, c.Item1, c.Item2) + Environment.NewLine;

                });



                _createTemplate += "" + Environment.NewLine +
                 "                </ul>" + Environment.NewLine +
                 "            </div>" + Environment.NewLine +
                 "        </div>" + Environment.NewLine +
                 "    </div>" + Environment.NewLine +
 "" + Environment.NewLine +
                 "<div class=\"col-md-10\" id=\"allLiContentsHere\">" + Environment.NewLine +
  string.Format("@using(Html.BeginForm(\"Create\",\"{0}\", new {{ area = \"{1}\" }}, FormMethod.Post, new {{ @role = \"form\", @id = \"mainForm\", @autocomplete = \"off\" }}))", moduleSetup.DatabaseTable, moduleSetup.Module) + Environment.NewLine +
 "            {" + Environment.NewLine +
 "            @Html.Hidden(\"Id\", Model.Where(x => x.ColumnName == \"Id\").Select(z => z.Value).FirstOrDefault())" + Environment.NewLine +
 "            @Html.Hidden(\"CurrentAction\", GSAssetManagement.Entity.CurrentAction.Create)" + Environment.NewLine +
 "            @Html.AntiForgeryToken();" + Environment.NewLine +
 "" + Environment.NewLine +
 "            <div class=\"shadow\">" + Environment.NewLine +
 "                <div class=\"preview-box\">" + Environment.NewLine +
 "                    <div class=\"row\">" + Environment.NewLine +
 "                        @foreach(var item in Model.Where(z => !z.Hidden))" + Environment.NewLine +
 "                        {" + Environment.NewLine +
 "                            if (item.HtmlDataType == \"text\")" + Environment.NewLine +
 "                            {" + Environment.NewLine +
 "                                <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
 "                                    <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title = \"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +
 "                                    @Html.TextBox(item.ColumnName, item.Value, item.Attributes)" + Environment.NewLine +
 "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
 "                                </div>" + Environment.NewLine +
 "                            }" + Environment.NewLine +
 "if (item.HtmlDataType == \"password\")" + Environment.NewLine +
 "                            {" + Environment.NewLine +
 "                                <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
 "                                    <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title = \"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +
 "                                    @Html.Password(item.ColumnName, item.Value, item.Attributes)" + Environment.NewLine +
 "                                    <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
 "                                </div>" + Environment.NewLine +
 "                            }" + Environment.NewLine +
 "                            if (item.HtmlDataType == \"textarea\")" + Environment.NewLine +
 "                            {" + Environment.NewLine +
 "                                <div class=\"form-group has-feedback col-lg-12\">" + Environment.NewLine +
 "                                    <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title = \"@item.HelpMessage\"></i> @item.FieldTitle </label> " + Environment.NewLine +
 "                                    @Html.TextArea(item.ColumnName, item.Value as string, 5, 20, item.Attributes)" + Environment.NewLine +
 "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
 "                                </div>" + Environment.NewLine +
 "                            }" + Environment.NewLine +
 "                            if (item.HtmlDataType == \"select\")" + Environment.NewLine +
 "                            {" + Environment.NewLine +
                                 "if (item.Attributes.Where(x => x.Key == \"ParameterizedSelect\" && x.Value.ToString() == \"True\").Count() > 0)" + Environment.NewLine +

                                    "    {" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +

                                    "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +

                                    "            <div class=\"input-group\">" + Environment.NewLine +

                                    "                <a href=\"#\" class=\"input-group-addon download-select\" data-entityname=\"@item.ColumnName\">" + Environment.NewLine +

                                    "                    <i class=\"fa fa-download\"></i>" + Environment.NewLine +

                                    "                </a>" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "                @Html.DropDownList(item.ColumnName, item.DropdownMasterList, \"--Select--\", item.Attributes)" + Environment.NewLine +

                                    "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +

                                    "            </div>" + Environment.NewLine +

                                    "        </div>" + Environment.NewLine +

                                    "    }" + Environment.NewLine +

                                    "    else" + Environment.NewLine +

                                    "    {" + Environment.NewLine +

                                    "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +

                                    "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "            @Html.DropDownList(item.ColumnName, item.DropdownMasterList, \"--Select--\", item.Attributes)" + Environment.NewLine +
                                    "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +


                                    "        </div>" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "    }" + Environment.NewLine +
 "                            }" + Environment.NewLine +
 "                        }" + Environment.NewLine +
 "                    </div>" + Environment.NewLine +
 "                    <div class=\"row\">" + Environment.NewLine +
 "                        <button class=\"btn icon-btn btn-success saveRecords\" type=\"submit\" style=\"float:right; margin-right:15px; \" disabled>" + Environment.NewLine +
 "                            <span class=\"btn-glyphicon glyphicon glyphicon-floppy-disk img-circle text-success\">" + Environment.NewLine +
 "                            </span>Save Record" + Environment.NewLine +
 "                        </button>" + Environment.NewLine +
 "" + Environment.NewLine +
 "                    </div>" + Environment.NewLine +
 "                </div>" + Environment.NewLine +
 "            </div>" + Environment.NewLine +
 "        }" + Environment.NewLine +
 "    </div>" + Environment.NewLine +
 "</div>" + Environment.NewLine +
 "" + Environment.NewLine +
 "" + Environment.NewLine +
 "@section Scripts" + Environment.NewLine +
 "{" + Environment.NewLine +
 string.Format("<script src=\"~/Areas/{0}/Scripts/app/{1}/dataSource.js\"></script>", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
 string.Format("<script src=\"~/Areas/{0}/Scripts/app/{1}/{1}.js\"></script>", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
 "}" + Environment.NewLine;

                return _createTemplate;
            }
            else
            {
                string _createTemplate = "@model List<GSAssetManagement.Web.Models.Utility.FieldRuleViewModel>" + Environment.NewLine +
                  "@{" + Environment.NewLine +
                  "    Layout = this.Request.IsAjaxRequest() ? \"\" : \"~/Views/Shared/_Layout.cshtml\";" + Environment.NewLine +
                  "}" + Environment.NewLine +


  "<div class=\"shadow\">" + Environment.NewLine +
     " <div class=\"preview-box\">" + Environment.NewLine +
     string.Format("@using(Html.BeginForm(\"Create\", \"{0}\", new {{ area = \"{1}\" }}, FormMethod.Post, new {{ @role = \"form\", @id = \"mainForm\" }}))", moduleSetup.DatabaseTable, moduleSetup.Module) + Environment.NewLine +
     "     {" + Environment.NewLine;

                _createTemplate += "@Html.AntiForgeryToken()" + Environment.NewLine;

                moduleSetup.ModuleRuleSetupList.Where(h => h.Hidden).ToList().ForEach(k =>
                {

                    _createTemplate += string.Format("@Html.Hidden(\"{0}Id\", Model.Where(x => x.ColumnName == \"{0}Id\").Select(z => z.Value).FirstOrDefault(), new {{ @Id =\"{1}_{0}\" }})", moduleSetup.ParentTable, moduleSetup.DatabaseTable) + Environment.NewLine;
                });


                _createTemplate += string.Format("@Html.Hidden(\"SubmissionRemarks\", null, new {{ @id = \"{0}_SubmissionRemarks\" }})", moduleSetup.DatabaseTable) + Environment.NewLine +
                  "" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "         <div class=\"row\">" + Environment.NewLine +
                  "             @foreach(var item in Model.Where(z => !z.Hidden))" + Environment.NewLine +
                  "             {" + Environment.NewLine +
                  "                 if (item.HtmlDataType == \"text\")" + Environment.NewLine +
                  "                 {" + Environment.NewLine +
                  "                     <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
                  "                         <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle</label>" + Environment.NewLine +

                  "                         @Html.TextBox(item.ColumnName, item.Value, item.Attributes)" + Environment.NewLine +
                  "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
                  "                     </div>" + Environment.NewLine +
                  "                 }" + Environment.NewLine +
                  "                 if (item.HtmlDataType ==\"textarea\")" + Environment.NewLine +
                  "                 {" + Environment.NewLine +
                  "                     <div class=\"form-group has-feedback col-lg-12\">" + Environment.NewLine +
                  "                         <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle</label>" + Environment.NewLine +

                  " @Html.TextArea(item.ColumnName, item.Value as string, 5, 20, item.Attributes)" + Environment.NewLine +
                  "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
                  "                     </div>" + Environment.NewLine +
                  "                 }" + Environment.NewLine +
                  "                 if (item.HtmlDataType == \"select\")" + Environment.NewLine +
                  "                 {" + Environment.NewLine +
                 "if (item.Attributes.Where(x => x.Key == \"ParameterizedSelect\" && x.Value.ToString() == \"True\").Count() > 0)" + Environment.NewLine +

                                                 "    {" + Environment.NewLine +

                                                 "" + Environment.NewLine +

                                                 "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +

                                                 "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +

                                                 "            <div class=\"input-group\">" + Environment.NewLine +

                                                 "                <a href=\"#\" class=\"input-group-addon download-select\" data-entityname=\"@item.ColumnName\">" + Environment.NewLine +

                                                 "                    <i class=\"fa fa-download\"></i>" + Environment.NewLine +

                                                 "                </a>" + Environment.NewLine +

                                                 "" + Environment.NewLine +

                                                 "                @Html.DropDownList(item.ColumnName, item.DropdownMasterList, \"--Select--\", item.Attributes)" + Environment.NewLine +
                                                 "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +


                                                 "            </div>" + Environment.NewLine +

                                                 "        </div>" + Environment.NewLine +

                                                 "    }" + Environment.NewLine +

                                                 "    else" + Environment.NewLine +

                                                 "    {" + Environment.NewLine +

                                                 "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +

                                                 "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +

                                                 "" + Environment.NewLine +

                                                 "            @Html.DropDownList(item.ColumnName, item.DropdownMasterList, \"--Select--\", item.Attributes)" + Environment.NewLine +
                                                 "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +


                                                 "        </div>" + Environment.NewLine +

                                                 "" + Environment.NewLine +

                                                 "    }" + Environment.NewLine +
                  "                 }" + Environment.NewLine +
                  "             }" + Environment.NewLine +
                  "         </div>" + Environment.NewLine +
                  "" + Environment.NewLine +
                  "         <div class=\"row\">" + Environment.NewLine +
                  "             <button class=\"btn icon-btn btn-success saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" disabled>" + Environment.NewLine +
                  "                 <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-disk img-circle text-success\">" + Environment.NewLine +
                  "                 </span>Save Record" + Environment.NewLine +
                  "             </button>" + Environment.NewLine +
                  "             <button class=\"btn icon-btn btn-primary cancel\" type=\"reset\" style=\"float:right;margin-right:15px;\">" + Environment.NewLine +
                  "                 <span class=\"glyphicon btn-glyphicon glyphicon glyphicon-refresh img-circle text-warning\">" + Environment.NewLine +
                  "                 </span>Cancel" + Environment.NewLine +
                  "             </button>" + Environment.NewLine +
                  "         </div>" + Environment.NewLine +
                  "         <div class=\"row\">" + Environment.NewLine +
                  "             <div class=\"col-sm-12 col-md-12\" style=\"margin-top:20px;\" id=\"responseContent\">" + Environment.NewLine +
                  "             </div>" + Environment.NewLine +
                  "         </div>    " + Environment.NewLine +
                  "                   " + Environment.NewLine +
                  "     }             " + Environment.NewLine +
                  " </div>            " + Environment.NewLine +
               "</div>                " + Environment.NewLine;

                return _createTemplate;
            }

        }
        public static string GenerateDetails(ModuleSetup moduleSetup)
        {
            if (string.IsNullOrEmpty(moduleSetup.ParentTable))
            {
                string _detailsTemplate = "@model GSAssetManagement.Web.Models.Utility.DetailsViewModel																					" + Environment.NewLine +
"@{                                                                                                                             " + Environment.NewLine +
"    Layout = this.Request.IsAjaxRequest() ? \"\" : \"~/Views/Shared/_Layout.cshtml\";                                          " + Environment.NewLine +
"}                                                                                                                              " + Environment.NewLine +
"                                                                                                                               " + Environment.NewLine +
"                                                                                                                               " + Environment.NewLine +
string.Format("<div class=\"row\" id=\"{0}Workspace\">", moduleSetup.DatabaseTable) + Environment.NewLine +
"    @Html.Hidden(\"ParentId\", Model.FieldRuleList.Where(x => x.ColumnName == \"Id\").Select(z => z.Value).FirstOrDefault())   " + Environment.NewLine +
"    <div class=\"col-md-2\">                                                                                                   " + Environment.NewLine +
"        <a href=\"#\" id=\"addMoreRecords\" class=\"btn btn-success btn-flat btn-block margin-bottom\">Add More</a>            " + Environment.NewLine +
"        <div class=\"box box-solid\">                                                                                          " + Environment.NewLine +
"            <div class=\"box-body no-padding\">                                                                                " + Environment.NewLine +
"                <ul class=\"nav nav-pills nav-stacked\">           " +
"                                                            " + Environment.NewLine;
                _detailsTemplate += string.Format("<li class=\"active\"><a href = \"/{0}/{2}/Details\" class=\"scopeChange\" data-scope=\"parent\"><i class=\"fa fa-hand-o-right\"></i> {1}</a></li>", moduleSetup.Module, moduleSetup.TableTitle, moduleSetup.DatabaseTable) + Environment.NewLine;

                moduleSetup.ChildLists.ForEach(c =>
                {

                    _detailsTemplate += string.Format("<li><a href=\"/{0}/{1}\" class=\"scopeChange\" data-scope=\"child\"><i class=\"fa fa-hand-o-right\"></i> {2}</a></li>", moduleSetup.Module, c.Item1, c.Item2) + Environment.NewLine;

                });


                _detailsTemplate += "                </ul>                                                                                                                                                                                           " + Environment.NewLine +
  "            </div>                                                                                                                                                                                              " + Environment.NewLine +
  "        </div>                                                                                                                                                                                                  " + Environment.NewLine +
  "    </div>" + Environment.NewLine +
  "" + Environment.NewLine +
  "    <div class=\"col-md-10\" id=\"allLiContentsHere\">" + Environment.NewLine +
  string.Format("@using (Html.BeginForm(\"ModifyRecords\", \"{0}\", new {{ area = \"{1}\" }}, FormMethod.Post, new {{ @role = \"form\", @id = \"mainForm\", @autocomplete = \"off\" }}))", moduleSetup.DatabaseTable, moduleSetup.Module) + Environment.NewLine +
  "            {           " +
  "                                                                                                                                                                              " + Environment.NewLine +
  "@Html.AntiForgeryToken()" + Environment.NewLine +
  "            @Html.Hidden(\"Id\", Model.FieldRuleList.Where(x => x.ColumnName == \"Id\").Select(z => z.Value).FirstOrDefault())                                                                        " + Environment.NewLine +
  "            @Html.Hidden(\"CurrentAction\", GSAssetManagement.Entity.CurrentAction.Create)                                                                                                                          " + Environment.NewLine +
  "                                                                                                                                                                                                      " + Environment.NewLine +
  "            <div class=\"shadow\">                                                                                                                                                                    " + Environment.NewLine +
  "                <div class=\"preview-box\">                                                                                                                                                           " + Environment.NewLine +
  "                    <fieldset class=\"scheduler-border\">                                                                                                                                             " + Environment.NewLine +
  "                        <legend class=\"scheduler-border\">Record Details</legend>                                                                                                                    " + Environment.NewLine +
  "                        <div class=\"row\">                                                                                                                                                           " + Environment.NewLine +
  "                            @foreach (var item in Model.FieldRuleList.Where(z => !z.Hidden))                                                                                                          " + Environment.NewLine +
  "                            {                                                                                                                                                                         " + Environment.NewLine +
  "                                if (item.HtmlDataType == \"text\")                                                                                                                                    " + Environment.NewLine +
  "                                {                                                                                                                                                                     " + Environment.NewLine +
  "                                    <div class=\"form-group has-feedback col-lg-3\">                                                                                                                  " + Environment.NewLine +
  "                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle</label>" + Environment.NewLine +
  "                                        @Html.TextBox(item.ColumnName, item.Value, item.Attributes)" + Environment.NewLine +
  "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +

  "                                    </div>" + Environment.NewLine +
  "                                }" + Environment.NewLine +
  "if (item.HtmlDataType == \"password\")" + Environment.NewLine +
 "                            {" + Environment.NewLine +
 "                                <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
 "                                    <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title = \"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +
 "                                    @Html.Password(item.ColumnName, item.Value, item.Attributes)" + Environment.NewLine +
 "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
 "                                </div>" + Environment.NewLine +
 "                            }" + Environment.NewLine +
  "                                if (item.HtmlDataType == \"textarea\")" + Environment.NewLine +
  "                                {" + Environment.NewLine +
  "                                    <div class=\"form-group has-feedback col-lg-12\">" + Environment.NewLine +
  "                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle</label>" + Environment.NewLine +
  "                                        @Html.TextArea(item.ColumnName, item.Value as string, 5, 20, item.Attributes)" + Environment.NewLine +
  "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +

  "                                    </div>" + Environment.NewLine +
  "                                }" + Environment.NewLine +
  "                                if (item.HtmlDataType == \"select\")" + Environment.NewLine +
  "                                {" + Environment.NewLine +
"if (item.Attributes.Where(x => x.Key == \"ParameterizedSelect\" && x.Value.ToString() == \"True\").Count() > 0)" + Environment.NewLine +

                                    "    {" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +

                                    "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +

                                    "            <div class=\"input-group\">" + Environment.NewLine +

                                    "                <a href=\"#\" class=\"input-group-addon download-select\" data-entityname=\"@item.ColumnName\">" + Environment.NewLine +

                                    "                    <i class=\"fa fa-download\"></i>" + Environment.NewLine +

                                    "                </a>" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "                @Html.DropDownList(item.ColumnName, item.DropdownMasterList, \"--Select--\", item.Attributes)" + Environment.NewLine +
                                    "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +


                                    "            </div>" + Environment.NewLine +

                                    "        </div>" + Environment.NewLine +

                                    "    }" + Environment.NewLine +

                                    "    else" + Environment.NewLine +

                                    "    {" + Environment.NewLine +

                                    "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +

                                    "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "            @Html.DropDownList(item.ColumnName, item.DropdownMasterList, \"--Select--\", item.Attributes)" + Environment.NewLine +
                                    "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +


                                    "        </div>" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "    }" + Environment.NewLine +
  "                                }         " + Environment.NewLine +
  "                            }             " + Environment.NewLine +
  "                        </div>            " + Environment.NewLine +
  "                    </fieldset>           " + Environment.NewLine +
  "                    <div class=\"row\">   " + Environment.NewLine +
  "                        <div class=\"shadow\">                                                                  " + Environment.NewLine +
  "                            <div class=\"preview-box\">                                                         " + Environment.NewLine +
  "                                <div class=\"alert alert-success\">                                             " + Environment.NewLine +
  "                                    <span class=\"glyphicon glyphicon-ok\"></span> <strong>Record Log</strong>  " + Environment.NewLine +
  "                                    <hr class=\"message-inner-separator\">                                      " + Environment.NewLine +
  "                                    <div class=\"box-body table-responsive no-padding\">                        " + Environment.NewLine +
  "                                        <table class=\"table table-bordered\">                                  " + Environment.NewLine +
  "                                            <thead>                                                             " + Environment.NewLine +
  "                                                <tr>                                                            " + Environment.NewLine +
  "                                                    <th>#</th>                                                  " + Environment.NewLine +
  "                                                    <th nowrap>Record Status</th>                               " + Environment.NewLine +
  "                                                    <th nowrap>Created By</th>                                  " + Environment.NewLine +
  "                                                    <th nowrap>Created Date</th>                                " + Environment.NewLine +
  "                                                    <th nowrap>Modified By</th>                                 " + Environment.NewLine +
  "                                                    <th nowrap>Modified Date</th>                               " + Environment.NewLine +
  "                                                    <th nowrap>Authorise By</th>                                " + Environment.NewLine +
  "                                                    <th nowrap>Authorise Date</th>                              " + Environment.NewLine +
  "                                                                                                                " + Environment.NewLine +
  "                                                </tr>                                                           " + Environment.NewLine +
  "                                            </thead>                                                            " + Environment.NewLine +
  "                                            <tbody>                                                             " + Environment.NewLine +
  "                                                <tr>                                                            " + Environment.NewLine +
  "                                                    <td>1</td>                                                  " + Environment.NewLine +
  "                                                    <th nowrap>@(((GSAssetManagement.Entity.RecordStatus)Model.FieldRuleList.Where(z => z.ColumnName == \"RecordStatus\").FirstOrDefault().Value).ToString())</th>" + Environment.NewLine +
  "                                                    <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"CreatedBy\").FirstOrDefault().Value</th>    " + Environment.NewLine +
  "                                                    <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"CreatedDate\").FirstOrDefault().Value</th>  " + Environment.NewLine +
  "                                                    <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"ModifiedBy\").FirstOrDefault().Value</th>   " + Environment.NewLine +
  "                                                    <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"ModifiedDate\").FirstOrDefault().Value</th> " + Environment.NewLine +
  "                                                    <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"AuthorisedBy\").Select(x => x.Value).FirstOrDefault()</th>" + Environment.NewLine +
  "                                                    <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"AuthorisedDate\").Select(x => x.Value).FirstOrDefault()</th>" + Environment.NewLine +
  "                                                </tr>" + Environment.NewLine +
  "                                            </tbody> " + Environment.NewLine +
  "                                        </table>     " + Environment.NewLine +
  "                                    </div>           " + Environment.NewLine +
  "                                </div>               " + Environment.NewLine +
  "                            </div>                   " + Environment.NewLine +
  "                        </div>                       " + Environment.NewLine +
  "                    </div>                           " + Environment.NewLine +
  "                    @if (Model.ChangeLogList.Count > 0)" + Environment.NewLine +
  "                    {                                  " + Environment.NewLine +
  "                        <div class=\"row\">            " + Environment.NewLine +
  "                            <div class=\"shadow\">     " + Environment.NewLine +
  "                                <div class=\"preview-box\">" + Environment.NewLine +
  "                                    <div class=\"alert alert-danger\">" + Environment.NewLine +
  "                                        <span class=\"glyphicon glyphicon-alert \"></span> <strong>Change Logs</strong>" + Environment.NewLine +
  "                                        <hr class=\"message-inner-separator\">                                         " + Environment.NewLine +
  "                                        <div class=\"box-body table-responsive no-padding\">                           " + Environment.NewLine +
  "                                            <table class=\"table table-bordered\">                                     " + Environment.NewLine +
  "                                                <thead>                                                                " + Environment.NewLine +
  "                                                    <tr>                                                               " + Environment.NewLine +
  "                                                        <th>#</th>                                                     " + Environment.NewLine +
  "                                                        <th>Field Name</th>                                            " + Environment.NewLine +
  "                                                        <th>Log Status</th>                                            " + Environment.NewLine +
  "                                                        <th>Modified Date</th>                                         " + Environment.NewLine +
  "                                                        <th>Original Value</th>                                        " + Environment.NewLine +
  "                                                        <th>Modified Value</th>                                        " + Environment.NewLine +
  "                                                    </tr>                                                              " + Environment.NewLine +
  "                                                </thead>                                                               " + Environment.NewLine +
  "                                                <tbody>                                                                " + Environment.NewLine +
  "                                                    @{                                                                 " + Environment.NewLine +
  "                                                        int i = 1;                                                     " + Environment.NewLine +
  "                                                        string _class = string.Empty;                                  " + Environment.NewLine +
  "                                                        foreach (var item in Model.ChangeLogList)                      " + Environment.NewLine +
  "                                                        {                                                              " + Environment.NewLine +
  "                                                            if (i % 2 != 0)                                            " + Environment.NewLine +
  "                                                            {                                                          " + Environment.NewLine +
  "                                                                _class = \"success\";                                  " + Environment.NewLine +
  "                                                            }                                                          " + Environment.NewLine +
  "                                                            else                                                       " + Environment.NewLine +
  "                                                            {                                                          " + Environment.NewLine +
  "                                                                _class = \"warning\";                                  " + Environment.NewLine +
  "                                                            }                                                          " + Environment.NewLine +
  "                                                            <tr class=\"@_class\">                                     " + Environment.NewLine +
  "                                                                <td scope=\"row\"> @i </td>                            " + Environment.NewLine +
  "                                                                <td> @item.PropertyName </td>                          " + Environment.NewLine +
  "                                                                <td> @item.LogStatus </td>                             " + Environment.NewLine +
  "                                                                <td> @item.ModifiedDate </td>                          " + Environment.NewLine +
  "                                                                <td> @item.Originalvalue </td>                         " + Environment.NewLine +
  "                                                                <td> @item.ModifiedValue </td>                         " + Environment.NewLine +
  "                                                                                                                       " + Environment.NewLine +
  "                                                            </tr>                                                      " + Environment.NewLine +
  "                                                            i++;                                                       " + Environment.NewLine +
  "                                                        }                                                              " + Environment.NewLine +
  "                                                    }                                                                  " + Environment.NewLine +
  "                                                </tbody>                                                               " + Environment.NewLine +
  "                                            </table>                                                                   " + Environment.NewLine +
  "                                        </div>                                                                         " + Environment.NewLine +
  "                                    </div>                                                                             " + Environment.NewLine +
  "                                </div>                                                                                 " + Environment.NewLine +
  "                            </div>                                                                                     " + Environment.NewLine +
  "                                                                                                                       " + Environment.NewLine +
  "                                                                                                                       " + Environment.NewLine +
  "                                                                                                                       " + Environment.NewLine +
  "                                                                                                                       " + Environment.NewLine +
  "                        </div>                                                                                         " + Environment.NewLine +
  "                                                        }                                                              " + Environment.NewLine +
  "                    <div class=\"row\">                                                                                " + Environment.NewLine +
  "                        <div class=\"col-sm-12 col-md-12\" style=\"margin-top:20px;\" id=\"responseContent\">          " + Environment.NewLine +
  "                                                                                                                       " + Environment.NewLine +
  "                        </div>                                                                                         " + Environment.NewLine +
  "                    </div>                                                                                             " + Environment.NewLine +
  "                    <div class=\"row\">                                                                                " + Environment.NewLine +
  "                                                                                                                       " + Environment.NewLine +
  "                        @if ((GSAssetManagement.Entity.RecordStatus)Model.FieldRuleList.Where(x => x.ColumnName == \"RecordStatus\").Select(z => z.Value).FirstOrDefault() == GSAssetManagement.Entity.RecordStatus.Active || ((GSAssetManagement.Entity.RecordStatus)Model.FieldRuleList.Where(x => x.ColumnName == \"RecordStatus\").Select(z => z.Value).FirstOrDefault() == GSAssetManagement.Entity.RecordStatus.Reverted && (Guid)Model.FieldRuleList.Where(x => x.ColumnName == \"ModifiedById\").Select(z => z.Value).FirstOrDefault() == ViewUserInformation.GetCurrentUserId()))" + Environment.NewLine +
  "                        {" + Environment.NewLine +
  string.Format("if(GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{0}, \"{1}\", GSAssetManagement.Entity.CurrentAction.Edit))", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
  "                            {" + Environment.NewLine +
  "                                <button class=\"btn icon-btn btn-success update saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Edit\">" + Environment.NewLine +
  "                                    <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-disk img-circle text-success\">" + Environment.NewLine +
  "                                    </span>Update Record" + Environment.NewLine +
  "                                </button>" + Environment.NewLine +
  "                                <button class=\"btn icon-btn btn-warning unlock\" type=\"button\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Edit\">" + Environment.NewLine +
  "                                    <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-lock img-circle text-success\">" + Environment.NewLine +
  "                                    </span>Unlock Record" + Environment.NewLine +
  "                                </button>" + Environment.NewLine +
  "                            }" + Environment.NewLine +
     string.Format("if (GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{0}, \"{1}\", GSAssetManagement.Entity.CurrentAction.Close))", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
                "                    {" + Environment.NewLine +
                "                        <button class=\"btn icon-btn btn-danger update saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Delete\">" + Environment.NewLine +
                "                            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-trash img-circle text-success\">" + Environment.NewLine +
                "                            </span>Delete Record" + Environment.NewLine +
                "                        </button>" + Environment.NewLine +
                "                    }" + Environment.NewLine +
                string.Format("if (GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{0}, \"{1}\", GSAssetManagement.Entity.CurrentAction.Edit))", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
                "                    {" + Environment.NewLine +
                "                        <button class=\"btn icon-btn btn-warning update saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Close\">" + Environment.NewLine +
                "                            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-remove img-circle text-success\">" + Environment.NewLine +
                "                            </span>Close Record" + Environment.NewLine +
                "                        </button>" + Environment.NewLine +
                "                    }" + Environment.NewLine +
  "                        }    " + Environment.NewLine +
  "                             " + Environment.NewLine +
  "                        else if ((GSAssetManagement.Entity.RecordStatus)Model.FieldRuleList.Where(x => x.ColumnName == \"RecordStatus\").Select(z => z.Value).FirstOrDefault() == GSAssetManagement.Entity.RecordStatus.UnVerified)" + Environment.NewLine +
  "                        {" + Environment.NewLine +
  string.Format("if (GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{0}, \"{1}\", GSAssetManagement.Entity.CurrentAction.Revert))", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
  "                            {" + Environment.NewLine +
  "                                <button class=\"btn icon-btn btn-warning revert saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Revert\">" + Environment.NewLine +
  "                                    <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-remove img-circle text-success\">" + Environment.NewLine +
  "                                    </span>Revert Record" + Environment.NewLine +
  "                                </button>               " + Environment.NewLine +
  "                            }                           " + Environment.NewLine +
  "                                                        " + Environment.NewLine +
  string.Format("if (GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{0}, \"{1}\", GSAssetManagement.Entity.CurrentAction.Revert))", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
  "                            {" + Environment.NewLine +
  "                                <button class=\"btn icon-btn btn-success authorise saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Authorise\">" + Environment.NewLine +
  "                                    <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-saved img-circle text-success\">" + Environment.NewLine +
  "                                    </span>Authorise Record                                                                                    " + Environment.NewLine +
  "                                </button>                                                                                                      " + Environment.NewLine +
  "                            }                                                                                                                  " + Environment.NewLine +
  "                        }                                                                                                                      " + Environment.NewLine +
  "                                                                                                                                               " + Environment.NewLine +
  "                    </div>                                                                                                                     " + Environment.NewLine +
  "                                                                                                                                               " + Environment.NewLine +
  "                </div>                                                                                                                         " + Environment.NewLine +
  "            </div>                                                                                                                             " + Environment.NewLine +
  "                                                        }                                                                                      " + Environment.NewLine +
  "    </div>                                                                                                                                     " + Environment.NewLine +
  "</div>                                                                                                                                         " + Environment.NewLine +
  "                                                                                                                                               " + Environment.NewLine +
  "                                                                                                                                               " + Environment.NewLine +
  "                                                                                                                                               " + Environment.NewLine +
  "                                                                                                                                               " + Environment.NewLine +
"@if(!this.Request.IsAjaxRequest())   " + Environment.NewLine +
"{" + Environment.NewLine +
                    "@section Scripts {                                                                                                                             " + Environment.NewLine +
   string.Format("<script src=\"~/Areas/{0}/Scripts/app/{1}/dataSource.js\"></script>", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
   string.Format("<script src=\"~/Areas/{0}/Scripts/app/{1}/{1}.js\"></script>", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
  "}}" + Environment.NewLine;

                return _detailsTemplate;


            }
            else
            {
                string _detailsTemplate = "@model GSAssetManagement.Web.Models.Utility.DetailsViewModel" + Environment.NewLine +
                "@{" + Environment.NewLine +
                "    Layout = this.Request.IsAjaxRequest() ? \"\" : \"~/Views/Shared/_Layout.cshtml\";" + Environment.NewLine +
                "}                                                                                    " + Environment.NewLine +
                "                                                                                     " + Environment.NewLine +
                "" + Environment.NewLine +
                string.Format("@using (Html.BeginForm(\"ModifyRecords\", \"{1}\", new {{ area = \"{0}\" }}, FormMethod.Post, new {{ @role = \"form\", @id = \"mainForm\", @autocomplete = \"off\" }}))", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
                "            {" + Environment.NewLine +
                "@Html.AntiForgeryToken()" + Environment.NewLine +
                "@Html.Hidden(\"Id\", Model.FieldRuleList.Where(x => x.ColumnName == \"Id\").Select(z => z.Value).FirstOrDefault())" + Environment.NewLine +

                string.Format("@Html.Hidden(\"{0}Id\", Model.FieldRuleList.Where(z => z.ColumnName == \"{0}Id\").Select(x => x.Value).FirstOrDefault())", moduleSetup.ParentTable) + Environment.NewLine +

                "    @Html.Hidden(\"CurrentAction\", GSAssetManagement.Entity.CurrentAction.Edit)                      " + Environment.NewLine +
                "                                                                                          " + Environment.NewLine +
                "    <div class=\"shadow\">                                                                " + Environment.NewLine +
                "        <div class=\"preview-box\">                                                       " + Environment.NewLine +
                "            <fieldset class=\"scheduler-border\">                                         " + Environment.NewLine +
                "                <legend class=\"scheduler-border\">Record Details</legend>                " + Environment.NewLine +
                "                <div class=\"row\">                                                       " + Environment.NewLine +
                "                    @foreach (var item in Model.FieldRuleList.Where(z => !z.Hidden))      " + Environment.NewLine +
                "                {                                                                         " + Environment.NewLine +
                "                    if (item.HtmlDataType == \"text\")                                    " + Environment.NewLine +
                "                    {                                                                     " + Environment.NewLine +
                "                            <div class=\"form-group has-feedback col-lg-3\">              " + Environment.NewLine +
                "                                <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle</label>" + Environment.NewLine +

                "                                @Html.TextBox(item.ColumnName, item.Value, item.Attributes)" + Environment.NewLine +
                "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
                "                            </div>                                                 " + Environment.NewLine +
                "                        }                                                          " + Environment.NewLine +
                "                        if (item.HtmlDataType == \"textarea\")                     " + Environment.NewLine +
                "                        {                                                          " + Environment.NewLine +
                "                            <div class=\"form-group has-feedback col-lg-12\">      " + Environment.NewLine +
                "                                <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle</label>" + Environment.NewLine +

                "                                @Html.TextArea(item.ColumnName, item.Value as string, 5, 20, item.Attributes)" + Environment.NewLine +
                "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
                "                            </div>                                                     " + Environment.NewLine +
                "                        }                                                              " + Environment.NewLine +
                "                        if (item.HtmlDataType == \"select\")                           " + Environment.NewLine +
                "                        {                                                              " + Environment.NewLine +
               "if (item.Attributes.Where(x => x.Key == \"ParameterizedSelect\" && x.Value.ToString() == \"True\").Count() > 0)" + Environment.NewLine +

                                    "    {" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +

                                    "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +

                                    "            <div class=\"input-group\">" + Environment.NewLine +

                                    "                <a href=\"#\" class=\"input-group-addon download-select\" data-entityname=\"@item.ColumnName\">" + Environment.NewLine +

                                    "                    <i class=\"fa fa-download\"></i>" + Environment.NewLine +

                                    "                </a>" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "                @Html.DropDownList(item.ColumnName, item.DropdownMasterList, \"--Select--\", item.Attributes)" + Environment.NewLine +
                                    "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +


                                    "            </div>" + Environment.NewLine +

                                    "        </div>" + Environment.NewLine +

                                    "    }" + Environment.NewLine +

                                    "    else" + Environment.NewLine +

                                    "    {" + Environment.NewLine +

                                    "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +

                                    "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.FieldTitle </label>" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "            @Html.DropDownList(item.ColumnName, item.DropdownMasterList, \"--Select--\", item.Attributes)" + Environment.NewLine +
                                    "                                          <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +


                                    "        </div>" + Environment.NewLine +

                                    "" + Environment.NewLine +

                                    "    }" + Environment.NewLine +
                "                        }                                                                                 " + Environment.NewLine +
                "                    }                                                                                     " + Environment.NewLine +
                "                </div>                                                                                    " + Environment.NewLine +
                "            </fieldset>                                                                                   " + Environment.NewLine +
                "            <div class=\"row\">                                                                           " + Environment.NewLine +
                "                <div class=\"shadow\">                                                                    " + Environment.NewLine +
                "                    <div class=\"preview-box\">                                                           " + Environment.NewLine +
                "                        <div class=\"alert alert-success\">                                               " + Environment.NewLine +
                "                            <span class=\"glyphicon glyphicon-ok\"></span> <strong>Record Details</strong>" + Environment.NewLine +
                "                            <hr class=\"message-inner-separator\">                                        " + Environment.NewLine +
                "                            <div class=\"box-body table-responsive no-padding\">                          " + Environment.NewLine +
                "                                <table class=\"table table-bordered\">                                    " + Environment.NewLine +
                "                                    <thead>                                                               " + Environment.NewLine +
                "                                        <tr>                                                              " + Environment.NewLine +
                "                                            <th>#</th>                                                    " + Environment.NewLine +
                "                                            <th nowrap>Record Status</th>                                 " + Environment.NewLine +
                "                                            <th nowrap>Created By</th>                                    " + Environment.NewLine +
                "                                            <th nowrap>Created Date</th>                                  " + Environment.NewLine +
                "                                            <th nowrap>Modified By</th>                                   " + Environment.NewLine +
                "                                            <th nowrap>Modified Date</th>                                 " + Environment.NewLine +
                "                                            <th nowrap>Authorise By</th>                                  " + Environment.NewLine +
                "                                            <th nowrap>Authorise Date</th>                                " + Environment.NewLine +
                "                                                                                                          " + Environment.NewLine +
                "                                        </tr>                                                             " + Environment.NewLine +
                "                                    </thead>                                                              " + Environment.NewLine +
                "                                    <tbody>                                                               " + Environment.NewLine +
                "                                        <tr>                                                              " + Environment.NewLine +
                "                                            <td>1</td> " + Environment.NewLine +
                "                                            <th nowrap>@(((GSAssetManagement.Entity.RecordStatus)Model.FieldRuleList.Where(z => z.ColumnName == \"RecordStatus\").FirstOrDefault().Value).ToString())</th>" + Environment.NewLine +
                "                                            <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"CreatedBy\").FirstOrDefault().Value</th>" + Environment.NewLine +
                "                                            <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"CreatedDate\").FirstOrDefault().Value</th> " + Environment.NewLine +
                "                                            <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"ModifiedBy\").FirstOrDefault().Value</th>" + Environment.NewLine +
                "                                            <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"ModifiedDate\").FirstOrDefault().Value</th>" + Environment.NewLine +
                "                                            <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"AuthorisedBy\").Select(x => x.Value).FirstOrDefault()</th>  " + Environment.NewLine +
                "                                            <th nowrap>@Model.FieldRuleList.Where(z => z.ColumnName == \"AuthorisedDate\").Select(x => x.Value).FirstOrDefault()</th>" + Environment.NewLine +
                "                                        </tr>          " + Environment.NewLine +
                "                                    </tbody>           " + Environment.NewLine +
                "                                </table>               " + Environment.NewLine +
                "                            </div>                     " + Environment.NewLine +
                "                        </div>                         " + Environment.NewLine +
                "                    </div>                             " + Environment.NewLine +
                "                </div>                                 " + Environment.NewLine +
                "            </div>                                     " + Environment.NewLine +
                "            @if (Model.ChangeLogList.Count > 0)        " + Environment.NewLine +
                "                {                                      " + Environment.NewLine +
                "                <div class=\"row\">                    " + Environment.NewLine +
                "                    <div class=\"shadow\">             " + Environment.NewLine +
                "                        <div class=\"preview-box\">    " + Environment.NewLine +
                "                            <div class=\"alert alert-danger\">" + Environment.NewLine +
                "                                <span class=\"glyphicon glyphicon-alert \"></span> <strong>Change Logs</strong>" + Environment.NewLine +
                "                                <hr class=\"message-inner-separator\">" + Environment.NewLine +
                "                                <div class=\"box-body table-responsive no-padding\">" + Environment.NewLine +
                "                                    <table class=\"table table-bordered\">          " + Environment.NewLine +
                "                                        <thead>                                     " + Environment.NewLine +
                "                                            <tr>                                    " + Environment.NewLine +
                "                                                <th>#</th>                          " + Environment.NewLine +
                "                                                <th>Field Name</th>                 " + Environment.NewLine +
                "                                                <th>Log Status</th>                 " + Environment.NewLine +
                "                                                <th>Modified Date</th>              " + Environment.NewLine +
                "                                                <th>Original Value</th>             " + Environment.NewLine +
                "                                                <th>Modified Value</th>             " + Environment.NewLine +
                "                                            </tr>                                   " + Environment.NewLine +
                "                                        </thead>                                    " + Environment.NewLine +
                "                                        <tbody>                                     " + Environment.NewLine +
                "                                            @{                                      " + Environment.NewLine +
                "                                                int i = 1;                          " + Environment.NewLine +
                "                                                string _class = string.Empty;       " + Environment.NewLine +
                "                                                foreach (var item in Model.ChangeLogList)" + Environment.NewLine +
                "                                                {                                        " + Environment.NewLine +
                "                                                    if (i % 2 != 0)                      " + Environment.NewLine +
                "                                                    {                                    " + Environment.NewLine +
                "                                                        _class = \"success\";            " + Environment.NewLine +
                "                                                    }                                    " + Environment.NewLine +
                "                                                    else                                 " + Environment.NewLine +
                "                                                    {                                    " + Environment.NewLine +
                "                                                        _class = \"warning\";            " + Environment.NewLine +
                "                                                    }                                    " + Environment.NewLine +
                "                                                    <tr class=\"@_class\">               " + Environment.NewLine +
                "                                                        <td scope=\"row\"> @i </td>      " + Environment.NewLine +
                "                                                        <td> @item.PropertyName </td>    " + Environment.NewLine +
                "                                                        <td> @item.LogStatus </td>       " + Environment.NewLine +
                "                                                        <td> @item.ModifiedDate </td>    " + Environment.NewLine +
                "                                                        <td> @item.Originalvalue </td>   " + Environment.NewLine +
                "                                                        <td> @item.ModifiedValue </td>   " + Environment.NewLine +
                "                                                                                         " + Environment.NewLine +
                "                                                    </tr>                                " + Environment.NewLine +
                "                                                    i++;                                 " + Environment.NewLine +
                "                                                }                                        " + Environment.NewLine +
                "                                            }                                            " + Environment.NewLine +
                "                                        </tbody>                                         " + Environment.NewLine +
                "                                    </table>                                             " + Environment.NewLine +
                "                                </div>                                                   " + Environment.NewLine +
                "                            </div>                                                       " + Environment.NewLine +
                "                        </div>                                                           " + Environment.NewLine +
                "                    </div>                                                               " + Environment.NewLine +
                "                                                                                         " + Environment.NewLine +
                "                                                                                         " + Environment.NewLine +
                "                                                                                         " + Environment.NewLine +
                "                                                                                         " + Environment.NewLine +
                "                </div>                                                                   " + Environment.NewLine +
                "                                                                                         " + Environment.NewLine +
                "                                                }                                        " + Environment.NewLine +
                "            <div class=\"row\">                                                          " + Environment.NewLine +
                "                <div class=\"col-sm-12 col-md-12\" style=\"margin-top:20px;\" id=\"responseContent\">" + Environment.NewLine +
                "                                                                                                     " + Environment.NewLine +
                "                </div>                                                                               " + Environment.NewLine +
                "            </div>                                                                                   " + Environment.NewLine +
                "            <div class=\"row\">                                                                      " + Environment.NewLine +
                "                                                                                                     " + Environment.NewLine +
                "                @if ((GSAssetManagement.Entity.RecordStatus)Model.FieldRuleList.Where(x => x.ColumnName == \"RecordStatus\").Select(z => z.Value).FirstOrDefault() == GSAssetManagement.Entity.RecordStatus.Active || ((GSAssetManagement.Entity.RecordStatus)Model.FieldRuleList.Where(x => x.ColumnName == \"RecordStatus\").Select(z => z.Value).FirstOrDefault() == GSAssetManagement.Entity.RecordStatus.Reverted && (Guid)Model.FieldRuleList.Where(x => x.ColumnName == \"ModifiedById\").Select(z => z.Value).FirstOrDefault() == ViewUserInformation.GetCurrentUserId()))" + Environment.NewLine +
                "                {" + Environment.NewLine +
               string.Format("if (GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{0}, \"{1}\", GSAssetManagement.Entity.CurrentAction.Edit))", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
                "                    {" + Environment.NewLine +
                "                        <button class=\"btn icon-btn btn-success update saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Edit\">" + Environment.NewLine +
                "                            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-disk img-circle text-success\">" + Environment.NewLine +
                "                            </span>Update Record" + Environment.NewLine +
                "                        </button>" + Environment.NewLine +
                "" + Environment.NewLine +
                "                        <button class=\"btn icon-btn btn-warning unlock\" type=\"button\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Edit\">" + Environment.NewLine +
                "                            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-lock img-circle text-success\">" + Environment.NewLine +
                "                            </span>Unlock Record" + Environment.NewLine +
                "                        </button>" + Environment.NewLine +
                "                    }" + Environment.NewLine +
                string.Format("if (GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{0}, \"{1}\", GSAssetManagement.Entity.CurrentAction.Close))", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
                "                    {" + Environment.NewLine +
                "                        <button class=\"btn icon-btn btn-danger update saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Delete\">" + Environment.NewLine +
                "                            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-trash img-circle text-success\">" + Environment.NewLine +
                "                            </span>Delete Record" + Environment.NewLine +
                "                        </button>" + Environment.NewLine +
                "                    }" + Environment.NewLine +
                string.Format("if (GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{0}, \"{1}\", GSAssetManagement.Entity.CurrentAction.Edit))", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
                "                    {" + Environment.NewLine +
                "                        <button class=\"btn icon-btn btn-warning update saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Close\">" + Environment.NewLine +
                "                            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-remove img-circle text-success\">" + Environment.NewLine +
                "                            </span>Close Record" + Environment.NewLine +
                "                        </button>" + Environment.NewLine +
                "                    }" + Environment.NewLine +
                "                }    " + Environment.NewLine +
                "                     " + Environment.NewLine +
                "                else if ((GSAssetManagement.Entity.RecordStatus)Model.FieldRuleList.Where(x => x.ColumnName == \"RecordStatus\").Select(z => z.Value).FirstOrDefault() == GSAssetManagement.Entity.RecordStatus.UnVerified)" + Environment.NewLine +
                "                {" + Environment.NewLine +
                string.Format("if (GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{0}, \"{1}\", GSAssetManagement.Entity.CurrentAction.Revert))", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
                "                    {" + Environment.NewLine +
                "                        <button class=\"btn icon-btn btn-warning revert saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Revert\">" + Environment.NewLine +
                "                            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-remove img-circle text-success\">" + Environment.NewLine +
                "                            </span>Revert Record" + Environment.NewLine +
                "                        </button>" + Environment.NewLine +
                "                    }            " + Environment.NewLine +
                "                                 " + Environment.NewLine +
                string.Format("if (GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{0}, \"{1}\", GSAssetManagement.Entity.CurrentAction.Revert))", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
                "                    {" + Environment.NewLine +
                "                        <button class=\"btn icon-btn btn-success authorise saveRecords\" type=\"submit\" style=\"float:right;margin-right:15px;\" data-currentaction=\"@GSAssetManagement.Entity.CurrentAction.Authorise\">" + Environment.NewLine +
                "                            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-saved img-circle text-success\">" + Environment.NewLine +
                "                            </span>Authorise Record" + Environment.NewLine +
                "                        </button>                  " + Environment.NewLine +
                "                    }                              " + Environment.NewLine +
                "                }                                  " + Environment.NewLine +
                "                                                   " + Environment.NewLine +
                "            </div>                                 " + Environment.NewLine +
                "                                                   " + Environment.NewLine +
                "                                                   " + Environment.NewLine +
                "                                                   " + Environment.NewLine +
                "        </div>                                     " + Environment.NewLine +
                "    </div>                                         " + Environment.NewLine +
                                                                "}  " + Environment.NewLine;
                return _detailsTemplate;
            }
        }

    }
}
