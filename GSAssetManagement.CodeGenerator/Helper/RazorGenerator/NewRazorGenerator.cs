﻿using GSAssetManagement.CodeGenerator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace GSAssetManagement.CodeGenerator
{
    public static class NewRazorGenerator
    {
        public static string GenerateIndex(ModuleSetupNew moduleSetup)
        {
            string projectName = ConfigurationManager.AppSettings["ProjectName"].ToString();
            string _indexTemplate = "@model " + projectName + ".Entity.DTO.ModuleSummary" + Environment.NewLine +
                "" + Environment.NewLine +
"@{" + Environment.NewLine +

"  Layout = this.Request.IsAjaxRequest() ? \"\" : \"~/Views/Shared/_Layout.cshtml\";" + Environment.NewLine +
"}" + Environment.NewLine +
"" + Environment.NewLine +
//"@using(Html.BeginForm(\"SearchIndex\", Model.ModuleSummaryName, new {area = \"Model.SchemaName\" }, FormMethod.Post, new { @role = \"form\", @id = \"searchForm\" }))" + Environment.NewLine +
string.Format("<form action=\"/{0}/{1}/SearchIndex\" id=\"searchForm\" method=\"get\" role=\"form\">", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
"@{" + Environment.NewLine +
"  @Html.Hidden(\"PageSize\", Model.SummaryRecord.PageSize);" + Environment.NewLine +
"  @Html.Hidden(\"PageNumber\", Model.SummaryRecord.PageNumber);" + Environment.NewLine +
"  foreach (var item in Model.moduleBussinesLogicSummaries.Where(h => h.HtmlDataType == \"hidden\" && h.SummaryHeader))" + Environment.NewLine +
"  {" + Environment.NewLine +
"      @Html.Hidden(item.ColumnName, item.CurrentValue);" + Environment.NewLine +
"  }" + Environment.NewLine +
"  @Html.AntiForgeryToken();" + Environment.NewLine +
"                    <div class=\"row\">" + Environment.NewLine +
"        <div class=\"col-lg-12\">" + Environment.NewLine +
"        <div class=\"box box-primary\">" + Environment.NewLine +
"            <div class=\"box-header\" style=\"padding: 5px; \">" + Environment.NewLine +
string.Format("<h5 class=\"box-title\">{0}</h5>", moduleSetup.Name) + Environment.NewLine +
"            </div>" + Environment.NewLine +
"            <div class=\"box-body\">" + Environment.NewLine +
"                        <div class=\"row\">" + Environment.NewLine +
"                            @foreach (var item in Model.moduleBussinesLogicSummaries.Where(P => P.ParameterForSummaryHeader).OrderBy(c => c.Position))" + Environment.NewLine +
"                            {" + Environment.NewLine +
"                                if (item.HtmlDataType == \"input\")" + Environment.NewLine +
"                                {" + Environment.NewLine +
"                                    <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
"                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name </label>" + Environment.NewLine +
"                                        @Html.TextBox(item.ColumnName, item.CurrentValue ?? item.DefaultValue, item.Attributes)" + Environment.NewLine +
"                                    </div>" + Environment.NewLine +
"                                }" + Environment.NewLine +
"                                if (item.HtmlDataType == \"textarea\")" + Environment.NewLine +
"                                {" + Environment.NewLine +
"                                    <div class=\"form-group has-feedback col-lg-12\">" + Environment.NewLine +
"                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name </label>" + Environment.NewLine +
"                                        @Html.TextArea(item.ColumnName, item.CurrentValue as string ?? item.DefaultValue as string, 5, 20, item.Attributes)" + Environment.NewLine +
"                                    </div>" + Environment.NewLine +
"                                }" + Environment.NewLine +
                                "    if (item.HtmlDataType == \"select\")" + Environment.NewLine +
                                "    {" + Environment.NewLine +
                                "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
                                "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name  </label>" + Environment.NewLine +
                                "            @Html.DropDownList(item.ColumnName, item.SelectList.Select(s => new SelectListItem() { Text = s.Text, Value = s.Value, Selected = s.Selected }), \"--Select--\", item.Attributes)" + Environment.NewLine +
                                "        </div>" + Environment.NewLine +
                                "    }" + Environment.NewLine +
"                            }" + Environment.NewLine +
"                            <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
"                                <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\"></i> Record Status</label>" + Environment.NewLine +
"                                @Html.DropDownList(\"RecordStatus\", EnumDropdownList.ToSelectList(" + projectName + ".Entity.RecordStatus.Active), \"--Select--\", new { @class = \"form-control\", placeholder = \"Select Record Status\" })" + Environment.NewLine +
"                            </div>" + Environment.NewLine +
"                        </div>    " + Environment.NewLine +
"                <div class=\"row\">" + Environment.NewLine +
"                    <button class=\"btn icon-btn btn-success\" type=\"submit\" style=\"float:right;margin-right:15px;\" id=\"searchRecords\">" + Environment.NewLine +
"                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon-search img-circle text-success\">                          " + Environment.NewLine +
"                        </span>Search                                                                                                        " + Environment.NewLine +
"                    </button>                                                                                                                " + Environment.NewLine +
"                    <button class=\"btn icon-btn btn-primary\" type=\"reset\" style=\"float:right;margin-right:15px;\" id=\"resetForm\">     " + Environment.NewLine +
"                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon-refresh img-circle text-warning\">                         " + Environment.NewLine +
"                        </span>Reset                                                                                                         " + Environment.NewLine +
"                    </button>  " + Environment.NewLine +
"                </div>         " + Environment.NewLine +
"            </div>             " + Environment.NewLine +
"        </div>                 " + Environment.NewLine +
"    </div>                     " + Environment.NewLine +
"  </div>                       " + Environment.NewLine +
"}                              " + Environment.NewLine +
"</form>                              " + Environment.NewLine +
"<div class=\"box-footer table-responsive\" id=\"parenttblcontent\">" + Environment.NewLine +
"    <div style =\"margin-bottom:45px; \">" + Environment.NewLine +
"    @if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Create))" + Environment.NewLine +
"    {" + Environment.NewLine;

            if (!(moduleSetup.IsParent && string.IsNullOrEmpty(moduleSetup.ParentModule)))
            {
                _indexTemplate += string.Format("<a class=\"btn icon-btn btn-primary\" href=\"/{0}/{1}/Create\" id = \"addNewChildRecord\" style=\"float:right;margin-right:15px; \">", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine;
            }
            else
            { //id = "addNewChildRecord"
                _indexTemplate += string.Format("<a class=\"btn icon-btn btn-primary\" href=\"/{0}/{1}/Create\" style=\"float:right;margin-right:15px; \">", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine;
            }
            _indexTemplate +=
"            <span class=\"glyphicon btn-glyphicon glyphicon glyphicon-plus img-circle text-warning\">" + Environment.NewLine +
"            </span>Add New Record" + Environment.NewLine +
"        </a>                     " + Environment.NewLine +
"    }                       " + Environment.NewLine +
"    </div>                       " + Environment.NewLine +
"    <div id =\"RecordsContent\"> " + Environment.NewLine +
"        @Html.Partial(\"SearchIndex\", Model)" + Environment.NewLine +
"    </div>                                              " + Environment.NewLine +
"</div>                                                  " + Environment.NewLine +
"                                                        " + Environment.NewLine;

            return _indexTemplate;
        }

        public static string GenerateSearchIndex(ModuleSetupNew moduleSetup)
        {
            string projectName = ConfigurationManager.AppSettings["ProjectName"].ToString();
            var _template = " @model " + projectName + ".Entity.DTO.ModuleSummary" + Environment.NewLine +
"@using System.Data" + Environment.NewLine +
"@{" + Environment.NewLine +
"                Layout = \"\";" + Environment.NewLine +
"}" + Environment.NewLine +
"<table class=\"table table-bordered searchme-table\">" + Environment.NewLine +
"    <thead>                                                                    " + Environment.NewLine +
"        <tr>                                                                   " + Environment.NewLine +
"            <th>#</th>                                                         " + Environment.NewLine +
"            @foreach (var column in Model.moduleBussinesLogicSummaries.Where(c => c.HtmlDataType != \"hidden\" && c.SummaryHeader).OrderBy(o => o.Position))   " + Environment.NewLine +
"        {                                                                      " + Environment.NewLine +
"                    <th nowrap> @column.Name </th>              " + Environment.NewLine +
"        }                                                                 " + Environment.NewLine +
"            <th>Modified Date</th>                                             " + Environment.NewLine +
"            <th>Modified By</th>                                               " + Environment.NewLine +
"            <th>Entity State</th>                                              " + Environment.NewLine +
"            <th>Record Status</th>                                              " + Environment.NewLine +
"            <th>Action</th>                                                    " + Environment.NewLine +
"        </tr>                                                             " + Environment.NewLine +
"    </thead>                                                              " + Environment.NewLine +
"    <tbody>                                                               " + Environment.NewLine +
"        @{                                                                " + Environment.NewLine +
"            int i = 1;                                                    " + Environment.NewLine +
"        string _class = string.Empty;                                     " + Environment.NewLine +
"            foreach (System.Data.DataRow item in Model.SummaryRecord.Results.Rows)      " + Environment.NewLine +
"            {                                                             " + Environment.NewLine +
"                if ((" + projectName + ".Entity.RecordStatus)Enum.Parse(typeof(" + projectName + ".Entity.RecordStatus), item[\"Record_Status\"].ToString()) == " + projectName + ".Entity.RecordStatus.UnVerified)" + Environment.NewLine +
"                {" + Environment.NewLine +
"                    _class = \"warning\";" + Environment.NewLine +
"                }                        " + Environment.NewLine +
"                else if ((" + projectName + ".Entity.RecordStatus)Enum.Parse(typeof(" + projectName + ".Entity.RecordStatus), item[\"Record_Status\"].ToString()) == " + projectName + ".Entity.RecordStatus.Inactive)" + Environment.NewLine +
"                {" + Environment.NewLine +
"                    _class = \"danger\";" + Environment.NewLine +
"                }                       " + Environment.NewLine +
"                else                    " + Environment.NewLine +
"                {                       " + Environment.NewLine +
"                    _class = \"success\";" + Environment.NewLine +
"                                         " + Environment.NewLine +
"                }                        " + Environment.NewLine +
"                <tr class=\"@_class\">   " + Environment.NewLine +
"                    <td scope =\"row\"> @i </td>" + Environment.NewLine;

            _template += "            @foreach (var column in Model.moduleBussinesLogicSummaries.Where(c => c.HtmlDataType != \"hidden\" && c.SummaryHeader).OrderBy(o => o.Position))" + Environment.NewLine +
                "{                                      " + Environment.NewLine +
            "    if (!column.ColumnName.Contains(\"PageCount\") && !column.ColumnName.Contains(\"TotalRecords\") && (!column.ColumnName.Contains(\"Id\") || column.HtmlDataType != \"hidden\")) " + Environment.NewLine +
            "                        {                                                                            " + Environment.NewLine +
            "        if (column.ColumnName == \"Record Status\")                                                  " + Environment.NewLine +
            "                            {                                                                        " + Environment.NewLine +
            "                                <td nowrap> @Enum.Parse(typeof(" + projectName + ".Entity.RecordStatus), item[column.ColumnName].ToString()).ToString() </td>" + Environment.NewLine +
            "                            }                                                                                                                   " + Environment.NewLine +
            "                            else if (column.ColumnName == \"EntityStatus\" && !string.IsNullOrEmpty(item[column.ColumnName].ToString()))      " + Environment.NewLine +
            "                            {                                                                                                                   " + Environment.NewLine +
            "                                <td nowrap> @Enum.Parse(typeof(" + projectName + ".Entity.DataEntityState), item[column.ColumnName].ToString()).ToString() </td>" + Environment.NewLine +
            "                            }              " + Environment.NewLine +
            "                            else           " + Environment.NewLine +
            "                            {              " + Environment.NewLine +
            "                                <td nowrap>@(column.DataType.ToLower() != \"guid\" && Model.SummaryRecord.Results.Columns.Contains(column.ColumnName) ? item[column.ColumnName].ToString() : Model.SummaryRecord.Results.Columns.Contains(column.Name) ? item[column.Name].ToString() : \"\") </td>   " + Environment.NewLine +
            "                            }                                                             " + Environment.NewLine +
            "    }                                                                                     " + Environment.NewLine +
            "}                                                                                         " + Environment.NewLine +
            "<td nowrap>@item[\"ModifiedDate\"]</td>" + Environment.NewLine +
            "<td nowrap> @item[\"ModifiedBy\"] </td> " + Environment.NewLine +
            "<td nowrap>@((" + projectName + ".Entity.DataEntityState)item[\"EntityState\"]) </td>" + Environment.NewLine +
            "<td nowrap>@((" + projectName + ".Entity.RecordStatus)item[\"Record_Status\"]) </td>" + Environment.NewLine;
            if (string.IsNullOrEmpty(moduleSetup.ParentModule))
            {
                _template += string.Format("<td title = \"Preview Record Detail\" ><a href = \"/{0}/{1}/Details/?Id=@item[\"Id\"].ToString()\" ><i class=\"glyphicon glyphicon-hand-right\"> Preview Deatils</i></a></td>", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine;
            }
            else
            {
                _template += string.Format("<td title = \"Preview Record Detail\" ><a href = \"/{0}/{1}/Details/?Id=@item[\"Id\"].ToString()\" target=\"_blank\" ><i class=\"glyphicon glyphicon-hand-right\"> Preview Deatils</i></a></td>", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine;
            }

            _template += "                </tr>                                                                     " + Environment.NewLine +
            "                i++;                                                                      " + Environment.NewLine +
            "            }                                                                             " + Environment.NewLine +
            "        }                                                                                 " + Environment.NewLine +
            "    </tbody>                                                                              " + Environment.NewLine +
            "</table>                                                                                  " + Environment.NewLine +
            "<div class=\"form-group\">                                                                " + Environment.NewLine +
            "    <div class=\"col-md-6 col-sm-3 col-xs-12\">                                           " + Environment.NewLine +
            "        <nav aria-label=\"Page navigation\" style=\"float:left;\">                        " + Environment.NewLine +
            "            <ul class=\"pagination recordSize\">                                          " + Environment.NewLine +
            "                <li>                                                                      " + Environment.NewLine +
            "                    <a href =\"#\" aria-label=\"Previous\">                               " + Environment.NewLine +
            "                        <span aria-hidden=\"true\">&larr;</span> Records Per Page         " + Environment.NewLine +
            "                    </a>                                                                  " + Environment.NewLine +
            "                </li>                                                                     " + Environment.NewLine +
            "                <li><a href =\"#\" data-pagesize=\"10\">10</a></li>                       " + Environment.NewLine +
            "                <li><a href =\"#\" data-pagesize=\"20\">20</a></li>                       " + Environment.NewLine +
            "                <li><a href =\"#\" data-pagesize=\"40\">40</a></li>                       " + Environment.NewLine +
            "                <li><a href =\"#\" data-pagesize=\"50\">50</a></li>                       " + Environment.NewLine +
            "                <li><a href =\"#\" data-pagesize=\"100\">100</a></li>                     " + Environment.NewLine +
            "            </ul>                                                                         " + Environment.NewLine +
            "        </nav>                                                                            " + Environment.NewLine +
            "    </div>                                                                                " + Environment.NewLine +
            "    <div class=\"col-md-6 col-sm-3 col-xs-12\">                                           " + Environment.NewLine +
            "        <nav aria-label=\"Page navigation\" style=\"float:right;\">   " + Environment.NewLine +
            "           @Html.RenderPager(Model.SchemaName, Model.ModuleSummaryName, \"SearchIndex\", (int)Math.Ceiling((decimal)Model.SummaryRecord.RowCount / ((decimal)Model.SummaryRecord.PageSize == 0 ? 1 : (decimal)Model.SummaryRecord.PageSize)), Model.SummaryRecord.PageSize, Model.SummaryRecord.PageNumber)" + Environment.NewLine +
            "        </nav>   " + Environment.NewLine +
            "    </div>       " + Environment.NewLine +
            "</div>           " + Environment.NewLine;


            return _template;
        }

        public static string GenerateCreate(ModuleSetupNew moduleSetup)
        {
            string projectName = ConfigurationManager.AppSettings["ProjectName"].ToString();
            if ((!moduleSetup.IsParent) || (moduleSetup.IsParent && moduleSetup.ChildTableInformations.Count() == 0))
            {
                string _createTemplate = "@model " + projectName + ".Entity.DTO.ModuleSummary" + Environment.NewLine +
                  "@using GSAssetManagement.Web.Utility" + Environment.NewLine +
                  "@{" + Environment.NewLine +
                  "    Layout = this.Request.IsAjaxRequest() ? \"\" : \"~/Views/Shared/_Layout.cshtml\";" + Environment.NewLine +
                  "}" + Environment.NewLine +
                  "<div class=\"row\">" + Environment.NewLine +
                  "    @Html.Hidden(\"ParentId\", null)" + Environment.NewLine +
 "<div class=\"col-md-12\" id=\"currentTabInformation\">" + Environment.NewLine +
 "@using (Html.BeginForm(\"Create\", Model.ModuleSummaryName, new { area = Model.SchemaName }, FormMethod.Post, new { @role = \"form\", @id = \"mainForm\", @autocomplete = \"off\", @enctype = \"multipart/form-data\" }))" + Environment.NewLine +
 "            {" + Environment.NewLine +
 "                  @Html.AntiForgeryToken()" + Environment.NewLine +
 "                  @Html.Hidden(\"Id\", Model.PrimaryRecordId)" + Environment.NewLine +
 "                  @Html.Hidden(\"CurrentAction\", " + projectName + ".Entity.CurrentAction.Create)" + Environment.NewLine +
 "                  foreach (var item in Model.moduleBussinesLogicSummaries.Where(x => x.HtmlDataType == \"hidden\"))" + Environment.NewLine +
 "                  {" + Environment.NewLine +
 "                       @Html.Hidden(item.ColumnName, item.CurrentValue)" + Environment.NewLine +
 "                  }" + Environment.NewLine +
 "        <div class=\"box box-primary\">" + Environment.NewLine +
"            <div class=\"box-header with-border\">" + Environment.NewLine +
string.Format("<h5 class=\"box-title\">{0}</h5>", moduleSetup.Name) + Environment.NewLine +
"            </div>" + Environment.NewLine +
"            <div class=\"box-body\">" + Environment.NewLine +
"                        <div class=\"row\">" + Environment.NewLine +
"                            @foreach (var item in Model.moduleBussinesLogicSummaries.Where(x => x.HtmlDataType != \"hidden\").OrderBy(c => c.Position))" + Environment.NewLine +
"                            {" + Environment.NewLine +
"                                if (item.HtmlDataType == \"input\")" + Environment.NewLine +
"                                {" + Environment.NewLine +
"                                    <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
"                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name </label>" + Environment.NewLine +
"                                        @Html.TextBox(item.ColumnName, item.CurrentValue ?? item.DefaultValue, item.Attributes)" + Environment.NewLine +
"                                    </div>" + Environment.NewLine +
"                                }" + Environment.NewLine +
"                                if (item.HtmlDataType == \"textarea\")" + Environment.NewLine +
"                                {" + Environment.NewLine +
"                                    <div class=\"form-group has-feedback col-lg-12\">" + Environment.NewLine +
"                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name </label>" + Environment.NewLine +
"                                        @Html.TextArea(item.ColumnName, item.CurrentValue as string ?? item.DefaultValue as string, 5, 20, item.Attributes)" + Environment.NewLine +
"                                    </div>" + Environment.NewLine +
"                                }" + Environment.NewLine +
                                "    if (item.HtmlDataType == \"select\")" + Environment.NewLine +
                                "    {" + Environment.NewLine +
                                "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
                                "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name  </label>" + Environment.NewLine +
                                "            @Html.DropDownList(item.ColumnName, item.SelectList.Select(s => new SelectListItem() { Text = s.Text, Value = s.Value, Selected = s.Selected }), \"--Select--\", item.Attributes)" + Environment.NewLine +
                                "        </div>" + Environment.NewLine +
                                "    }" + Environment.NewLine +
"                                }         " + Environment.NewLine +
"                        </div>    " + Environment.NewLine +
"                    <div class=\"row\">" + Environment.NewLine +
"                          <p style=\"float:right\">" + Environment.NewLine +
"                           @if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Create))" + Environment.NewLine +
"                           {" + Environment.NewLine +
 "                        <button class=\"btn icon-btn btn-success saveRecords\" type=\"submit\" disabled>" + Environment.NewLine +
 "                            <span class=\"btn-glyphicon glyphicon glyphicon-floppy-disk img-circle text-success\">" + Environment.NewLine +
 "                            </span>Save Record" + Environment.NewLine +
 "                        </button>" + Environment.NewLine +
"                           }" + Environment.NewLine +
"                           @if (Model.IsParent)" + Environment.NewLine +
"                           {" + Environment.NewLine +
string.Format("                                <a href=\"/{0}/{1}\" class=\"btn btn-primary btn-flat\">", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
"                                   <i class=\"fa fa-hand-o-left\" ></i> Back to Summary" + Environment.NewLine +
"                               </a>" + Environment.NewLine +
"                           }" + Environment.NewLine +
"                           else if(!Model.IsParent && Model.EntryType==\"S\") " + Environment.NewLine +
"                           {" + Environment.NewLine +
string.Format("                                <a href=\"/{0}/{1}\" class=\"btn btn-primary btn-flat\">", moduleSetup.ModuleName, moduleSetup.ParentModule) + Environment.NewLine +
"                                   <i class=\"fa fa-hand-o-left\" ></i> Back to Summary" + Environment.NewLine +
"                               </a>" + Environment.NewLine +
"                           }" + Environment.NewLine +
"                           else" + Environment.NewLine +
"                           {" + Environment.NewLine +
string.Format("                                <a href=\"/{0}/{1}\" class=\"btn btn-primary btn-flat\" id=\"backtoSummary\">", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
"                                   <i class=\"fa fa-hand-o-left\" ></i> Back to Summary" + Environment.NewLine +
"                               </a>" + Environment.NewLine +
"                           }" + Environment.NewLine +
"                           </p>" + Environment.NewLine +
 "" + Environment.NewLine +
 "                    </div>" + Environment.NewLine +
 "                </div>" + Environment.NewLine +
 "            </div>" + Environment.NewLine +
 "        }" + Environment.NewLine +
 "    </div>" + Environment.NewLine +
 "</div>" + Environment.NewLine;

                return _createTemplate;
            }
            else
            {
                string _createTemplate = "@model " + projectName + ".Entity.DTO.ModuleSummary" + Environment.NewLine +
                  "@using GSAssetManagement.Web.Utility" + Environment.NewLine +
                  "@{" + Environment.NewLine +
                  "    Layout = this.Request.IsAjaxRequest() ? \"\" : \"~/Views/Shared/_Layout.cshtml\";" + Environment.NewLine +
                  "}" + Environment.NewLine +
                  "<div class=\"row\">" + Environment.NewLine +
                  "    @Html.Hidden(\"ParentId\", null)" + Environment.NewLine +
                  "<div class=\"col-md-2\">" + Environment.NewLine +
                  string.Format("<a href=\"/{0}/{1}/Create\" class=\"btn btn-success btn-flat btn-block margin-bottom\">Add More</a>", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
                  "   <div class=\"box box-solid\">" + Environment.NewLine +
                  "     <div class=\"box-body no-padding\">" + Environment.NewLine +
                  "         <ul class=\"nav nav-pills nav-stacked\" id=\"childTabInformation\">" + Environment.NewLine +
    string.Format("            <li class=\"active\"><a href=\"/{0}/{1}/Details\" class=\"scopeChange\" data-scope=\"parent\"><i class=\"fa fa-hand-o-right\"></i> @Model.ModuleSummaryTitle</a></li>", moduleSetup.ModuleName, moduleSetup.DatabaseTable) +
                  "             @foreach (var child in Model.ChildInformations.OrderBy(o=>o.OrderValue))" + Environment.NewLine +
                  "             {" + Environment.NewLine +
                  "                 <li><a href=\"@child.Url\" class=\"scopeChange\" data-scope=\"child\"><i class=\"fa fa-hand-o-right\"></i> @child.ChildModuleSummaryTitle</a></li>" + Environment.NewLine +
                  "             }" + Environment.NewLine +
                  "         </ul>" + Environment.NewLine +
                  "     </div>" + Environment.NewLine +
                  "   </div>" + Environment.NewLine +
                  "</div>" + Environment.NewLine +

 "<div class=\"col-md-10\" id=\"currentTabInformation\">" + Environment.NewLine +
 "@using (Html.BeginForm(\"Create\", Model.ModuleSummaryName, new { area = Model.SchemaName }, FormMethod.Post, new { @role = \"form\", @id = \"mainForm\", @autocomplete = \"off\", @enctype = \"multipart/form-data\" }))" + Environment.NewLine +
 "            {" + Environment.NewLine +
 "                  @Html.AntiForgeryToken()" + Environment.NewLine +
 "                  @Html.Hidden(\"Id\", Model.PrimaryRecordId)" + Environment.NewLine +
 "                  @Html.Hidden(\"CurrentAction\", " + projectName + ".Entity.CurrentAction.Create)" + Environment.NewLine +
 "                  foreach (var item in Model.moduleBussinesLogicSummaries.Where(x => x.HtmlDataType == \"hidden\"))" + Environment.NewLine +
 "                  {" + Environment.NewLine +
 "                       @Html.Hidden(item.ColumnName, item.CurrentValue)" + Environment.NewLine +
 "                  }" + Environment.NewLine +
 "        <div class=\"box box-primary\">" + Environment.NewLine +
"            <div class=\"box-header with-border\">" + Environment.NewLine +
string.Format("<h5 class=\"box-title\">{0}</h5>", moduleSetup.Name) + Environment.NewLine +
"            </div>" + Environment.NewLine +
"            <div class=\"box-body\">" + Environment.NewLine +
"                        <div class=\"row\">" + Environment.NewLine +
"                            @foreach (var item in Model.moduleBussinesLogicSummaries.Where(x => x.HtmlDataType != \"hidden\").OrderBy(c => c.Position))" + Environment.NewLine +
"                            {" + Environment.NewLine +
"                                if (item.HtmlDataType == \"input\")" + Environment.NewLine +
"                                {" + Environment.NewLine +
"                                    <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
"                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name </label>" + Environment.NewLine +
"                                        @Html.TextBox(item.ColumnName, item.CurrentValue ?? item.DefaultValue, item.Attributes)" + Environment.NewLine +
"                                    </div>" + Environment.NewLine +
"                                }" + Environment.NewLine +
"                                if (item.HtmlDataType == \"textarea\")" + Environment.NewLine +
"                                {" + Environment.NewLine +
"                                    <div class=\"form-group has-feedback col-lg-12\">" + Environment.NewLine +
"                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name </label>" + Environment.NewLine +
"                                        @Html.TextArea(item.ColumnName, item.CurrentValue as string ?? item.DefaultValue as string, 5, 20, item.Attributes)" + Environment.NewLine +
"                                    </div>" + Environment.NewLine +
"                                }" + Environment.NewLine +
"                                if (item.HtmlDataType == \"select\")" + Environment.NewLine +
"                                {" + Environment.NewLine +
"                                   <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
"                                       <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name  </label>" + Environment.NewLine +
"                                       @Html.DropDownList(item.ColumnName, item.SelectList.Select(s => new SelectListItem() { Text = s.Text, Value = s.Value, Selected = s.Selected }), \"--Select--\", item.Attributes)" + Environment.NewLine +
"                                   </div>" + Environment.NewLine +
"                                }" + Environment.NewLine +
"                                }         " + Environment.NewLine +
"                        </div>    " + Environment.NewLine +
"                    <div class=\"row\">" + Environment.NewLine +
"                          <p style=\"float:right\">" + Environment.NewLine +
"                           @if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Create))" + Environment.NewLine +
"                           {" + Environment.NewLine +
 "                        <button class=\"btn icon-btn btn-success saveRecords\" type=\"submit\" disabled>" + Environment.NewLine +
 "                            <span class=\"btn-glyphicon glyphicon glyphicon-floppy-disk img-circle text-success\">" + Environment.NewLine +
 "                            </span>Save Record" + Environment.NewLine +
 "                        </button>" + Environment.NewLine +
"                           }" + Environment.NewLine +
"                           @if (Model.IsParent)" + Environment.NewLine +
"                           {" + Environment.NewLine +
string.Format("                                <a href=\"/{0}/{1}\" class=\"btn btn-primary btn-flat\">", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
"                                   <i class=\"fa fa-hand-o-left\" ></i> Back to Summary" + Environment.NewLine +
"                               </a>" + Environment.NewLine +
"                           }" + Environment.NewLine +
"                           else if(!Model.IsParent && Model.EntryType==\"S\") " + Environment.NewLine +
"                           {" + Environment.NewLine +
string.Format("                                <a href=\"/{0}/{1}\" class=\"btn btn-primary btn-flat\">", moduleSetup.ModuleName, moduleSetup.ParentModule) + Environment.NewLine +
"                                   <i class=\"fa fa-hand-o-left\" ></i> Back to Summary" + Environment.NewLine +
"                               </a>" + Environment.NewLine +
"                           }" + Environment.NewLine +
"                           else" + Environment.NewLine +
"                           {" + Environment.NewLine +
string.Format("                                <a href=\"/{0}/{1}\" class=\"btn btn-primary btn-flat\" id=\"backtoSummary\">", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
"                                   <i class=\"fa fa-hand-o-left\" ></i> Back to Summary" + Environment.NewLine +
"                               </a>" + Environment.NewLine +
"                           }" + Environment.NewLine +
"                           </p>" + Environment.NewLine +
 "" + Environment.NewLine +
 "                    </div>" + Environment.NewLine +
 "                </div>" + Environment.NewLine +
 "            </div>" + Environment.NewLine +
 "        }" + Environment.NewLine +
 "    </div>" + Environment.NewLine +
 "</div>" + Environment.NewLine;

                return _createTemplate;
            }

        }

        public static string GenerateDetails(ModuleSetupNew moduleSetup)
        {
            string projectName = ConfigurationManager.AppSettings["ProjectName"].ToString();
            if ((!moduleSetup.IsParent) || (moduleSetup.IsParent && moduleSetup.ChildTableInformations.Count() == 0))
            {
                string _detailsTemplate = "@model " + projectName + ".Entity.DTO.ModuleSummary" + Environment.NewLine +
"@using GSAssetManagement.Web.Utility" + Environment.NewLine +
"@{                                                                                                                             " + Environment.NewLine +
"    Layout = this.Request.IsAjaxRequest() ? \"\" : \"~/Views/Shared/_Layout.cshtml\";                                          " + Environment.NewLine +
"}                                                                                                                              " + Environment.NewLine +
"" + Environment.NewLine +
"" + Environment.NewLine +
"<div class=\"row\">" + Environment.NewLine +
"    @Html.Hidden(\"ParentId\", Model.PrimaryRecordId)   " + Environment.NewLine +
  "    <div class=\"col-md-12\" id=\"currentTabInformation\">" + Environment.NewLine +
  "    @using (Html.BeginForm(\"Update\", Model.ModuleSummaryName, new { area = Model.SchemaName }, FormMethod.Post, new { @role = \"form\", @id = \"mainForm\", @autocomplete = \"off\", @enctype = \"multipart/form-data\" }))" + Environment.NewLine +
  "            {           " +
  "                                                                                                                                                                              " + Environment.NewLine +
  "             @Html.Hidden(\"Id\", Model.PrimaryRecordId)" + Environment.NewLine +
  "             @Html.Hidden(\"CurrentAction\", GSAssetManagement.Entity.CurrentAction.Edit)" + Environment.NewLine +
  "             foreach (var item in Model.moduleBussinesLogicSummaries.Where(x => x.HtmlDataType == \"hidden\"))" + Environment.NewLine +
  "             {           " + Environment.NewLine +
  "                 @Html.Hidden(item.ColumnName, item.CurrentValue)" + Environment.NewLine +
  "             }           " + Environment.NewLine +
  "                        " + Environment.NewLine +
  "        <div class=\"box box-primary\">" + Environment.NewLine +
"            <div class=\"box-header with-border\">" + Environment.NewLine +
string.Format("<h5 class=\"box-title\">{0}</h5>", moduleSetup.Name) + Environment.NewLine +
"            </div>" + Environment.NewLine +
"            <div class=\"box-body\">" + Environment.NewLine +
"                        <div class=\"row\">" + Environment.NewLine +
"                            @foreach (var item in Model.moduleBussinesLogicSummaries.Where(x => x.HtmlDataType != \"hidden\").OrderBy(c => c.Position))" + Environment.NewLine +
"                            {" + Environment.NewLine +
"                                if (item.HtmlDataType == \"input\")" + Environment.NewLine +
"                                {" + Environment.NewLine +
"                                    <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
"                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name </label>" + Environment.NewLine +
"                                        @Html.TextBox(item.ColumnName, item.CurrentValue ?? item.DefaultValue, item.Attributes)" + Environment.NewLine +
"                                        <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
"                                    </div>" + Environment.NewLine +
"                                }" + Environment.NewLine +
"                                if (item.HtmlDataType == \"textarea\")" + Environment.NewLine +
"                                {" + Environment.NewLine +
"                                    <div class=\"form-group has-feedback col-lg-12\">" + Environment.NewLine +
"                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name </label>" + Environment.NewLine +
"                                        @Html.TextArea(item.ColumnName, item.CurrentValue as string ?? item.DefaultValue as string, 5, 20, item.Attributes)" + Environment.NewLine +
"                                        <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
"                                    </div>" + Environment.NewLine +
"                                }" + Environment.NewLine +
                                "    if (item.HtmlDataType == \"select\")" + Environment.NewLine +
                                "    {" + Environment.NewLine +
                                "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
                                "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name  </label>" + Environment.NewLine +
                                "            @Html.DropDownList(item.ColumnName, item.SelectList.Select(s => new SelectListItem() { Text = s.Text, Value = s.Value, Selected = s.Selected }), \"--Select--\", item.Attributes)" + Environment.NewLine +
                                "            <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
                                "        </div>" + Environment.NewLine +
                                "    }" + Environment.NewLine +
"                                }         " + Environment.NewLine +
"                        </div>" + Environment.NewLine +
"                    <div class=\"row\">																							" + Environment.NewLine +
"                        <div class=\"col-md-12\">																					" + Environment.NewLine +
"                            <div class=\"box box-primary\">																		" + Environment.NewLine +
"                                <h4 class=\"box-title\"><i class=\"glyphicon glyphicon-list-alt\"></i> Record Log</h4>				" + Environment.NewLine +
"                                <hr class=\"message-inner-separator\">																" + Environment.NewLine +
"                                <div class=\"box-body table-responsive no-padding\">												" + Environment.NewLine +
"                                    <table class=\"table table-bordered\">															" + Environment.NewLine +
"                                        <thead>																					" + Environment.NewLine +
"                                            <tr>																					" + Environment.NewLine +
"                                                <th>#</th>																			" + Environment.NewLine +
"                                                <th nowrap>Record Status</th>														" + Environment.NewLine +
"                                                <th nowrap>Created By</th>															" + Environment.NewLine +
"                                                <th nowrap>Created Date</th>														" + Environment.NewLine +
"                                                <th nowrap>Modified By</th>														" + Environment.NewLine +
"                                                <th nowrap>Modified Date</th>														" + Environment.NewLine +
"                                                <th nowrap>Authorise By</th>														" + Environment.NewLine +
"                                                <th nowrap>Authorise Date</th>														" + Environment.NewLine +
"																																	" + Environment.NewLine +
"                                            </tr>																					" + Environment.NewLine +
"                                        </thead>																					" + Environment.NewLine +
"                                        <tbody>																					" + Environment.NewLine +
"                                            <tr>																					" + Environment.NewLine +
"                                                <td>1</td>																			" + Environment.NewLine +
"                                                <td nowrap>@Model.RecordStatus</td>												" + Environment.NewLine +
"                                                <td nowrap>@Model.CreatedBy</td>													" + Environment.NewLine +
"                                                <td nowrap>@Model.CreatedDate</td>													" + Environment.NewLine +
"                                                <td nowrap>@Model.ModifiedBy</td>													" + Environment.NewLine +
"                                                <td nowrap>@Model.ModifiedDate</td>												" + Environment.NewLine +
"                                                <td nowrap>@Model.AuthorisedBy</td>												" + Environment.NewLine +
"                                                <td nowrap>@Model.AuthorisedDate</td>												" + Environment.NewLine +
"                                            </tr>																					" + Environment.NewLine +
"                                        </tbody>																					" + Environment.NewLine +
"                                    </table>																						" + Environment.NewLine +
"                                </div>																								" + Environment.NewLine +
"                            </div>																									" + Environment.NewLine +
"																																	" + Environment.NewLine +
"                        </div>																										" + Environment.NewLine +
"                    </div>																											" + Environment.NewLine +

"                    @if (Model.RecordChangeLog != null)																				" + Environment.NewLine +
"                    {																													" + Environment.NewLine +
"																																		" + Environment.NewLine +
"                        <div class=\"row\">																							" + Environment.NewLine +
"                            <div class=\"col-md-12\">																					" + Environment.NewLine +
"                                <div class=\"box box-primary\">																		" + Environment.NewLine +
"                                    <h4 class=\"box-title\"><i class=\"glyphicon glyphicon-warning-sign\"></i> Change Logs</h4>		" + Environment.NewLine +
"                                    <hr class=\"message-inner-separator\">																" + Environment.NewLine +
"                                    <div class=\"box-body table-responsive no-padding\">												" + Environment.NewLine +
"                                        <table class=\"table table-bordered\">															" + Environment.NewLine +
"                                            <thead>																					" + Environment.NewLine +
"                                                <tr>																					" + Environment.NewLine +
"                                                    <th>#</th>																			" + Environment.NewLine +
"                                                    <th>Field Name</th>																" + Environment.NewLine +
"                                                    <th>Log Status</th>																" + Environment.NewLine +
"                                                    <th>Modified Date</th>																" + Environment.NewLine +
"                                                    <th>Original Value</th>															" + Environment.NewLine +
"                                                    <th>Modified Value</th>															" + Environment.NewLine +
"                                                </tr>																					" + Environment.NewLine +
"                                            </thead>																					" + Environment.NewLine +
"                                            <tbody>																					" + Environment.NewLine +
"                                                @{																						" + Environment.NewLine +
"                                                    int i = 1;																			" + Environment.NewLine +
"                                                    string _class = string.Empty;														" + Environment.NewLine +
"                                                    foreach (var item in Model.RecordChangeLog.PropertyChangeLogs)						" + Environment.NewLine +
"                                                    {																					" + Environment.NewLine +
"																																		" + Environment.NewLine +
"                                                        <tr class=\"primary\">															" + Environment.NewLine +
"                                                            <td scope=\"row\"> @i </td>												" + Environment.NewLine +
"                                                            <td> @item.PropertyName </td>												" + Environment.NewLine +
"                                                            <td> @Model.RecordChangeLog.ChangeStatus.ToString()</td>					" + Environment.NewLine +
"                                                            <td> @Model.RecordChangeLog.ChangeDate </td>								" + Environment.NewLine +
"                                                            <td> @item.OldValue </td>													" + Environment.NewLine +
"                                                            <td> @item.NewValue </td>													" + Environment.NewLine +
"																																		" + Environment.NewLine +
"                                                        </tr>																			" + Environment.NewLine +
"                                                        i++;																			" + Environment.NewLine +
"                                                    }																					" + Environment.NewLine +
"                                                }																						" + Environment.NewLine +
"                                            </tbody>																					" + Environment.NewLine +
"                                        </table>																						" + Environment.NewLine +
"                                    </div>																								" + Environment.NewLine +
"                                </div>																									" + Environment.NewLine +
"																																		" + Environment.NewLine +
"                            </div>																										" + Environment.NewLine +
"                        </div>																											" + Environment.NewLine +
"                    }																													" + Environment.NewLine +


"					<div class=\"row\">																																						" + Environment.NewLine +
"                        <p style=\"float:right\">																																			" + Environment.NewLine +
"                            @if (Model.RecordStatus == " + projectName + ".Entity.RecordStatus.Active || Model.ModifiedById == ViewUserInformation.GetCurrentUserId())									" + Environment.NewLine +
"                            {																																								" + Environment.NewLine +
"                                if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Edit))						" + Environment.NewLine +
"                                {																																							" + Environment.NewLine +
"                                    <button class=\"btn icon-btn btn-success update saveRecords\" type=\"submit\" data-currentaction=\"@" + projectName + ".Entity.CurrentAction.Edit\">				" + Environment.NewLine +
"                                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-disk img-circle text-success\">											" + Environment.NewLine +
"                                        </span>Update Record																																" + Environment.NewLine +
"                                    </button>																																				" + Environment.NewLine +
"                                }																																							" + Environment.NewLine +
"                                if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Delete))						" + Environment.NewLine +
"                                {																																							" + Environment.NewLine +
"                                    <button class=\"btn icon-btn btn-danger update saveRecords\" type=\"submit\" data-currentaction=\"@" + projectName + ".Entity.CurrentAction.Delete\">				" + Environment.NewLine +
"                                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-trash img-circle text-success\">												" + Environment.NewLine +
"                                        </span>Delete Record																																" + Environment.NewLine +
"                                    </button>																																				" + Environment.NewLine +
"                                }																																							" + Environment.NewLine +
"                            }																																								" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                            else if (Model.RecordStatus == " + projectName + ".Entity.RecordStatus.UnVerified)																						" + Environment.NewLine +
"                            {																																								" + Environment.NewLine +
"                                if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Discard))					" + Environment.NewLine +
"                                {																																							" + Environment.NewLine +
"                                    <button class=\"btn icon-btn btn-warning revert saveRecords\" type=\"submit\" data-currentaction=\"@" + projectName + ".Entity.CurrentAction.Discard\">				" + Environment.NewLine +
"                                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-remove img-circle text-success\">										" + Environment.NewLine +
"                                        </span>Discard Changes																																" + Environment.NewLine +
"                                    </button>																																				" + Environment.NewLine +
"                                }																																							" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                                if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Authorise))					" + Environment.NewLine +
"                                {																																							" + Environment.NewLine +
"                                    <button class=\"btn icon-btn btn-warning revert saveRecords\" type=\"submit\" data-currentaction=\"@" + projectName + ".Entity.CurrentAction.Revert\">				" + Environment.NewLine +
"                                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-remove img-circle text-success\">										" + Environment.NewLine +
"                                        </span>Revert Changes																																" + Environment.NewLine +
"                                    </button>																																				" + Environment.NewLine +
"                                }																																							" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                                if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Authorise))					" + Environment.NewLine +
"                                {																																							" + Environment.NewLine +
"                                    <button class=\"btn icon-btn btn-success authorise saveRecords\" type=\"submit\" data-currentaction=\"@" + projectName + ".Entity.CurrentAction.Authorise\">		" + Environment.NewLine +
"                                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-saved img-circle text-success\">										" + Environment.NewLine +
"                                        </span>Authorise Changes																															" + Environment.NewLine +
"                                    </button>																																				" + Environment.NewLine +
"                                }																																							" + Environment.NewLine +
"																																															" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                            }																																								" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                            @if (Model.IsParent)																																			" + Environment.NewLine +
"                            {																																								" + Environment.NewLine +
string.Format("                                <a href=\"/{0}/{1}\" class=\"btn btn-primary btn-flat\">", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
"                                    <i class=\"fa  fa-hand-o-left\"></i> Back to Summary																									" + Environment.NewLine +
"                                </a>																																						" + Environment.NewLine +
"                            }																																								" + Environment.NewLine +
"                           else if(!Model.IsParent && Model.EntryType==\"S\") " + Environment.NewLine +
"                           {" + Environment.NewLine +
string.Format("                                <a href=\"/{0}/{1}\" class=\"btn btn-primary btn-flat\">", moduleSetup.ModuleName, moduleSetup.ParentModule) + Environment.NewLine +
"                                   <i class=\"fa fa-hand-o-left\" ></i> Back to Summary" + Environment.NewLine +
"                               </a>" + Environment.NewLine +
"                           }" + Environment.NewLine +
"                            else																																							" + Environment.NewLine +
"                            {																																								" + Environment.NewLine +
string.Format("                                <a href=\"/{0}/{1}\" class=\"btn btn-primary btn-flat\" id=\"backtoSummary\">", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
"                                    <i class=\"fa  fa-hand-o-left\"></i> Back to Summary																									" + Environment.NewLine +
"                                </a>																																						" + Environment.NewLine +
"                            }																																								" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                        </p>																																								" + Environment.NewLine +
"                    </div>																																									" + Environment.NewLine +

      "            </div>" + Environment.NewLine +
"        </div>" + Environment.NewLine +
"      }" + Environment.NewLine +
"   </div>" + Environment.NewLine +
"</div>" + Environment.NewLine;

                return _detailsTemplate;


            }
            else
            {
                string _detailsTemplate = "@model " + projectName + ".Entity.DTO.ModuleSummary" + Environment.NewLine +
"@using GSAssetManagement.Web.Utility" + Environment.NewLine +
"@{                                                                                                                             " + Environment.NewLine +
"    Layout = this.Request.IsAjaxRequest() ? \"\" : \"~/Views/Shared/_Layout.cshtml\";                                          " + Environment.NewLine +
"}                                                                                                                              " + Environment.NewLine +
"" + Environment.NewLine +
"" + Environment.NewLine +
"<div class=\"row\">" + Environment.NewLine +
"    @Html.Hidden(\"ParentId\", Model.PrimaryRecordId)   " + Environment.NewLine +

"<div class=\"col-md-2\">" + Environment.NewLine +
                  string.Format("<a href=\"/{0}/{1}/Create\" class=\"btn btn-success btn-flat btn-block margin-bottom\">Add More</a>", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
                  "   <div class=\"box box-solid\">" + Environment.NewLine +
                  "     <div class=\"box-body no-padding\">" + Environment.NewLine +
                  "         <ul class=\"nav nav-pills nav-stacked\" id=\"childTabInformation\">" + Environment.NewLine +
    string.Format("            <li class=\"active\"><a href=\"/{0}/{1}/Details\" class=\"scopeChange\" data-scope=\"parent\"><i class=\"fa fa-hand-o-right\"></i> @Model.ModuleSummaryTitle</a></li>", moduleSetup.ModuleName, moduleSetup.DatabaseTable) +
                  "             @foreach (var child in Model.ChildInformations.OrderBy(o=>o.OrderValue))" + Environment.NewLine +
                  "             {" + Environment.NewLine +
                  "                 <li><a href=\"@child.Url\" class=\"scopeChange\" data-scope=\"child\"><i class=\"fa fa-hand-o-right\"></i> @child.ChildModuleSummaryTitle</a></li>" + Environment.NewLine +
                  "             }" + Environment.NewLine +
                  "         </ul>" + Environment.NewLine +
                  "     </div>" + Environment.NewLine +
                  "   </div>" + Environment.NewLine +
                  "</div>" + Environment.NewLine +

  "    <div class=\"col-md-10\" id=\"currentTabInformation\">" + Environment.NewLine +
  "    @using (Html.BeginForm(\"Update\", Model.ModuleSummaryName, new { area = Model.SchemaName }, FormMethod.Post, new { @role = \"form\", @id = \"mainForm\", @autocomplete = \"off\", @enctype = \"multipart/form-data\" }))" + Environment.NewLine +
  "            {           " +
  "                                                                                                                                                                              " + Environment.NewLine +
  "             @Html.Hidden(\"Id\", Model.PrimaryRecordId)" + Environment.NewLine +
  "             @Html.Hidden(\"CurrentAction\", GSAssetManagement.Entity.CurrentAction.Edit)" + Environment.NewLine +
  "             foreach (var item in Model.moduleBussinesLogicSummaries.Where(x => x.HtmlDataType == \"hidden\"))" + Environment.NewLine +
  "             {           " + Environment.NewLine +
  "                 @Html.Hidden(item.ColumnName, item.CurrentValue)" + Environment.NewLine +
  "             }           " + Environment.NewLine +
  "                        " + Environment.NewLine +
  "        <div class=\"box box-primary\">" + Environment.NewLine +
"            <div class=\"box-header with-border\">" + Environment.NewLine +
string.Format("<h5 class=\"box-title\">{0}</h5>", moduleSetup.Name) + Environment.NewLine +
"            </div>" + Environment.NewLine +
"            <div class=\"box-body\">" + Environment.NewLine +
"                        <div class=\"row\">" + Environment.NewLine +
"                            @foreach (var item in Model.moduleBussinesLogicSummaries.Where(x => x.HtmlDataType != \"hidden\").OrderBy(c => c.Position))" + Environment.NewLine +
"                            {" + Environment.NewLine +
"                                if (item.HtmlDataType == \"input\")" + Environment.NewLine +
"                                {" + Environment.NewLine +
"                                    <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
"                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name </label>" + Environment.NewLine +
"                                        @Html.TextBox(item.ColumnName, item.CurrentValue ?? item.DefaultValue, item.Attributes)" + Environment.NewLine +
"                                        <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
"                                    </div>" + Environment.NewLine +
"                                }" + Environment.NewLine +
"                                if (item.HtmlDataType == \"textarea\")" + Environment.NewLine +
"                                {" + Environment.NewLine +
"                                    <div class=\"form-group has-feedback col-lg-12\">" + Environment.NewLine +
"                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name </label>" + Environment.NewLine +
"                                        @Html.TextArea(item.ColumnName, item.CurrentValue as string ?? item.DefaultValue as string, 5, 20, item.Attributes)" + Environment.NewLine +
"                                        <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
"                                    </div>" + Environment.NewLine +
"                                }" + Environment.NewLine +
                                "    if (item.HtmlDataType == \"select\")" + Environment.NewLine +
                                "    {" + Environment.NewLine +
                                "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
                                "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name  </label>" + Environment.NewLine +
                                "            @Html.DropDownList(item.ColumnName, item.SelectList.Select(s => new SelectListItem() { Text = s.Text, Value = s.Value, Selected = s.Selected }), \"--Select--\", item.Attributes)" + Environment.NewLine +
                                "            <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
                                "        </div>" + Environment.NewLine +
                                "    }" + Environment.NewLine +
"                                }         " + Environment.NewLine +
"                        </div>" + Environment.NewLine +
"                    <div class=\"row\">																							" + Environment.NewLine +
"                        <div class=\"col-md-12\">																					" + Environment.NewLine +
"                            <div class=\"box box-primary\">																		" + Environment.NewLine +
"                                <h4 class=\"box-title\"><i class=\"glyphicon glyphicon-list-alt\"></i> Record Log</h4>				" + Environment.NewLine +
"                                <hr class=\"message-inner-separator\">																" + Environment.NewLine +
"                                <div class=\"box-body table-responsive no-padding\">												" + Environment.NewLine +
"                                    <table class=\"table table-bordered\">															" + Environment.NewLine +
"                                        <thead>																					" + Environment.NewLine +
"                                            <tr>																					" + Environment.NewLine +
"                                                <th>#</th>																			" + Environment.NewLine +
"                                                <th nowrap>Record Status</th>														" + Environment.NewLine +
"                                                <th nowrap>Created By</th>															" + Environment.NewLine +
"                                                <th nowrap>Created Date</th>														" + Environment.NewLine +
"                                                <th nowrap>Modified By</th>														" + Environment.NewLine +
"                                                <th nowrap>Modified Date</th>														" + Environment.NewLine +
"                                                <th nowrap>Authorise By</th>														" + Environment.NewLine +
"                                                <th nowrap>Authorise Date</th>														" + Environment.NewLine +
"																																	" + Environment.NewLine +
"                                            </tr>																					" + Environment.NewLine +
"                                        </thead>																					" + Environment.NewLine +
"                                        <tbody>																					" + Environment.NewLine +
"                                            <tr>																					" + Environment.NewLine +
"                                                <td>1</td>																			" + Environment.NewLine +
"                                                <td nowrap>@Model.RecordStatus</td>												" + Environment.NewLine +
"                                                <td nowrap>@Model.CreatedBy</td>													" + Environment.NewLine +
"                                                <td nowrap>@Model.CreatedDate</td>													" + Environment.NewLine +
"                                                <td nowrap>@Model.ModifiedBy</td>													" + Environment.NewLine +
"                                                <td nowrap>@Model.ModifiedDate</td>												" + Environment.NewLine +
"                                                <td nowrap>@Model.AuthorisedBy</td>												" + Environment.NewLine +
"                                                <td nowrap>@Model.AuthorisedDate</td>												" + Environment.NewLine +
"                                            </tr>																					" + Environment.NewLine +
"                                        </tbody>																					" + Environment.NewLine +
"                                    </table>																						" + Environment.NewLine +
"                                </div>																								" + Environment.NewLine +
"                            </div>																									" + Environment.NewLine +
"																																	" + Environment.NewLine +
"                        </div>																										" + Environment.NewLine +
"                    </div>																											" + Environment.NewLine +

"                    @if (Model.RecordChangeLog != null)																				" + Environment.NewLine +
"                    {																													" + Environment.NewLine +
"																																		" + Environment.NewLine +
"                        <div class=\"row\">																							" + Environment.NewLine +
"                            <div class=\"col-md-12\">																					" + Environment.NewLine +
"                                <div class=\"box box-primary\">																		" + Environment.NewLine +
"                                    <h4 class=\"box-title\"><i class=\"glyphicon glyphicon-warning-sign\"></i> Change Logs</h4>		" + Environment.NewLine +
"                                    <hr class=\"message-inner-separator\">																" + Environment.NewLine +
"                                    <div class=\"box-body table-responsive no-padding\">												" + Environment.NewLine +
"                                        <table class=\"table table-bordered\">															" + Environment.NewLine +
"                                            <thead>																					" + Environment.NewLine +
"                                                <tr>																					" + Environment.NewLine +
"                                                    <th>#</th>																			" + Environment.NewLine +
"                                                    <th>Field Name</th>																" + Environment.NewLine +
"                                                    <th>Log Status</th>																" + Environment.NewLine +
"                                                    <th>Modified Date</th>																" + Environment.NewLine +
"                                                    <th>Original Value</th>															" + Environment.NewLine +
"                                                    <th>Modified Value</th>															" + Environment.NewLine +
"                                                </tr>																					" + Environment.NewLine +
"                                            </thead>																					" + Environment.NewLine +
"                                            <tbody>																					" + Environment.NewLine +
"                                                @{																						" + Environment.NewLine +
"                                                    int i = 1;																			" + Environment.NewLine +
"                                                    string _class = string.Empty;														" + Environment.NewLine +
"                                                    foreach (var item in Model.RecordChangeLog.PropertyChangeLogs)						" + Environment.NewLine +
"                                                    {																					" + Environment.NewLine +
"																																		" + Environment.NewLine +
"                                                        <tr class=\"primary\">															" + Environment.NewLine +
"                                                            <td scope=\"row\"> @i </td>												" + Environment.NewLine +
"                                                            <td> @item.PropertyName </td>												" + Environment.NewLine +
"                                                            <td> @Model.RecordChangeLog.ChangeStatus.ToString()</td>					" + Environment.NewLine +
"                                                            <td> @Model.RecordChangeLog.ChangeDate </td>								" + Environment.NewLine +
"                                                            <td> @item.OldValue </td>													" + Environment.NewLine +
"                                                            <td> @item.NewValue </td>													" + Environment.NewLine +
"																																		" + Environment.NewLine +
"                                                        </tr>																			" + Environment.NewLine +
"                                                        i++;																			" + Environment.NewLine +
"                                                    }																					" + Environment.NewLine +
"                                                }																						" + Environment.NewLine +
"                                            </tbody>																					" + Environment.NewLine +
"                                        </table>																						" + Environment.NewLine +
"                                    </div>																								" + Environment.NewLine +
"                                </div>																									" + Environment.NewLine +
"																																		" + Environment.NewLine +
"                            </div>																										" + Environment.NewLine +
"                        </div>																											" + Environment.NewLine +
"                    }																													" + Environment.NewLine +


"					<div class=\"row\">																																						" + Environment.NewLine +
"                        <p style=\"float:right\">																																			" + Environment.NewLine +
"                            @if (Model.RecordStatus == " + projectName + ".Entity.RecordStatus.Active || Model.ModifiedById == ViewUserInformation.GetCurrentUserId())									" + Environment.NewLine +
"                            {																																								" + Environment.NewLine +
"                                if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Edit))						" + Environment.NewLine +
"                                {																																							" + Environment.NewLine +
"                                    <button class=\"btn icon-btn btn-success update saveRecords\" type=\"submit\" data-currentaction=\"@" + projectName + ".Entity.CurrentAction.Edit\">				" + Environment.NewLine +
"                                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-disk img-circle text-success\">											" + Environment.NewLine +
"                                        </span>Update Record																																" + Environment.NewLine +
"                                    </button>																																				" + Environment.NewLine +
"                                }																																							" + Environment.NewLine +
"                                if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Delete))						" + Environment.NewLine +
"                                {																																							" + Environment.NewLine +
"                                    <button class=\"btn icon-btn btn-danger update saveRecords\" type=\"submit\" data-currentaction=\"@" + projectName + ".Entity.CurrentAction.Delete\">				" + Environment.NewLine +
"                                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-trash img-circle text-success\">												" + Environment.NewLine +
"                                        </span>Delete Record																																" + Environment.NewLine +
"                                    </button>																																				" + Environment.NewLine +
"                                }																																							" + Environment.NewLine +
"                            }																																								" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                            else if (Model.RecordStatus == " + projectName + ".Entity.RecordStatus.UnVerified)																						" + Environment.NewLine +
"                            {																																								" + Environment.NewLine +
"                                if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Discard))					" + Environment.NewLine +
"                                {																																							" + Environment.NewLine +
"                                    <button class=\"btn icon-btn btn-warning revert saveRecords\" type=\"submit\" data-currentaction=\"@" + projectName + ".Entity.CurrentAction.Discard\">				" + Environment.NewLine +
"                                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-remove img-circle text-success\">										" + Environment.NewLine +
"                                        </span>Discard Changes																																" + Environment.NewLine +
"                                    </button>																																				" + Environment.NewLine +
"                                }																																							" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                                if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Authorise))					" + Environment.NewLine +
"                                {																																							" + Environment.NewLine +
"                                    <button class=\"btn icon-btn btn-warning revert saveRecords\" type=\"submit\" data-currentaction=\"@" + projectName + ".Entity.CurrentAction.Revert\">				" + Environment.NewLine +
"                                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-remove img-circle text-success\">										" + Environment.NewLine +
"                                        </span>Revert Changes																																" + Environment.NewLine +
"                                    </button>																																				" + Environment.NewLine +
"                                }																																							" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                                if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Authorise))					" + Environment.NewLine +
"                                {																																							" + Environment.NewLine +
"                                    <button class=\"btn icon-btn btn-success authorise saveRecords\" type=\"submit\" data-currentaction=\"@" + projectName + ".Entity.CurrentAction.Authorise\">		" + Environment.NewLine +
"                                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-saved img-circle text-success\">										" + Environment.NewLine +
"                                        </span>Authorise Changes																															" + Environment.NewLine +
"                                    </button>																																				" + Environment.NewLine +
"                                }																																							" + Environment.NewLine +
"																																															" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                            }																																								" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                            @if (Model.IsParent)																																			" + Environment.NewLine +
"                            {																																								" + Environment.NewLine +
string.Format("                                <a href=\"/{0}/{1}\" class=\"btn btn-primary btn-flat\">", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
"                                    <i class=\"fa  fa-hand-o-left\"></i> Back to Summary																									" + Environment.NewLine +
"                                </a>																																						" + Environment.NewLine +
"                            }																																								" + Environment.NewLine +
"                           else if(!Model.IsParent && Model.EntryType==\"S\") " + Environment.NewLine +
"                           {" + Environment.NewLine +
string.Format("                                <a href=\"/{0}/{1}\" class=\"btn btn-primary btn-flat\">", moduleSetup.ModuleName, moduleSetup.ParentModule) + Environment.NewLine +
"                                   <i class=\"fa fa-hand-o-left\" ></i> Back to Summary" + Environment.NewLine +
"                               </a>" + Environment.NewLine +
"                           }" + Environment.NewLine +
"                            else																																							" + Environment.NewLine +
"                            {																																								" + Environment.NewLine +
string.Format("                                <a href=\"/{0}/{1}\" class=\"btn btn-primary btn-flat\" id=\"backtoSummary\">", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
"                                    <i class=\"fa  fa-hand-o-left\"></i> Back to Summary																									" + Environment.NewLine +
"                                </a>																																						" + Environment.NewLine +
"                            }																																								" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                        </p>																																								" + Environment.NewLine +
"                    </div>																																									" + Environment.NewLine +

      "            </div>" + Environment.NewLine +
"        </div>" + Environment.NewLine +
"      }" + Environment.NewLine +
"   </div>" + Environment.NewLine +
"</div>" + Environment.NewLine;

                return _detailsTemplate;
            }
        }

        public static string GeneratePartialDetails(ModuleSetupNew moduleSetup)
        {
            string projectName = ConfigurationManager.AppSettings["ProjectName"].ToString();
            string _detailsTemplate = "@model " + projectName + ".Entity.DTO.ModuleSummary" + Environment.NewLine +
"@using GSAssetManagement.Web.Utility" + Environment.NewLine +
"@{                                                                                                                             " + Environment.NewLine +
"    Layout = this.Request.IsAjaxRequest() ? \"\" : \"~/Views/Shared/_Layout.cshtml\";                                          " + Environment.NewLine +
"}                                                                                                                              " + Environment.NewLine +
"" + Environment.NewLine +
"" + Environment.NewLine +
  "    @using (Html.BeginForm(\"Update\", Model.ModuleSummaryName, new { area = Model.SchemaName }, FormMethod.Post, new { @role = \"form\", @id = \"mainForm\", @autocomplete = \"off\", @enctype = \"multipart/form-data\" }))" + Environment.NewLine +
  "            {           " +
  "                                                                                                                                                                              " + Environment.NewLine +
  "             @Html.Hidden(\"Id\", Model.PrimaryRecordId)" + Environment.NewLine +
  "             @Html.Hidden(\"CurrentAction\", GSAssetManagement.Entity.CurrentAction.Edit)" + Environment.NewLine +
  "             foreach (var item in Model.moduleBussinesLogicSummaries.Where(x => x.HtmlDataType == \"hidden\"))" + Environment.NewLine +
  "             {           " + Environment.NewLine +
  "                 @Html.Hidden(item.ColumnName, item.CurrentValue)" + Environment.NewLine +
  "             }           " + Environment.NewLine +
  "                        " + Environment.NewLine +
  "        <div class=\"box box-primary\">" + Environment.NewLine +
"            <div class=\"box-header with-border\">" + Environment.NewLine +
string.Format("<h5 class=\"box-title\">{0}</h5>", moduleSetup.Name) + Environment.NewLine +
"            </div>" + Environment.NewLine +
"            <div class=\"box-body\">" + Environment.NewLine +
"                        <div class=\"row\">" + Environment.NewLine +
"                            @foreach (var item in Model.moduleBussinesLogicSummaries.Where(x => x.HtmlDataType != \"hidden\").OrderBy(c => c.Position))" + Environment.NewLine +
"                            {" + Environment.NewLine +
"                                if (item.HtmlDataType == \"input\")" + Environment.NewLine +
"                                {" + Environment.NewLine +
"                                    <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
"                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name </label>" + Environment.NewLine +
"                                        @Html.TextBox(item.ColumnName, item.CurrentValue ?? item.DefaultValue, item.Attributes)" + Environment.NewLine +
"                                        <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
"                                    </div>" + Environment.NewLine +
"                                }" + Environment.NewLine +
"                                if (item.HtmlDataType == \"textarea\")" + Environment.NewLine +
"                                {" + Environment.NewLine +
"                                    <div class=\"form-group has-feedback col-lg-12\">" + Environment.NewLine +
"                                        <label class=\"control-label\" for=\"inputSuccess\"> <i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name </label>" + Environment.NewLine +
"                                        @Html.TextArea(item.ColumnName, item.CurrentValue as string ?? item.DefaultValue as string, 5, 20, item.Attributes)" + Environment.NewLine +
"                                        <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
"                                    </div>" + Environment.NewLine +
"                                }" + Environment.NewLine +
                                "    if (item.HtmlDataType == \"select\")" + Environment.NewLine +
                                "    {" + Environment.NewLine +
                                "        <div class=\"form-group has-feedback col-lg-3\">" + Environment.NewLine +
                                "            <label class=\"control-label\" for=\"inputSuccess\"><i class=\"fa fa-question-circle\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"@item.HelpMessage\"></i> @item.Name  </label>" + Environment.NewLine +
                                "            @Html.DropDownList(item.ColumnName, item.SelectList.Select(s => new SelectListItem() { Text = s.Text, Value = s.Value, Selected = s.Selected }), \"--Select--\", item.Attributes)" + Environment.NewLine +
                                "            <span class=\"field-validation-valid help-block\" data-valmsg-for=\"@item.ColumnName\" data-valmsg-replace=\"true\"></span>" + Environment.NewLine +
                                "        </div>" + Environment.NewLine +
                                "    }" + Environment.NewLine +
"                                }         " + Environment.NewLine +
"                        </div>" + Environment.NewLine +
"                    <div class=\"row\">																							" + Environment.NewLine +
"                        <div class=\"col-md-12\">																					" + Environment.NewLine +
"                            <div class=\"box box-primary\">																		" + Environment.NewLine +
"                                <h4 class=\"box-title\"><i class=\"glyphicon glyphicon-list-alt\"></i> Record Log</h4>				" + Environment.NewLine +
"                                <hr class=\"message-inner-separator\">																" + Environment.NewLine +
"                                <div class=\"box-body table-responsive no-padding\">												" + Environment.NewLine +
"                                    <table class=\"table table-bordered\">															" + Environment.NewLine +
"                                        <thead>																					" + Environment.NewLine +
"                                            <tr>																					" + Environment.NewLine +
"                                                <th>#</th>																			" + Environment.NewLine +
"                                                <th nowrap>Record Status</th>														" + Environment.NewLine +
"                                                <th nowrap>Created By</th>															" + Environment.NewLine +
"                                                <th nowrap>Created Date</th>														" + Environment.NewLine +
"                                                <th nowrap>Modified By</th>														" + Environment.NewLine +
"                                                <th nowrap>Modified Date</th>														" + Environment.NewLine +
"                                                <th nowrap>Authorise By</th>														" + Environment.NewLine +
"                                                <th nowrap>Authorise Date</th>														" + Environment.NewLine +
"																																	" + Environment.NewLine +
"                                            </tr>																					" + Environment.NewLine +
"                                        </thead>																					" + Environment.NewLine +
"                                        <tbody>																					" + Environment.NewLine +
"                                            <tr>																					" + Environment.NewLine +
"                                                <td>1</td>																			" + Environment.NewLine +
"                                                <td nowrap>@Model.RecordStatus</td>												" + Environment.NewLine +
"                                                <td nowrap>@Model.CreatedBy</td>													" + Environment.NewLine +
"                                                <td nowrap>@Model.CreatedDate</td>													" + Environment.NewLine +
"                                                <td nowrap>@Model.ModifiedBy</td>													" + Environment.NewLine +
"                                                <td nowrap>@Model.ModifiedDate</td>												" + Environment.NewLine +
"                                                <td nowrap>@Model.AuthorisedBy</td>												" + Environment.NewLine +
"                                                <td nowrap>@Model.AuthorisedDate</td>												" + Environment.NewLine +
"                                            </tr>																					" + Environment.NewLine +
"                                        </tbody>																					" + Environment.NewLine +
"                                    </table>																						" + Environment.NewLine +
"                                </div>																								" + Environment.NewLine +
"                            </div>																									" + Environment.NewLine +
"																																	" + Environment.NewLine +
"                        </div>																										" + Environment.NewLine +
"                    </div>																											" + Environment.NewLine +

"                    @if (Model.RecordChangeLog != null)																				" + Environment.NewLine +
"                    {																													" + Environment.NewLine +
"																																		" + Environment.NewLine +
"                        <div class=\"row\">																							" + Environment.NewLine +
"                            <div class=\"col-md-12\">																					" + Environment.NewLine +
"                                <div class=\"box box-primary\">																		" + Environment.NewLine +
"                                    <h4 class=\"box-title\"><i class=\"glyphicon glyphicon-warning-sign\"></i> Change Logs</h4>		" + Environment.NewLine +
"                                    <hr class=\"message-inner-separator\">																" + Environment.NewLine +
"                                    <div class=\"box-body table-responsive no-padding\">												" + Environment.NewLine +
"                                        <table class=\"table table-bordered\">															" + Environment.NewLine +
"                                            <thead>																					" + Environment.NewLine +
"                                                <tr>																					" + Environment.NewLine +
"                                                    <th>#</th>																			" + Environment.NewLine +
"                                                    <th>Field Name</th>																" + Environment.NewLine +
"                                                    <th>Log Status</th>																" + Environment.NewLine +
"                                                    <th>Modified Date</th>																" + Environment.NewLine +
"                                                    <th>Original Value</th>															" + Environment.NewLine +
"                                                    <th>Modified Value</th>															" + Environment.NewLine +
"                                                </tr>																					" + Environment.NewLine +
"                                            </thead>																					" + Environment.NewLine +
"                                            <tbody>																					" + Environment.NewLine +
"                                                @{																						" + Environment.NewLine +
"                                                    int i = 1;																			" + Environment.NewLine +
"                                                    string _class = string.Empty;														" + Environment.NewLine +
"                                                    foreach (var item in Model.RecordChangeLog.PropertyChangeLogs)						" + Environment.NewLine +
"                                                    {																					" + Environment.NewLine +
"																																		" + Environment.NewLine +
"                                                        <tr class=\"primary\">															" + Environment.NewLine +
"                                                            <td scope=\"row\"> @i </td>												" + Environment.NewLine +
"                                                            <td> @item.PropertyName </td>												" + Environment.NewLine +
"                                                            <td> @Model.RecordChangeLog.ChangeStatus.ToString()</td>					" + Environment.NewLine +
"                                                            <td> @Model.RecordChangeLog.ChangeDate </td>								" + Environment.NewLine +
"                                                            <td> @item.OldValue </td>													" + Environment.NewLine +
"                                                            <td> @item.NewValue </td>													" + Environment.NewLine +
"																																		" + Environment.NewLine +
"                                                        </tr>																			" + Environment.NewLine +
"                                                        i++;																			" + Environment.NewLine +
"                                                    }																					" + Environment.NewLine +
"                                                }																						" + Environment.NewLine +
"                                            </tbody>																					" + Environment.NewLine +
"                                        </table>																						" + Environment.NewLine +
"                                    </div>																								" + Environment.NewLine +
"                                </div>																									" + Environment.NewLine +
"																																		" + Environment.NewLine +
"                            </div>																										" + Environment.NewLine +
"                        </div>																											" + Environment.NewLine +
"                    }																													" + Environment.NewLine +


"					<div class=\"row\">																																						" + Environment.NewLine +
"                        <p style=\"float:right\">																																			" + Environment.NewLine +
"                            @if (Model.RecordStatus == " + projectName + ".Entity.RecordStatus.Active || Model.ModifiedById == ViewUserInformation.GetCurrentUserId())									" + Environment.NewLine +
"                            {																																								" + Environment.NewLine +
"                                if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Edit))						" + Environment.NewLine +
"                                {																																							" + Environment.NewLine +
"                                    <button class=\"btn icon-btn btn-success update saveRecords\" type=\"submit\" data-currentaction=\"@" + projectName + ".Entity.CurrentAction.Edit\">				" + Environment.NewLine +
"                                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-disk img-circle text-success\">											" + Environment.NewLine +
"                                        </span>Update Record																																" + Environment.NewLine +
"                                    </button>																																				" + Environment.NewLine +
"                                }																																							" + Environment.NewLine +
"                                if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Delete))						" + Environment.NewLine +
"                                {																																							" + Environment.NewLine +
"                                    <button class=\"btn icon-btn btn-danger update saveRecords\" type=\"submit\" data-currentaction=\"@" + projectName + ".Entity.CurrentAction.Delete\">				" + Environment.NewLine +
"                                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-trash img-circle text-success\">												" + Environment.NewLine +
"                                        </span>Delete Record																																" + Environment.NewLine +
"                                    </button>																																				" + Environment.NewLine +
"                                }																																							" + Environment.NewLine +
"                            }																																								" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                            else if (Model.RecordStatus == " + projectName + ".Entity.RecordStatus.UnVerified)																						" + Environment.NewLine +
"                            {																																								" + Environment.NewLine +
"                                if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Discard))					" + Environment.NewLine +
"                                {																																							" + Environment.NewLine +
"                                    <button class=\"btn icon-btn btn-warning revert saveRecords\" type=\"submit\" data-currentaction=\"@" + projectName + ".Entity.CurrentAction.Discard\">				" + Environment.NewLine +
"                                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-remove img-circle text-success\">										" + Environment.NewLine +
"                                        </span>Discard Changes																																" + Environment.NewLine +
"                                    </button>																																				" + Environment.NewLine +
"                                }																																							" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                                if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Authorise))					" + Environment.NewLine +
"                                {																																							" + Environment.NewLine +
"                                    <button class=\"btn icon-btn btn-warning revert saveRecords\" type=\"submit\" data-currentaction=\"@" + projectName + ".Entity.CurrentAction.Revert\">				" + Environment.NewLine +
"                                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-remove img-circle text-success\">										" + Environment.NewLine +
"                                        </span>Revert Changes																																" + Environment.NewLine +
"                                    </button>																																				" + Environment.NewLine +
"                                }																																							" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                                if (" + projectName + ".Web.Utility.AuthorizeViewHelper.IsAuthorize(Model.SchemaName, Model.ModuleSummaryName, " + projectName + ".Entity.CurrentAction.Authorise))					" + Environment.NewLine +
"                                {																																							" + Environment.NewLine +
"                                    <button class=\"btn icon-btn btn-success authorise saveRecords\" type=\"submit\" data-currentaction=\"@" + projectName + ".Entity.CurrentAction.Authorise\">		" + Environment.NewLine +
"                                        <span class=\"glyphicon btn-glyphicon glyphicon glyphicon glyphicon-floppy-saved img-circle text-success\">										" + Environment.NewLine +
"                                        </span>Authorise Changes																															" + Environment.NewLine +
"                                    </button>																																				" + Environment.NewLine +
"                                }																																							" + Environment.NewLine +
"																																															" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                            }																																								" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                            @if (Model.IsParent)																																			" + Environment.NewLine +
"                            {																																								" + Environment.NewLine +
string.Format("                                <a href=\"/{0}/{1}\" class=\"btn btn-primary btn-flat\">", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
"                                    <i class=\"fa  fa-hand-o-left\"></i> Back to Summary																									" + Environment.NewLine +
"                                </a>																																						" + Environment.NewLine +
"                            }																																								" + Environment.NewLine +
"                           else if(!Model.IsParent && Model.EntryType==\"S\") " + Environment.NewLine +
"                           {" + Environment.NewLine +
string.Format("                                <a href=\"/{0}/{1}\" class=\"btn btn-primary btn-flat\">", moduleSetup.ModuleName, moduleSetup.ParentModule) + Environment.NewLine +
"                                   <i class=\"fa fa-hand-o-left\" ></i> Back to Summary" + Environment.NewLine +
"                               </a>" + Environment.NewLine +
"                           }" + Environment.NewLine +
"                            else																																							" + Environment.NewLine +
"                            {																																								" + Environment.NewLine +
string.Format("                                <a href=\"/{0}/{1}\" class=\"btn btn-primary btn-flat\" id=\"backtoSummary\">", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
"                                    <i class=\"fa  fa-hand-o-left\"></i> Back to Summary																									" + Environment.NewLine +
"                                </a>																																						" + Environment.NewLine +
"                            }																																								" + Environment.NewLine +
"																																															" + Environment.NewLine +
"                        </p>																																								" + Environment.NewLine +
"                    </div>																																									" + Environment.NewLine +

      "            </div>" + Environment.NewLine +
"        </div>" + Environment.NewLine +
"      }" + Environment.NewLine;

            return _detailsTemplate;
        }
    }
}
