﻿using GSAssetManagement.CodeGenerator.Model;
using GSAssetManagement.CodeGenerator.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.CodeGenerator
{
    public static class EntityGenerator
    {
        public static string Generate(ModuleSetup ModuleSetup)
        {
            string _template = "using System;" + Environment.NewLine +
                                 "using System.Collections.Generic;" + Environment.NewLine +
                                 "using System.ComponentModel.DataAnnotations;" + Environment.NewLine +
                                 "using System.ComponentModel.DataAnnotations.Schema;" + Environment.NewLine +
                                 "using System.Linq;" + Environment.NewLine +
                                 "using System.Text;" + Environment.NewLine +
                                 "using System.Threading.Tasks;" + Environment.NewLine +
                                string.Format("using {0}.Entity; ", ModuleSetup.ProjectName) + Environment.NewLine;

            _template += string.Format("namespace {0}.Entity", ModuleSetup.ProjectName) + Environment.NewLine;
            _template += "{" + Environment.NewLine;
            _template += "[Table(\"" + ModuleSetup.DatabaseTable + "\", Schema = \"" + ModuleSetup.Module + "\")]" + Environment.NewLine;
            _template += "public class " + ModuleSetup.DatabaseTable + "" + " " + ":BaseEntity<Guid>" + Environment.NewLine;
            _template += "{" + Environment.NewLine;

            _template += string.Format("public {0}()", ModuleSetup.DatabaseTable) + Environment.NewLine;
            _template += "{" + Environment.NewLine;

            if (ModuleSetup.ChildLists.Count > 0)
            {
                ModuleSetup.ChildLists.ForEach(c =>
                {
                    _template += string.Format("{0}s = new HashSet<{0}>();", c.Item1) + Environment.NewLine;

                });
            }


            _template += "}" + Environment.NewLine;


            ModuleSetup.ModuleRuleSetupList.ForEach(x =>
            {
                if (x.IsRequired)
                {
                    _template += "[Required(ErrorMessage = \"" + x.ErrorMessage + "\")]" + Environment.NewLine;

                }
                if (x.DataType == "string")
                {
                    if (!string.IsNullOrEmpty(x.Length) && x.Length.ToUpper() != "MAX")
                        _template += "[StringLength(" + x.Length + ")]" + Environment.NewLine;
                    else
                        _template += "[Column(TypeName = \"NVARCHAR(MAX)\")]" + Environment.NewLine;
                }

                //[Column(TypeName = "xml")]

                if (x.DataType.ToLower() == "xml")
                {
                    _template += "[Column(TypeName = \"xml\")]" + Environment.NewLine;
                }

                if (x.DataType.ToLower() == "byte")
                {
                    _template += "[Column(TypeName = \"VARBINARY(MAX)\")]" + Environment.NewLine;
                }



                if (!x.IsRequired)
                {
                    if (x.DataType.ToString().ToLower() != "string" && x.DataType.ToString().ToLower() != "xml" && x.DataType.ToString().ToLower() != "byte")
                    {
                        _template += "public Nullable<" + x.DataType.ToString() + ">" + " " + x.ColumnName + " " + "{get;set;}" + Environment.NewLine;
                    }
                    else if (x.DataType.ToString().ToLower() != "xml" && x.DataType.ToString().ToLower() == "string")
                    {
                        _template += "public  " + x.DataType.ToString() + " " + x.ColumnName + " " + "{get;set;}" + Environment.NewLine;
                    }
                    else
                    {
                        if (x.DataType.ToString().ToLower() == "xml")
                        {
                            _template += "public  string " + x.ColumnName + " " + "{get;set;}" + Environment.NewLine;
                        }

                        if (x.DataType.ToString().ToLower() == "byte")
                        {
                            _template += "public  Nullable<byte>[] " + x.ColumnName + " " + "{get;set;}" + Environment.NewLine;
                        }

                    }
                }

                else
                {
                    if (x.DataType.ToString().ToLower() != "xml" && x.DataType.ToString().ToLower() != "byte")
                    {
                        _template += "public " + x.DataType.ToString() + " " + x.ColumnName + " " + "{get;set;}" + Environment.NewLine;
                    }
                    else
                    {
                        if (x.DataType.ToString().ToLower() == "xml")
                        {
                            _template += "public  string " + x.ColumnName + " " + "{get;set;}" + Environment.NewLine;
                        }

                        if (x.DataType.ToString().ToLower() == "byte")
                        {
                            _template += "public  byte[] " + x.ColumnName + " " + "{get;set;}" + Environment.NewLine;
                        }
                    }


                }


            });


            if (ModuleSetup.ChildLists.Count > 0)
            {

                ModuleSetup.ChildLists.ForEach(c =>
                {

                    _template += "public virtual ICollection<" + c.Item1 + "> " + string.Format("{0}s", c.Item1) + " " + "{get;set;}" + Environment.NewLine;


                });
            }

            ModuleSetup.ModuleRuleSetupList.Where(x => x.IsForeignKey).ToList().ForEach(c =>
            {

                _template += "[ForeignKey(\"" + string.Format("{0}", c.ColumnName) + "\")]" + Environment.NewLine; ;
                _template += string.Format("public virtual {0} {1} {{get;set;}}", c.ForeignTable, c.ForeignTableAlais) + Environment.NewLine;


            });


            _template += "}" + Environment.NewLine;
            _template += "}" + Environment.NewLine;

            return _template;

        }


        public static string GenerateDTO(ModuleSetup ModuleSetup)
        {
            string _template = "using System;" + Environment.NewLine +
                                "using System.Collections.Generic;" + Environment.NewLine +
                                "using System.ComponentModel.DataAnnotations;" + Environment.NewLine +
                                "using System.ComponentModel.DataAnnotations.Schema;" + Environment.NewLine +
                                "using System.Linq;" + Environment.NewLine +
                                "using System.Text;" + Environment.NewLine +
                                "using System.Threading.Tasks;" + Environment.NewLine;

            _template += string.Format("namespace {0}.Entity.DTO", ModuleSetup.ProjectName) + Environment.NewLine;
            _template += "{" + Environment.NewLine;
            _template += "public class " + ModuleSetup.DatabaseTable + "DTO" + " " + ":BaseEntityDTO<Guid>" + Environment.NewLine;
            _template += "{" + Environment.NewLine;

            _template += string.Format("public {0}DTO()", ModuleSetup.DatabaseTable) + Environment.NewLine;
            _template += "{" + Environment.NewLine;

            _template += "ArrayOfChangeLogDTO = new List<ChangeLogDTO>();" + Environment.NewLine;

            _template += "}" + Environment.NewLine;

            ModuleSetup.ModuleRuleSetupList.ForEach(x =>
            {
                if (x.IsRequired)
                {
                    _template += "[Required(ErrorMessage = \"" + x.ErrorMessage + "\")]" + Environment.NewLine;

                }
                if (x.DataType == "string")
                {
                    if (!string.IsNullOrEmpty(x.Length) && x.Length.ToUpper() != "MAX")
                        _template += "[StringLength(" + x.Length + ")]" + Environment.NewLine;
                    else
                        _template += "[Column(TypeName = \"NVARCHAR(MAX)\")]" + Environment.NewLine;
                }


                if (!x.IsRequired)
                {
                    if (x.DataType.ToString().ToLower() != "string" && x.DataType.ToString().ToLower() != "xml" && x.DataType.ToString().ToLower() != "byte")
                    {
                        _template += "public Nullable<" + x.DataType.ToString() + ">" + " " + x.ColumnName + " " + "{get;set;}" + Environment.NewLine;
                    }
                    else
                    {
                        if (x.DataType.ToString().ToLower() == "xml")
                        {
                            _template += "public  string " + x.ColumnName + " " + "{get;set;}" + Environment.NewLine;
                        }

                        else if (x.DataType.ToString().ToLower() == "byte")
                        {
                            _template += "public  Nullable<byte>[] " + x.ColumnName + " " + "{get;set;}" + Environment.NewLine;
                        }
                        else
                        {
                            _template += "public  " + x.DataType.ToString() + " " + x.ColumnName + " " + "{get;set;}" + Environment.NewLine;
                        }
                    }
                }

                else
                {



                    if (x.DataType.ToString().ToLower() != "xml" && x.DataType.ToString().ToLower() != "byte")
                    {
                        _template += "public " + x.DataType.ToString() + " " + x.ColumnName + " " + "{get;set;}" + Environment.NewLine;
                    }
                    else
                    {
                        if (x.DataType.ToString().ToLower() == "xml")
                        {
                            _template += "public  string " + x.ColumnName + " " + "{get;set;}" + Environment.NewLine;
                        }

                        if (x.DataType.ToString().ToLower() == "byte")
                        {
                            _template += "public  byte[] " + x.ColumnName + " " + "{get;set;}" + Environment.NewLine;
                        }
                    }
                }


            });

            _template += "public List<ChangeLogDTO> ArrayOfChangeLogDTO { get; set; }" + Environment.NewLine;

            _template += "}" + Environment.NewLine;
            _template += "}" + Environment.NewLine;

            return _template;
        }
    }
}
