﻿using GSAssetManagement.CodeGenerator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Information;
using System.Windows.Forms;
using System.Windows.Media.Animation;
using GSAssetManagement.CodeGenerator.Properties;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Entity;

namespace GSAssetManagement.CodeGenerator
{
    public static class StoreprocedureGeneratorNew
    {
        public static string GenerateList(ModuleSetupNew moduleSetup)
        {
            var _List_template = string.Format("CREATE PROCEDURE [{0}].[SP_Get{1}List]", moduleSetup.DatabaseSchemaName, moduleSetup.DatabaseTable) + Environment.NewLine;


            int j = 0;



            string _searchParamter = string.Empty;

            string searchTemplate = string.Empty;

            moduleSetup.ModuleBussinesLogicSetups.Where(x => x.ParameterForSummaryHeader).OrderBy(c => c.Position).ToList().ForEach(z =>
            {
                string dataType = string.Empty;
                _searchParamter += string.Format("@{0},", z.ColumnName) + Environment.NewLine;

                switch (z.DataType.ToLower())
                {
                    case "string":
                        dataType = "NVARCHAR(" + z.StringLength + ")";
                        break;
                    case "int":
                        dataType = "INT";
                        break;
                    case "datetime":
                        dataType = "DATE";
                        break;
                    case "guid":
                        dataType = "UNIQUEIDENTIFIER";
                        break;
                    case "decimal":
                        dataType = "DECIMAL(18,2)";
                        break;
                    case "bool":
                        dataType = "BIT";
                        break;

                }

                if (j == 0)
                {
                    searchTemplate += string.Format("@{0} {1} = NULL,", z.ColumnName, dataType) + Environment.NewLine;
                }
                else
                {
                    searchTemplate += string.Format("@{0} {1}  = NULL,", z.ColumnName, dataType) + Environment.NewLine;
                }

                j++;

            });

            _searchParamter += "@RecordStatus," + Environment.NewLine;
            _searchParamter += "@PageNumber ,  " + Environment.NewLine;
            _searchParamter += "@PageSize ,    " + Environment.NewLine;
            _searchParamter += "@TotalRows     " + Environment.NewLine;





            _List_template += searchTemplate;

            searchTemplate += "@RecordStatus INT = NULL," + Environment.NewLine +
                              "@PageNumber INT," + Environment.NewLine +
                              "@PageSize INT," + Environment.NewLine +
                              "@TotalRows INT" + Environment.NewLine;




            _List_template += "@RecordStatus INT = NULL," + Environment.NewLine +
                              "@PageNumber INT," + Environment.NewLine +
                              "@PageSize INT," + Environment.NewLine +
                              "@Debug INT = 0" + Environment.NewLine +

                            "AS" + Environment.NewLine +
                            "" + Environment.NewLine +

             "BEGIN TRY" + Environment.NewLine +
             "" + Environment.NewLine +
                " SET NOCOUNT ON;" + Environment.NewLine +
                "" + Environment.NewLine +
       " DECLARE @ColumnList NVARCHAR(MAX)," + Environment.NewLine +
       " @TSQL NVARCHAR(MAX)," + Environment.NewLine +
       " @ParameterList NVARCHAR(MAX)," + Environment.NewLine +
       " @Where NVARCHAR(MAX) = ''," + Environment.NewLine +
       " @TotalRows DECIMAL(18, 2)," + Environment.NewLine +
       " @NextLine CHAR(2) = CHAR(13) + CHAR(10)," + Environment.NewLine +
       " @TCount INT" + Environment.NewLine +

       "" + Environment.NewLine +
       "" + Environment.NewLine +


       " SET @PageNumber = ISNULL(@PageNumber, 1)" + Environment.NewLine +
       " SET @PageSize = ISNULL(@PageSize, 20)   " + Environment.NewLine +
       " SET @TotalRows = 20 * 1.0;              " + Environment.NewLine +
       "                                         " + Environment.NewLine +
       "                                         " + Environment.NewLine;

            string _where = string.Format("SET @Where =+ 'WHERE 1=1 '");


            moduleSetup.ModuleBussinesLogicSetups.Where(x => x.ParameterForSummaryHeader).OrderBy(c => c.Position).ToList().ForEach(z =>
            {
                if (z.DataType.ToLower() == "string")
                {
                    _where += string.Format("+CASE WHEN @{0} IS NOT NULL THEN 'AND {1}.{0} LIKE @{0} ' ELSE '' END", z.ColumnName, moduleSetup.DatabaseTable) + Environment.NewLine;
                }
                else
                {
                    _where += string.Format("+CASE WHEN @{0} IS NOT NULL THEN 'AND {1}.{0} = @{0} ' ELSE '' END", z.ColumnName, moduleSetup.DatabaseTable) + Environment.NewLine;
                }




            });

            _where += string.Format("+ CASE WHEN @RecordStatus IS NOT NULL THEN 'AND {0}.Record_Status = @RecordStatus ' ELSE ' AND {0}.Record_Status <> 1' END", moduleSetup.DatabaseTable) + Environment.NewLine;

            _List_template += _where + Environment.NewLine +
       "" + Environment.NewLine +
       string.Format("SELECT @TSQL = N';WITH {0}_CTE", moduleSetup.DatabaseTable) + Environment.NewLine +
                    "AS" + Environment.NewLine +
                    "(" + Environment.NewLine +
                    "   SELECT  [Id]" + Environment.NewLine;

            moduleSetup.ModuleBussinesLogicSetups.OrderBy(c => c.Position).ToList().ForEach(moduleBussinesLogicSetup =>
{
    _List_template += ",[" + moduleBussinesLogicSetup.ColumnName + "]" + Environment.NewLine;
});

            _List_template += ",[TotalModification]" + Environment.NewLine +
            ",[CreatedBy]" + Environment.NewLine +
      ",[CreatedById]" + Environment.NewLine +
      ",[CreatedByStaffNo]" + Environment.NewLine +
      ",[CreatedDate]" + Environment.NewLine +
      ",[ModifiedBy]" + Environment.NewLine +
      ",[ModifiedById]" + Environment.NewLine +
      ",[ModifiedByStaffNo]" + Environment.NewLine +
      ",[ModifiedDate]" + Environment.NewLine +
      ",[AuthorisedBy]" + Environment.NewLine +
      ",[AuthorisedById]" + Environment.NewLine +
      ",[AuthorisedByStaffNo]" + Environment.NewLine +
      ",[AuthorisedDate]" + Environment.NewLine +
      ",[Record_Status]" + Environment.NewLine +
",[EntityState]" + Environment.NewLine +
",[DataEntry]" + Environment.NewLine +
      string.Format("FROM [{0}].[{1}](NOLOCK) AS {1} '+@Where+')    , ", moduleSetup.DatabaseSchemaName, moduleSetup.DatabaseTable) + Environment.NewLine +
                         "Count_CTE" + Environment.NewLine +
                        "AS" + Environment.NewLine +
"(" + Environment.NewLine +
                           string.Format("SELECT COUNT(*) AS TotalRows FROM {0}_CTE", moduleSetup.DatabaseTable) + Environment.NewLine +
                        ")" + Environment.NewLine +
                        "" + Environment.NewLine +
                        "" + Environment.NewLine;



            _List_template += "SELECT CONVERT(INT, (TotalRows / @TotalRows)) [PageCount]," + Environment.NewLine +
                "CONVERT(INT, TotalRows)[TotalRecords]," + Environment.NewLine +
                string.Format(" {0}.*", moduleSetup.DatabaseTable) + Environment.NewLine +
                string.Format("FROM {0}_CTE AS {0}", moduleSetup.DatabaseTable) + Environment.NewLine +
                "CROSS JOIN Count_CTE" + Environment.NewLine +
                "ORDER BY ModifiedDate DESC" + Environment.NewLine +
                "OFFSET @PageSize * (@PageNumber - 1) ROWS" + Environment.NewLine +
                "FETCH NEXT @PageSize ROWS ONLY';" + Environment.NewLine +
                "" + Environment.NewLine +
                        "" + Environment.NewLine +
                        "" + Environment.NewLine +
            " IF @Debug = 1                                                                           " + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
               " BEGIN                                                                                   " + Environment.NewLine +
               "   PRINT @TSQL                                                                           " + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
               "  END                                                                                    " + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
                string.Format(" SET @ParameterList = N'{0}'", searchTemplate) + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
              string.Format(" EXECUTE sp_executesql @TSQL, @ParameterList,{0}", _searchParamter) + Environment.NewLine +
                "                                                                                                 " + Environment.NewLine +
                "                                                                                                 " + Environment.NewLine +
                "                                                                                                 " + Environment.NewLine +
                "END TRY                                                                                         " + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "BEGIN CATCH                                                                                     " + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "INSERT INTO[dbo].[ExceptionLoggers]																																" + Environment.NewLine +
"                ([Id]																																				" + Environment.NewLine +
"                 ,[ExceptionMessage]																																" + Environment.NewLine +
"                 ,[ControllerName]																																	" + Environment.NewLine +
"                 ,[ExceptionStackTrace]																															" + Environment.NewLine +
"                 ,[TotalModification]																																" + Environment.NewLine +
"                 ,[CreatedBy]																																		" + Environment.NewLine +
"                 ,[ModifiedBy]																																		" + Environment.NewLine +
"                 ,[AuthorisedBy]																																	" + Environment.NewLine +
"				 ,[CreatedByStaffNo]																																" + Environment.NewLine +
"                 ,[ModifiedByStaffNo]																																" + Environment.NewLine +
"                 ,[AuthorisedByStaffNo]																															" + Environment.NewLine +
"                 ,[CreatedById]																																	" + Environment.NewLine +
"                 ,[ModifiedById]																																	" + Environment.NewLine +
"                 ,[AuthorisedById]																																	" + Environment.NewLine +
"                 ,[CreatedDate]																																	" + Environment.NewLine +
"                 ,[ModifiedDate]																																	" + Environment.NewLine +
"                 ,[AuthorisedDate]																																	" + Environment.NewLine +
"                 ,[EntityState]																																	" + Environment.NewLine +
"                 ,[Record_Status]																																	" + Environment.NewLine +
"                 ,[DataEntry]																																		" + Environment.NewLine +
"                 ,[ChangeLog]																																		" + Environment.NewLine +
"                 )																																					" + Environment.NewLine +
"             VALUES																																				" + Environment.NewLine +
"                   (NEWID(),																																		" + Environment.NewLine +
"                    ERROR_MESSAGE(),																																" + Environment.NewLine +
                string.Format("'{0}',", moduleSetup.DatabaseTable) + Environment.NewLine +
"                    CONVERT(NVARCHAR(MAX), ERROR_LINE()) + ', ' + CONVERT(NVARCHAR(MAX), ERROR_NUMBER()) + ', ' + CONVERT(NVARCHAR(MAX), ERROR_STATE()),			" + Environment.NewLine +
"                    1,																																				" + Environment.NewLine +
"                    'administrator',																																" + Environment.NewLine +
"                    'administrator',																																" + Environment.NewLine +
"                    'administrator',																																" + Environment.NewLine +
"					'000',																																			" + Environment.NewLine +
"					'000',																																			" + Environment.NewLine +
"					'000',																																			" + Environment.NewLine +
"                    'DE69AA3E-CC18-430F-9818-6B7A45691ECF',																										" + Environment.NewLine +
"                    'DE69AA3E-CC18-430F-9818-6B7A45691ECF',																										" + Environment.NewLine +
"                    'DE69AA3E-CC18-430F-9818-6B7A45691ECF',																										" + Environment.NewLine +
"                     GETDATE(),																																	" + Environment.NewLine +
"                     GETDATE(),																																	" + Environment.NewLine +
"                     GETDATE(),																																	" + Environment.NewLine +
"                     1,																																			" + Environment.NewLine +
"                     2,																																			" + Environment.NewLine +
"                     2,																																			" + Environment.NewLine +
"                     NULL																																			" + Environment.NewLine +
"                    )																																				" + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "END CATCH    ";

            return _List_template;
        }
    }
}
