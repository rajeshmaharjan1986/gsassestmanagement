﻿using GSAssetManagement.CodeGenerator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.CodeGenerator
{
    public static class StoreprocedureGenerator
    {
        public static string GenerateDetails(ModuleSetup moduleSetup)
        {

            var _template = string.Format("CREATE PROCEDURE [{1}].[IRM_SP_Get{0}Details]", moduleSetup.DatabaseTable, moduleSetup.Module) + Environment.NewLine +
                          "@Id UNIQUEIDENTIFIER," + Environment.NewLine +
                          "@Schema VARCHAR(20),         " + Environment.NewLine +
                          "@ModuleTable VARCHAR(200),   " + Environment.NewLine +
                          "@ChangeLogRequired BIT,      " + Environment.NewLine +
                          "@Storeprocedure BIT          " + Environment.NewLine +
          "                                     " + Environment.NewLine +
          "        AS                           " + Environment.NewLine +
          "                                     " + Environment.NewLine +
          "                                     " + Environment.NewLine +
          "                                     " + Environment.NewLine +
          "                                     " + Environment.NewLine +
          "    IF OBJECT_ID('tempdb..##DropdownMaster') IS NOT NULL DROP TABLE ##DropdownMaster" + Environment.NewLine +
          "    IF OBJECT_ID('tempdb..##RecordsDetails') IS NOT NULL DROP TABLE ##RecordsDetails" + Environment.NewLine +
          "                                                                                    " + Environment.NewLine +
          "    DECLARE @TSQL NVARCHAR(MAX),                                                    " + Environment.NewLine +
          "    @NextLine CHAR(2) = CHAR(13)+CHAR(10),                                          " + Environment.NewLine +
          "    @InsertQuery NVARCHAR(MAX)                                                      " + Environment.NewLine +
          "                                                                                    " + Environment.NewLine +
          "    CREATE TABLE ##DropdownMaster (                                                 " + Environment.NewLine +
                 "	ColumnName VARCHAR(200),                                                         " + Environment.NewLine +
                 "	DisplayText VARCHAR(200),                                                        " + Environment.NewLine +
                 "	ValueText VARCHAR(200),                                                           " + Environment.NewLine +
                 "  OrderValue VARCHAR(50) " + Environment.NewLine +
      "    )                                                                                   " + Environment.NewLine +
      "                                                                                        " + Environment.NewLine +
      "                                                                                        " + Environment.NewLine +
      "	IF @Storeprocedure = 1                                                               " + Environment.NewLine +
      "                                                                                        " + Environment.NewLine +
      "                                                                                        " + Environment.NewLine +
      "        BEGIN                                                                           " + Environment.NewLine +
      "                                                                                        " + Environment.NewLine +
      "                                                                                        " + Environment.NewLine +
      "                                                                                        " + Environment.NewLine +
      "                SELECT @TSQL = COALESCE(@TSQL+' UNION ALL '++@NextLine,'')+ F.Datasource" + Environment.NewLine +
      "                FROM Setting.ModuleTableFieldSetup F                                    " + Environment.NewLine +
      "                                                                                        " + Environment.NewLine +
      "                INNER JOIN Setting.ModuleTableSetup M ON M.Id = F.ModuleTableSetupId    " + Environment.NewLine +
      "                WHERE M.DatabaseTable = @ModuleTable AND Datasource IS NOT NULL AND ParameterisedSource = 0 AND SearchField = 1 AND[Enable] = 1" + Environment.NewLine +
      "" + Environment.NewLine +
      "                ORDER BY F.[Order]         " + Environment.NewLine +
      "                                           " + Environment.NewLine +
      "                SET @InsertQuery = ''      " + Environment.NewLine +
      "                                           " + Environment.NewLine +
      "                                           " + Environment.NewLine +
      "                                           " + Environment.NewLine +
      "                                           " + Environment.NewLine +
      "                                           " + Environment.NewLine +
      "                SELECT @InsertQuery += 'INSERT INTO ##DropdownMaster SELECT * FROM ('+@TSQL+') Z'" + Environment.NewLine +
      "                                                                                                 " + Environment.NewLine +
      "                                                                                                 " + Environment.NewLine +
      "                EXEC sp_executesql @InsertQuery;                                                 " + Environment.NewLine +
      "                                                                                                 " + Environment.NewLine +
      "                                                                                                 " + Environment.NewLine +
      "                                                                                                 " + Environment.NewLine +
      "                                                                                                 " + Environment.NewLine +
      "                                                                                                 " + Environment.NewLine +
      "                                                                                                 " + Environment.NewLine +
      "        SELECT" +
     "                                       [ColumnName]               " + Environment.NewLine +
      "                                      ,[FieldTitle]               " + Environment.NewLine +
      "                                      ,[DataType]                 " + Environment.NewLine +
      "                                      ,[HtmlDataType]             " + Environment.NewLine +
      "                                      ,[IsRequired]               " + Environment.NewLine +
      "                                      ,[Regex]                    " + Environment.NewLine +
      "                                      ,[Length]                   " + Environment.NewLine +
      "                                      ,[ErrorMessage]             " + Environment.NewLine +
      "                                      ,[HelpMessage]              " + Environment.NewLine +
      "                                      ,[RangeForm]                " + Environment.NewLine +
      "                                      ,[RangeTo]                  " + Environment.NewLine +
      "                                      ,[IsUpdateable]             " + Environment.NewLine +
      "                                      ,[Order]                    " + Environment.NewLine +
      "                                      ,[SummaryHeader]            " + Environment.NewLine +
      "                                      ,[Enable]                   " + Environment.NewLine +
      "                                      ,[SearchField]              " + Environment.NewLine +
      "                                      ,[Hidden]                   " + Environment.NewLine +
      "                                      ,[HtmlClass]                " + Environment.NewLine +
      "                                      ,[ReadOnly]                 " + Environment.NewLine +
      "                                      ,[EnableRemoteValidation]   " + Environment.NewLine +
      "                                      ,[RemoteValidationService]  " + Environment.NewLine +
      "                                      ,[RemoteParameters]         " + Environment.NewLine +
      "                                      ,[ParameterisedSource]      " + Environment.NewLine +
      "                                      ,[Parameter]                " + Environment.NewLine +
      "					  ,(SELECT ColumnName,                                                        " + Environment.NewLine +
      "                               DisplayText,                                                      " + Environment.NewLine +
      "                               ValueText,                                                         " + Environment.NewLine +
      "                               OrderValue                                                        " + Environment.NewLine +
      "                                                                                                 " + Environment.NewLine +
      "                        FROM ##DropdownMaster DropdownMaster                                     " + Environment.NewLine +
      "						WHERE DropdownMaster.ColumnName = FieldRule.ColumnName                    " + Environment.NewLine +
      "                         FOR XML AUTO, TYPE, ELEMENTS, ROOT('DropdownMasterList'))               " + Environment.NewLine +
      "                                                                                                 " + Environment.NewLine +
      "                FROM Setting.ModuleTableFieldSetup  FieldRule                                    " + Environment.NewLine +
      "                INNER JOIN Setting.ModuleTableSetup M ON M.Id = FieldRule.ModuleTableSetupId     " + Environment.NewLine +
      "                                                                                                 " + Environment.NewLine +
      "                WHERE (SearchField = 1 OR SummaryHeader = 1)  AND[Enable] = 1 AND M.DatabaseTable = @ModuleTable" + Environment.NewLine +
      "               ORDER BY[Order]                                                                                  " + Environment.NewLine +
      "               FOR XML AUTO, TYPE, ELEMENTS, ROOT('FieldRuleList')                                              " + Environment.NewLine +
      "                                                                                                                " + Environment.NewLine +
      "                                                                                                                " + Environment.NewLine +
      "                                                                                                                " + Environment.NewLine +
      "                                                                                                                " + Environment.NewLine +
      "         END                                                                                                    " + Environment.NewLine +
      "                                                                                                                " + Environment.NewLine +
      "   ELSE                                                                                                         " + Environment.NewLine +
      "                                                                                                                " + Environment.NewLine +
      "                                                                                                                " + Environment.NewLine +
      "        BEGIN                                                                                                   " + Environment.NewLine +
      "                                                                                                                " + Environment.NewLine +
      "            IF @Id IS NULL AND @Storeprocedure = 0                                                              " + Environment.NewLine +
      "                                                                                                                " + Environment.NewLine +
      "			BEGIN                                                                                                " + Environment.NewLine +
      "                                                                                                                " + Environment.NewLine +
      "                SELECT @TSQL = COALESCE(@TSQL+' UNION ALL '++@NextLine,'')+ F.Datasource                        " + Environment.NewLine +
      "                FROM Setting.ModuleTableFieldSetup F                                                            " + Environment.NewLine +
      "                                                                                                                " + Environment.NewLine +
      "                INNER JOIN Setting.ModuleTableSetup M ON M.Id = F.ModuleTableSetupId                            " + Environment.NewLine +
      "                WHERE M.DatabaseTable = @ModuleTable AND Datasource IS NOT NULL AND ParameterisedSource = 0 AND[Enable] = 1" + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                ORDER BY F.[Order]                                                                                         " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                SET @InsertQuery = ''                                                                                      " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                SELECT @InsertQuery += 'INSERT INTO  ##DropdownMaster SELECT * FROM ('+@TSQL+') Z'                         " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                EXEC sp_executesql @InsertQuery;                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "        SELECT  " + Environment.NewLine +

      "                                       [ColumnName]               " + Environment.NewLine +
      "                                      ,[FieldTitle]               " + Environment.NewLine +
      "                                      ,[DataType]                 " + Environment.NewLine +
      "                                      ,[HtmlDataType]             " + Environment.NewLine +
      "                                      ,[IsRequired]               " + Environment.NewLine +
      "                                      ,[Regex]                    " + Environment.NewLine +
      "                                      ,[Length]                   " + Environment.NewLine +
      "                                      ,[ErrorMessage]             " + Environment.NewLine +
      "                                      ,[HelpMessage]              " + Environment.NewLine +
      "                                      ,[RangeForm]                " + Environment.NewLine +
      "                                      ,[RangeTo]                  " + Environment.NewLine +
      "                                      ,[IsUpdateable]             " + Environment.NewLine +
      "                                      ,[Order]                    " + Environment.NewLine +
      "                                      ,[SummaryHeader]            " + Environment.NewLine +
      "                                      ,[Enable]                   " + Environment.NewLine +
      "                                      ,[SearchField]              " + Environment.NewLine +
      "                                      ,[Hidden]                   " + Environment.NewLine +
      "                                      ,[HtmlClass]                " + Environment.NewLine +
      "                                      ,[ReadOnly]                 " + Environment.NewLine +
      "                                      ,[EnableRemoteValidation]   " + Environment.NewLine +
      "                                      ,[RemoteValidationService]  " + Environment.NewLine +
      "                                      ,[RemoteParameters]         " + Environment.NewLine +
      "                                      ,[ParameterisedSource]      " + Environment.NewLine +
      "                                      ,[Parameter]                " + Environment.NewLine +
      "					  ,(SELECT ColumnName,                                                                                  " + Environment.NewLine +
      "                               DisplayText,                                                                                " + Environment.NewLine +
      "                               ValueText,                                                                                   " + Environment.NewLine +
      "                               OrderValue                                                                                   " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                        FROM ##DropdownMaster DropdownMaster                                                               " + Environment.NewLine +
      "						WHERE DropdownMaster.ColumnName = FieldRule.ColumnName                                              " + Environment.NewLine +
      "                         FOR XML AUTO, TYPE, ELEMENTS, ROOT('DropdownMasterList'))                                         " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                FROM Setting.ModuleTableFieldSetup  FieldRule                                                              " + Environment.NewLine +
      "                INNER JOIN Setting.ModuleTableSetup M ON M.Id = FieldRule.ModuleTableSetupId                               " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                WHERE[Enable] = 1 AND M.DatabaseTable = @ModuleTable                                                       " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                ORDER BY [Order]                                                                                           " + Environment.NewLine +
      "                FOR XML AUTO, TYPE, ELEMENTS, ROOT('FieldRuleList')                                                        " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "            END                                                                                                            " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "            IF @Id IS NOT NULL AND @Storeprocedure = 0                                                                     " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "            BEGIN                                                                                                          " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                SELECT @TSQL = COALESCE(@TSQL+' UNION ALL '++@NextLine,'')+ F.Datasource                                   " + Environment.NewLine +
      "                FROM Setting.ModuleTableFieldSetup F                                                                       " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                INNER JOIN Setting.ModuleTableSetup M ON M.Id = F.ModuleTableSetupId                                       " + Environment.NewLine +
      "                WHERE M.DatabaseTable = @ModuleTable AND Datasource IS NOT NULL AND ParameterisedSource = 0 AND[Enable] = 1" + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                ORDER BY F.[Order]                                                                                         " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                SET @InsertQuery = ''                                                                                      " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                SELECT @InsertQuery += 'INSERT INTO  ##DropdownMaster SELECT * FROM ('+@TSQL+') Z'                         " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                EXEC sp_executesql @InsertQuery;                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      string.Format("SELECT* INTO #Temp{1}  FROM [{0}].[{1}] WHERE Id = @Id", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                                                                                                                           " + Environment.NewLine +
      "                SELECT Record.*,                                                                                           " + Environment.NewLine +
      "                       CASE WHEN Record.RecordStatus = 3 and Record.DeletedStatus IS NULL AND @ChangeLogRequired = 1 THEN  " + Environment.NewLine +
      "					   (SELECT[Log] FROM ChangeLog.audit                                                                    " + Environment.NewLine +
      "                        WHERE EntityPK = @Id AND DateCreated = (SELECT MAX(DateCreated) FROM ChangeLog.audit WHERE EntityPK = @Id))" + Environment.NewLine +
      "						END,                                                                                                        " + Environment.NewLine +
      "					   (SELECT                                                                                                      " + Environment.NewLine +
                                        " [ColumnName]               " + Environment.NewLine +
                                        ",[FieldTitle]               " + Environment.NewLine +
                                        ",[DataType]                 " + Environment.NewLine +
                                        ",[HtmlDataType]             " + Environment.NewLine +
                                        ",[IsRequired]               " + Environment.NewLine +
                                        ",[Regex]                    " + Environment.NewLine +
                                        ",[Length]                   " + Environment.NewLine +
                                        ",[ErrorMessage]             " + Environment.NewLine +
                                        ",[HelpMessage]              " + Environment.NewLine +
                                        ",[RangeForm]                " + Environment.NewLine +
                                        ",[RangeTo]                  " + Environment.NewLine +
                                        ",[IsUpdateable]             " + Environment.NewLine +
                                        ",[Order]                    " + Environment.NewLine +
                                        ",[SummaryHeader]            " + Environment.NewLine +
                                        ",[Enable]                   " + Environment.NewLine +
                                        ",[SearchField]              " + Environment.NewLine +
                                        ",[Hidden]                   " + Environment.NewLine +
                                        ",[HtmlClass]                " + Environment.NewLine +
                                        ",[ReadOnly]                 " + Environment.NewLine +
                                        ",[EnableRemoteValidation]   " + Environment.NewLine +
                                        ",[RemoteValidationService]  " + Environment.NewLine +
                                        ",[RemoteParameters]         " + Environment.NewLine +
                                        ",[ParameterisedSource]      " + Environment.NewLine +
                                        ",[Parameter]                " + Environment.NewLine +
      "					  ,(SELECT ColumnName,                                                                                          " + Environment.NewLine +
      "                               DisplayText,                                                                                        " + Environment.NewLine +
      "                               ValueText,                                                                                           " + Environment.NewLine +
      "                               OrderValue                                                                                   " + Environment.NewLine +
      "                                                                                                                                   " + Environment.NewLine +
      "                        FROM ##DropdownMaster DropdownMaster                                                                       " + Environment.NewLine +
      "						WHERE DropdownMaster.ColumnName = FieldRule.ColumnName                                                      " + Environment.NewLine +
      "                         FOR XML AUTO, TYPE, ELEMENTS, ROOT('DropdownMasterList'))                                                 " + Environment.NewLine +
      "                                                                                                                                   " + Environment.NewLine +
      "                FROM Setting.ModuleTableFieldSetup  FieldRule                                                                      " + Environment.NewLine +
      "                INNER JOIN Setting.ModuleTableSetup M ON M.Id = FieldRule.ModuleTableSetupId                                       " + Environment.NewLine +
      "                                                                                                                                   " + Environment.NewLine +
      "                WHERE[Enable] = 1 AND M.DatabaseTable = @ModuleTable                                                               " + Environment.NewLine +
      "                                                                                                                                   " + Environment.NewLine +
      "                ORDER BY [Order]                                                                                                   " + Environment.NewLine +
      "                FOR XML AUTO, TYPE, ELEMENTS, ROOT('FieldRuleList'))                                                               " + Environment.NewLine +
       string.Format("FROM #Temp{0} Record", moduleSetup.DatabaseTable) + Environment.NewLine +
      "				FOR XML AUTO, TYPE, ELEMENTS                                                                                        " + Environment.NewLine +
      "                                                                                                                                   " + Environment.NewLine +
      "                                                                                                                                   " + Environment.NewLine +
      "            END                                                                                                                    " + Environment.NewLine +
      "        END                                                                                                                        " + Environment.NewLine +
      "                                                                                                                                   " + Environment.NewLine +
      "IF OBJECT_ID('tempdb..##DropdownMaster') IS NOT NULL DROP TABLE ##DropdownMaster                                                   " + Environment.NewLine +
      "IF OBJECT_ID('tempdb..#TempModuleTableFieldSetup') IS NOT NULL DROP TABLE ##RecordsDetails                                         " + Environment.NewLine;

            return _template;

        }

        public static string GenerateList(ModuleSetup moduleSetup)
        {
            var _List_template = string.Format("CREATE PROCEDURE [{0}].[IRM_SP_Get{1}List]", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine;


            int j = 0;



            string _searchParamter = string.Empty;

            string searchTemplate = string.Empty;

            moduleSetup.ModuleRuleSetupList.Where(x => x.SearchField).OrderBy(c => c.Order).ToList().ForEach(z =>
            {
                string dataType = string.Empty;
                _searchParamter += string.Format("@{0},", z.ColumnName) + Environment.NewLine;

                switch (z.DataType.ToLower())
                {
                    case "string":
                        dataType = "NVARCHAR(" + z.Length + ")";
                        break;
                    case "int":
                        dataType = "INT";
                        break;
                    case "datetime":
                        dataType = "DATE";
                        break;
                    case "guid":
                        dataType = "UNIQUEIDENTIFIER";
                        break;
                    case "decimal":
                        dataType = "DECIMAL(18,2)";
                        break;
                    case "bool":
                        dataType = "BIT";
                        break;

                }

                if (j == 0)
                {
                    searchTemplate += string.Format("@{0} {1}", z.ColumnName, dataType) + Environment.NewLine;
                }
                else
                {
                    searchTemplate += string.Format(",@{0} {1}", z.ColumnName, dataType) + Environment.NewLine;
                }

                j++;

            });

            _searchParamter += "@RecordStatus," + Environment.NewLine;
            _searchParamter += "@PageNumber ,  " + Environment.NewLine;
            _searchParamter += "@PageSize ,    " + Environment.NewLine;
            _searchParamter += "@TotalRows     " + Environment.NewLine;





            _List_template += searchTemplate;

            searchTemplate += ",@RecordStatus INT" + Environment.NewLine +
                              ",@PageNumber INT" + Environment.NewLine +
                              ",@PageSize INT" + Environment.NewLine +
                              ",@TotalRows DECIMAL(18,2)" + Environment.NewLine;




            _List_template += ",@RecordStatus INT" + Environment.NewLine +
                              ",@PageNumber INT" + Environment.NewLine +
                              ",@PageSize INT" + Environment.NewLine +
                              ",@Debug INT = 0" + Environment.NewLine +

                            "AS" + Environment.NewLine +


             "BEGIN TRY" + Environment.NewLine +
                " SET NOCOUNT ON;" + Environment.NewLine +

       " DECLARE @ColumnList NVARCHAR(MAX)," + Environment.NewLine +
       " @TSQL NVARCHAR(MAX)," + Environment.NewLine +
       " @ParameterList NVARCHAR(MAX)," + Environment.NewLine +
       " @Where NVARCHAR(MAX) = ''," + Environment.NewLine +
       " @TotalRows DECIMAL(18, 2)," + Environment.NewLine +
       " @NextLine CHAR(2) = CHAR(13) + CHAR(10)" + Environment.NewLine +

       "" + Environment.NewLine +
       "" + Environment.NewLine +


       " SET @PageNumber = ISNULL(@PageNumber, 1)" + Environment.NewLine +
       " SET @PageSize = ISNULL(@PageSize, 20)   " + Environment.NewLine +
       " SET @TotalRows = 20 * 1.0;              " + Environment.NewLine +
       "                                         " + Environment.NewLine +
       "                                         " + Environment.NewLine +
      string.Format("SELECT @ColumnList = COALESCE(@ColumnList + ','++@NextLine, '') + '{0}.[' + F.ColumnName + '] AS ' + '[' + F.FieldTitle + ']'", moduleSetup.DatabaseTable) + Environment.NewLine +
       " FROM Setting.ModuleTableFieldSetup F         " + Environment.NewLine +
       " INNER JOIN Setting.ModuleTableSetup M ON M.Id = F.ModuleTableSetupId           " + Environment.NewLine +
      String.Format(" WHERE M.DatabaseTable = '{0}' AND F.SummaryHeader = 1 AND (F.ForeignColumnAlais IS NULL OR F.ForeignColumnAlais ='Id')", moduleSetup.DatabaseTable) + Environment.NewLine +
       " ORDER BY F.[Order]" + Environment.NewLine + " " + "" + Environment.NewLine;



            string _where = string.Format("SET @Where =+ 'WHERE 1=1 '");


            moduleSetup.ModuleRuleSetupList.Where(x => x.SearchField).OrderBy(c => c.Order).ToList().ForEach(z =>
            {
                if (z.DataType.ToLower() == "string")
                {
                    _where += string.Format("+CASE WHEN @{0} IS NOT NULL THEN 'AND {1}.{0} LIKE @{0} ' ELSE '' END", z.ColumnName, z.TableName) + Environment.NewLine;
                }
                else
                {
                    _where += string.Format("+CASE WHEN @{0} IS NOT NULL THEN 'AND {1}.{0} = @{0} ' ELSE '' END", z.ColumnName, z.TableName) + Environment.NewLine;
                }




            });

            _where += string.Format("+ CASE WHEN @RecordStatus IS NOT NULL THEN 'AND {0}.RecordStatus = @RecordStatus ' ELSE ' AND {0}.RecordStatus <> 1' END", moduleSetup.DatabaseTable) + Environment.NewLine;

            _List_template += _where + Environment.NewLine +

       "" + Environment.NewLine +
       "" + Environment.NewLine +
       "" + Environment.NewLine +
       "" + Environment.NewLine +
       "" + Environment.NewLine +
       "" + Environment.NewLine +
       "" + Environment.NewLine +
       " SET @TSQL = N'SELECT  CONVERT(INT, CEILING(COUNT(*) OVER () / @TotalRows)) [PageCount], " + Environment.NewLine +
       "                                                                                         " + Environment.NewLine +
       "              CONVERT(INT, CEILING(COUNT(*) OVER()))[TotalRecords]," + Environment.NewLine +
       string.Format("{0}.[Id],", moduleSetup.DatabaseTable) + Environment.NewLine +
            "			'+@ColumnList+','+ @NextLine+                                                " + Environment.NewLine +
" " + Environment.NewLine;

            _List_template += "'";
            moduleSetup.ModuleRuleSetupList.Where(B => !string.IsNullOrEmpty(B.ForeignTable) && B.ForeignColumn != "Id" && B.SummaryHeader).ToList().ForEach(z =>
             {
                 _List_template += string.Format("{0}.{1} [{2}],", z.ForeignTableAlais, z.ForeignColumn, z.ForeignColumnAlais);
             });

            _List_template += "'+";
            _List_template += string.Format("'{0}.[RecordStatus] AS [Record Status],' +", moduleSetup.DatabaseTable) + Environment.NewLine +
             string.Format("'{0}.[DeletedStatus] AS [Deleted Status]  ", moduleSetup.DatabaseTable) + Environment.NewLine +
             "                                                                                         " + Environment.NewLine +
             "                                                                                         " + Environment.NewLine +
            string.Format("  FROM [{0}].[{1}] AS {1} ", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine;

            moduleSetup.ModuleRuleSetupList.Where(x => !string.IsNullOrEmpty(x.ForeignTable)).ToList().ForEach(z =>
            {
                if (z.IsRequired && z.DataType != "bool" && z.SummaryHeader && !z.Hidden)
                {
                    if (string.IsNullOrEmpty(z.Condition) && z.IsForeignKey)
                    {
                        _List_template += string.Format("INNER JOIN {0}.{1} AS {4} ON {4}.Id = {2}.{3}", z.ForeignModule, z.ForeignTable, z.TableName, z.ColumnName, z.ForeignTableAlais) + Environment.NewLine;
                    }
                    else
                    {
                        _List_template += string.Format("INNER JOIN {0}.{1} AS {4} ON {5}", z.ForeignModule, z.ForeignTable, z.TableName, z.ColumnName, z.ForeignTableAlais, z.Condition) + Environment.NewLine;
                    }

                }
                
                else
                {
                    if (z.DataType != "bool" && z.SummaryHeader && !z.Hidden)
                    {
                        if (string.IsNullOrEmpty(z.Condition))
                        {

                            _List_template += string.Format("LEFT JOIN {0}.{1} AS {4} ON {4}.Id = {2}.{3} ", z.ForeignModule, z.ForeignTable, z.TableName, z.ColumnName, z.ForeignTableAlais) + Environment.NewLine;
                        }
                        else
                        {
                            _List_template += string.Format("LEFT JOIN {0}.{1} AS {4} ON {5}", z.ForeignModule, z.ForeignTable, z.TableName, z.ColumnName, z.ForeignTableAlais, z.Condition) + Environment.NewLine;
                        }
                    }
                }
            });

            _List_template += "'+@Where +'" + Environment.NewLine +

               "                                                                                         " + Environment.NewLine +
              string.Format("  ORDER BY {0}.ModifiedDate DESC", moduleSetup.DatabaseTable) + Environment.NewLine +
               "  OFFSET @PageSize * (@PageNumber - 1) ROWS                                              " + Environment.NewLine +
               "  FETCH NEXT @PageSize ROWS ONLY                                                         " + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
               " '                                                                                       " + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
               " IF @Debug = 1                                                                           " + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
               " BEGIN                                                                                   " + Environment.NewLine +
               "   PRINT @TSQL                                                                           " + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
               "  END                                                                                    " + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
                string.Format(" SET @ParameterList = N'{0}'", searchTemplate) + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
               "                                                                                         " + Environment.NewLine +
              string.Format(" EXECUTE sp_executesql @TSQL, @ParameterList,{0}", _searchParamter) + Environment.NewLine +
                "                                                                                                 " + Environment.NewLine +
                "                                                                                                 " + Environment.NewLine +
                "                                                                                                 " + Environment.NewLine +
                "END TRY                                                                                         " + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "BEGIN CATCH                                                                                     " + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "    INSERT INTO[dbo].[ExceptionLoggers]                                                         " + Environment.NewLine +
                "           ([Id]                                                                                " + Environment.NewLine +
                "           ,[ExceptionMessage]                                                                  " + Environment.NewLine +
                "           ,[ControllerName]                                                                    " + Environment.NewLine +
                "           ,[ExceptionStackTrace]                                                               " + Environment.NewLine +
                "           ,[LogTime]                                                                           " + Environment.NewLine +
                "           ,[UserId])                                                                           " + Environment.NewLine +
                "     VALUES                                                                                     " + Environment.NewLine +
                "           (NEWID(),                                                                            " + Environment.NewLine +
                "            ERROR_MESSAGE(),                                                                    " + Environment.NewLine +
                             string.Format("'{0}',", moduleSetup.DatabaseTable) + Environment.NewLine +
                "            ERROR_LINE() + ', ' + ERROR_NUMBER() + ', ' + ERROR_STATE(),                        " + Environment.NewLine +
                "            GETDATE(),                                                                          " + Environment.NewLine +
                "            'DE69AA3E-CC18-430F-9818-6B7A45691ECF'                                              " + Environment.NewLine +
                "            )                                                                                   " + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "                                                                                                " + Environment.NewLine +
                "END CATCH    ";

            return _List_template;
        }
    }
}
