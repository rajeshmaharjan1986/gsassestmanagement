﻿using GSAssetManagement.CodeGenerator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.CodeGenerator
{
    public static class ControllerGenerator
    {
        public static string Generate(ModuleSetup moduleSetup)
        {


            var _controllerTemplate = "" +
                     string.Format("using {0}.Entity;", moduleSetup.ProjectName) + Environment.NewLine +
                     string.Format("using {0}.Entity.DTO;        ", moduleSetup.ProjectName) + Environment.NewLine +
                     string.Format("using {0}.Repository;        ", moduleSetup.ProjectName) + Environment.NewLine +
                     string.Format("using {0}.Web.Models.Utility;", moduleSetup.ProjectName) + Environment.NewLine +
                     string.Format("using {0}.Web.Utility;       ", moduleSetup.ProjectName) + Environment.NewLine +
                     string.Format("using {0}.Web.ViewModel;", moduleSetup.ProjectName) + Environment.NewLine +
                     "using System;    " + Environment.NewLine +
                     "using System.Collections.Generic;        " + Environment.NewLine +
                     "using System.Data.Entity.Validation;     " + Environment.NewLine +
                     "using System.Linq;" + Environment.NewLine +
                     "using System.Threading.Tasks;" + Environment.NewLine +
                     "using System.Web;" + Environment.NewLine +
                     "using System.Web.Mvc;        " + Environment.NewLine +


            string.Format("namespace {0}.Web.Areas.{1}.Controllers", moduleSetup.ProjectName, moduleSetup.Module) + Environment.NewLine +
              "{" + Environment.NewLine +
              string.Format("[ModuleInfo(ModuleName = ModuleName.{0}, SubModuleName = \"{1}\", Url = \"/{0}/{2}\", Parent = {3})]", moduleSetup.Module, moduleSetup.TableTitle, moduleSetup.DatabaseTable, (moduleSetup.ParentTable == null).ToString().ToLower()) + Environment.NewLine +
              string.Format("[CRUDAuthorize(ModuleName = ModuleName.{0}, SubModuleName = \"{1}\", Action = CurrentAction.View)]", moduleSetup.Module, moduleSetup.TableTitle) + Environment.NewLine +
              "[ExceptionHandler]       " + Environment.NewLine +
              string.Format("public class {0}Controller : Controller", moduleSetup.DatabaseTable) + Environment.NewLine +
              "{" + Environment.NewLine +

                 string.Format("private readonly I{0}Repository _{1}Repository;", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                 string.Format("private readonly IDataMapper<{0}> _dataMapperHelper;", moduleSetup.DatabaseTable) + Environment.NewLine +
                  "private readonly IChangeLogRepository _changelogRepository;" + Environment.NewLine +
                  "private readonly ICommonRepository _commonRepository;      " + Environment.NewLine +
                 string.Format("public {0}Controller(I{0}Repository {1}Repository,", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                    string.Format("IDataMapper<{0}> dataMapperHelper,", moduleSetup.DatabaseTable) + Environment.NewLine +
                      "IChangeLogRepository changelogRepository,  " + Environment.NewLine +
                      "ICommonRepository commonRepository)        " + Environment.NewLine +
                  "{" + Environment.NewLine +
                     string.Format("this._{0}Repository = {0}Repository;", moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                      "this._dataMapperHelper = dataMapperHelper;    " + Environment.NewLine +
                      "this._changelogRepository = changelogRepository;" + Environment.NewLine +
                      "this._commonRepository = commonRepository;    " + Environment.NewLine +
                  "}" + Environment.NewLine +



                  string.Format("[CRUDAuthorize(ModuleName = ModuleName.{1}, SubModuleName = \"{0}\", Action = CurrentAction.View)]", moduleSetup.TableTitle, moduleSetup.Module) + Environment.NewLine +
                  "[ExceptionHandler]    " + Environment.NewLine;

            if (moduleSetup.ParentTable == null)
            {
                _controllerTemplate += "public async Task<ActionResult> Index()" + Environment.NewLine;
            }
            else
            {


                _controllerTemplate += "public async Task<ActionResult> Index(Guid Id)" + Environment.NewLine;
            }

            _controllerTemplate += "{         " + Environment.NewLine +
          "    try   " + Environment.NewLine +
          "    {     " + Environment.NewLine +
          "         " + Environment.NewLine +
          "        var model = new IndexViewModel();" + Environment.NewLine;
            if (!string.IsNullOrEmpty(moduleSetup.ParentTable))
            {
                _controllerTemplate += "  model.ParentId = Id;";
            }
            _controllerTemplate += "         " + Environment.NewLine +
             string.Format("model.FieldRuleList =  await this._dataMapperHelper.GetFieldRule(null, ModuleName.{0}.ToString(), \"{1}\", true, \"FieldRuleList\");", moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
            "" + Environment.NewLine +
             "     " + Environment.NewLine +
             "        List<Parameter> _parameterList = new List<Parameter>();  " + Environment.NewLine +
             "     " + Environment.NewLine +
             "     " + Environment.NewLine +
             "     " + Environment.NewLine +
             "        model.FieldRuleList.Where(p => p.SearchField).OrderBy(x => x.Order).ToList().ForEach(c =>          " + Environment.NewLine +
             "        {  " + Environment.NewLine +
             "_parameterList.Add(new Parameter()  " + Environment.NewLine +
             "{          " + Environment.NewLine +
             "    Name = c.ColumnName," + Environment.NewLine;

            if (!string.IsNullOrEmpty(moduleSetup.ParentTable))
            {
                _controllerTemplate += string.Format(" Value = c.ColumnName == \"{0}Id\" ? model.ParentId : c.Value", moduleSetup.ParentTable) + Environment.NewLine;
            }
            else
            {
                _controllerTemplate += "    Value = c.Value    " + Environment.NewLine;

            }




            _controllerTemplate += "});        " + Environment.NewLine +
              "     " + Environment.NewLine +
              "     " + Environment.NewLine +
              "        });" + Environment.NewLine +
              "     " + Environment.NewLine +
              "        _parameterList.Add(new Parameter()      " + Environment.NewLine +
              "        {  " + Environment.NewLine +
              "Name =\"RecordStatus\", " + Environment.NewLine +
              "Value = null" + Environment.NewLine +
              "        });" + Environment.NewLine +
              "     " + Environment.NewLine +
              "        _parameterList.Add(new Parameter()      " + Environment.NewLine +
              "        {  " + Environment.NewLine +
              "Name = \"PageNumber\",   " + Environment.NewLine +
              "Value = 1  " + Environment.NewLine +
              "        });" + Environment.NewLine +
              "     " + Environment.NewLine +
              "        _parameterList.Add(new Parameter()      " + Environment.NewLine +
              "        {  " + Environment.NewLine +
              "Name = \"PageSize\",     " + Environment.NewLine +
              "Value = 20 " + Environment.NewLine +
              "        });" + Environment.NewLine +
              "     " + Environment.NewLine +
              string.Format("var recordList = await this._{1}Repository.Get{0}List(_parameterList.ToArray()); ", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
             " " + Environment.NewLine +
              "        model.IndexData = recordList;" + Environment.NewLine +
              "     " + Environment.NewLine +
              "        return View(model);        " + Environment.NewLine +
              "    } " + Environment.NewLine +
              "    catch (Exception ex)" + Environment.NewLine +
              "    { " + Environment.NewLine +
              "        throw ex;      " + Environment.NewLine +
              "    } " + Environment.NewLine +
              "}     " + Environment.NewLine +
              "     " + Environment.NewLine +

              "[ValidateAntiForgeryToken]" + Environment.NewLine +
               string.Format("[CRUDAuthorize(ModuleName = ModuleName.{1}, SubModuleName = \"{0}\", Action = CurrentAction.View)]", moduleSetup.TableTitle, moduleSetup.Module) + Environment.NewLine +
              "[ExceptionHandler]" + Environment.NewLine +
              "[HttpPost]        " + Environment.NewLine +
              "public async Task<ActionResult> PartialIndex(FormCollection model)" + Environment.NewLine +
              "{          " + Environment.NewLine +
              "    try    " + Environment.NewLine +
              "    {      " + Environment.NewLine +
             string.Format("var _formData = await this._dataMapperHelper.Map(this.HttpContext, model, null, ModuleName.{1}.ToString(), \"{0}\", true, \"FieldRuleList\", CurrentAction.View);", moduleSetup.DatabaseTable, moduleSetup.Module) + Environment.NewLine +
              "        List<Parameter> _parameterList = new List<Parameter>();" + Environment.NewLine +
              "        _formData.ToList().ForEach(p =>" + Environment.NewLine +
              "        {       " + Environment.NewLine +
              "_parameterList.Add(new Parameter()     " + Environment.NewLine +
              "{   " + Environment.NewLine +
              "    Name = p.FormFieldName," + Environment.NewLine +
              "    Value = p.FormFieldValue" + Environment.NewLine +
              "}); " + Environment.NewLine +
              "        });     " + Environment.NewLine +
              "  " + Environment.NewLine +
             string.Format("        var recordList = await this._{0}Repository.Get{1}List(_parameterList.ToArray());", moduleSetup.DatabaseTable.ToLower(), moduleSetup.DatabaseTable) + Environment.NewLine +
              " " + Environment.NewLine +
              "        return View(\"PartialIndex\", recordList);          " + Environment.NewLine +
              "    }        " + Environment.NewLine +
              "    catch (Exception ex) " + Environment.NewLine +
              "    {        " + Environment.NewLine +
              "        throw ex;        " + Environment.NewLine +
              "    }        " + Environment.NewLine +
              "}" + Environment.NewLine +


         string.Format("[CRUDAuthorize(ModuleName = ModuleName.{0}, SubModuleName = \"{1}\", Action = CurrentAction.Create)]", moduleSetup.Module, moduleSetup.TableTitle) + Environment.NewLine +
          "[ExceptionHandler]     " + Environment.NewLine;

            _controllerTemplate += "[HttpGet]  " + Environment.NewLine;

            if (moduleSetup.ParentTable == null)
            {
                _controllerTemplate += "public async Task<ActionResult> Create()" + Environment.NewLine;
            }
            else
            {


                _controllerTemplate += "public async Task<ActionResult> Create(Guid Id)" + Environment.NewLine;
            }





            _controllerTemplate += "{     " + Environment.NewLine +
              "    try    " + Environment.NewLine +
              "    { " + Environment.NewLine +
              "     " + Environment.NewLine +
              string.Format("var model = await this._dataMapperHelper.GetFieldRule(null, ModuleName.{1}.ToString(), \"{0}\", false, \"FieldRuleList\");", moduleSetup.DatabaseTable, moduleSetup.Module) + Environment.NewLine;

            if (!string.IsNullOrEmpty(moduleSetup.ParentTable))
            {
                _controllerTemplate += string.Format("model.Where(x => x.ColumnName == \"{0}Id\").ToList().ForEach(x => x.Value = Id);", moduleSetup.ParentTable);
            }


            _controllerTemplate += "" + Environment.NewLine +
              "" + Environment.NewLine +
              "        var _parameterSelctsList = model.Where(x => x.ParameterisedSource).Select(z => new" + Environment.NewLine +
              "        {        " + Environment.NewLine +
              "z.ColumnName,     " + Environment.NewLine +
              "z.Parameter       " + Environment.NewLine +
              "" + Environment.NewLine +
              "        }).ToList();          " + Environment.NewLine +
              "" + Environment.NewLine +
              "        List<SelectListParameter> _SelectListParameter = new List<SelectListParameter>();" + Environment.NewLine +
              "" + Environment.NewLine +
              "        foreach (var item in _parameterSelctsList)         " + Environment.NewLine +
              "        {        " + Environment.NewLine +
              "SelectListParameter _selectPara = new SelectListParameter();" + Environment.NewLine +
              "_selectPara.FieldName = item.ColumnName;       " + Environment.NewLine +
              "" + Environment.NewLine +
              "" + Environment.NewLine +
              "string[] _parameters = item.Parameter.Split(',');" + Environment.NewLine +
              "" + Environment.NewLine +
              "foreach (var para in _parameters)  " + Environment.NewLine +
              "{    " + Environment.NewLine +
              "" + Environment.NewLine +
              "    Parameter _itemPara = new Parameter();     " + Environment.NewLine +
              "    _itemPara.Name = para.Replace(\"*.\", \"@\");  " + Environment.NewLine +
               "    _itemPara.Value = model.Where(x => x.ColumnName == para.Replace(\"*.\", \"\")).Select(z => z.Value).FirstOrDefault();  " + Environment.NewLine +
                "" + Environment.NewLine +
                "    _selectPara.Parameters.Add(_itemPara);     " + Environment.NewLine +
                "" + Environment.NewLine +
                "}    " + Environment.NewLine +
                "" + Environment.NewLine +
                "_SelectListParameter.Add(_selectPara);         " + Environment.NewLine +
                "" + Environment.NewLine +
                "        }        " + Environment.NewLine +
                "" + Environment.NewLine +
               string.Format("var _selects = await this._commonRepository.GetSelectList(\"{0}\", _SelectListParameter);", moduleSetup.DatabaseTable) + Environment.NewLine +
                "" + Environment.NewLine +
                "        foreach (var selectLists in _selects.GroupBy(c => c.FieldName)) " + Environment.NewLine +
                "        {" + Environment.NewLine +
                "model.Where(x => x.ColumnName == selectLists.Key).ToList().ForEach(z => z.DropdownMasterList = new SelectList(selectLists.Where(y => y.FieldName  == selectLists.Key).Select(v => v.SelectValues).FirstOrDefault().AsEnumerable(), \"Value\", \"Title\")); " + Environment.NewLine +
              "        }          " + Environment.NewLine +
              " " + Environment.NewLine +
              " " + Environment.NewLine +
              "        return View(\"Create\", model);       " + Environment.NewLine +
              "    }  " + Environment.NewLine +
              "    catch (Exception ex)       " + Environment.NewLine +
              "    {  " + Environment.NewLine +
              "        throw ex;  " + Environment.NewLine +
              "    }  " + Environment.NewLine +
              "} " + Environment.NewLine +
              " " + Environment.NewLine +

              "[ValidateAntiForgeryToken]" + Environment.NewLine +
              string.Format("[CRUDAuthorize(ModuleName = ModuleName.{1}, SubModuleName = \"{0}\", Action = CurrentAction.Create)]", moduleSetup.TableTitle, moduleSetup.Module) + Environment.NewLine +
              "[ExceptionHandler] " + Environment.NewLine +
              "[HttpPost]         " + Environment.NewLine +
              "public async Task<JsonResult> Create(FormCollection model,string SubmissionRemarks)   " + Environment.NewLine +
              "{ " + Environment.NewLine +
              "    var response = new JsonResponse();      " + Environment.NewLine +
              " " + Environment.NewLine +
              "    try" + Environment.NewLine +
              "    {  " + Environment.NewLine +
              " " + Environment.NewLine +
              string.Format("var _formData = await this._dataMapperHelper.Map(this.HttpContext, model, null, ModuleName.{1}.ToString(), \"{0}\", false, \"FieldRuleList\", CurrentAction.Create);", moduleSetup.DatabaseTable, moduleSetup.Module) + Environment.NewLine +
                  "        if (_formData.Where(x => !x.IsValid).Count() == 0)" + Environment.NewLine +
              "        {" + Environment.NewLine +
           string.Format("var dtoModel = await this._dataMapperHelper.GetItem<{0}DTO>(_formData); ", moduleSetup.DatabaseTable) + Environment.NewLine +
              "        " + Environment.NewLine +
              "_formData.ForEach(x =>    " + Environment.NewLine +
              "{" + Environment.NewLine +
              "    ChangeLogDTO _changeLog = new ChangeLogDTO();      " + Environment.NewLine +
              "    _changeLog.PropertyName = x.FormFieldName;         " + Environment.NewLine +
              "    _changeLog.Originalvalue = x.Originalvalue;        " + Environment.NewLine +
              "    _changeLog.ModifiedValue = x.FormFieldValue;       " + Environment.NewLine +
              "    _changeLog.ModifiedDate = DateTime.Now;" + Environment.NewLine +
              "    _changeLog.LogStatus = x.RecordState.ToString();   " + Environment.NewLine +
              "        " + Environment.NewLine +
              "    dtoModel.ArrayOfChangeLogDTO.Add(_changeLog);      " + Environment.NewLine +
              "        " + Environment.NewLine +
              "});          " + Environment.NewLine +
              "        " + Environment.NewLine +
              "dtoModel.RecordStatus = (int)RecordStatus.UnVerified;" + Environment.NewLine +
              "        " + Environment.NewLine +
             string.Format("var _serverResponse = await this._{0}Repository.InsertUpdateRecords(dtoModel, CurrentAction.Create,SubmissionRemarks,GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{1}, \"{2}\", GSAssetManagement.Entity.CurrentAction.AutoAuthorise));  ", moduleSetup.DatabaseTable.ToLower(), moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
              "        " + Environment.NewLine +
              "if (_serverResponse.Item2)" + Environment.NewLine +
              "{" + Environment.NewLine +
              "    response.IsSuccess = true; " + Environment.NewLine +
              "    response.ResponseType = ResponseType.Success;      " + Environment.NewLine +
              "        " + Environment.NewLine +
              "    return Json(new       " + Environment.NewLine +
              "    {        " + Environment.NewLine +
              "   IsSuccess = true, " + Environment.NewLine +
              "   Id = _serverResponse.Item1,        " + Environment.NewLine +
              "   Summary = this.RenderRazorViewToString(\"Response\", response)" + Environment.NewLine +
              "        " + Environment.NewLine +
              "    }, JsonRequestBehavior.DenyGet);       " + Environment.NewLine +
              "}" + Environment.NewLine +
              "else         " + Environment.NewLine +
              "{" + Environment.NewLine +
              "        " + Environment.NewLine +
              "    response.IsSuccess = false;" + Environment.NewLine +
              "    response.ResponseType = ResponseType.Error;        " + Environment.NewLine +
              "    response.Message = \"Something went wrong.\";        " + Environment.NewLine +
              "        " + Environment.NewLine +
              "    return Json(new       " + Environment.NewLine +
              "    {        " + Environment.NewLine +
              "   IsSuccess = false," + Environment.NewLine +
              "   Summary = this.RenderRazorViewToString(\"Response\", response)" + Environment.NewLine +
              "        " + Environment.NewLine +
              "    }, JsonRequestBehavior.DenyGet);       " + Environment.NewLine +
              "}" + Environment.NewLine +
              "        " + Environment.NewLine +
              "        }    " + Environment.NewLine +
              "        else " + Environment.NewLine +
              "        {    " + Environment.NewLine +
              "        " + Environment.NewLine +
              "response.ResponseType = ResponseType.Warning;          " + Environment.NewLine +
              "response.Message = \"Submited data has validation errors.Please fix and resubmit it.\";" + Environment.NewLine +
              "_formData.Where(x => !x.IsValid).ToList().ForEach(x => " + Environment.NewLine +
              "{" + Environment.NewLine +
              "    response.Errors.Add(x.FormFieldName, x.ErrorMessage);" + Environment.NewLine +
              "});          " + Environment.NewLine +
              "        " + Environment.NewLine +
              "return Json(new" + Environment.NewLine +
              "{" + Environment.NewLine +
              "    IsSuccess = false,    " + Environment.NewLine +
              "    ResponseType = ResponseType.Warning,   " + Environment.NewLine +
              "    Summary = this.RenderRazorViewToString(\"Response\", response)    " + Environment.NewLine +
              "        " + Environment.NewLine +
              "}, JsonRequestBehavior.DenyGet);" + Environment.NewLine +
              "        " + Environment.NewLine +
              "        }    " + Environment.NewLine +
              "        " + Environment.NewLine +
              "        " + Environment.NewLine +
              "    }        " + Environment.NewLine +
              "    catch (DbEntityValidationException ex) " + Environment.NewLine +
              "    {        " + Environment.NewLine +
              "        response.IsSuccess = false;   " + Environment.NewLine +
              "        response.ResponseType = ResponseType.Warning;  " + Environment.NewLine +
              "        response.Message = ex.Message;" + Environment.NewLine +
              "        " + Environment.NewLine +
              "        ex.EntityValidationErrors.Where(x => !x.IsValid).ToList().ForEach(x =>  " + Environment.NewLine +
              "        {    " + Environment.NewLine +
              "x.ValidationErrors.ToList().ForEach(z =>   " + Environment.NewLine +
              "{" + Environment.NewLine +
              "    response.Errors.Add(z.PropertyName, z.ErrorMessage);" + Environment.NewLine +
              "});          " + Environment.NewLine +
              "        " + Environment.NewLine +
              "        });  " + Environment.NewLine +
              "        " + Environment.NewLine +
              "        return Json(new  " + Environment.NewLine +
              "        {    " + Environment.NewLine +
              "IsSuccess = false,        " + Environment.NewLine +
              "Summary = this.RenderRazorViewToString(\"Response\", response)        " + Environment.NewLine +
              "        " + Environment.NewLine +
              "        }, JsonRequestBehavior.DenyGet);   " + Environment.NewLine +
              "        " + Environment.NewLine +
              "        " + Environment.NewLine +
              "    }        " + Environment.NewLine +
              "    catch (Exception ex) " + Environment.NewLine +
              "    {        " + Environment.NewLine +
              "        response.IsSuccess = false;   " + Environment.NewLine +
              "        response.ResponseType = ResponseType.Error;    " + Environment.NewLine +
              "        response.Message = ex.Message;" + Environment.NewLine +
              "        " + Environment.NewLine +
              "        " + Environment.NewLine +
              "        " + Environment.NewLine +
              "        return Json(new  " + Environment.NewLine +
              "        {    " + Environment.NewLine +
              "IsSuccess = false,        " + Environment.NewLine +
              "Summary = this.RenderRazorViewToString(\"Response\", response)        " + Environment.NewLine +
              "        " + Environment.NewLine +
              "        }, JsonRequestBehavior.DenyGet);   " + Environment.NewLine +
              "    }        " + Environment.NewLine +
              "        " + Environment.NewLine +
              "}" + Environment.NewLine +
              "        " + Environment.NewLine +
            string.Format("[CRUDAuthorize(ModuleName = ModuleName.{1}, SubModuleName = \"{0}\", Action = CurrentAction.View)]", moduleSetup.TableTitle, moduleSetup.Module) + Environment.NewLine +
              "[ExceptionHandler]       " + Environment.NewLine +
              "[HttpGet]    " + Environment.NewLine +
              "public async Task<ActionResult> Details(Guid Id)       " + Environment.NewLine +
              "{" + Environment.NewLine +
              "    try      " + Environment.NewLine +
              "    {        " + Environment.NewLine +
             string.Format("var _ruleList = await this._dataMapperHelper.GetRecordDetailsById<{0}DTO>(Id, ModuleName.{1}.ToString(), \"{0}\"); ", moduleSetup.DatabaseTable, moduleSetup.Module) + Environment.NewLine +
              "        " + Environment.NewLine +
              "        " + Environment.NewLine +
              "        var _parameterSelctsList = _ruleList.FieldRuleList.Where(x => x.ParameterisedSource).Select(z => new " + Environment.NewLine +
              "        {    " + Environment.NewLine +
              "z.ColumnName," + Environment.NewLine +
              "z.Parameter  " + Environment.NewLine +
              "        " + Environment.NewLine +
              "        }).ToList();     " + Environment.NewLine +
              "        " + Environment.NewLine +
              "        List<SelectListParameter> _SelectListParameter = new List<SelectListParameter>();        " + Environment.NewLine +
              "        " + Environment.NewLine +
              "        foreach (var item in _parameterSelctsList)     " + Environment.NewLine +
              "        {    " + Environment.NewLine +
              "SelectListParameter _selectPara = new SelectListParameter();        " + Environment.NewLine +
              "_selectPara.FieldName = item.ColumnName;   " + Environment.NewLine +
              "        " + Environment.NewLine +
              "        " + Environment.NewLine +
              "string[] _parameters = item.Parameter.Split(',');      " + Environment.NewLine +
              "        " + Environment.NewLine +
              "foreach (var para in _parameters)          " + Environment.NewLine +
              "{" + Environment.NewLine +
              "        " + Environment.NewLine +
              "    Parameter _itemPara = new Parameter(); " + Environment.NewLine +
              "    _itemPara.Name = para.Replace(\"*.\", \"@\");          " + Environment.NewLine +
              "    _itemPara.Value = _ruleList.FieldRuleList.Where(x => x.ColumnName == para.Replace(\"*.\", \"\")).Select(z => z.Value).FirstOrDefault();    " + Environment.NewLine +
               "        " + Environment.NewLine +
               "    _selectPara.Parameters.Add(_itemPara); " + Environment.NewLine +
               "        " + Environment.NewLine +
               "}" + Environment.NewLine +
               "        " + Environment.NewLine +
               "_SelectListParameter.Add(_selectPara);     " + Environment.NewLine +
               "        " + Environment.NewLine +
               "        }    " + Environment.NewLine +
               "        " + Environment.NewLine +
               string.Format("var _selects = await this._commonRepository.GetSelectList(\"{0}\", _SelectListParameter); ", moduleSetup.DatabaseTable) + Environment.NewLine +
               "        " + Environment.NewLine +
               "        foreach (var selectLists in _selects.GroupBy(c => c.FieldName))         " + Environment.NewLine +
               "        {    " + Environment.NewLine +
               "_ruleList.FieldRuleList.Where(x => x.ColumnName == selectLists.Key).ToList().ForEach(z => z.DropdownMasterList = new SelectList(selectLists.Where(y => y.FieldName == selectLists.Key).Select(v => v.SelectValues).FirstOrDefault().AsEnumerable(), \"Value\", \"Title\", _ruleList.FieldRuleList.Where(s => s.ColumnName == selectLists.Key).Select(v => v.Value).FirstOrDefault()));" + Environment.NewLine +
               "        }        " + Environment.NewLine;


            if (string.IsNullOrEmpty(moduleSetup.ParentTable))
            {
                _controllerTemplate += "if (Request.IsAjaxRequest())" + Environment.NewLine +
                                         "{" + Environment.NewLine +

                                              "return View(\"PartialDetails\", _ruleList);" + Environment.NewLine +
                                          "}" + Environment.NewLine +
                                          "else" + Environment.NewLine +
                                           "{" + Environment.NewLine +
                                                 "return View(\"Details\", _ruleList);" + Environment.NewLine +
                "}" + Environment.NewLine +
                 "" + Environment.NewLine +
                     "" + Environment.NewLine +
                     "  }" + Environment.NewLine +
                     "  catch (Exception ex)" + Environment.NewLine +
                     "  {       " + Environment.NewLine +
                     "      throw ex;       " + Environment.NewLine +
                     "  }       " + Environment.NewLine +
                     "        " + Environment.NewLine +
                     "          }" + Environment.NewLine +
                     "        " + Environment.NewLine;
            }
            else
            {
                _controllerTemplate += "return View(\"Details\", _ruleList);" + Environment.NewLine +
                     "" + Environment.NewLine +
                     "" + Environment.NewLine +
                     "  }" + Environment.NewLine +
                     "  catch (Exception ex)" + Environment.NewLine +
                     "  {       " + Environment.NewLine +
                     "      throw ex;       " + Environment.NewLine +
                     "  }       " + Environment.NewLine +
                     "        " + Environment.NewLine +
                     "          }" + Environment.NewLine +
                     "        " + Environment.NewLine;
            }


            _controllerTemplate += "[ValidateAntiForgeryToken]" + Environment.NewLine;
            _controllerTemplate += string.Format("[CRUDAuthorize(ModuleName = ModuleName.{1}, SubModuleName = \"{0}\", Action = CurrentAction.Edit)] ", moduleSetup.TableTitle, moduleSetup.Module) + Environment.NewLine +
                 "[ExceptionHandler]        " + Environment.NewLine +
                 "[HttpPost]    " + Environment.NewLine +
                 "public async Task<ActionResult> ModifyRecords(FormCollection model, CurrentAction CurrentAction,string SubmissionRemarks)  " + Environment.NewLine +
                 "{ " + Environment.NewLine +
                 "    var response = new JsonResponse(); " + Environment.NewLine +
                 "         " + Environment.NewLine +
                 string.Format("if(AuthorizeViewHelper.IsAuthorize(ModuleName.{1}, \"{0}\", CurrentAction))   ", moduleSetup.DatabaseTable, moduleSetup.Module) + Environment.NewLine +
                 "    {         " + Environment.NewLine +
                 "        try   " + Environment.NewLine +
                 "        {     " + Environment.NewLine +
                 "         " + Environment.NewLine +
               string.Format("var _formData = await this._dataMapperHelper.GetRecordDetailsByIdForPost<{0}DTO>(this.HttpContext, model, Guid.Parse(model[\"Id\"].ToString()), ModuleName.{1}.ToString(), \"{0}\"); ", moduleSetup.DatabaseTable, moduleSetup.Module) + Environment.NewLine +
                "        " + Environment.NewLine +
                "        " + Environment.NewLine +
                "        " + Environment.NewLine +
                "if (_formData.Where(x => !x.IsValid).Count() == 0)     " + Environment.NewLine +
                "{" + Environment.NewLine +
                string.Format("var dtoModel = await this._dataMapperHelper.GetItem<{0}DTO>(_formData);", moduleSetup.DatabaseTable) + Environment.NewLine +
                "     " + Environment.NewLine +
                "    _formData.Where(z => z.RecordState == System.Data.Entity.EntityState.Modified).ToList().ForEach(x =>   " + Environment.NewLine +
                "    {      " + Environment.NewLine +
                "   ChangeLogDTO _changeLog = new ChangeLogDTO();" + Environment.NewLine +
                "   _changeLog.PropertyName = x.FormFieldName;   " + Environment.NewLine +
                "   _changeLog.Originalvalue = x.Originalvalue;  " + Environment.NewLine +
                "   _changeLog.ModifiedValue = x.FormFieldValue; " + Environment.NewLine +
                "   _changeLog.ModifiedDate = DateTime.Now;      " + Environment.NewLine +
                "   _changeLog.LogStatus = x.RecordState.ToString();         " + Environment.NewLine +
                "     " + Environment.NewLine +
                "   dtoModel.ArrayOfChangeLogDTO.Add(_changeLog);" + Environment.NewLine +
                "     " + Environment.NewLine +
                "    });    " + Environment.NewLine +
                "     " + Environment.NewLine +
                "     " + Environment.NewLine +
                string.Format("var _serverResponse = await this._{0}Repository.InsertUpdateRecords(dtoModel, CurrentAction,SubmissionRemarks,GSAssetManagement.Web.Utility.AuthorizeViewHelper.IsAuthorize(GSAssetManagement.Entity.ModuleName.{1}, \"{2}\", GSAssetManagement.Entity.CurrentAction.AutoAuthorise));  ", moduleSetup.DatabaseTable.ToLower(), moduleSetup.Module, moduleSetup.DatabaseTable) + Environment.NewLine +
                "         " + Environment.NewLine +
                "    if (_serverResponse.Item2)          " + Environment.NewLine +
                "    {          " + Environment.NewLine +
                "   response.IsSuccess = true;      " + Environment.NewLine +
                "   response.ResponseType = ResponseType.Success;    " + Environment.NewLine +
                "         " + Environment.NewLine +
                "         " + Environment.NewLine +
                "         " + Environment.NewLine +
                "   return Json(new    " + Environment.NewLine +
                "   {      " + Environment.NewLine +
                "       IsSuccess = true," + Environment.NewLine +
                "       Id = _serverResponse.Item1, " + Environment.NewLine +
                "       Summary = this.RenderRazorViewToString(\"Response\", response)          " + Environment.NewLine +
                "         " + Environment.NewLine +
                "   }, JsonRequestBehavior.DenyGet);" + Environment.NewLine +
                "    }          " + Environment.NewLine +
                "    else       " + Environment.NewLine +
                "    {          " + Environment.NewLine +
                "         " + Environment.NewLine +
                "   response.IsSuccess = false;     " + Environment.NewLine +
                "   response.ResponseType = ResponseType.Error;      " + Environment.NewLine +
                "   response.Message = \"Something went wrong.\";      " + Environment.NewLine +
                "         " + Environment.NewLine +
                "   return Json(new    " + Environment.NewLine +
                "   {      " + Environment.NewLine +
                "       IsSuccess = false,          " + Environment.NewLine +
                "       Summary = this.RenderRazorViewToString(\"Response\", response)          " + Environment.NewLine +
                "         " + Environment.NewLine +
                "   }, JsonRequestBehavior.DenyGet);" + Environment.NewLine +
                "    }          " + Environment.NewLine +
                "         " + Environment.NewLine +
                "}  " + Environment.NewLine +
                "else" + Environment.NewLine +
                "{  " + Environment.NewLine +
                "         " + Environment.NewLine +
                "    response.IsSuccess = false;         " + Environment.NewLine +
                "    response.ResponseType = ResponseType.Warning;        " + Environment.NewLine +
                "    response.Message = \"Validation errors in the form submited\";     " + Environment.NewLine +
                "         " + Environment.NewLine +
                "         " + Environment.NewLine +
                "    _formData.Where(x => !x.IsValid).ToList().ForEach(x =>" + Environment.NewLine +
                "    {          " + Environment.NewLine +
                "   response.Errors.Add(x.FormFieldName, x.ErrorMessage);        " + Environment.NewLine +
                "    });        " + Environment.NewLine +
                "         " + Environment.NewLine +
                "    return Json(new        " + Environment.NewLine +
                "    {          " + Environment.NewLine +
                "   IsSuccess = false, " + Environment.NewLine +
                "   ResponseType = ResponseType.Warning, " + Environment.NewLine +
                "   Summary = this.RenderRazorViewToString(\"Response\", response) " + Environment.NewLine +
                "         " + Environment.NewLine +
                "    }, JsonRequestBehavior.DenyGet);    " + Environment.NewLine +
                "         " + Environment.NewLine +
                "}  " + Environment.NewLine +
                "         " + Environment.NewLine +
                "         " + Environment.NewLine +
                "        } " + Environment.NewLine +
                "        catch (Exception ex)" + Environment.NewLine +
                "        { " + Environment.NewLine +
                "response.IsSuccess = false;" + Environment.NewLine +
                "response.ResponseType = ResponseType.Error;  " + Environment.NewLine +
                "response.Message = ex.Message;          " + Environment.NewLine +
                "         " + Environment.NewLine +
                "return Json(new" + Environment.NewLine +
                "{  " + Environment.NewLine +
                "    IsSuccess = false,     " + Environment.NewLine +
                "    Summary = this.RenderRazorViewToString(\"Response\", response)     " + Environment.NewLine +
                "         " + Environment.NewLine +
                "}, JsonRequestBehavior.DenyGet);        " + Environment.NewLine +
                "        } " + Environment.NewLine +
                "    }     " + Environment.NewLine +
                "    else  " + Environment.NewLine +
                "    {     " + Environment.NewLine +
                "        response.IsSuccess = false;    " + Environment.NewLine +
                "        response.ResponseType = ResponseType.Error; " + Environment.NewLine +
                "        response.Message = \"Resources not found.\";  " + Environment.NewLine +
                "         " + Environment.NewLine +
                "        return Json(new    " + Environment.NewLine +
                "        { " + Environment.NewLine +
                "IsSuccess = false,         " + Environment.NewLine +
                "Summary = this.RenderRazorViewToString(\"Response\", response)         " + Environment.NewLine +
                "         " + Environment.NewLine +
                "        }, JsonRequestBehavior.DenyGet);" + Environment.NewLine +
                "    }     " + Environment.NewLine +
                "         " + Environment.NewLine +
                "}         " + Environment.NewLine +
                "         " + Environment.NewLine +
                "        }" + Environment.NewLine +
                "    }    " + Environment.NewLine;


            return _controllerTemplate;

        }
    }
}

