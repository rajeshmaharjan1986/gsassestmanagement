﻿using GSAssetManagement.CodeGenerator.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSAssetManagement.CodeGenerator
{
    public class ControllerGeneratorNew
    {
        public static string Generate(ModuleSetupNew moduleSetup)
        {
            string projectName = ConfigurationManager.AppSettings["ProjectName"].ToString();

            var _controllerTemplate = "using System;    " + Environment.NewLine +
                      "using System.Data.SqlClient;" + Environment.NewLine +
                     "using System.Collections.Generic;        " + Environment.NewLine +
                     "using System.Data.Entity.Validation;     " + Environment.NewLine +
                     "using System.Linq;" + Environment.NewLine +
                     "using System.Threading.Tasks;" + Environment.NewLine +
                     "using System.Web;" + Environment.NewLine +
                     "using System.Web.Mvc;        " + Environment.NewLine +
            string.Format("using {0}.Entity;", projectName) + Environment.NewLine +
                     string.Format("using {0}.Entity.DTO;        ", projectName) + Environment.NewLine +
                     string.Format("using {0}.Infrastructure;        ", projectName) + Environment.NewLine +
                     string.Format("using {0}.Repository;        ", projectName) + Environment.NewLine +
                     string.Format("using {0}.Web;", projectName) + Environment.NewLine +
                     string.Format("using {0}.Web.Utility;       ", projectName) + Environment.NewLine +
                     string.Format("using {0}.Entity.Validation;", projectName) + Environment.NewLine +



            string.Format("namespace {0}.Web.Areas.{1}.Controllers", projectName, moduleSetup.ModuleName) + Environment.NewLine +
              "{" + Environment.NewLine +
              string.Format("    [ModuleInfo(ModuleName = ModuleName.{0}, SubModuleName = \"{1}\", Url = \"/{0}/{1}\", Parent = {2})]", moduleSetup.ModuleName, moduleSetup.DatabaseTable, moduleSetup.IsParent.ToString().ToLower()) + Environment.NewLine +
              string.Format("    [CRUDAuthorize(ModuleName = ModuleName.{0}, SubModuleName = \"{1}\", Action = CurrentAction.View)]", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
              "    [ExceptionHandler]       " + Environment.NewLine +
              string.Format("    public class {0}Controller : Controller", moduleSetup.DatabaseTable) + Environment.NewLine +
              "    {" + Environment.NewLine +

                 string.Format("        private readonly I{0}Repository _{0}Repository;", moduleSetup.DatabaseTable) + Environment.NewLine +
                  "        private IExceptionLoggerRepository _exceptionLoggerRepository;" + Environment.NewLine +
                  "        private readonly IUnitOfWork _unitOfWork;      " + Environment.NewLine +


                 string.Format("        public {0}Controller(I{0}Repository {1}Repository,", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                      "        IUnitOfWork unitOfWork,  " + Environment.NewLine +
                      "        IExceptionLoggerRepository exceptionLoggerRepository)        " + Environment.NewLine +
                  "        {" + Environment.NewLine +
                     string.Format("            this._{0}Repository = {1}Repository;", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                      "            _exceptionLoggerRepository = exceptionLoggerRepository;    " + Environment.NewLine +
                      "            _unitOfWork = unitOfWork;;" + Environment.NewLine +
                  "        }" + Environment.NewLine +
                  "" + Environment.NewLine +


                  string.Format("        [CRUDAuthorize(ModuleName = ModuleName.{1}, SubModuleName = \"{0}\", Action = CurrentAction.View)]", moduleSetup.DatabaseTable, moduleSetup.ModuleName) + Environment.NewLine +
                  "        [ExceptionHandler]    " + Environment.NewLine +
            "        [HttpGet]    " + Environment.NewLine;

            if (moduleSetup.ParentModule == null)
            {
                _controllerTemplate += "        public async Task<ActionResult> Index()" + Environment.NewLine;
            }
            else
            {
                _controllerTemplate += "        public async Task<ActionResult> Index(Guid ParentPrimaryRecordId)" + Environment.NewLine;
            }

            _controllerTemplate += "        {" + Environment.NewLine +
          "            try   " + Environment.NewLine +
          "            {     " + Environment.NewLine +
          "                " + Environment.NewLine;
            if (moduleSetup.ParentModule == null)
            {
                _controllerTemplate += string.Format("                ModuleSummary moduleSummary = await _{0}Repository.GetModuleBussinesLogicSetup();", moduleSetup.DatabaseTable) + Environment.NewLine;
            }
            else
            {
                _controllerTemplate += string.Format("                ModuleSummary moduleSummary = await _{0}Repository.GetModuleBussinesLogicSetup();", moduleSetup.DatabaseTable) + Environment.NewLine;
                _controllerTemplate += string.Format("                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == \"{0}Id\").ToList().ForEach(c => c.CurrentValue = ParentPrimaryRecordId);", moduleSetup.ParentModule) + Environment.NewLine;
            }
            //ModuleSummary moduleSummary = await _ModuleSetupRepository.GetModuleBussinesLogicSetup(null, null, true, true);
            _controllerTemplate += string.Format("                moduleSummary.SchemaName = ModuleName.{0}.ToString();", moduleSetup.ModuleName) + Environment.NewLine +

              "List < SqlParameter > sqlParameters = new List<SqlParameter>();" + Environment.NewLine;
            if (moduleSetup.ParentModule == null)
            {
                _controllerTemplate += " " + Environment.NewLine;
            }
            else
            {
                _controllerTemplate += string.Format("sqlParameters.Add(new SqlParameter(\"{0}Id\", ParentPrimaryRecordId));", moduleSetup.ParentModule) + Environment.NewLine;
            }
            _controllerTemplate += "sqlParameters.Add(new SqlParameter(\"PageNumber\", 1));" + Environment.NewLine +
            "sqlParameters.Add(new SqlParameter(\"PageSize\", 20));" + Environment.NewLine +
             string.Format("moduleSummary.SummaryRecord = await _{0}Repository.GetAllByProcedure(\"{1}.SP_Get{0}List\", sqlParameters.ToArray());", moduleSetup.DatabaseTable, moduleSetup.DatabaseSchemaName) + Environment.NewLine +
             "    return View(moduleSummary); " + Environment.NewLine +
            "    } " + Environment.NewLine +
              "    catch (Exception ex)" + Environment.NewLine +
              "    { " + Environment.NewLine +
              "        throw ex;      " + Environment.NewLine +
              "    } " + Environment.NewLine +
              "}     " + Environment.NewLine +
              "     " + Environment.NewLine +
              string.Format("[CRUDAuthorize(ModuleName = ModuleName.{1}, SubModuleName = \"{0}\", Action = CurrentAction.View)]", moduleSetup.DatabaseTable, moduleSetup.ModuleName) + Environment.NewLine +
                  "[ExceptionHandler]    " + Environment.NewLine +
            "[HttpPost]    " + Environment.NewLine +
            "[ValidateAntiForgeryToken]    " + Environment.NewLine +
            "public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)" + Environment.NewLine +
            "{" + Environment.NewLine +
                "try" + Environment.NewLine +
                "{" + Environment.NewLine +
                Environment.NewLine +
                string.Format("ModuleSummary moduleSummary = await _{0}Repository.GetModuleBussinesLogicSetup();",moduleSetup.DatabaseTable) + Environment.NewLine +
                Environment.NewLine +
                "var sqlParameters = SearchParameters.GetSearchParameters(moduleSummary.moduleBussinesLogicSummaries);" + Environment.NewLine +
            Environment.NewLine +
                string.Format("moduleSummary.SummaryRecord = await _{0}Repository.GetAllByProcedure(\"{1}.SP_Get{0}List\", sqlParameters.ToArray());", moduleSetup.DatabaseTable, moduleSetup.DatabaseSchemaName) + Environment.NewLine +
            Environment.NewLine +
                "return PartialView(moduleSummary); " + Environment.NewLine +
        "}" + Environment.NewLine +
            "catch (Exception ex)" + Environment.NewLine +
            "{" + Environment.NewLine +
                "throw ex;" + Environment.NewLine +
            "}" + Environment.NewLine +
        "}" + Environment.NewLine +


        string.Format("[CRUDAuthorize(ModuleName = ModuleName.{0}, SubModuleName = \"{1}\", Action = CurrentAction.Create)]", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
          "[ExceptionHandler]     " + Environment.NewLine;

            _controllerTemplate += "[HttpGet]  " + Environment.NewLine;

            if (moduleSetup.ParentModule == null)
            {
                _controllerTemplate += "public async Task<ActionResult> Create()" + Environment.NewLine;
            }
            else
            {


                _controllerTemplate += "public async Task<ActionResult> Create(Guid ParentPrimaryRecordId)" + Environment.NewLine;
            }





            _controllerTemplate += "{     " + Environment.NewLine +
              "    try    " + Environment.NewLine +
              "    { " + Environment.NewLine +
              "     " + Environment.NewLine;
            if (moduleSetup.ParentModule == null)
            {
                _controllerTemplate += string.Format("ModuleSummary moduleSummary = await _{0}Repository.GetModuleBussinesLogicSetup();", moduleSetup.DatabaseTable) + Environment.NewLine;
            }
            else
            {
                _controllerTemplate += string.Format("ModuleSummary moduleSummary = await _{0}Repository.GetModuleBussinesLogicSetup();", moduleSetup.DatabaseTable) + Environment.NewLine;
                _controllerTemplate += string.Format("moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == \"{0}Id\").ToList().ForEach(c => c.CurrentValue = ParentPrimaryRecordId);", moduleSetup.ParentModule) + Environment.NewLine;
            }
            _controllerTemplate += "if (!moduleSummary.IsParent && moduleSummary.DoRecordExists)" + Environment.NewLine +
            "{" + Environment.NewLine +
            "    return View(\"Details\", moduleSummary);" + Environment.NewLine +
            "}" + Environment.NewLine +
            "else" + Environment.NewLine +
            "{" + Environment.NewLine +
            "    return View(\"Create\", moduleSummary);" + Environment.NewLine +
            "}" + Environment.NewLine +
        "}" + Environment.NewLine +
            "catch (Exception ex)" + Environment.NewLine +
            "{" + Environment.NewLine +
            "    throw ex;" + Environment.NewLine +
            "}" + Environment.NewLine +
            "}" + Environment.NewLine +
            string.Format("[CRUDAuthorize(ModuleName = ModuleName.{0}, SubModuleName = \"{1}\", Action = CurrentAction.Create)]", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
        "[ExceptionHandler]" + Environment.NewLine +
        "[HttpPost]" + Environment.NewLine +
        "[ValidateAntiForgeryToken]" + Environment.NewLine +
        "public async Task<ActionResult> Create(FormCollection model)" + Environment.NewLine +
        "{" + Environment.NewLine +
         "    try    " + Environment.NewLine +
              "    { " + Environment.NewLine +
              "     " + Environment.NewLine +
            string.Format("{0}DTO {1}DTO = new {0}DTO();", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
            string.Format("TryUpdateModel<{0}DTO>({1}DTO);", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
            string.Format("List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<{0}DTO>({1}DTO);", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
             "if (EntityValidationResults.Count() == 0)" + Environment.NewLine +
            "{" + Environment.NewLine +
                "if (Request.Files.Count > 0)" + Environment.NewLine +
                "{" + Environment.NewLine +
                    "HttpFileCollectionBase httpFileCollectionBases = Request.Files;" + Environment.NewLine +
                    "FileUploaderHelper.GetPath(httpFileCollectionBases);" + Environment.NewLine +
                "}" + Environment.NewLine +
                Environment.NewLine +
            string.Format("Guid Id = this._{0}Repository.Add({1}DTO, AuthorizeViewHelper.IsAuthorize(ModuleName.{2}.ToString(), \"{0}\", CurrentAction.AutoAuthorise));", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower(), moduleSetup.ModuleName) + Environment.NewLine +
            "await this._unitOfWork.CommitAsync();" + Environment.NewLine +
             Environment.NewLine +


                "return new JsonHttpStatusResult(new" + Environment.NewLine +
                "{" + Environment.NewLine +
                    "Id = Id," + Environment.NewLine +
                    "IsSuccess = true," + Environment.NewLine +
                    "ResponseView = this.RenderRazorViewToString(\"SuccessfulResponseView\", EntityValidationResults)" + Environment.NewLine +

                "}, System.Net.HttpStatusCode.OK);" + Environment.NewLine +

            "}" + Environment.NewLine +
            "else" + Environment.NewLine +
            "{" + Environment.NewLine +

                "return Json(new" + Environment.NewLine +
                "{" + Environment.NewLine +
                    "IsSuccess = false," + Environment.NewLine +
                    "ResponseView = this.RenderRazorViewToString(\"ValidationResultView\", EntityValidationResults)" + Environment.NewLine +

"                }, JsonRequestBehavior.DenyGet);" + Environment.NewLine +
            "}" + Environment.NewLine +
        "}" + Environment.NewLine +
            "catch (Exception ex)" + Environment.NewLine +
            "{" + Environment.NewLine +
                "throw ex;" + Environment.NewLine +
            "}" + Environment.NewLine +

"}" + Environment.NewLine +

string.Format("[CRUDAuthorize(ModuleName = ModuleName.{0}, SubModuleName = \"{1}\", Action = CurrentAction.View)]", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
          "[ExceptionHandler]     " + Environment.NewLine +
           "[HttpGet]  " + Environment.NewLine +
            "public async Task<ActionResult> Details(Guid Id)" + Environment.NewLine +
             "{" + Environment.NewLine +
         "    try    " + Environment.NewLine +
              "    { " + Environment.NewLine +
              "     " + Environment.NewLine +
              string.Format("var dto = await _{0}Repository.GetDTOById(Id);", moduleSetup.DatabaseTable) + Environment.NewLine;
            if (moduleSetup.ParentModule == null)
            {
                _controllerTemplate += string.Format("ModuleSummary moduleSummary = await _{0}Repository.GetModuleBussinesLogicSetup(dto,null);", moduleSetup.DatabaseTable) + Environment.NewLine;
            }
            else
            {
                _controllerTemplate += string.Format("ModuleSummary moduleSummary = await _{0}Repository.GetModuleBussinesLogicSetup(dto,dto.{1}Id);", moduleSetup.DatabaseTable, moduleSetup.ParentModule) + Environment.NewLine;
            }
            
              _controllerTemplate += "if (Request.IsAjaxRequest())" + Environment.NewLine +
            "{" + Environment.NewLine +
                "return View(\"PartialDetails\", moduleSummary);" + Environment.NewLine +
            "}" + Environment.NewLine +
            "else" + Environment.NewLine +
            "{" + Environment.NewLine +
                "return View(\"Details\", moduleSummary);" + Environment.NewLine +
            "};" + Environment.NewLine +
            "}" + Environment.NewLine +
            "catch (Exception ex)" + Environment.NewLine +
            "{" + Environment.NewLine +
                "throw ex;" + Environment.NewLine +
            "}" + Environment.NewLine +

"}" + Environment.NewLine +


string.Format("[CRUDAuthorize(ModuleName = ModuleName.{0}, SubModuleName = \"{1}\", Action = CurrentAction.Edit)]", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
          "[ExceptionHandler]     " + Environment.NewLine +
           "[HttpPost]  " + Environment.NewLine +
           "public async Task<ActionResult> Update(FormCollection formCollection)" + Environment.NewLine +
           "{" + Environment.NewLine +
         "    try    " + Environment.NewLine +
              "    { " + Environment.NewLine +
              "     " + Environment.NewLine +
              string.Format("{0}DTO {1}DTO = new {0}DTO();", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
            string.Format("TryUpdateModel<{0}DTO>({1}DTO);", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +

            string.Format("List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<{0}DTO>({1}DTO);", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
             "if (EntityValidationResults.Count() == 0)" + Environment.NewLine +
                "{" + Environment.NewLine +

            string.Format("await this._{0}Repository.Update({1}DTO, AuthorizeViewHelper.IsAuthorize(ModuleName.{2}.ToString(), \"{0}\", CurrentAction.AutoAuthorise));", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower(), moduleSetup.ModuleName) + Environment.NewLine +
            "await this._unitOfWork.CommitAsync();" + Environment.NewLine +

                "return Json(new" + Environment.NewLine +
                "{" + Environment.NewLine +
                   string.Format("Id = {0}DTO.Id,", moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                    "IsSuccess = true," + Environment.NewLine +
                    "ResponseMessage = this.RenderRazorViewToString(\"SuccessfulResponseView\", EntityValidationResults)" + Environment.NewLine +

                "}, JsonRequestBehavior.DenyGet);" + Environment.NewLine +

            "}" + Environment.NewLine +
            "else" + Environment.NewLine +
            "{" + Environment.NewLine +
                "return Json(new" + Environment.NewLine +
                "{" + Environment.NewLine +
                    "IsSuccess = false," + Environment.NewLine +
                    "ResponseView = this.RenderRazorViewToString(\"RecordNotFound\", EntityValidationResults)" + Environment.NewLine +

                "}, JsonRequestBehavior.DenyGet);" + Environment.NewLine +

            "}" + Environment.NewLine +

              "}" + Environment.NewLine +
            "catch (Exception ex)" + Environment.NewLine +
            "{" + Environment.NewLine +
                "throw ex;" + Environment.NewLine +
            "}" + Environment.NewLine +

"}" + Environment.NewLine +

string.Format("[CRUDAuthorize(ModuleName = ModuleName.{0}, SubModuleName = \"{1}\", Action = CurrentAction.Delete)]", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
          "[ExceptionHandler]     " + Environment.NewLine +
           "[HttpPost]  " + Environment.NewLine +
           "public async Task<ActionResult> Delete(FormCollection formCollection)" + Environment.NewLine +
           "{" + Environment.NewLine +
         "    try    " + Environment.NewLine +
              "    { " + Environment.NewLine +
              "     " + Environment.NewLine +
              string.Format("{0}DTO {1}DTO = new {0}DTO();", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
            string.Format("TryUpdateModel<{0}DTO>({1}DTO);", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
            string.Format("if ({0}DTO != null)", moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
            "{" + Environment.NewLine +

            string.Format("await this._{0}Repository.Delete({1}DTO.Id, AuthorizeViewHelper.IsAuthorize(ModuleName.{2}.ToString(), \"{0}\", CurrentAction.AutoAuthorise));", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower(), moduleSetup.ModuleName) + Environment.NewLine +
            "await this._unitOfWork.CommitAsync();" + Environment.NewLine +

                "return Json(new" + Environment.NewLine +
                "{" + Environment.NewLine +
                   string.Format("Id = {0}DTO.Id,", moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                    "IsSuccess = true," + Environment.NewLine +
                    "ResponseMessage = this.RenderRazorViewToString(\"SuccessfulResponseView\", null)" + Environment.NewLine +

                "}, JsonRequestBehavior.DenyGet);" + Environment.NewLine +

            "}" + Environment.NewLine +
            "else" + Environment.NewLine +
            "{" + Environment.NewLine +
                "return Json(new" + Environment.NewLine +
                "{" + Environment.NewLine +
                    "IsSuccess = false," + Environment.NewLine +
                    "ResponseView = this.RenderRazorViewToString(\"RecordNotFound\", null)" + Environment.NewLine +

                "}, JsonRequestBehavior.DenyGet);" + Environment.NewLine +

            "}" + Environment.NewLine +

              "}" + Environment.NewLine +
            "catch (Exception ex)" + Environment.NewLine +
            "{" + Environment.NewLine +
                "throw ex;" + Environment.NewLine +
            "}" + Environment.NewLine +

"}" + Environment.NewLine +

            string.Format("[CRUDAuthorize(ModuleName = ModuleName.{0}, SubModuleName = \"{1}\", Action = CurrentAction.Authorise)]", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
                      "[ExceptionHandler]     " + Environment.NewLine +
                       "[HttpPost]  " + Environment.NewLine +
                       "public async Task<ActionResult> Authorise(FormCollection formCollection)" + Environment.NewLine +
                       "{" + Environment.NewLine +
                     "    try    " + Environment.NewLine +
                          "    { " + Environment.NewLine +
                          "     " + Environment.NewLine +
                          string.Format("{0}DTO {1}DTO = new {0}DTO();", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
            string.Format("TryUpdateModel<{0}DTO>({1}DTO);", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
            string.Format("if ({0}DTO != null)", moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
            "{" + Environment.NewLine +

            string.Format("await this._{0}Repository.Authorise({1}DTO.Id);", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                "await this._unitOfWork.CommitAsync();" + Environment.NewLine +

                "return Json(new" + Environment.NewLine +
                "{" + Environment.NewLine +
                    string.Format("Id = {0}DTO.Id,", moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                    "IsSuccess = true," + Environment.NewLine +
                    "ResponseMessage = this.RenderRazorViewToString(\"SuccessfulResponseView\", null)" + Environment.NewLine +

                "}, JsonRequestBehavior.DenyGet);" + Environment.NewLine +

            "}" + Environment.NewLine +
            "else" + Environment.NewLine +
            "{" + Environment.NewLine +
                "return Json(new" + Environment.NewLine +
                "{" + Environment.NewLine +
                    "IsSuccess = false," + Environment.NewLine +
                    "ResponseView = this.RenderRazorViewToString(\"RecordNotFound\", null)" + Environment.NewLine +

                "}, JsonRequestBehavior.DenyGet);" + Environment.NewLine +

            "}" + Environment.NewLine +

                          "}" + Environment.NewLine +
                        "catch (Exception ex)" + Environment.NewLine +
                        "{" + Environment.NewLine +
                            "throw ex;" + Environment.NewLine +
                        "}" + Environment.NewLine +

            "}" + Environment.NewLine +

            string.Format("[CRUDAuthorize(ModuleName = ModuleName.{0}, SubModuleName = \"{1}\", Action = CurrentAction.Revert)]", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
                      "[ExceptionHandler]     " + Environment.NewLine +
                       "[HttpPost]  " + Environment.NewLine +
                       "public async Task<ActionResult> Revert(FormCollection formCollection)" + Environment.NewLine +
                       "{" + Environment.NewLine +
                     "    try    " + Environment.NewLine +
                          "    { " + Environment.NewLine +
                          "     " + Environment.NewLine +
                          string.Format("{0}DTO {1}DTO = new {0}DTO();", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
            string.Format("TryUpdateModel<{0}DTO>({1}DTO);", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
            string.Format("if ({0}DTO != null)", moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
            "{" + Environment.NewLine +

            string.Format("await this._{0}Repository.Revert({1}DTO.Id);", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                "await this._unitOfWork.CommitAsync();" + Environment.NewLine +

                "return Json(new" + Environment.NewLine +
                "{" + Environment.NewLine +
                    string.Format("Id = {0}DTO.Id,", moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                    "IsSuccess = true," + Environment.NewLine +
                    "ResponseMessage = this.RenderRazorViewToString(\"SuccessfulResponseView\", null)" + Environment.NewLine +

                "}, JsonRequestBehavior.DenyGet);" + Environment.NewLine +

            "}" + Environment.NewLine +
            "else" + Environment.NewLine +
            "{" + Environment.NewLine +
                "return Json(new" + Environment.NewLine +
                "{" + Environment.NewLine +
                    "IsSuccess = false," + Environment.NewLine +
                    "ResponseView = this.RenderRazorViewToString(\"RecordNotFound\", null)" + Environment.NewLine +

                "}, JsonRequestBehavior.DenyGet);" + Environment.NewLine +

            "}" + Environment.NewLine +

                          "}" + Environment.NewLine +
                        "catch (Exception ex)" + Environment.NewLine +
                        "{" + Environment.NewLine +
                            "throw ex;" + Environment.NewLine +
                        "}" + Environment.NewLine +

            "}" + Environment.NewLine +

string.Format("[CRUDAuthorize(ModuleName = ModuleName.{0}, SubModuleName = \"{1}\", Action = CurrentAction.Discard)]", moduleSetup.ModuleName, moduleSetup.DatabaseTable) + Environment.NewLine +
                      "[ExceptionHandler]     " + Environment.NewLine +
                       "[HttpPost]  " + Environment.NewLine +
                       "public async Task<ActionResult> Discard(FormCollection formCollection)" + Environment.NewLine +
                       "{" + Environment.NewLine +
                     "    try    " + Environment.NewLine +
                          "    { " + Environment.NewLine +
                          "     " + Environment.NewLine +
                          string.Format("{0}DTO {1}DTO = new {0}DTO();", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
            string.Format("TryUpdateModel<{0}DTO>({1}DTO);", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +

            string.Format("if ({0}DTO != null)", moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
            "{" + Environment.NewLine +

            string.Format("await this._{0}Repository.DiscardChanges({1}DTO.Id);", moduleSetup.DatabaseTable, moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                "await this._unitOfWork.CommitAsync();" + Environment.NewLine +

                "return Json(new" + Environment.NewLine +
                "{" + Environment.NewLine +
                    string.Format("Id = {0}DTO.Id,", moduleSetup.DatabaseTable.ToLower()) + Environment.NewLine +
                    "IsSuccess = true," + Environment.NewLine +
                    "ResponseMessage = this.RenderRazorViewToString(\"SuccessfulResponseView\", null)" + Environment.NewLine +

                "}, JsonRequestBehavior.DenyGet);" + Environment.NewLine +

            "}" + Environment.NewLine +
            "else" + Environment.NewLine +
            "{" + Environment.NewLine +
                "return Json(new" + Environment.NewLine +
                "{" + Environment.NewLine +
                    "IsSuccess = false," + Environment.NewLine +
                    "ResponseView = this.RenderRazorViewToString(\"RecordNotFound\", null)" + Environment.NewLine +

                "}, JsonRequestBehavior.DenyGet);" + Environment.NewLine +

            "}" + Environment.NewLine +



            "}" + Environment.NewLine +
                        "catch (Exception ex)" + Environment.NewLine +
                        "{" + Environment.NewLine +
                            "throw ex;" + Environment.NewLine +
                        "}" + Environment.NewLine +

            "}" + Environment.NewLine +
            "}" + Environment.NewLine +
        "}" + Environment.NewLine;
            return _controllerTemplate;

        }
    }
}
