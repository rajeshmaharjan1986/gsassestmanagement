using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GSAssetManagement.CodeGenerator.Model
{
    public class ChildTableInformation
    {
        public ChildTableInformation()
        {
        }
        public Guid ModuleSetupId { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public int OrderValue { get; set; }
    }
}
