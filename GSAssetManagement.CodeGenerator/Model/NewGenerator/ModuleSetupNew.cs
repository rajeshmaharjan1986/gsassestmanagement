using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace GSAssetManagement.CodeGenerator.Model
{
    public class ModuleSetupNew
    {
        public ModuleSetupNew() {
            ModuleBussinesLogicSetups = new List<ModuleBussinesLogicSetup>();
            ChildTableInformations = new List<ChildTableInformation>();
        }
        public Guid Id { get; set; }
        public string ModuleName { get; set; }
        public string DatabaseSchemaName { get; set; }
        public string Name { get; set; }
        public string ModuleCode { get; set; }
        public string Description { get; set; }
        public string DatabaseTable { get; set; }
        public string ApplicationClass { get; set; }
        public string EntryType { get; set; }
        public bool IsParent { get; set; }
        public string ParentModule { get; set; }
        public List<ModuleBussinesLogicSetup> ModuleBussinesLogicSetups { get; set; }
        public List<ChildTableInformation> ChildTableInformations { get; set; }
    }

    public class ModuleSetupNewList {

        public ModuleSetupNewList() {
            ModuleSetupNews = new List<ModuleSetupNew>();
            ModuleBussinesLogicSetups = new List<ModuleBussinesLogicSetup>();
            ChildTableInformations = new List<ChildTableInformation>();
        }
        public List<ModuleSetupNew> ModuleSetupNews { get; set; }
        public List<ModuleBussinesLogicSetup> ModuleBussinesLogicSetups { get; set; }
        public List<ChildTableInformation> ChildTableInformations { get; set; }
    }
}
