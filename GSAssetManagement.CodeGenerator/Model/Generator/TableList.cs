﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.CodeGenerator
{
    public class TableList
    {
        public string TableName { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
