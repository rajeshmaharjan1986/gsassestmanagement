﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.CodeGenerator.Model
{
    public class ModuleRuleSetup
    {
        public string Module { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string FieldTitle { get; set; }
        public string DataType { get; set; }
        public string HtmlDataType { get; set; }
        public bool IsRequired { get; set; }
        public string Regex { get; set; }
        public string Length { get; set; }
        public string ErrorMessage { get; set; }
        public string HelpMessage { get; set; }
        public Nullable<int> RangeForm { get; set; }
        public Nullable<int> RangeTo { get; set; }
        public bool IsUpdateable { get; set; }
        public int Order { get; set; }
        public bool SummaryHeader { get; set; }
        public bool Enable { get; set; }
        public bool SearchField { get; set; }
        public bool Hidden { get; set; }
        public string HtmlClass { get; set; }
        public bool ReadOnly { get; set; }
        public bool IsForeignKey { get; set; }
        public string ForeignModule { get; set; }
        public string ForeignTable { get; set; }
        public string ForeignColumn { get; set; }
        public string ForeignColumnAlais { get; set; }
        public string ForeignTableAlais { get; set; }
        public string Condition { get; set; }
        public bool EnableRemoteValidation { get; set; }
        public string RemoteValidationService { get; set; }
        public string RemoteParameters { get; set; }
        public string Datasource { get; set; }
        public bool ParameterisedSource { get; set; }
        public string Parameter { get; set; }

    }
}
