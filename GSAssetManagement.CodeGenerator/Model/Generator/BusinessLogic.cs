﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.CodeGenerator
{
    public class BusinessLogic
    {
        public string Module { get; set; }
        public string SubModuleName { get; set; }
        public string URL { get; set; }
        public string EntityName { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }

        public string FieldTitle { get; set; }
        public string DataType { get; set; }
        public string HtmlDataType { get; set; }
        public bool IsRequired { get; set; }
        public string Regex { get; set; }
        public Nullable<int> Length { get; set; }
        public string ErrorMessage { get; set; }
        public string HelpMessage { get; set; }
        public Nullable<int> RangeForm { get; set; }
        public Nullable<int> RangeTo { get; set; }
        public bool IsUpdateable { get; set; }
        public int Order { get; set; }
        public bool SummaryHeader { get; set; }
        public bool Enable { get; set; }
        public bool SearchField { get; set; }
        public bool Hidden { get; set; }
        public string HtmlClass { get; set; }
        public string ChildTables { get; set; }
        public string ParentTables { get; set; }
        public bool IsParent { get; set; }
        public bool IsParentKey { get; set; }

    }
}
