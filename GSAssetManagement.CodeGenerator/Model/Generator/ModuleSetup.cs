﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.CodeGenerator.Model
{
    public class ModuleSetup
    {

        public ModuleSetup()
        {
            ModuleRuleSetupList = new List<ModuleRuleSetup>();
            ChildLists = new List<Tuple<string, string>>();
            ParentLists = new List<string>();
        }

        public string ProjectName { get; set; }
        public string Module { get; set; }
        public string TableTitle { get; set; }
        public string DatabaseTable { get; set; }
        public string ControllerName { get; set; }
        public string ChildTable { get; set; }
        public string ParentTable { get; set; }
        public List<ModuleRuleSetup> ModuleRuleSetupList { get; set; }
        public List<Tuple<string,string>> ChildLists { get; set; }
        public List<string> ParentLists { get; set; }

    }
}
