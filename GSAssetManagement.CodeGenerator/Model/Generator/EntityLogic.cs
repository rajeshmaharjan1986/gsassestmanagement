﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.CodeGenerator
{
    public class EntityLogic
    {
        public string SchemaType { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string DataType { get; set; }
        public int ColumnId { get; set; }
        public int DataLength { get; set; }        
        public string IsNullable { get; set; }
        public bool SearchFeild { get; set; }
        public bool DisplayFeild { get; set; }
    }
}
