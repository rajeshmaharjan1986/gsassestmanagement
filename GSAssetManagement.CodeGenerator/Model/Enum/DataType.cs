﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSAssetManagement.CodeGenerator
{
    public enum DataType
    {
        p_datetime=1,
        p_decimal,
        p_double,
        p_int,
        p_list,
        p_long,
        p_object,
        p_string,
        p_guid

    }
}
