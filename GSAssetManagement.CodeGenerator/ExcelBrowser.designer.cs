﻿namespace GSAssetManagement.CodeGenerator
{
    partial class ExcelBrowser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnFileUpload = new System.Windows.Forms.Button();
            this.btnModuleDataSetup = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnFileUpload
            // 
            this.btnFileUpload.Location = new System.Drawing.Point(348, 86);
            this.btnFileUpload.Name = "btnFileUpload";
            this.btnFileUpload.Size = new System.Drawing.Size(254, 83);
            this.btnFileUpload.TabIndex = 0;
            this.btnFileUpload.Text = "Upload Excel";
            this.btnFileUpload.UseVisualStyleBackColor = true;
            this.btnFileUpload.Click += new System.EventHandler(this.btnFileUpload_Click);
            // 
            // btnModuleDataSetup
            // 
            this.btnModuleDataSetup.Location = new System.Drawing.Point(43, 86);
            this.btnModuleDataSetup.Name = "btnModuleDataSetup";
            this.btnModuleDataSetup.Size = new System.Drawing.Size(262, 83);
            this.btnModuleDataSetup.TabIndex = 1;
            this.btnModuleDataSetup.Text = "Migrate Module Data";
            this.btnModuleDataSetup.UseVisualStyleBackColor = true;
            this.btnModuleDataSetup.Click += new System.EventHandler(this.btnModuleDataSetup_Click);
            // 
            // ExcelBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 345);
            this.Controls.Add(this.btnModuleDataSetup);
            this.Controls.Add(this.btnFileUpload);
            this.Name = "ExcelBrowser";
            this.Text = "ExcelBrowser";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnFileUpload;
        private System.Windows.Forms.Button btnModuleDataSetup;
    }
}