﻿using GSAssetManagement.CodeGenerator.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using GSAssetManagement.CodeGenerator.Utility;

namespace GSAssetManagement.CodeGenerator
{
    public partial class GenerateMVC : Form
    {
        private string _applicationPath;
        public GenerateMVC()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<ModuleSetupNew> _applicationModuleSetupList = ModuleDataReader.GetDataInformation();


            _applicationPath = ConfigurationManager.AppSettings["path"].ToString();

            _applicationModuleSetupList.ForEach(g =>
            {
                var _entityControllerTemplate = ControllerGeneratorNew.Generate(g);
                if (_entityControllerTemplate != string.Empty)
                {
                    var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Web\Areas\{0}\Controllers", g.ModuleName);
                    string path = _diectoryPath + @"\" + g.DatabaseTable + "Controller" + ".cs";

                    if (!Directory.Exists(_diectoryPath))
                    {
                        Directory.CreateDirectory(_diectoryPath);
                        File.WriteAllText(path, _entityControllerTemplate);
                    }
                    else
                    {
                        File.WriteAllText(path, _entityControllerTemplate);
                    }

                }
            });

            _applicationModuleSetupList.ForEach(g =>
            {


                var _razorindexTemplate = NewRazorGenerator.GenerateIndex(g);
                if (_razorindexTemplate != string.Empty)
                {
                    var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Web\Areas\{0}\Views\{1}", g.ModuleName, g.DatabaseTable);
                    string path = _diectoryPath + @"\" + "Index" + ".cshtml";

                    if (!Directory.Exists(_diectoryPath))
                    {
                        Directory.CreateDirectory(_diectoryPath);
                        File.WriteAllText(path, _razorindexTemplate);
                    }
                    else
                    {
                        File.WriteAllText(path, _razorindexTemplate);
                    }

                }
            });

            _applicationModuleSetupList.ForEach(g =>
            {
                var _razorpartialindexTemplate = NewRazorGenerator.GenerateSearchIndex(g);
                if (_razorpartialindexTemplate != string.Empty)
                {
                    var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Web\Areas\{0}\Views\{1}", g.ModuleName, g.DatabaseTable);
                    string path = _diectoryPath + @"\" + "SearchIndex" + ".cshtml";

                    if (!Directory.Exists(_diectoryPath))
                    {
                        Directory.CreateDirectory(_diectoryPath);
                        File.WriteAllText(path, _razorpartialindexTemplate);
                    }
                    else
                    {
                        File.WriteAllText(path, _razorpartialindexTemplate);
                    }

                }
            });

            _applicationModuleSetupList.ForEach(g =>
            {
                var _razorcreateTemplate = NewRazorGenerator.GenerateCreate(g);
                if (_razorcreateTemplate != string.Empty)
                {
                    var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Web\Areas\{0}\Views\{1}", g.ModuleName, g.DatabaseTable);
                    string path = _diectoryPath + @"\" + "Create" + ".cshtml";

                    if (!Directory.Exists(_diectoryPath))
                    {
                        Directory.CreateDirectory(_diectoryPath);
                        File.WriteAllText(path, _razorcreateTemplate);
                    }
                    else
                    {
                        File.WriteAllText(path, _razorcreateTemplate);
                    }

                }
            });

            _applicationModuleSetupList.ForEach(g =>
            {
                var _razordetailsTemplate = NewRazorGenerator.GenerateDetails(g);
                if (_razordetailsTemplate != string.Empty)
                {
                    var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Web\Areas\{0}\Views\{1}", g.ModuleName, g.DatabaseTable);
                    string path = _diectoryPath + @"\" + "Details" + ".cshtml";

                    if (!Directory.Exists(_diectoryPath))
                    {
                        Directory.CreateDirectory(_diectoryPath);
                        File.WriteAllText(path, _razordetailsTemplate);
                    }
                    else
                    {
                        File.WriteAllText(path, _razordetailsTemplate);
                    }

                }
            });

            _applicationModuleSetupList.Where(v => !string.IsNullOrEmpty(v.ParentModule)).ToList().ForEach(g =>
            {
                var _razordetailsTemplate = NewRazorGenerator.GeneratePartialDetails(g);
                if (_razordetailsTemplate != string.Empty)
                {
                    var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Web\Areas\{0}\Views\{1}", g.ModuleName, g.DatabaseTable);
                    string path = _diectoryPath + @"\" + "PartialDetails" + ".cshtml";

                    if (!Directory.Exists(_diectoryPath))
                    {
                        Directory.CreateDirectory(_diectoryPath);
                        File.WriteAllText(path, _razordetailsTemplate);
                    }
                    else
                    {
                        File.WriteAllText(path, _razordetailsTemplate);
                    }

                }
            });

            _applicationModuleSetupList.ForEach(g =>
            {

                var _storeprocedureTemplate = RepositoryGeneratorNew.Generate(g);
                if (_storeprocedureTemplate != string.Empty)
                {
                    var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Repository\Repository\{0}\", g.ModuleName);
                    string path = _diectoryPath + @"\" + g.DatabaseTable + "Repository" + ".cs";

                    if (!Directory.Exists(_diectoryPath))
                    {
                        Directory.CreateDirectory(_diectoryPath);
                        File.WriteAllText(path, _storeprocedureTemplate);
                    }
                    else
                    {
                        File.WriteAllText(path, _storeprocedureTemplate);
                    }

                }
            });

            _applicationModuleSetupList.ForEach(g =>
            {

                var _storeprocedureTemplate = StoreprocedureGeneratorNew.GenerateList(g);
                if (_storeprocedureTemplate != string.Empty)
                {
                    var _diectoryPath = _applicationPath + @"\GSAssetManagement.Database\Stored Procedures";//string.Format(@"\GSAssetManagement.Database\{0}\Stored Procedures\{1}", g.ModuleName, g.DatabaseTable);
                    string path = _diectoryPath + @"\" + "SP_Get" + g.DatabaseTable + "List" + ".sql";

                    if (!Directory.Exists(_diectoryPath))
                    {
                        Directory.CreateDirectory(_diectoryPath);
                        File.WriteAllText(path, _storeprocedureTemplate);
                    }
                    else
                    {
                        File.WriteAllText(path, _storeprocedureTemplate);
                    }

                }
            });


            MessageBox.Show("Successfully code generated.");
        }
    }
}
