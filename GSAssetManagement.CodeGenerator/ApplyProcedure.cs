﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Entity;

namespace GSAssetManagement.CodeGenerator
{
    public partial class ApplyProcedure : Form
    {
        public static List<TableList> _storeProceudreList = new List<TableList>();
        public ApplyProcedure()
        {
            InitializeComponent();
        }

        private async void btnCreateProcedure_Click(object sender, EventArgs e)
        {
            try
            {

                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    await _connection.OpenAsync();
                    using (SqlCommand _commnd = new SqlCommand("SELECT name +'.sql' TableName,modify_date ModifiedDate FROM sys.procedures WHERE type='P'", _connection))
                    {
                        using (SqlDataReader _reader = await _commnd.ExecuteReaderAsync())
                        {
                            DataTable _table = new DataTable();
                            _table.Load(_reader);

                            _storeProceudreList.AddRange(_table.ToList<TableList>());


                        }
                    }
                }


                await ProcessDirectory(string.Format(@"E:\Nabil\irm-v1\GSAssetManagement.Database\{0}\Stored Procedures",ModuleName.AdminSetting.ToString()));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task ProcessDirectory(string targetDirectory)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries)
                await ProcessFile(fileName);

            // Recurse into subdirectories of this directory.
            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string subdirectory in subdirectoryEntries)
                await ProcessDirectory(subdirectory);
        }

        public async static Task ProcessFile(string path)
        {

            var filename = Path.GetFileName(path);

            DateTime lastModified = System.IO.File.GetLastWriteTime(path);


            if (_storeProceudreList.Where(x => x.TableName == filename).Count() == 0)
            {

                string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                var scripts = File.ReadAllText(path);

                using (SqlConnection _connection = new SqlConnection(_connectionString))
                {
                    try
                    {
                        await _connection.OpenAsync();
                        using (SqlCommand _commnd = new SqlCommand(scripts, _connection))
                        {
                            await _commnd.ExecuteNonQueryAsync();

                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }

            //if ((_storeProceudreList.Where(x => x.TableName == filename).Count() > 0))
            //{
            //    string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

            //    var scripts = File.ReadAllText(path);

            //    using (SqlConnection _connection = new SqlConnection(_connectionString))
            //    {
            //        try
            //        {
            //            await _connection.OpenAsync();
            //            using (SqlCommand _commnd = new SqlCommand(scripts.Replace("CREATE PROCEDURE", "ALTER PROCEDURE"), _connection))
            //            {
            //                await _commnd.ExecuteNonQueryAsync();

            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            throw ex;
            //        }
            //    }
            //}
        }
    }
}
