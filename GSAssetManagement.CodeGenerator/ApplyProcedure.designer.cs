﻿namespace GSAssetManagement.CodeGenerator
{
    partial class ApplyProcedure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreateProcedure = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCreateProcedure
            // 
            this.btnCreateProcedure.Location = new System.Drawing.Point(28, 84);
            this.btnCreateProcedure.Name = "btnCreateProcedure";
            this.btnCreateProcedure.Size = new System.Drawing.Size(228, 92);
            this.btnCreateProcedure.TabIndex = 1;
            this.btnCreateProcedure.Text = "Create Procedure";
            this.btnCreateProcedure.UseVisualStyleBackColor = true;
            this.btnCreateProcedure.Click += new System.EventHandler(this.btnCreateProcedure_Click);
            // 
            // ApplyProcedure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnCreateProcedure);
            this.Name = "ApplyProcedure";
            this.Text = "ApplyProcedure";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCreateProcedure;
    }
}