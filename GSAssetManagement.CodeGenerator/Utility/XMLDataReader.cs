﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using GSAssetManagement.CodeGenerator.Model;
using GSAssetManagement.Infrastructure.Core;

namespace GSAssetManagement.CodeGenerator.Utility
{
    public class XMLDataReader
    {
        public static List<ModuleSetupNew> GetXMLDataInformation()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        using (SqlCommand _commnd = new SqlCommand("GetModuleSetupData", connection))
                        {
                            _commnd.CommandTimeout = 6000;

                            _commnd.CommandType = System.Data.CommandType.StoredProcedure;


                            using (XmlReader _reader = _commnd.ExecuteXmlReader())
                            {
                                XmlRootAttribute xRoot = new XmlRootAttribute();
                                xRoot.ElementName = "ModuleSetupNewList";
                                xRoot.IsNullable = false;

                                XmlSerializer s = CachingXmlSerializerFactory.Create(typeof(ModuleSetupNewList), xRoot);
                                var goamldatainformation = (ModuleSetupNewList)s.Deserialize(_reader);

                                return goamldatainformation.ModuleSetupNews;

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ModuleSetupNew> GetXMLDataInformationFromFile()
        {
            string RootPath = ConfigurationManager.AppSettings["RootPath"].ToString();

            System.Xml.Serialization.XmlRootAttribute xRoot = new System.Xml.Serialization.XmlRootAttribute();
            xRoot.ElementName = "ModuleSetupNewList";
            xRoot.IsNullable = false;

            var file = File.OpenText(RootPath + @"\Schema\NabilXML.xml");

            System.Xml.Serialization.XmlSerializer s = CachingXmlSerializerFactory.Create(typeof(ModuleSetupNewList), xRoot);
            var goamldatainformation = (ModuleSetupNewList)s.Deserialize(file);

            return goamldatainformation.ModuleSetupNews;
        }
    }
}
