﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSAssetManagement.CodeGenerator.Model;
using GSAssetManagement.Infrastructure;

namespace GSAssetManagement.CodeGenerator.Utility
{
    public class ModuleDataReader
    {
        public static List<ModuleSetupNew> GetDataInformation()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            try
            {
                var moduleSetupList = new List<ModuleSetupNew>();
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        using (SqlCommand _commnd = new SqlCommand("GetNewModuleSetupData", connection))
                        {
                            _commnd.CommandTimeout = 6000;

                            _commnd.CommandType = System.Data.CommandType.StoredProcedure;

                            SqlDataAdapter _reader = new SqlDataAdapter(_commnd);

                            DataSet dataSet = new DataSet();
                            _reader.Fill(dataSet);

                            var moduleSetupNews = dataSet.Tables[0].ToList<ModuleSetupNew>();
                            var moduleBussinesLogicSetups = dataSet.Tables[1].ToList<ModuleBussinesLogicSetup>();
                            var childTableInformations = dataSet.Tables[2].ToList<ChildTableInformation>();

                            foreach (ModuleSetupNew moduleSetupNew in moduleSetupNews) {
                                moduleSetupNew.ModuleBussinesLogicSetups.AddRange(moduleBussinesLogicSetups.Where(n => n.ModuleSetupId == moduleSetupNew.Id));
                                moduleSetupNew.ChildTableInformations.AddRange(childTableInformations.Where(c => c.ModuleSetupId == moduleSetupNew.Id));
                                moduleSetupList.Add(moduleSetupNew);
                            }

                        }



                        return moduleSetupList;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
