﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Configuration;
using GSAssetManagement.CodeGenerator.Model;
using GSAssetManagement.Data;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;


namespace GSAssetManagement.CodeGenerator
{
    public partial class ExcelBrowser : Form
    {
        private string _applicationPath;
        public ExcelBrowser()
        {
            InitializeComponent();
        }

        private void btnFileUpload_Click(object sender, EventArgs e)
        {
            List<ModuleSetup> _applicationModuleSetupList = new List<ModuleSetup>();
            List<ModuleRuleSetup> _applicationModuleRuleSetupList = new List<ModuleRuleSetup>();
            List<Tuple<string, string>> _chilTables = new List<Tuple<string, string>>();


            _applicationPath = ConfigurationManager.AppSettings["path"].ToString();

            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "All Files (*.*)|*.*";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = true;

            if (choofdlog.ShowDialog() == DialogResult.OK)
            {


                FileInfo _info = new FileInfo(choofdlog.FileName);
                ExcelPackage _openXml = new ExcelPackage(_info);
                var _workSheets = _openXml.Workbook.Worksheets;
                _workSheets.ToList().ForEach(x =>
                {
                    if (x.Name == "ModuleSetup")
                    {
                        DataTable _workSheetDT = WorksheetToDataTable(x);
                        _applicationModuleSetupList.AddRange(_workSheetDT.ToList<ModuleSetup>().ToList());


                    }

                    if (x.Name == "ModuleRuleSetup")
                    {
                        DataTable _workSheetDT = WorksheetToDataTable(x);

                        _applicationModuleRuleSetupList.AddRange(_workSheetDT.ToList<ModuleRuleSetup>().ToList());


                    }

                });


                _applicationModuleSetupList.ForEach(c =>
                {
                    if (!string.IsNullOrEmpty(c.ChildTable))
                    {

                        c.ChildTable.Split(',').ToList().ForEach(v =>
                        {
                            string _tableName = _applicationModuleSetupList.Where(x => x.Module == c.Module && x.DatabaseTable == v).Select(z => z.DatabaseTable
                       ).FirstOrDefault();

                            string _title = _applicationModuleSetupList.Where(x => x.Module == c.Module && x.DatabaseTable == v).Select(z => z.TableTitle
                           ).FirstOrDefault();


                            if (!string.IsNullOrEmpty(_tableName) && !string.IsNullOrEmpty(_title))
                                c.ChildLists.Add(new Tuple<string, string>(_tableName, _title));

                        });
                    }

                    if (!string.IsNullOrEmpty(c.ParentTable))
                    {
                        c.ParentLists.Add(c.ParentTable);
                    }

                    c.ModuleRuleSetupList.AddRange(_applicationModuleRuleSetupList.Where(x => x.Module == c.Module && x.TableName == c.DatabaseTable));

                });



                _applicationModuleSetupList.ForEach(V =>
                {


                    V.ModuleRuleSetupList.ForEach(R =>
                    {


                        if (R.DataType == "DateTime")
                            R.HtmlClass = "form-control datepicker";
                        else if (R.HtmlDataType == "select" && !R.ParameterisedSource)
                            R.HtmlClass = "form-control dropdown";
                        else if (R.HtmlDataType == "select" && R.ParameterisedSource)
                            R.HtmlClass = "form-control parameteriseddropdown";
                        else
                            R.HtmlClass = "form-control";



                        if (R.IsForeignKey && R.SummaryHeader && !R.Hidden && R.DataType != "Guid")
                        {

                            R.Condition = string.Format("{0}.{1} = {2}.{3}", R.TableName, R.ColumnName, R.ForeignTableAlais, "Id");
                        }

                        if (!R.IsForeignKey && R.HtmlDataType == "select" && R.SummaryHeader && !R.Hidden && R.DataType != "Guid")
                        {
                            R.Condition = string.Format("{0}.{1} = {2}.Value AND {2}.ColumnName = ''{1}''", R.TableName, R.ColumnName, R.ForeignTableAlais);
                        }





                        R.EnableRemoteValidation = R.EnableRemoteValidation;
                        R.RemoteValidationService = R.RemoteValidationService;
                        R.RemoteParameters = R.RemoteParameters;

                        if (R.HtmlDataType == "select" && R.DataType != "bool")
                        {

                            if (R.HtmlDataType == "select")
                            {
                                if (R.ForeignTable == "StaticDataDetails")
                                {
                                    if (R.ParameterisedSource)
                                    {
                                        string parameterCondition = string.Empty;
                                        int i = 1;

                                        R.Parameter.Split(',').ToList().ForEach(c =>
                                        {
                                            if (i == 1)
                                            {
                                                parameterCondition += string.Format("Parameter{1} = {0}", c.Replace("*.", "@"), i);
                                            }
                                            else
                                            {
                                                parameterCondition += string.Format("AND Parameter{1} = {0}", c.Replace("*.", "@"), i);
                                            }

                                            i += 1;
                                        });

                                        R.Datasource = string.Format("SELECT '{0}' ColumnName, Title, Value, OrderValue FROM Setting.StaticDataDetails WHERE ColumnName ='{0}' AND RecordStatus = 2 AND {1}", R.ColumnName, parameterCondition);

                                    }
                                    else
                                    {
                                        R.Datasource = string.Format("SELECT '{0}' ColumnName, Title, Value, OrderValue FROM Setting.StaticDataDetails WHERE ColumnName ='{0}' AND RecordStatus = 2", R.ColumnName);
                                    }
                                }
                                else
                                {
                                    if (R.ParameterisedSource)
                                    {
                                        string parameterCondition = string.Empty;
                                        int i = 1;

                                        R.Parameter.Split(',').ToList().ForEach(c =>
                                        {
                                            if (i == 1)
                                            {
                                                parameterCondition += string.Format("{1} = {0}", c.Replace("*.", "@"), c.Replace("*.", ""));
                                            }
                                            else
                                            {
                                                parameterCondition += string.Format("AND {1} = {0}", c.Replace("*.", "@"), c.Replace("*.", ""));
                                            }

                                            i += 1;
                                        });

                                        R.Datasource = string.Format("SELECT '{0}' ColumnName,  {1} Title, CONVERT(NVARCHAR(50),Id) Value,CONVERT(NVARCHAR,CreatedDate) OrderValue FROM {2}.{3} WHERE RecordStatus = 2 AND {4}", R.ColumnName, R.ForeignColumn, R.Module, R.ForeignTable, parameterCondition);

                                    }
                                    else
                                    {
                                        R.Datasource = string.Format("SELECT '{0}' ColumnName,  {1} Title, CONVERT(NVARCHAR(50),Id) Value,CONVERT(NVARCHAR,CreatedDate) OrderValue FROM {2}.{3} WHERE RecordStatus = 2", R.ColumnName, R.ForeignColumn, R.Module, R.ForeignTable);
                                    }
                                }

                            }
                        }
                        else if (R.DataType == "select" && R.DataType == "bool")
                        {
                            R.Datasource = string.Format("SELECT '{0}' ColumnName, Title, Value, OrderValue FROM Setting.StaticDataDetails WHERE ColumnName ='Argument' AND RecordStatus = 2", R.ColumnName);
                        }


                    });


                });


                _applicationModuleSetupList.ForEach(g =>
                {

                    var _entityTemplate = EntityGenerator.Generate(g);
                    if (_entityTemplate != string.Empty)
                    {
                        var _diectoryPath = _applicationPath + string.Format(@"\{1}.Entity\Entity\{0}\", g.Module, g.ProjectName);
                        string path = _diectoryPath + @"\" + g.DatabaseTable + ".cs";

                        if (!Directory.Exists(_diectoryPath))
                        {
                            Directory.CreateDirectory(_diectoryPath);
                            File.WriteAllText(path, _entityTemplate);
                        }
                        else
                        {
                            File.WriteAllText(path, _entityTemplate);
                        }

                    }


                    var _entityDTOTemplate = EntityGenerator.GenerateDTO(g);
                    if (_entityDTOTemplate != string.Empty)
                    {
                        var _diectoryPath = _applicationPath + string.Format(@"\{1}.Entity\DTO\{0}\", g.Module, g.ProjectName);
                        string path = _diectoryPath + @"\" + g.DatabaseTable + "DTO.cs";

                        if (!Directory.Exists(_diectoryPath))
                        {
                            Directory.CreateDirectory(_diectoryPath);
                            File.WriteAllText(path, _entityDTOTemplate);
                        }
                        else
                        {
                            File.WriteAllText(path, _entityDTOTemplate);
                        }

                    }


                });




                _applicationModuleSetupList.ForEach(g =>
                {


                    var _entityRepositoryTemplate = RepositoryGeneratorLatest.Generate(g);
                    if (_entityRepositoryTemplate != string.Empty)
                    {
                        var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Repository\Repository\{0}\", g.Module);
                        string path = _diectoryPath + @"\" + g.DatabaseTable + "Repository" + ".cs";

                        if (!Directory.Exists(_diectoryPath))
                        {
                            Directory.CreateDirectory(_diectoryPath);
                            File.WriteAllText(path, _entityRepositoryTemplate);
                        }
                        else
                        {
                            File.WriteAllText(path, _entityRepositoryTemplate);
                        }

                    }
                });

                _applicationModuleSetupList.ForEach(g =>
                {


                    var _entityControllerTemplate = ControllerGenerator.Generate(g);
                    if (_entityControllerTemplate != string.Empty)
                    {
                        var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Web\Areas\{0}\Controllers", g.Module);
                        string path = _diectoryPath + @"\" + g.DatabaseTable + "Controller" + ".cs";

                        if (!Directory.Exists(_diectoryPath))
                        {
                            Directory.CreateDirectory(_diectoryPath);
                            File.WriteAllText(path, _entityControllerTemplate);
                        }
                        else
                        {
                            File.WriteAllText(path, _entityControllerTemplate);
                        }

                    }
                });

                _applicationModuleSetupList.ForEach(g =>
                    {


                        var _razorindexTemplate = RazorGenerator.GenerateIndex(g);
                        if (_razorindexTemplate != string.Empty)
                        {
                            var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Web\Areas\{0}\Views\{1}", g.Module, g.DatabaseTable);
                            string path = _diectoryPath + @"\" + "Index" + ".cshtml";

                            if (!Directory.Exists(_diectoryPath))
                            {
                                Directory.CreateDirectory(_diectoryPath);
                                File.WriteAllText(path, _razorindexTemplate);
                            }
                            else
                            {
                                File.WriteAllText(path, _razorindexTemplate);
                            }

                        }
                    });

                _applicationModuleSetupList.ForEach(g =>
                {
                    var _razorpartialindexTemplate = RazorGenerator.GeneratePartialIndex(g);
                    if (_razorpartialindexTemplate != string.Empty)
                    {
                        var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Web\Areas\{0}\Views\{1}", g.Module, g.DatabaseTable);
                        string path = _diectoryPath + @"\" + "PartialIndex" + ".cshtml";

                        if (!Directory.Exists(_diectoryPath))
                        {
                            Directory.CreateDirectory(_diectoryPath);
                            File.WriteAllText(path, _razorpartialindexTemplate);
                        }
                        else
                        {
                            File.WriteAllText(path, _razorpartialindexTemplate);
                        }

                    }
                });

                _applicationModuleSetupList.ForEach(g =>
                {
                    var _razorcreateTemplate = RazorGenerator.GenerateCreate(g);
                    if (_razorcreateTemplate != string.Empty)
                    {
                        var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Web\Areas\{0}\Views\{1}", g.Module, g.DatabaseTable);
                        string path = _diectoryPath + @"\" + "Create" + ".cshtml";

                        if (!Directory.Exists(_diectoryPath))
                        {
                            Directory.CreateDirectory(_diectoryPath);
                            File.WriteAllText(path, _razorcreateTemplate);
                        }
                        else
                        {
                            File.WriteAllText(path, _razorcreateTemplate);
                        }

                    }
                });

                _applicationModuleSetupList.ForEach(g =>
                {
                    var _razordetailsTemplate = RazorGenerator.GenerateDetails(g);
                    if (_razordetailsTemplate != string.Empty)
                    {
                        var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Web\Areas\{0}\Views\{1}", g.Module, g.DatabaseTable);
                        string path = _diectoryPath + @"\" + "Details" + ".cshtml";

                        if (!Directory.Exists(_diectoryPath))
                        {
                            Directory.CreateDirectory(_diectoryPath);
                            File.WriteAllText(path, _razordetailsTemplate);
                        }
                        else
                        {
                            File.WriteAllText(path, _razordetailsTemplate);
                        }

                    }
                });

                _applicationModuleSetupList.Where(v => string.IsNullOrEmpty(v.ParentTable)).ToList().ForEach(g =>
                  {
                      var _razordetailsTemplate = RazorGenerator.GeneratePartialDetails(g);
                      if (_razordetailsTemplate != string.Empty)
                      {
                          var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Web\Areas\{0}\Views\{1}", g.Module, g.DatabaseTable);
                          string path = _diectoryPath + @"\" + "PartialDetails" + ".cshtml";

                          if (!Directory.Exists(_diectoryPath))
                          {
                              Directory.CreateDirectory(_diectoryPath);
                              File.WriteAllText(path, _razordetailsTemplate);
                          }
                          else
                          {
                              File.WriteAllText(path, _razordetailsTemplate);
                          }

                      }
                  });


                _applicationModuleSetupList.ForEach(g =>
                {

                    var _storeprocedureTemplate = StoreprocedureGenerator.GenerateList(g);
                    if (_storeprocedureTemplate != string.Empty)
                    {
                        var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Database\{0}\Stored Procedures\{1}", g.Module, g.DatabaseTable);
                        string path = _diectoryPath + @"\" + "IRM_SP_Get" + g.DatabaseTable + "List" + ".sql";

                        if (!Directory.Exists(_diectoryPath))
                        {
                            Directory.CreateDirectory(_diectoryPath);
                            File.WriteAllText(path, _storeprocedureTemplate);
                        }
                        else
                        {
                            File.WriteAllText(path, _storeprocedureTemplate);
                        }

                    }
                });

                _applicationModuleSetupList.ForEach(g =>
                {
                    var _storeprocedureDetailsTemplate = StoreprocedureGenerator.GenerateDetails(g);
                    if (_storeprocedureDetailsTemplate != string.Empty)
                    {
                        var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Database\{0}\Stored Procedures\{1}", g.Module, g.DatabaseTable);
                        string path = _diectoryPath + @"\" + "IRM_SP_Get" + g.DatabaseTable + "Details" + ".sql";

                        if (!Directory.Exists(_diectoryPath))
                        {
                            Directory.CreateDirectory(_diectoryPath);
                            File.WriteAllText(path, _storeprocedureDetailsTemplate);
                        }
                        else
                        {
                            File.WriteAllText(path, _storeprocedureDetailsTemplate);
                        }

                    }
                });


                _applicationModuleSetupList.Where(c => string.IsNullOrEmpty(c.ParentTable) || c.ParentTable == "NULL").ToList().ForEach(g =>
                 {
                     var _javascriptUITemplate = JavascriptGenerator.GenerateUIScript(g);

                     if (_javascriptUITemplate != string.Empty)
                     {
                         var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Web\Areas\{0}\Scripts\app\{1}", g.Module, g.DatabaseTable);

                         string path = string.Format(@"{0}\{1}.js", _diectoryPath, g.DatabaseTable);

                         if (!Directory.Exists(_diectoryPath))
                         {
                             Directory.CreateDirectory(_diectoryPath);
                             File.WriteAllText(path, _javascriptUITemplate);
                         }
                         else
                         {
                             File.WriteAllText(path, _javascriptUITemplate);
                         }

                     }
                 });

                _applicationModuleSetupList.Where(c => string.IsNullOrEmpty(c.ParentTable) || c.ParentTable == "NULL").ToList().ForEach(g =>
                {
                    var _javascriptDataSourceTemplate = JavascriptGenerator.GenerateDataSourceScript(g);

                    if (_javascriptDataSourceTemplate != string.Empty)
                    {
                        var _diectoryPath = _applicationPath + string.Format(@"\GSAssetManagement.Web\Areas\{0}\Scripts\app\{1}", g.Module, g.DatabaseTable);

                        string path = string.Format(@"{0}\dataSource.js", _diectoryPath);

                        if (!Directory.Exists(_diectoryPath))
                        {
                            Directory.CreateDirectory(_diectoryPath);
                            File.WriteAllText(path, _javascriptDataSourceTemplate);
                        }
                        else
                        {
                            File.WriteAllText(path, _javascriptDataSourceTemplate);
                        }

                    }

                });

            }


            MessageBox.Show("Successfully code generated.");



        }


        private DataTable WorksheetToDataTable(ExcelWorksheet oSheet)
        {
            int totalRows = oSheet.Dimension.End.Row;
            int totalCols = oSheet.Dimension.End.Column;
            DataTable dt = new DataTable(oSheet.Name);
            DataRow dr = null;
            for (int i = 1; i <= totalRows; i++)
            {
                if (i > 1) dr = dt.Rows.Add();
                for (int j = 1; j <= totalCols; j++)
                {
                    if (i == 1)
                    {
                        if (oSheet.Cells[i, j].Value != null)
                        {
                            dt.Columns.Add(oSheet.Cells[i, j].Value.ToString());
                        }
                    }
                    else
                    {
                        if (oSheet.Cells[i, j].Value != null)
                        {
                            dr[j - 1] = oSheet.Cells[i, j].Value.ToString();
                        }
                    }
                }
            }
            return dt;
        }

        private void btnModuleDataSetup_Click(object sender, EventArgs e)
        {
            List<ModuleSetup> _applicationModuleSetupList = new List<ModuleSetup>();
            List<ModuleRuleSetup> _applicationModuleRuleSetupList = new List<ModuleRuleSetup>();
            List<Tuple<string, string>> _chilTables = new List<Tuple<string, string>>();


            _applicationPath = ConfigurationManager.AppSettings["path"].ToString();

            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "All Files (*.*)|*.*";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = true;

            if (choofdlog.ShowDialog() == DialogResult.OK)
            {


                FileInfo _info = new FileInfo(choofdlog.FileName);
                ExcelPackage _openXml = new ExcelPackage(_info);
                var _workSheets = _openXml.Workbook.Worksheets;
                _workSheets.ToList().ForEach(x =>
                {
                    if (x.Name == "ModuleSetup")
                    {
                        DataTable _workSheetDT = WorksheetToDataTable(x);
                        _applicationModuleSetupList.AddRange(_workSheetDT.ToList<ModuleSetup>().ToList());


                    }

                    if (x.Name == "ModuleRuleSetup")
                    {
                        DataTable _workSheetDT = WorksheetToDataTable(x);

                        _applicationModuleRuleSetupList.AddRange(_workSheetDT.ToList<ModuleRuleSetup>().ToList());


                    }

                });


                _applicationModuleSetupList.ForEach(c =>
                {
                    if (!string.IsNullOrEmpty(c.ChildTable))
                    {

                        c.ChildTable.Split(',').ToList().ForEach(v =>
                        {
                            string _tableName = _applicationModuleSetupList.Where(x => x.Module == c.Module && x.DatabaseTable == v).Select(z => z.DatabaseTable
                       ).FirstOrDefault();

                            string _title = _applicationModuleSetupList.Where(x => x.Module == c.Module && x.DatabaseTable == v).Select(z => z.TableTitle
                           ).FirstOrDefault();


                            if (!string.IsNullOrEmpty(_tableName) && !string.IsNullOrEmpty(_title))
                                c.ChildLists.Add(new Tuple<string, string>(_tableName, _title));

                        });
                    }

                    if (!string.IsNullOrEmpty(c.ParentTable))
                    {
                        c.ParentLists.Add(c.ParentTable);
                    }

                    c.ModuleRuleSetupList.AddRange(_applicationModuleRuleSetupList.Where(x => x.Module == c.Module && x.TableName == c.DatabaseTable));

                });




            };

            ApplicationDbContext _context = new ApplicationDbContext();



            //_applicationModuleSetupList.ForEach(g =>
            //{

            //    if (_context.ModuleSetups.Where(x => x.Module == g.Module).Count() > 0)
            //    {

            //        List<modulebusin> _ModuleTableFieldSetupList = _context.ModuleTableFieldSetups.Where(c => _context.ModuleTableSetups.Where(x => x.Module == g.Module && x.DatabaseTable == g.DatabaseTable).Select(z => z.Id).Contains(c.ModuleTableSetupId)).ToList();
            //        List<ModuleTableSetup> _ModuleTableSetupList = _context.ModuleTableSetups.Where(x => x.Module == g.Module && x.DatabaseTable == g.DatabaseTable).ToList();

            //        _context.ModuleTableFieldSetups.RemoveRange(_ModuleTableFieldSetupList);
            //        _context.ModuleTableSetups.RemoveRange(_ModuleTableSetupList);

            //        _context.Commit();


            //    }



            //    ModuleTableSetup _parentEntity = new ModuleTableSetup();

            //    _parentEntity.Id = Guid.NewGuid();
            //    _parentEntity.Module = g.Module;
            //    _parentEntity.TableTitle = g.TableTitle;
            //    _parentEntity.DatabaseTable = g.DatabaseTable;
            //    _parentEntity.ControllerName = g.ControllerName;
            //    _parentEntity.ChildTable = g.ChildTable;
            //    _parentEntity.ParentTable = g.ParentTable;
            //    _parentEntity.CreatedBy = "administrator";
            //    _parentEntity.ModifiedBy = "administrator";
            //    _parentEntity.AuthorisedBy = "administrator";
            //    _parentEntity.CreatedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF");
            //    _parentEntity.ModifiedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF");
            //    _parentEntity.AuthorisedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF");
            //    _parentEntity.CreatedDate = DateTime.Now;
            //    _parentEntity.ModifiedDate = DateTime.Now;
            //    _parentEntity.AuthorisedDate = DateTime.Now;
            //    _parentEntity.RecordStatus = RecordStatus.Active;
            //    _parentEntity.DeletedStatus = null;
            //    _parentEntity.DataEntry = DataEntry.Excel;

            //    _context.ModuleTableSetups.Add(_parentEntity);

            //    g.ModuleRuleSetupList.OrderBy(c => c.Order).ToList().ForEach(b =>
            //    {
            //        ModuleTableFieldSetup _childTable = new ModuleTableFieldSetup();

            //        _childTable.Id = Guid.NewGuid();
            //        _childTable.ModuleTableSetupId = _parentEntity.Id;
            //        _childTable.ColumnName = b.ColumnName;
            //        _childTable.FieldTitle = b.FieldTitle;
            //        _childTable.DataType = b.DataType;
            //        _childTable.HtmlDataType = b.HtmlDataType;
            //        _childTable.IsRequired = b.IsRequired;
            //        _childTable.Regex = b.Regex;
            //        _childTable.Length = b.DataType == "string" && b.Length == null ? "100" : b.Length;
            //        _childTable.ErrorMessage = b.ErrorMessage;
            //        _childTable.HelpMessage = b.HelpMessage;
            //        _childTable.RangeForm = null;
            //        _childTable.RangeTo = null;
            //        _childTable.IsUpdateable = b.IsUpdateable;
            //        _childTable.Order = b.Order;
            //        _childTable.SummaryHeader = b.SummaryHeader;
            //        _childTable.Enable = b.Enable;
            //        _childTable.SearchField = b.SearchField;
            //        _childTable.Hidden = b.Hidden;

            //        if (b.DataType == "DateTime")
            //            b.HtmlClass = "form-control datepicker";
            //        else if (b.HtmlDataType == "select" && !b.ParameterisedSource)
            //            b.HtmlClass = "form-control dropdown";
            //        else if (b.HtmlDataType == "select" && b.ParameterisedSource)
            //            b.HtmlClass = "form-control parameteriseddropdown";
            //        else
            //            b.HtmlClass = "form-control";


            //        _childTable.HtmlClass = b.HtmlClass;
            //        _childTable.ReadOnly = b.ReadOnly;
            //        _childTable.IsForeignKey = b.IsForeignKey;
            //        _childTable.ForeignModule = b.ForeignModule;
            //        _childTable.ForeignTable = b.ForeignTable;
            //        _childTable.ForeignColumn = b.ForeignColumn;
            //        _childTable.ForeignColumnAlais = b.ForeignColumnAlais;
            //        _childTable.ForeignTableAlais = b.ForeignTableAlais;

            //        if (b.IsForeignKey && b.SummaryHeader && !b.Hidden)
            //        {

            //            b.Condition = string.Format("{0}.{1} = {2}.{3}", b.TableName, b.ColumnName, b.ForeignTableAlais, "Id");
            //        }

            //        if (!b.IsForeignKey && b.HtmlDataType == "select" && b.SummaryHeader && !b.Hidden)
            //        {
            //            b.Condition = string.Format("{0}.{1} = {2}.Value AND {2}.ColumnName = ''{1}''", b.TableName, b.ColumnName, b.ForeignTableAlais);
            //        }





            //        _childTable.EnableRemoteValidation = b.EnableRemoteValidation;
            //        _childTable.RemoteValidationService = b.RemoteValidationService;
            //        _childTable.RemoteParameters = b.RemoteParameters;

            //        if (b.HtmlDataType == "select" && b.DataType != "bool")
            //        {

            //            if (b.HtmlDataType == "select")
            //            {
            //                if (b.ForeignTable == "StaticDataDetails")
            //                {
            //                    if (b.ParameterisedSource)
            //                    {
            //                        string parameterCondition = string.Empty;
            //                        int i = 1;

            //                        b.Parameter.Split(',').ToList().ForEach(c =>
            //                        {
            //                            if (i == 1)
            //                            {
            //                                parameterCondition += string.Format("Parameter{1} = {0}", c.Replace("*.", "@"), i);
            //                            }
            //                            else
            //                            {
            //                                parameterCondition += string.Format("AND Parameter{1} = {0}", c.Replace("*.", "@"), i);
            //                            }

            //                            i += 1;
            //                        });

            //                        _childTable.Datasource = string.Format("SELECT '{0}' ColumnName, Title, Value, OrderValue FROM Setting.StaticDataDetails WHERE ColumnName ='{0}' AND RecordStatus = 2 AND {1}", b.ColumnName, parameterCondition);

            //                    }
            //                    else
            //                    {
            //                        _childTable.Datasource = string.Format("SELECT '{0}' ColumnName, Title, Value, OrderValue FROM Setting.StaticDataDetails WHERE ColumnName ='{0}' AND RecordStatus = 2", b.ColumnName);
            //                    }
            //                }
            //                else
            //                {
            //                    if (b.ParameterisedSource)
            //                    {
            //                        string parameterCondition = string.Empty;
            //                        int i = 1;

            //                        b.Parameter.Split(',').ToList().ForEach(c =>
            //                        {
            //                            if (i == 1)
            //                            {
            //                                parameterCondition += string.Format("{1} = {0}", c.Replace("*.", "@"), c.Replace("*.", ""));
            //                            }
            //                            else
            //                            {
            //                                parameterCondition += string.Format("AND {1} = {0}", c.Replace("*.", "@"), c.Replace("*.", ""));
            //                            }

            //                            i += 1;
            //                        });

            //                        _childTable.Datasource = string.Format("SELECT '{0}' ColumnName,  {1} Title, CONVERT(NVARCHAR(50),Id) Value,CONVERT(NVARCHAR,CreatedDate) OrderValue FROM {2}.{3} WHERE RecordStatus = 2 AND {4}", b.ColumnName, b.ForeignColumn, b.Module, b.ForeignTable, parameterCondition);

            //                    }
            //                    else
            //                    {
            //                        _childTable.Datasource = string.Format("SELECT '{0}' ColumnName,  {1} Title, CONVERT(NVARCHAR(50),Id) Value,CONVERT(NVARCHAR,CreatedDate) OrderValue FROM {2}.{3} WHERE RecordStatus = 2", b.ColumnName, b.ForeignColumn, b.Module, b.ForeignTable);
            //                    }
            //                }
            //            }


            //        }
            //        else if (b.HtmlDataType == "select" && b.DataType == "bool")
            //        {
            //            _childTable.Datasource = string.Format("SELECT '{0}' ColumnName, Title, Value, OrderValue FROM Setting.StaticDataDetails WHERE ColumnName ='Argument' AND RecordStatus = 2", b.ColumnName);
            //        }

            //        //_childTable.Datasource = b.Datasource;
            //        _childTable.ParameterisedSource = b.ParameterisedSource;
            //        _childTable.Parameter = b.Parameter;
            //        _childTable.CreatedBy = "administrator";
            //        _childTable.ModifiedBy = "administrator";
            //        _childTable.AuthorisedBy = "administrator";
            //        _childTable.CreatedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF");
            //        _childTable.ModifiedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF");
            //        _childTable.AuthorisedById = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF");
            //        _childTable.CreatedDate = DateTime.Now;
            //        _childTable.ModifiedDate = DateTime.Now;
            //        _childTable.AuthorisedDate = DateTime.Now;
            //        _childTable.RecordStatus = RecordStatus.Active;
            //        _childTable.DeletedStatus = null;
            //        _childTable.DataEntry = DataEntry.Excel;

            //        _context.ModuleTableFieldSetups.Add(_childTable);



            //    });






            //});



            _context.Commit();

            MessageBox.Show("Successfully code generated.");


        }
    }
}
