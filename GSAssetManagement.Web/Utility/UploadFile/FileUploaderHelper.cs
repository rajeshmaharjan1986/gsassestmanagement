﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;

namespace GSAssetManagement.Web.Utility
{
    public static class FileUploaderHelper
    {
        public static string GetPath(HttpFileCollectionBase httpFileCollectionBases)
        {
            try
            {
                List<SelectListItem> staticDataDetailsDTOs = DropdownHelper.GetDropdownInformation("Extension", null, null, true, false, null, null, null);
                List<string> paths = new List<string>();

                foreach (HttpPostedFileBase httpPostedFileBase in httpFileCollectionBases)
                {
                    var staticDataDetails = staticDataDetailsDTOs.Where(v => v.Value == Path.GetExtension(httpPostedFileBase.FileName)).FirstOrDefault();

                    if (staticDataDetails != null)
                    {
                        string FileName = Path.Combine(staticDataDetails.ColumnName, httpPostedFileBase.FileName);
                        paths.Add(FileName);
                        httpPostedFileBase.SaveAs(FileName);
                    }
                }

                return string.Join("#", paths);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}