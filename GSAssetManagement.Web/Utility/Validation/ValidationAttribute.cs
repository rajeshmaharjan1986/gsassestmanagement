﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.Validation;
using GSAssetManagement.Infrastructure;

namespace GSAssetManagement.Web.Utility
{
    public static class ValidationAttribute
    {
        public static List<EntityValidationResult> IsValid<T>(T DTO)
        {
            List<EntityValidationResult> EntityValidationResults = new List<EntityValidationResult>();

            var ModuleSetupInformation = ModuleHelper.GetModuleSetup<T>();

            PropertyInfo[] sourceProprties = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public);

            ModuleSetupInformation.ModuleBussinesLogicSetups.OrderBy(o => o.Position).ToList().ForEach(x =>
            {
                try
                {
                    EntityValidationResult EntityValidationResult = new EntityValidationResult()
                    {

                        Name = x.Name,
                        ColumnName = x.ColumnName

                    };


                    List<PropertyInfo> list = sourceProprties.Where(p => p.Name == x.ColumnName).ToList();
                    PropertyInfo propertyInfo = list.FirstOrDefault();

                    object PropertyValue = null;
                    if (propertyInfo != null)
                    {
                        PropertyValue = propertyInfo.GetValue(DTO, null);
                        //sourceProprties.Where(p => p.Name == x.ColumnName).FirstOrDefault().GetValue(DTO, null);
                    }


                    foreach (var attribute in x.ModuleValidationAttributeSetups)
                    {
                        switch (attribute.AttributeType)
                        {
                            case "Required":

                                if (PropertyValue == null && (PropertyValue != null && string.IsNullOrEmpty(PropertyValue.ToString())))
                                {
                                    EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                }

                                break;

                            case "StringLength":
                                if (PropertyValue != null && !string.IsNullOrEmpty(PropertyValue.ToString()))
                                {
                                    if (!string.IsNullOrEmpty(attribute.Value) && attribute.Value.ToString().ToLower() != "max")
                                    {
                                        int length = int.Parse(attribute.Value);

                                        if (PropertyValue.ToString().Length > length)
                                            EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                    }
                                }


                                break;

                            case "RegularExpression":
                                if (PropertyValue != null)
                                {

                                    if (!Regex.IsMatch(PropertyValue != null ? PropertyValue.ToString() : null, attribute.Value))
                                    {
                                        EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                    }
                                }

                                break;

                            case "IntRange":
                                if (PropertyValue != null)
                                {

                                    var rangeList = attribute.Value.Split(',').ToList();

                                    int minValue = 0;
                                    int maxValue = 0;
                                    int compareValue = int.Parse(PropertyValue != null ? PropertyValue.ToString() : "0");

                                    if (rangeList.Count() > 1)
                                    {
                                        minValue = int.Parse(rangeList.First());
                                        maxValue = int.Parse(rangeList.Last());

                                        if (!(compareValue >= minValue && compareValue <= maxValue))
                                        {
                                            EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                        }

                                    }
                                }
                                break;

                            case "DecimalRange":
                                if (PropertyValue != null)
                                {

                                    var rangeDecimalList = attribute.Value.Split(',').ToList();

                                    decimal minDecimalValue = 0;
                                    decimal maxDecimalValue = 0;
                                    decimal compareDecimalValue = int.Parse(PropertyValue != null ? PropertyValue.ToString() : "0");

                                    if (rangeDecimalList.Count() > 1)
                                    {
                                        minDecimalValue = decimal.Parse(rangeDecimalList.First());
                                        maxDecimalValue = decimal.Parse(rangeDecimalList.Last());

                                        if (!(compareDecimalValue >= minDecimalValue && compareDecimalValue <= maxDecimalValue))
                                        {
                                            EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                        }

                                    }
                                }

                                break;

                            case "AgeByDate":
                                if (PropertyValue != null)
                                {
                                    DateTime compareAgeValue = DateTime.Parse(PropertyValue.ToString());

                                    var condition = attribute.Value.Split(',').ToList();

                                    int Age = int.Parse(condition.Last().ToString());

                                    if (condition.First() == ">")
                                    {
                                        if (((DateTime.Now.Date > compareAgeValue.AddYears(Age).Date)))
                                        {
                                            EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                        }
                                    }

                                    if (condition.First() == "<")
                                    {
                                        if (((DateTime.Now.Date > compareAgeValue.AddYears(Age).Date)))
                                        {
                                            EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                        }
                                    }
                                }


                                break;

                            case "Age":
                                if (PropertyValue != null)
                                {
                                    int compareAgeValue = int.Parse(PropertyValue.ToString());
                                    var condition = attribute.Value.Split(',').ToList();

                                    int Age = int.Parse(condition.Last().ToString());

                                    if (condition.First() == ">")
                                    {
                                        if ((compareAgeValue > Age))
                                        {
                                            EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                        }
                                    }

                                    if (condition.First() == "<")
                                    {
                                        if ((compareAgeValue < Age))
                                        {
                                            EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                        }
                                    }
                                }


                                break;

                            case "DateRange":
                                if (PropertyValue != null)
                                {
                                    var rangeDateList = attribute.Value.Split(',').ToList();

                                    DateTime minDateValue = rangeDateList.First() == "Today" ? DateTime.Now : DateTime.Parse(rangeDateList.First());
                                    DateTime maxDateValue = rangeDateList.Last() == "Today" ? DateTime.Now : DateTime.Parse(rangeDateList.Last()); ;
                                    DateTime compareDateValue = DateTime.Parse(PropertyValue.ToString());

                                    if (!(compareDateValue >= minDateValue.Date && compareDateValue <= maxDateValue.Date))
                                    {
                                        EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                    }

                                }


                                break;


                            case "CreditCard":

                                if (PropertyValue != null)
                                {
                                    IList<char> cleaned = CleanInput(PropertyValue.ToString().ToCharArray());

                                    int result = Validate(cleaned);

                                    if (!(result % 10 == 0))
                                    {
                                        EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                    }
                                }


                                break;

                            case "EmailAddress":
                                if (PropertyValue != null)
                                {
                                    try
                                    {
                                        var addr = new System.Net.Mail.MailAddress(PropertyValue.ToString());
                                        if (addr.Address != PropertyValue.ToString())
                                        {
                                            EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                    }
                                }

                                break;

                            case "FileExtension":
                                if (PropertyValue != null)
                                {
                                    var extensionList = attribute.Value.Split(',').ToList();

                                    if (extensionList.Where(e => e.ToLower() == PropertyValue.ToString()).Count() == 0)
                                    {
                                        EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                    }
                                }
                                break;

                            case "MaxLength":

                                if (PropertyValue != null)
                                {
                                    var maxLength = int.Parse(attribute.Value.ToString());

                                    if (int.Parse(PropertyValue.ToString()) > maxLength)
                                    {
                                        EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                    }
                                }

                                break;

                            case "MinLength":

                                if (PropertyValue != null)
                                {
                                    var minLength = int.Parse(attribute.Value.ToString());

                                    if (int.Parse(PropertyValue.ToString()) < minLength)
                                    {
                                        EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                    }
                                }

                                break;

                            case "Phone":

                                if (PropertyValue != null)
                                {
                                    var minLength = int.Parse(attribute.Value.ToString());

                                    if (PropertyValue.ToString().Length < minLength)
                                    {
                                        EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                    }
                                }

                                break;

                            case "Conditional":

                                var _condition = attribute.Value;

                                var conditionFunction = CreateExpression(typeof(T), _condition);
                                var conditionMet = (bool)conditionFunction.DynamicInvoke(DTO);
                                if (conditionMet)
                                {
                                    EntityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails(attribute.AttributeType, attribute.ErrorMessage));
                                };


                                break;

                        }


                    }

                    if (EntityValidationResult.ValidationResultDetails.Count > 0)
                        EntityValidationResults.Add(EntityValidationResult);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            });

            return EntityValidationResults;
        }

        private static IList<char> CleanInput(char[] chars)
        {
            IList<char> temp = chars.ToList();
            foreach (char ch in chars)
            {
                if (!char.IsNumber(ch))
                {
                    temp.Remove(ch);
                }

            }
            return temp;
        }

        private static int Validate(IList<char> cleaned)
        {
            IList<int> numbersToAdd = new List<int>();
            bool multiply = false;
            int result = 0;
            for (int i = cleaned.Count - 1; i >= 0; i--)
            {
                int num = int.Parse(cleaned[i].ToString());
                if (multiply)
                {
                    num *= 2;
                    multiply = false;
                }
                else
                {
                    multiply = true;
                }

                if (num > 9)
                {
                    numbersToAdd.Add(int.Parse(num.ToString().Substring(0, 1)));

                    numbersToAdd.Add(int.Parse(num.ToString().Substring(1, 1)));

                }
                else
                {
                    numbersToAdd.Add(num);
                }

            }

            foreach (int value in numbersToAdd)

            {

                result += value;

            }



            //return the result

            return result;

        }

        private static Delegate CreateExpression(Type objectType, string expression)
        {
            var lambdaExpression =
                DynamicExpression.ParseLambda(
                    objectType, typeof(bool), expression);
            var func = lambdaExpression.Compile();
            return func;
        }
    }
}
