﻿using GSAssetManagement.Entity.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GSAssetManagement.Web.Utility
{
    public class GetSelectList
    {
        public static List<SelectListItem> GetLocationSelectList(List<StaticDataDetailsDTO> list, string value)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            try {
                var newlist = list.Select(x => new { ColumnName = x.ColumnName, Title = x.Title, Value = x.Value }).Distinct().ToList();

                int i = 1;
                foreach (var dto in newlist.OrderBy(x => x.Title))
                {
                    bool selectedValue = false;
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (dto.Value == value)
                        {
                            selectedValue = true;
                        }
                    }

                    SelectListItem item = new SelectListItem()
                    {
                        ColumnName = dto.ColumnName,
                        Text = dto.Title,
                        Value = dto.Value,
                        OrderValue = i.ToString(),
                        Selected = selectedValue
                    };
                    selectList.Add(item);
                    i++;
                }

                return selectList;
            }
            catch {
                return selectList;
            }
        }
    }
}