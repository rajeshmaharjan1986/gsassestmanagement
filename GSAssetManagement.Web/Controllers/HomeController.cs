﻿using GSAssetManagement.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace GSAssetManagement.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ICommonRepository _commonRepository;
        public HomeController( ICommonRepository commonRepository)
        {
            this._commonRepository = commonRepository;
        }
        public async Task<ActionResult> Index()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public async Task<ActionResult> PartialIndex(string ActionStatus, int PageNumber, int PageSize)
        {
            try
            {
                return View("Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

    }
}