﻿if (!window.app) {
    window.app = {};
};



app.onlineAccountOpening = (function () {

    var mainBody = $('#irm-workspace'),
        dataSource = app.dataSourceonlineAccountOpening,
        initializer = appInit.applicationInitializer,
        init = function () {
            mainBody.on('click', '.actionStatus', filterByactionStatus);
            mainBody.on('click', '.actionStatus', filterByactionStatus);
            mainBody.on('click', 'ul.recordSize li a', resizeContentRecords);
            mainBody.on('click', 'ul#paginationUL li a', paginatedContentRecords);
            mainBody.on('click', '.ProceedCustomerRegistration', ProceedCustomerRegistration);
            mainBody.on('click', '.RejectCustomerRegistration', RejectCustomerRegistration);
            mainBody.on('click', '.UpdateOnlineRegistrationDetails', UpdateOnlineRegistrationDetails);
        },
        UpdateOnlineRegistrationDetails = function (event) {
            event.preventDefault();
            //dataSource.updateForm($(model)).done(function (response) {
            //    $.alert('Successfully reverted.');
            //    location.reload();

            //}).fail(function (response) {
            //    $.alert(response);
            //    location.reload();
            //});
            if (mainBody.find('#onlineAccountOPeningForm').valid()) {
                var serviceType = "";
                $('#ServiceType').val(null);

                $('input[name=ServiceTypeCheckBox]').each(function () {
                    if (this.checked) {

                        serviceType = serviceType + $(this).val() + ",";

                        $('#ServiceType').val(serviceType);
                    }
                });
                mainBody.find('#onlineAccountOPeningForm').submit();
            }
            

        }, filterByactionStatus = function (event) {
            event.preventDefault();

            var accountStatus = $(this).attr('data-actionstatus');

            mainBody.find('#currentActionStatus').val(accountStatus);

            dataSource.SearchRecords(accountStatus, 1, 20).done(function (response) {

                mainBody.html(response);

            }).fail(function () {



            });


        }, resizeContentRecords = function (event) {

            event.preventDefault();

            var accountStatus = mainBody.find('#currentActionStatus').val();
            var contentRecords = $(this).attr('data-pagesize');
            var pageNumber = $('#paginationUL li.active a').attr('data-page');

            dataSource.SearchRecords(accountStatus, pageNumber, contentRecords).done(function (response) {

                mainBody.html(response);

            }).fail(function () {



            });

        }, paginatedContentRecords = function (event) {

            event.preventDefault();

            var accountStatus = mainBody.find('#currentActionStatus').val();
            var pageNumber = $(this).attr('data-page');

            dataSource.SearchRecords(accountStatus, pageNumber, 20).done(function (response) {

                mainBody.html(response);

            }).fail(function () {



            });


        }, ProceedCustomerRegistration = function (event) {

            event.preventDefault();
            var url = $(this).attr('href');


            $.confirm({
                title: 'Proceed Customer Registration',
                content: function () {
                    var self = this;
                    return $.ajax({
                        url: url,
                        dataType: 'json',
                        method: 'POST'
                    }).done(function (response) {
                        window.location.href = response.redirectUrl;
                    }).fail(function () {
                        self.setContent('Something went wrong.');
                    });
                }
            });
        }, RejectCustomerRegistration = function (event) {

            event.preventDefault();
            var url = $(this).attr('href');


            $.confirm({
                title: 'Are you sure you want to reject this record!',
                content: '' +
                    '<form action="" class="formName">' +
                    '<div class="form-group">' +
                    '<label>Enter fields remarks due to which form was rejected(separate fields by ",")</label>' +
                    '<input type="text" placeholder="Your remarks" class="remarks form-control" required />' +
                    '</div>' +
                    '</form>',
                buttons: {
                    formSubmit: {
                        text: 'Submit',
                        btnClass: 'btn-blue',
                        action: function () {
                            initializer.showloader();
                            var remarks = this.$content.find('.remarks').val();
                            if (!remarks) {
                                $.alert('Provide a valid remarks.Remarks is empty.');
                                return false;
                            }
                            url = url + remarks;
                            var self = this;
                            return $.ajax({
                                url: url,
                                dataType: 'json',
                                method: 'POST'
                            }).done(function (response) {
                                initializer.hideloader();
                                window.location.href = response.redirectUrl;
                            }).fail(function () {
                                self.setContent('Something went wrong.');
                                initializer.hideloader();
                            });
                          
                        }
                    },
                    cancel: function () {
                        //$this.prop("checked", false);
                        //$this.iCheck('update');
                    }
                },
                onContentReady: function () {
                    // bind to events
                    var jc = this;
                    this.$content.find('form').on('submit', function (e) {
                        // if the user submits the form by pressing enter in the field.
                        e.preventDefault();
                        jc.$$formSubmit.trigger('click'); // reference the button and click it
                    });
                }
            });

        };

    return {

        init: init
    };


}(jQuery));


jQuery(app.GSAssetManagement.init);