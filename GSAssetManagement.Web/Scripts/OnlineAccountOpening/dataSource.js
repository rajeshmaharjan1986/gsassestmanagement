﻿if (!window.app) {
    window.app = {};
}



app.dataSourceonlineAccountOpening = (function ($) {

    var SearchRecords = function (ActionStatus, PageNumber, PageSize) {


        var $d = $.Deferred();
        $.ajax({
            type: "POST",
            url: '/Home/PartialIndex',
            data: { ActionStatus: ActionStatus, PageNumber: PageNumber, PageSize: PageSize }

        }).done(function (response) {

            $d.resolve(response);

        }).fail(function (response) {

            $d.reject(response);
        });


        return $d.promise();
    }, updateform = function (model) {

        var $d = $.Deferred();

        $form.find(':input').prop("disabled", false);

        var data = $form.serialize();

        $form.find("button[type='submit']").prop('disabled', true);
        var url = getUrl($form.attr('action'), $form.find('#CurrentAction').val());

        $.ajax({
            type: "POST",
            url: url,
            data: data
        }).done(function (response) {

            if (response.IsSuccess === false) {
                $form.find(':input').prop("disabled", false);
            }
            $d.resolve(response);

        }).fail(function (response) {

            $d.reject(response);
        });

        return $d.promise();


    }, ProceedCustomerRegistration = function (url) {

        var $d = $.Deferred();
        $.ajax({
            type: "POST",
            url: url

        }).done(function (response) {

            $d.resolve(response);

        }).fail(function (response) {

            $d.reject(response);
        });


        return $d.promise();


    };

    return {
        SearchRecords: SearchRecords,
        updatedForm: updateform,
        ProceedCustomerRegistration: ProceedCustomerRegistration
    };
}(jQuery));
