if (!window.app) {
    window.app = {};
}




app.CustomerComplianceInformation = (function ($) {
    var mainBody = $('#irm-workspace'),
        parentId = mainBody.find('#CustomerComplianceInformationWorkspace #ParentId').val(),
        dataSource = app.CustomerComplianceInformationDatasource,
        initializer = appInit.applicationInitializer,
        validator = {},

        init = function () {

            mainBody.find('#CustomerComplianceInformationWorkspace').on('click', '.scopeChange', changeScope);
            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere').on('click', 'form#mainForm .saveRecords', confirmation);
            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere').on('click', 'form#mainForm .unlock', unlockRecords);
            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere').on('submit', 'form#mainForm', submitForm);

            mainBody.on('submit', 'form#parentSearchForm', parentSearchRecords);
            mainBody.find('#main-workspace #parenttblcontent #RecordsContent').on('click', 'ul.recordSize li a', parentResizeContentRecords);
            mainBody.find('#main-workspace #parenttblcontent #RecordsContent').on('click', 'ul#paginationUL li a', parentPaginatedContentRecords);
            mainBody.find('#main-workspace #CustomerComplianceInformationWorkspace #allLiContentsHere').on('click', '#RecordsContentul.recordSizelia', resizeContentRecords);
            mainBody.find('#main-workspace #CustomerComplianceInformationWorkspace #allLiContentsHere').on('click', '#RecordsContentul#paginationULlia', paginatedContentRecords);
            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere').on('submit', 'form#searchForm', searchRecords);
            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere').on('change', 'form :input', onChangeFormValues);
            mainBody.find('#CustomerComplianceInformationWorkspace').on('click', '#addMoreRecords', addMoreRecords);
            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere').on('click', '.childDataSetup', childDataSetup);
            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere').on('click', '.viewRecordDetails', viewRecordDetails);
            mainBody.find('#cofirmMessgae').on('click', '#confirmOperation', confirmOperation);
            mainBody.find('#cofirmMessgae').on('click', '#cancelOperation', cancelOperation);
            mainBody.on('click', 'form#parentSearchForm #export2Excel', export2Excel);
            mainBody.on('click', '.gotoProfile', gotoProfile);
            mainBody.on('click', '.fetchCustomerMismatch', fetchCustomerMismatch);
            summary();

        }, onChangeFormValues = function () {

            if (mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form#mainForm').valid()) {
                mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form .saveRecords').attr('disabled', false);
            }
        }, changeScope = function (event) {
            event.preventDefault();
            var scope = $(this).attr('data-scope');

            if (!parentId) {
                mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html('Invalid action attempted.');
                mainBody.find('#cofirmMessgae').find('#confirmDialog').hide();
                mainBody.find('#cofirmMessgae').find('#alertDialog').show();
                mainBody.find('#cofirmMessgae').modal({ backdrop: 'static', keyboard: false });
                mainBody.find('#cofirmMessgae').modal('show');
                return false;
            } else {

                var url = $(this).attr('href');

                mainBody.find('#CustomerComplianceInformationWorkspace li').removeClass('active');
                $(this).closest('li').addClass('active');

                initializer.showloader();

                dataSource.accessLink(url, parentId).done(function (response) {

                    mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere').html(response);


                    initializer.hideloader();

                }).fail(function (response) {

                    mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html(response.Summary);
                    mainBody.find('#cofirmMessgae').find('#confirmDialog').hide();
                    mainBody.find('#cofirmMessgae').find('#confirmresponseContent').show();
                    mainBody.find('#cofirmMessgae').modal({ backdrop: 'static', keyboard: false });
                    mainBody.find('#cofirmMessgae').modal('show');
                    initializer.hideloader();

                });
            }

        }, confirmation = function (event) {

            event.preventDefault();

            var CurrentAction = $(this).attr('data-currentaction');
            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form #CurrentAction').val(CurrentAction);

            mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html('');
            mainBody.find('#cofirmMessgae').find('#confirmresponseContent').hide();
            mainBody.find('#cofirmMessgae').find('#confirmDialog').show();

            mainBody.find('#cofirmMessgae').modal({ backdrop: 'static', keyboard: false });
            mainBody.find('#cofirmMessgae').modal('show');




        }, confirmOperation = function (event) {
            if (mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form').valid()) {
                mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form').submit();
            }
        }, cancelOperation = function (event) {

            mainBody.find('#cofirmMessgae').modal('hide');


        }, submitForm = function (event) {

            event.preventDefault();


            var $form = $(this);

            initializer.showloader();

            dataSource.submitForm($form).done(function (response) {

                if (response.IsSuccess === true) {

                    if (mainBody.find('#CustomerComplianceInformationWorkspace .nav.nav-pills.nav-stacked li.active a').attr('data-scope') === 'parent') {
                        mainBody.find('#ParentId').val(response.Id);
                        parentId = response.Id;
                    }
                    mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html(response.Summary);
                    mainBody.find('#cofirmMessgae').find('#confirmDialog').hide();
                    mainBody.find('#cofirmMessgae').find('#confirmresponseContent').show();
                    $('body').find('#loader-wrapper').hide();

                    initializer.hideloader();

                }
                else {

                    mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html(response.Summary);
                    mainBody.find('#cofirmMessgae').find('#confirmDialog').hide();
                    mainBody.find('#cofirmMessgae').find('#confirmresponseContent').show();
                    mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form :button').attr('disabled', false);

                    initializer.hideloader();
                }

            }).fail(function (response) {

                mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html(response.Summary);
                mainBody.find('#cofirmMessgae').find('#confirmDialog').hide();
                mainBody.find('#cofirmMessgae').find('#confirmresponseContent').show();
                mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form :button').attr('disabled', false);
                initializer.hideloader();

            });
        }, childDataSetup = function (event) {

            event.preventDefault();

            var scope = $(this).attr('data-scope');

            if (parentId === undefined) {

                mainBody.find('#alertScope').find('.alert').removeClass().addClass('alert alert-warning');
                mainBody.find('#alertScope').find('.alert p').text('Please fill the main form and proceed');


                return false;
            } else {
                var url = $(this).attr('href');

                initializer.showloader();

                dataSource.accessChildLink(url, parentId).done(function (response) {

                    mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere').html(response);
                    mainBody.find('#alertScope').find('.alert').removeClass().addClass('alert alert-info');
                    mainBody.find('#alertScope').find('.alert p').text('New workspace is loaded. Please fill valid details.');

                    initializer.hideloader();


                }).fail(function (response) {

                    mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere').html(response);
                    mainBody.find('#alertScope').find('.alert').removeClass().addClass('alert alert-danger');
                    mainBody.find('#alertScope').find('.alert p').text('New workspace is loaded. Please fill valid details.');

                    initializer.hideloader();


                });
            }


        }, parentSearchRecords = function (event) {

            event.preventDefault();
            var $form = $(this);

            initializer.showloader();

            dataSource.submitForm($form).done(function (response) {

                mainBody.find('#main-workspace #parenttblcontent #RecordsContent').html(response);
                mainBody.find('form#parentSearchForm :input').attr('disabled', false);
                initializer.hideloader();


            }).fail(function (response) {

                mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html(response.Summary);
                mainBody.find('#cofirmMessgae').find('#confirmDialog').hide();
                mainBody.find('#cofirmMessgae').find('#confirmresponseContent').show();
                initializer.hideloader();

            });
        }, searchRecords = function (event) {
            event.preventDefault();


            var $form = $(this);

            initializer.showloader();

            dataSource.submitForm($form).done(function (response) {

                mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere #RecordsContent').html(response);
                mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form#searchForm :input').attr('disabled', false);
                initializer.hideloader();


            }).fail(function (response) {
                mainBody.find('#cofirmMessgae').find('#confirmresponseContent').html(response.Summary);
                mainBody.find('#cofirmMessgae').find('#confirmDialog').hide();
                mainBody.find('#cofirmMessgae').find('#confirmresponseContent').show();
                initializer.hideloader();

            });


        }, parentResizeContentRecords = function (event) {
            event.preventDefault();
            var activeSize = $(this).attr('data-pagesize');
            mainBody.find('form#parentSearchForm #PageSize').val(activeSize);
            mainBody.find('form#parentSearchForm #PageNumber').val(1);
            mainBody.find('form#parentSearchForm').submit();
        }, parentPaginatedContentRecords = function () {
            event.preventDefault();

            var activePage = $(this).attr('data-page');
            mainBody.find('form#parentSearchForm #PageNumber').val(activePage);
            mainBody.find('form#parentSearchForm').submit();



        }, resizeContentRecords = function (event) {
            event.preventDefault();
            var activeSize = $(this).attr('data-pagesize');
            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form#searchForm #PageSize').val(activeSize);
            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form#searchForm #PageNumber').val(1);
            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form#searchForm').submit();

        }, paginatedContentRecords = function () {
            event.preventDefault();

            var activePage = $(this).attr('data-page');
            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form#searchForm #PageNumber').val(activePage);
            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form#searchForm').submit();



        }, viewRecordDetails = function (event) {

            event.preventDefault();

            var url = $(this).attr('href');
            var recordId = $(this).attr('data-id');


            initializer.showloader();

            dataSource.accessLink(url, recordId).done(function (response) {

                mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere').html(response);
                mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form#mainForm').attr('disabled', false);

                mainBody.find('#alertScope').find('.alert').removeClass().addClass('alert alert-info');
                mainBody.find('#alertScope').find('.alert p').text('New workspace is loaded. Please fill valid details.');

                initializer.hideloader();


            }).fail(function (response) {

                mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere').html(response);
                mainBody.find('#alertScope').find('.alert').removeClass().addClass('alert alert-danger');
                mainBody.find('#alertScope').find('.alert p').text('New workspace is loaded. Please fill valid details.');

                initializer.hideloader();


            });
        }, addMoreRecords = function (event) {
            event.preventDefault();
            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form :input').attr('disabled', false);
            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form')[0].reset();

        }, unlockRecords = function (event) {

            mainBody.find('#CustomerComplianceInformationWorkspace #allLiContentsHere form :input:not([readonly])').attr('disabled', false);



        }, export2Excel = function (event) {

            $("#table2excel").table2excel({
                exclude: ".noExl",
                name: "Worksheet Name",
                filename: "Compliance Records" //do not include extension
            });

        }, gotoProfile = function (event) {

            event.preventDefault();

            var CustomerNumber = $(this).attr('data-customernumber');
            var herf = $(this).attr('href');
            var CustomerType = '';

            $.confirm({
                title: 'Customer Type Confirmation',
                content: '<div>' +
                    '    <p class="text">' +
                    '        <mark style="background-color: #FFFF00">To proceed with KYC update user is required to verify the customer type.</mark>' +
                    '    </p>' +
                    '    <div class="form-group">' +
                    '        <label class="control-label"> Select Correct Customer Type From Account Opening Form </label>' +
                    '<select class="form-control" name="CustomerType" id="CustomerType">                                      ' +
                    '    <option value="">--Select--</option>   ' +
                    '    <option value="Individual Single">Individual Single</option>   ' +
                    '    <option value="Individual Joint">Individual Joint</option>     ' +
                    '    <option value="Corporate">Corporate</option>                   ' +
                    '</select>                                                          ' +
                    '    </div>' +

                    '</div>',
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function () {
                            CustomerType = this.$content.find('select#CustomerType').val();
                            if (!CustomerType) {
                                $.alert({
                                    title: 'User alert',
                                    content: "Please select customer type.",
                                    type: 'red'
                                });
                                return false;
                            } else {


                                initializer.showloader();

                                dataSource.confirmCustomerType(CustomerType, CustomerNumber).done(function (response) {

                                    if (response.IsSuccess == true) {

                                        if (response.CustomerConfirmation.MatchedCustomerType == 1) {

                                            location.href = '/KYCManagement/CustomerProfile/Index/?Id=' + response.CustomerConfirmation.CustomerRegistrationId;

                                        } else {

                                            $.confirm({
                                                title: 'User alert',
                                                content: 'Selected customer type does not match with the system. You should not proceed to update KYC of this customer until the customer type has been verified and rectified in the system. This record has been locked and sent to Central Unit for review. Please proceed with KYC update of next customer',
                                                buttons: {
                                                    somethingElse: {
                                                        text: 'Get Back to next customer',
                                                        btnClass: 'btn-blue',
                                                        keys: ['enter', 'shift'],
                                                        action: function () {
                                                            location.href = '/KYCManagement/CustomerComplianceInformation'
                                                        }
                                                    }

                                                }
                                            });

                                        }
                                        initializer.showloader();


                                    } else {

                                        location.href = '/KYCManagement/CustomerComplianceInformation/StartKYCInformation/?CustomerID=' + CustomerNumber;
                                    }


                                }).fail(function () {

                                    initializer.showloader();

                                });




                                return false;

                            }
                        }
                    },
                    cancel: function () {
                        initializer.hideloader();
                    }
                }
            });
        }, fetchCustomerMismatch = function (event) {

            var CustomerRegistrationId = $(this).attr('data-customernumber');

            $.confirm({
                title: 'Confirm!',
                content: 'Do you want fetch records from finacle for this mismatch type !',
                buttons: {
                    Yes: function () {

                        dataSource.fetchCustomerMismatch(CustomerRegistrationId).done(function (response) {





                        }).fail(function (response) {




                        });



                    },
                    No: function () {
                        $.alert('Canceled!');
                    }
                }
            });


        }, summary = function () {

            dataSource.summary().done(function (resposne) {

                mainBody.find('#UserInformation').html(resposne);

            }).fail(function () {


            });


        };
    return {
        init: init
    };

}(jQuery));

jQuery(app.CustomerComplianceInformation.init);
