if (window.app) {
    window.app = {};
}



app.CustomerComplianceInformationDatasource = (function ($) {

    var accessLink = function (url, parentId) {


        var $d = $.Deferred();
        $.ajax({
            type: "GET",
            url: url,
            data: { Id: parentId }

        }).done(function (response) {

            $d.resolve(response);

        }).fail(function (response) {

            $d.reject(response);
        });


        return $d.promise();




    }, submitForm = function ($form) {


        var $d = $.Deferred();

        $form.find(':input').prop("disabled", false);

        var data = $form.serialize();

        $form.find(':input').prop("disabled", true);

        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: data
        }).done(function (response) {

            if (response.IsSuccess == false) {
                $form.find(':input').prop("disabled", false);
            }
            $d.resolve(response);

        }).fail(function (response) {

            $d.reject(response);
        });

        return $d.promise();


    }, accessChildLink = function (url, parentId) {


        var $d = $.Deferred();

        $.ajax({
            type: "GET",
            url: url,
            data: { Id: parentId }

        }).done(function (response) {

            $d.resolve(response);

        }).fail(function (response) {

            $d.reject(response);
        });

        return $d.promise();




    }, confirmCustomerType = function (CustomerType, CustomerNumber) {

        var $d = $.Deferred();
        $.ajax({
            type: "POST",
            url: '/KYCManagement/CustomerComplianceInformation/ConfirmCustmerType',
            data: { CustomerType: CustomerType, CustomerNumber: CustomerNumber }

        }).done(function (response) {

            $d.resolve(response);

        }).fail(function (response) {

            $d.reject(response);
        });


        return $d.promise();

    }, summary = function () {

        var $d = $.Deferred();
        $.ajax({
            type: "GET",
            url: '/AccountOpeningHelper/GetUserInformation'

        }).done(function (response) {

            $d.resolve(response);

        }).fail(function (response) {

            $d.reject(response);
        });

        return $d.promise();

    }, fetchCustomerMismatch = function (CustomerRegistrationId) {

        var $d = $.Deferred();
        $.ajax({
            type: "POST",
            url: '/KYCManagement/CustomerComplianceInformation/FetchCustomerMismatch',
            data: { CustomerRegistrationId: CustomerRegistrationId }

        }).done(function (response) {

            $d.resolve(response);

        }).fail(function (response) {

            $d.reject(response);
        });


        return $d.promise();

    };
    return {
        accessLink: accessLink,
        submitForm: submitForm,
        accessChildLink: accessChildLink,
        confirmCustomerType: confirmCustomerType,
        summary: summary,
        fetchCustomerMismatch: fetchCustomerMismatch

    };
}(jQuery));
