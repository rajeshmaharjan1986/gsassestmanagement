﻿if (!window.app) {
    window.app = {};
}



app.commonServiceDataSource = (function () {

    var previewDocument = function (id) {

        var $d = $.Deferred();

        $.ajax({
            type: "GET",
            url: "/Service/GetDocumentPreview",
            data: { Id: id }

        }).done(function (response) {

            $d.resolve(response);

        }).fail(function (response) {

            $d.reject(response);
        });

        return $d.promise();

    },
        ChangeAssetNumber = function (id, number, categoryselect, categoryId) {
            var $d = $.Deferred();

            $.ajax({
                type: "POST",
                url: "/AssetTypeSetting/AssetDetail/NumberChange",
                data: { Id: id, number: number, assetCategory: categoryselect, assetCategoryId: categoryId }
            }).done(function (response) {

                $d.resolve(response);

            }).fail(function (response) {

                $d.reject(response);
            });

            return $d.promise();
        };

    return {
        previewDocument: previewDocument,
        ChangeAssetNumber: ChangeAssetNumber
    }



}(jQuery))