﻿if (!window.app) {
    window.app = {};
}


app.commonService = (function () {

    var mainBody = $('#main-workspace'),
        dataSource = app.commonServiceDataSource,
        initializer = appInit.applicationInitializer,
        init = function () {
            mainBody.on('click', 'form#mainFormNew .saveRecords', confirmation);
            mainBody.on('submit', 'form#mainFormNew', submitForm);
            mainBody.on('change', 'form#mainFormNew :input', onChangeFormValues);

            mainBody.on('click', '.filePreview', previewDocument);
            mainBody.on('change', '#AssetNumber', ChangeAssetNumber);
            mainBody.on('change', '#AssetCategoryId', ChangeAssetCetagory);
            mainBody.on('change', 'select.AssetStatus', ChangeAssetStatus);
            mainBody.on('change', '.AssetExpireDate', ChangeAssetExpireDate);
            mainBody.on('change', '#CCTVBackup', ChangeCCTVBackup);

            mainBody.on('click', '#btnExportExcel', ExportToExcel);
        },
        ExportToExcel = function (event) {
            try {
                var fileNameStr = $(this).attr('data-content');
                event.preventDefault();
                mainBody.find('#RecordsContent').table2excel({
                    exclude: ".xls",
                    name: "GS_Report",
                    filename: fileNameStr + new Date().toString().replace(/[\-\:\.]/g, "") + ".xls"
                });
            } catch (err) {

                $.alert({
                    title: 'Alert!',
                    content: err.message,
                });

            }
        },
        confirmation = function (event) {
            try {
                event.preventDefault();

                var CurrentAction = $(this).attr('data-currentaction');

                $.confirm({
                    title: 'Confirm!',
                    content: 'Do you really want to submit the form!',
                    buttons: {
                        Yes: function () {

                            mainBody.find('form#mainFormNew input#CurrentAction').val(CurrentAction);
                            if (mainBody.find('form#mainFormNew').valid()) {
                                mainBody.find('form#mainFormNew').submit();
                            }
                        },
                        No: function () {
                        }
                    }
                });

            } catch (err) {

                $.alert({
                    title: 'Alert!',
                    content: err.message,
                });

            }

        },
        submitForm = function (event) {
            try {
                event.preventDefault();


                var $form = $(this);

                initializer.showloader();

                dataSource.submitForm($form).done(function (response) {

                    if (response.IsSuccess === true) {

                        if (mainBody.find('.nav.nav-pills.nav-stacked li.active a').attr('data-scope') === 'parent') {
                            mainBody.find('#ParentId').val(response.Id);

                            var path = window.location.pathname;

                            if (path.includes('Create')) {

                                path = window.location.pathname.replace('Create', 'Details');
                                path = path + '?Id=' + response.Id;
                                window.history.pushState({ urlPath: path }, "", path);
                            }
                        }

                        $.dialog({
                            title: 'Successful Response',
                            content: "",
                            animation: 'scale',
                            columnClass: 'medium',
                            closeAnimation: 'scale',
                            backgroundDismiss: true,
                        });

                        $('body').find('#loader-wrapper').hide();

                        initializer.hideloader();

                    }
                    else {
                        var content = 'Try Again or Contact to Administrator!'
                        if (response.ResponseView.length > 0) {
                            content = response.ResponseView;
                        }

                        $.dialog({
                            title: 'Failed',
                            content: content,
                            animation: 'scale',
                            columnClass: 'medium',
                            closeAnimation: 'scale',
                            backgroundDismiss: true,
                        });

                        mainBody.find('form#mainFormNew :button').attr('disabled', false);
                        $form.find(':input').prop("disabled", false);

                        initializer.hideloader();
                    }

                }).fail(function (response) {

                    $.dialog({
                        title: 'Failed',
                        content: 'Try Again or Contact to Administrator!',
                        animation: 'scale',
                        columnClass: 'medium',
                        closeAnimation: 'scale',
                        backgroundDismiss: true,
                    });

                    mainBody.find('form#mainFormNew :button').attr('disabled', false);
                    $form.find(':input').prop("disabled", false);

                    initializer.hideloader();

                });


            } catch (err) {

                $.alert({
                    title: 'Alert!',
                    content: err.message,
                });

            }

        },
        onChangeFormValues = function (event) {
            try {
                event.preventDefault();

                if (mainBody.find('form#mainFormNew').valid()) {
                    mainBody.find('form#mainFormNew button.saveRecords').attr('disabled', false);
                }

            } catch (err) {

                $.alert({
                    title: 'Alert!',
                    content: err.message,
                });

            }

        },
        ChangeAssetStatus = function (event) {
            try {
                event.preventDefault();
                var dropdownElement = $(this);
                var tagName = dropdownElement.prop("tagName")
                var status = dropdownElement.val();
                if (tagName == 'SELECT') {

                    var parentElement = dropdownElement.parent().parent();
                    console.log(parentElement.prop("tagName"));

                    if (parentElement.prop("tagName") == "TD") {
                        parentElement = parentElement.parent();
                    }
                    console.log(parentElement);

                    var tagId = dropdownElement.attr('id');
                    var array = tagId.split('-');
                    var remarksId = 'Remarks-' + array[array.length - 1];
                    var spanId = 'SpanRemarks_' + array[array.length - 1];
                    console.log(remarksId);
                    if (status == 'NotWorking') {
                        
                        mainBody.find('#' + remarksId).show();
                        parentElement.find('.lblRamarks').show();
                    }
                    else {
                        mainBody.find('#' + remarksId).hide();
                        parentElement.find('.lblRamarks').hide();
                    }
                }

                
                console.log(dropdownElement);
                console.log(dropdownElement.val());
                console.log(dropdownElement.prop("tagName"));
                console.log(dropdownElement.attr('id'));
                
            } catch (err) {

                $.alert({
                    title: 'Alert!',
                    content: err.message,
                });

            }
        },
        ChangeAssetExpireDate = function (event) {
            try {
                event.preventDefault();
                var dropdownElement = $(this);
                var tagName = dropdownElement.prop("tagName")
                var expireDate = dropdownElement.val();
                if (expireDate.length === 0) {
                } else {
                    console.log(expireDate);
                    var tagId = dropdownElement.attr('id');
                    var array = tagId.split('-');
                    var statusId = 'Status-' + array[array.length - 1];
                    var statusTag = mainBody.find('select#' + statusId);
                    var remarksId = 'Remarks-' + array[array.length - 1];
                    console.log(Date.now());
                    console.log(Date.parse(expireDate));
                    if (Date.parse(expireDate) <= Date.now()) {
                        console.log('Less');
                        console.log(statusTag);
                        mainBody.find('#' + statusId).val('NotWorking').trigger('change');
                        console.log(mainBody.find('select#' + statusId).val());
                        mainBody.find('#' + statusId).prop("disabled", true);
                        mainBody.find('#' + remarksId).show();
                    } else {
                        console.log('Greater');
                        mainBody.find('#' + statusId).val('Working').trigger('change');
                        mainBody.find('#' + remarksId).hide();
                        mainBody.find('#' + statusId).prop("disabled", true);
                    }
                }
            } catch (err) {

                $.alert({
                    title: 'Alert!',
                    content: err.message,
                });

            }
        },
        ChangeAssetNumber = function (event) {
            try {
                event.preventDefault();
                var categoryselect = mainBody.find('#AssetCategoryId option:selected').text();
                var categoryselectId = mainBody.find('#AssetCategoryId option:selected').val();
                console.log(categoryselect);
                var Id = mainBody.find('#ParentId').val();
                var number = $(this).val();
                if (number > 0) {
                    dataSource.ChangeAssetNumber(Id, number, categoryselect, categoryselectId).done(function (response) {
                        if (jQuery.type(response) == 'string') {
                            mainBody.find('#AssetExtraPartialView').html(response);
                        } else {
                            $.alert({
                                title: 'Alert!',
                                content: response.ResponseView,
                            });
                        }

                    }).fail(function (response) {

                        $.dialog({
                            title: 'Failed',
                            content: 'Try Again or Contact to Administrator!',
                            animation: 'scale',
                            columnClass: 'medium',
                            closeAnimation: 'scale',
                            backgroundDismiss: true,
                        });


                    });
                }
            } catch (err) {

                $.alert({
                    title: 'Alert!',
                    content: err.message,
                });

            }
        },
        ChangeCCTVBackup = function (event) {
            try {
                event.preventDefault();
                var CCTVBackupnumber = $(this).val();
                if (CCTVBackupnumber < 90) {
                    mainBody.find('#CCTVStatus').val('Not properly working');
                } else {
                    mainBody.find('#CCTVStatus').val('Working');
                }
            } catch (err) {

                $.alert({
                    title: 'Alert!',
                    content: err.message,
                });

            }
        },
        ChangeAssetCetagory = function (event) {
            try {
                event.preventDefault();
                var categoryselect = mainBody.find('#AssetCategoryId option:selected').text();
                var categoryselectId = mainBody.find('#AssetCategoryId option:selected').val();
                console.log(categoryselect);
                var Id = mainBody.find('#ParentId').val();
                var number = mainBody.find('#AssetNumber').val();
                console.log(number);
                if (number > 0) {
                    dataSource.ChangeAssetNumber(Id, number, categoryselect, categoryselectId).done(function (response) {
                        if (jQuery.type(response) == 'string') {
                            mainBody.find('#AssetExtraPartialView').html(response);
                        } else {
                            $.alert({
                                title: 'Alert!',
                                content: response.ResponseView,
                            });
                        }
                    }).fail(function (response) {

                        $.dialog({
                            title: 'Failed',
                            content: 'Try Again or Contact to Administrator!',
                            animation: 'scale',
                            columnClass: 'medium',
                            closeAnimation: 'scale',
                            backgroundDismiss: true,
                        });


                    });
                }
            } catch (err) {

                $.alert({
                    title: 'Alert!',
                    content: err.message,
                });

            }
        },
        previewDocument = function (event) {

            event.preventDefault();

            var id = $(this).attr('data-id');

            var width = window.innerWidth * 0.66;

            var height = width * window.innerHeight / window.innerWidth;

            window.open(this.href, 'newwindow', 'width=' + width + ', height=' + height + ', top=' + ((window.innerHeight - height) / 2) + ', left=' + ((window.innerWidth - width) / 2));


        };

    return {
        init: init
    }


}(jQuery))

jQuery(app.commonService.init());