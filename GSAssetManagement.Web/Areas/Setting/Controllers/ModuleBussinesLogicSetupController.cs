using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;
//using GSAssetManagement.AttributeHelper;
using GSAssetManagement.Entity.Validation;

namespace GSAssetManagement.Web.Areas.Setting.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleBussinesLogicSetup", Url = "/Setting/ModuleBussinesLogicSetup", Parent = false)]
    [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleBussinesLogicSetup", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class ModuleBussinesLogicSetupController : Controller
    {
        private readonly IModuleBussinesLogicSetupRepository _ModuleBussinesLogicSetupRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ModuleBussinesLogicSetupController(IModuleBussinesLogicSetupRepository ModuleBussinesLogicSetupRepository,
            IUnitOfWork unitOfWork,
            IExceptionLoggerRepository exceptionLoggerRepository)
        {
            _ModuleBussinesLogicSetupRepository = ModuleBussinesLogicSetupRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork;
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleBussinesLogicSetup", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index(Guid ParentPrimaryRecordId)
        {
            try
            {
                ModuleSummary moduleSummary = await _ModuleBussinesLogicSetupRepository.GetModuleBussinesLogicSetup();
                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ModuleSetupId").ToList().ForEach(c => c.CurrentValue = ParentPrimaryRecordId);

                moduleSummary.SchemaName = ModuleName.AdminSetting.ToString();
                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("ModuleSetupId", ParentPrimaryRecordId));
                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));

                moduleSummary.SummaryRecord = await _ModuleBussinesLogicSetupRepository.GetAllByProcedure("Setting.SP_GetModuleBussinesLogicSetupList", sqlParameters.ToArray());

                return View(moduleSummary);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleBussinesLogicSetup", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {
                ModuleSummary moduleSummary = await _ModuleBussinesLogicSetupRepository.GetModuleBussinesLogicSetup();

                var sqlParameters = SearchParameters.GetSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                moduleSummary.SummaryRecord = await _ModuleBussinesLogicSetupRepository.GetAllByProcedure("Setting.SP_GetModuleBussinesLogicSetupList", sqlParameters.ToArray());

                SearchParameters.Clear();

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleBussinesLogicSetup", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Create(Guid ParentPrimaryRecordId)
        {
            try
            {
                ModuleSummary moduleSummary = await _ModuleBussinesLogicSetupRepository.GetModuleBussinesLogicSetup();
                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ModuleSetupId").ToList().ForEach(c => c.CurrentValue = ParentPrimaryRecordId);
                if (!moduleSummary.IsParent && moduleSummary.DoRecordExists)
                {
                    return View("Details", moduleSummary);
                }
                else
                {
                    return View("Create", moduleSummary);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleBussinesLogicSetup", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection model)
        {
            try
            {
                ModuleBussinesLogicSetupDTO modulebussineslogicsetupDTO = new ModuleBussinesLogicSetupDTO();
                TryUpdateModel<ModuleBussinesLogicSetupDTO>(modulebussineslogicsetupDTO);

                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ModuleBussinesLogicSetupDTO>(modulebussineslogicsetupDTO);

                model.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    Guid Id = this._ModuleBussinesLogicSetupRepository.Add(modulebussineslogicsetupDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "ModuleBussinesLogicSetup", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();



                    return new JsonHttpStatusResult(new
                    {
                        Id = Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)

                    }, System.Net.HttpStatusCode.OK);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleBussinesLogicSetup", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Details(Guid Id)
        {
            try
            {
                var dto = await _ModuleBussinesLogicSetupRepository.GetDTOById(Id);
                ModuleSummary moduleSummary = await _ModuleBussinesLogicSetupRepository.GetModuleBussinesLogicSetup(dto, dto.ModuleSetupId);

                if (Request.IsAjaxRequest())
                {
                    return View("PartialDetails", moduleSummary);
                }
                else
                {
                    return View("Details", moduleSummary);
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleBussinesLogicSetup", Action = CurrentAction.Edit)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Update(FormCollection formCollection)
        {
            try
            {
                ModuleBussinesLogicSetupDTO modulebussineslogicsetupDTO = new ModuleBussinesLogicSetupDTO();
                TryUpdateModel<ModuleBussinesLogicSetupDTO>(modulebussineslogicsetupDTO);

                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ModuleBussinesLogicSetupDTO>(modulebussineslogicsetupDTO);

                formCollection.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    await this._ModuleBussinesLogicSetupRepository.Update(modulebussineslogicsetupDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "ModuleBussinesLogicSetup", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = modulebussineslogicsetupDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleBussinesLogicSetup", Action = CurrentAction.Delete)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Delete(FormCollection formCollection)
        {
            try
            {
                ModuleBussinesLogicSetupDTO modulebussineslogicsetupDTO = new ModuleBussinesLogicSetupDTO();
                TryUpdateModel<ModuleBussinesLogicSetupDTO>(modulebussineslogicsetupDTO);

                formCollection.Clear();

                if (modulebussineslogicsetupDTO != null)
                {
                    await this._ModuleBussinesLogicSetupRepository.Delete(modulebussineslogicsetupDTO.Id, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "ModuleBussinesLogicSetup", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = modulebussineslogicsetupDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleBussinesLogicSetup", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Authorise(FormCollection formCollection)
        {
            try
            {
                ModuleBussinesLogicSetupDTO modulebussineslogicsetupDTO = new ModuleBussinesLogicSetupDTO();
                TryUpdateModel<ModuleBussinesLogicSetupDTO>(modulebussineslogicsetupDTO);

                formCollection.Clear();

                if (modulebussineslogicsetupDTO != null)
                {
                    await this._ModuleBussinesLogicSetupRepository.Authorise(modulebussineslogicsetupDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        IsSuccess = true,
                        Id = modulebussineslogicsetupDTO.Id,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleBussinesLogicSetup", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Revert(FormCollection formCollection)
        {
            try
            {
                ModuleBussinesLogicSetupDTO modulebussineslogicsetupDTO = new ModuleBussinesLogicSetupDTO();
                TryUpdateModel<ModuleBussinesLogicSetupDTO>(modulebussineslogicsetupDTO);

                formCollection.Clear();

                if (modulebussineslogicsetupDTO != null)
                {
                    await this._ModuleBussinesLogicSetupRepository.Revert(modulebussineslogicsetupDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = modulebussineslogicsetupDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleBussinesLogicSetup", Action = CurrentAction.Discard)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Discard(FormCollection formCollection)
        {
            try
            {
                ModuleBussinesLogicSetupDTO modulebussineslogicsetupDTO = new ModuleBussinesLogicSetupDTO();
                TryUpdateModel<ModuleBussinesLogicSetupDTO>(modulebussineslogicsetupDTO);

                formCollection.Clear();

                if (modulebussineslogicsetupDTO != null)
                {
                    await this._ModuleBussinesLogicSetupRepository.DiscardChanges(modulebussineslogicsetupDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = modulebussineslogicsetupDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}