 using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;
//using GSAssetManagement.AttributeHelper;
using GSAssetManagement.Entity.Validation;

namespace GSAssetManagement.Web.Areas.Setting.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleValidationAttributeSetup", Url = "/Setting/ModuleValidationAttributeSetup", Parent = false)]
    [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleValidationAttributeSetup", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class ModuleValidationAttributeSetupController : Controller
    {
        private readonly IModuleValidationAttributeSetupRepository _ModuleValidationAttributeSetupRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ModuleValidationAttributeSetupController(IModuleValidationAttributeSetupRepository ModuleValidationAttributeSetupRepository,
            IUnitOfWork unitOfWork,
            IExceptionLoggerRepository exceptionLoggerRepository)
        {
            _ModuleValidationAttributeSetupRepository = ModuleValidationAttributeSetupRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork;
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleValidationAttributeSetup", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index(Guid ParentPrimaryRecordId)
        {
            try
            {
                ModuleSummary moduleSummary = await _ModuleValidationAttributeSetupRepository.GetModuleBussinesLogicSetup();
                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ModuleBussinesLogicSetupId").ToList().ForEach(c => c.CurrentValue = ParentPrimaryRecordId);

                moduleSummary.SchemaName = ModuleName.AdminSetting.ToString();
                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("ModuleBussinesLogicSetupId", ParentPrimaryRecordId));
                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));

                moduleSummary.SummaryRecord = await _ModuleValidationAttributeSetupRepository.GetAllByProcedure("Setting.SP_GetModuleValidationAttributeSetupList", sqlParameters.ToArray());

                return View(moduleSummary);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleValidationAttributeSetup", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {
                ModuleSummary moduleSummary = await _ModuleValidationAttributeSetupRepository.GetModuleBussinesLogicSetup();

                var sqlParameters = SearchParameters.GetSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                moduleSummary.SummaryRecord = await _ModuleValidationAttributeSetupRepository.GetAllByProcedure("Setting.SP_GetModuleValidationAttributeSetupList", sqlParameters.ToArray());

                SearchParameters.Clear();

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleValidationAttributeSetup", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Create(Guid ParentPrimaryRecordId)
        {
            try
            {
                ModuleSummary moduleSummary = await _ModuleValidationAttributeSetupRepository.GetModuleBussinesLogicSetup();
                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ModuleBussinesLogicSetupId").ToList().ForEach(c => c.CurrentValue = ParentPrimaryRecordId);

                if (!moduleSummary.IsParent && moduleSummary.DoRecordExists)
                {
                    return View("Details", moduleSummary);
                }
                else
                {
                    return View("Create", moduleSummary);
                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleValidationAttributeSetup", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection model)
        {
            try
            {
                ModuleValidationAttributeSetupDTO modulevalidationattributesetupDTO = new ModuleValidationAttributeSetupDTO();
                TryUpdateModel<ModuleValidationAttributeSetupDTO>(modulevalidationattributesetupDTO);

                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ModuleValidationAttributeSetupDTO>(modulevalidationattributesetupDTO);

                model.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    Guid Id = this._ModuleValidationAttributeSetupRepository.Add(modulevalidationattributesetupDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "ModuleValidationAttributeSetup", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();



                    return new JsonHttpStatusResult(new
                    {
                        Id = Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)

                    }, System.Net.HttpStatusCode.OK);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleValidationAttributeSetup", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Details(Guid Id)
        {
            try
            {
                var dto = await _ModuleValidationAttributeSetupRepository.GetDTOById(Id);
                ModuleSummary moduleSummary = await _ModuleValidationAttributeSetupRepository.GetModuleBussinesLogicSetup(dto, dto.ModuleBussinesLogicSetupId);

                return View("Details", moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleValidationAttributeSetup", Action = CurrentAction.Edit)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Update(FormCollection formCollection)
        {
            try
            {
                ModuleValidationAttributeSetupDTO modulevalidationattributesetupDTO = new ModuleValidationAttributeSetupDTO();
                TryUpdateModel<ModuleValidationAttributeSetupDTO>(modulevalidationattributesetupDTO);

                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ModuleValidationAttributeSetupDTO>(modulevalidationattributesetupDTO);

                formCollection.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    await this._ModuleValidationAttributeSetupRepository.Update(modulevalidationattributesetupDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "ModuleValidationAttributeSetup", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = modulevalidationattributesetupDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleValidationAttributeSetup", Action = CurrentAction.Delete)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Delete(FormCollection formCollection)
        {
            try
            {
                ModuleValidationAttributeSetupDTO modulevalidationattributesetupDTO = new ModuleValidationAttributeSetupDTO();
                TryUpdateModel<ModuleValidationAttributeSetupDTO>(modulevalidationattributesetupDTO);

                formCollection.Clear();

                if (modulevalidationattributesetupDTO != null)
                {
                    await this._ModuleValidationAttributeSetupRepository.Delete(modulevalidationattributesetupDTO.Id, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "ModuleValidationAttributeSetup", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = modulevalidationattributesetupDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleValidationAttributeSetup", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Authorise(FormCollection formCollection)
        {
            try
            {
                ModuleValidationAttributeSetupDTO modulevalidationattributesetupDTO = new ModuleValidationAttributeSetupDTO();
                TryUpdateModel<ModuleValidationAttributeSetupDTO>(modulevalidationattributesetupDTO);

                formCollection.Clear();

                if (modulevalidationattributesetupDTO != null)
                {
                    await this._ModuleValidationAttributeSetupRepository.Authorise(modulevalidationattributesetupDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        IsSuccess = true,
                        Id = modulevalidationattributesetupDTO.Id,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleValidationAttributeSetup", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Revert(FormCollection formCollection)
        {
            try
            {
                ModuleValidationAttributeSetupDTO modulevalidationattributesetupDTO = new ModuleValidationAttributeSetupDTO();
                TryUpdateModel<ModuleValidationAttributeSetupDTO>(modulevalidationattributesetupDTO);

                formCollection.Clear();

                if (modulevalidationattributesetupDTO != null)
                {
                    await this._ModuleValidationAttributeSetupRepository.Revert(modulevalidationattributesetupDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = modulevalidationattributesetupDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleValidationAttributeSetup", Action = CurrentAction.Discard)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Discard(FormCollection formCollection)
        {
            try
            {
                ModuleValidationAttributeSetupDTO modulevalidationattributesetupDTO = new ModuleValidationAttributeSetupDTO();
                TryUpdateModel<ModuleValidationAttributeSetupDTO>(modulevalidationattributesetupDTO);

                formCollection.Clear();

                if (modulevalidationattributesetupDTO != null)
                {
                    await this._ModuleValidationAttributeSetupRepository.DiscardChanges(modulevalidationattributesetupDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = modulevalidationattributesetupDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }
    }
}