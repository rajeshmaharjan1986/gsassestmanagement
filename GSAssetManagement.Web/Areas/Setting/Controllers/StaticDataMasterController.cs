 using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;
//using GSAssetManagement.AttributeHelper;
using GSAssetManagement.Entity.Validation;

namespace GSAssetManagement.Web.Areas.Setting.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataMaster", Url = "/Setting/StaticDataMaster", Parent = true)]
    [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataMaster", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class StaticDataMasterController : Controller
    {
        private readonly IStaticDataMasterRepository _StaticDataMasterRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;

        public StaticDataMasterController(IStaticDataMasterRepository StaticDataMasterRepository,
            IUnitOfWork unitOfWork,
            IExceptionLoggerRepository exceptionLoggerRepository)
        {
            _StaticDataMasterRepository = StaticDataMasterRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork;
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataMaster", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            try
            {
                ModuleSummary moduleSummary = await _StaticDataMasterRepository.GetModuleBussinesLogicSetup();
                moduleSummary.SchemaName = ModuleName.AdminSetting.ToString();
                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                
                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));

                moduleSummary.SummaryRecord = await _StaticDataMasterRepository.GetAllByProcedure("Setting.SP_GetStaticDataMasterList", sqlParameters.ToArray());


                return View(moduleSummary);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataMaster", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {
                ModuleSummary moduleSummary = await _StaticDataMasterRepository.GetModuleBussinesLogicSetup();

                var sqlParameters = SearchParameters.GetSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                moduleSummary.SummaryRecord = await _StaticDataMasterRepository.GetAllByProcedure("Setting.SP_GetStaticDataMasterList", sqlParameters.ToArray());

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataMaster", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Create()
        {
            try
            {
                ModuleSummary moduleSummary = await _StaticDataMasterRepository.GetModuleBussinesLogicSetup();
                if (!moduleSummary.IsParent && moduleSummary.DoRecordExists)
                {
                    return View("Details", moduleSummary);
                }
                else
                {
                    return View("Create", moduleSummary);
                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataMaster", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection model)
        {
            try
            {
                StaticDataMasterDTO staticdatamasterDTO = new StaticDataMasterDTO();
                TryUpdateModel<StaticDataMasterDTO>(staticdatamasterDTO);

                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<StaticDataMasterDTO>(staticdatamasterDTO);

                model.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    Guid Id = this._StaticDataMasterRepository.Add(staticdatamasterDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "StaticDataMaster", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();



                    return new JsonHttpStatusResult(new
                    {
                        Id = Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)

                    }, System.Net.HttpStatusCode.OK);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataMaster", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Details(Guid Id)
        {
            try
            {
                var staticDataMasterDTO = await _StaticDataMasterRepository.GetDTOById(Id);
                ModuleSummary moduleSummary = await _StaticDataMasterRepository.GetModuleBussinesLogicSetup(staticDataMasterDTO, null);

                if (Request.IsAjaxRequest())
                {
                    return View("PartialDetails", moduleSummary);
                }
                else
                {
                    return View("Details", moduleSummary);
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataMaster", Action = CurrentAction.Edit)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Update(FormCollection formCollection)
        {
            try
            {
                StaticDataMasterDTO staticdatamasterDTO = new StaticDataMasterDTO();
                TryUpdateModel<StaticDataMasterDTO>(staticdatamasterDTO);

                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<StaticDataMasterDTO>(staticdatamasterDTO);

                formCollection.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    await this._StaticDataMasterRepository.Update(staticdatamasterDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "StaticDataMaster", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = staticdatamasterDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataMaster", Action = CurrentAction.Delete)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Delete(FormCollection formCollection)
        {
            try
            {
                StaticDataMasterDTO staticdatamasterDTO = new StaticDataMasterDTO();
                TryUpdateModel<StaticDataMasterDTO>(staticdatamasterDTO);

                formCollection.Clear();

                if (staticdatamasterDTO != null)
                {
                    await this._StaticDataMasterRepository.Delete(staticdatamasterDTO.Id, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "StaticDataMaster", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = staticdatamasterDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataMaster", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Authorise(FormCollection formCollection)
        {
            try
            {
                StaticDataMasterDTO staticdatamasterDTO = new StaticDataMasterDTO();
                TryUpdateModel<StaticDataMasterDTO>(staticdatamasterDTO);

                formCollection.Clear();

                if (staticdatamasterDTO != null)
                {
                    await this._StaticDataMasterRepository.Authorise(staticdatamasterDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        IsSuccess = true,
                        Id = staticdatamasterDTO.Id,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataMaster", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Revert(FormCollection formCollection)
        {
            try
            {
                StaticDataMasterDTO staticdatamasterDTO = new StaticDataMasterDTO();
                TryUpdateModel<StaticDataMasterDTO>(staticdatamasterDTO);

                formCollection.Clear();

                if (staticdatamasterDTO != null)
                {
                    await this._StaticDataMasterRepository.Revert(staticdatamasterDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = staticdatamasterDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataMaster", Action = CurrentAction.Discard)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Discard(FormCollection formCollection)
        {
            try
            {
                StaticDataMasterDTO staticdatamasterDTO = new StaticDataMasterDTO();
                TryUpdateModel<StaticDataMasterDTO>(staticdatamasterDTO);

                formCollection.Clear();

                if (staticdatamasterDTO != null)
                {
                    await this._StaticDataMasterRepository.DiscardChanges(staticdatamasterDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = staticdatamasterDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }
    }
}