 using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;
//using GSAssetManagement.AttributeHelper;
using GSAssetManagement.Entity.Validation;

namespace GSAssetManagement.Web.Areas.Setting.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataDetails", Url = "/Setting/StaticDataDetails", Parent = false)]
    [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataDetails", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class StaticDataDetailsController : Controller
    {
        private readonly IStaticDataDetailsRepository _StaticDataDetailsRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;

        public StaticDataDetailsController(IStaticDataDetailsRepository StaticDataDetailsRepository,
            IUnitOfWork unitOfWork,
            IExceptionLoggerRepository exceptionLoggerRepository)
        {
            _StaticDataDetailsRepository = StaticDataDetailsRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork;
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataDetails", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index(Guid ParentPrimaryRecordId)
        {
            try
            {
                ModuleSummary moduleSummary = await _StaticDataDetailsRepository.GetModuleBussinesLogicSetup();
                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName== "StaticDataMasterId").ToList().ForEach(c => c.CurrentValue = ParentPrimaryRecordId);

                moduleSummary.SchemaName = ModuleName.AdminSetting.ToString();
                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("StaticDataMasterId", ParentPrimaryRecordId));
                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));

                moduleSummary.SummaryRecord = await _StaticDataDetailsRepository.GetAllByProcedure("Setting.SP_GetStaticDataDetailsList", sqlParameters.ToArray());

                return View(moduleSummary);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataDetails", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {
                ModuleSummary moduleSummary = await _StaticDataDetailsRepository.GetModuleBussinesLogicSetup();

                var sqlParameters = SearchParameters.GetSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                moduleSummary.SummaryRecord = await _StaticDataDetailsRepository.GetAllByProcedure("Setting.SP_GetStaticDataDetailsList", sqlParameters.ToArray());

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataDetails", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Create(Guid ParentPrimaryRecordId)
        {
            try
            {
                ModuleSummary moduleSummary = await _StaticDataDetailsRepository.GetModuleBussinesLogicSetup();
                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "StaticDataMasterId").ToList().ForEach(c => c.CurrentValue = ParentPrimaryRecordId);

                if (!moduleSummary.IsParent && moduleSummary.DoRecordExists)
                {
                    return View("Details", moduleSummary);
                }
                else
                {
                    return View("Create", moduleSummary);
                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataDetails", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection model)
        {
            try
            {
                StaticDataDetailsDTO staticdatadetailsDTO = new StaticDataDetailsDTO();
                TryUpdateModel<StaticDataDetailsDTO>(staticdatadetailsDTO);

                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<StaticDataDetailsDTO>(staticdatadetailsDTO);

                model.Clear();

                if (EntityValidationResults.Count() == 0)
                {

                    Guid Id = this._StaticDataDetailsRepository.Add(staticdatadetailsDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "StaticDataDetails", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();



                    return new JsonHttpStatusResult(new
                    {
                        Id = Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)

                    }, System.Net.HttpStatusCode.OK);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataDetails", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Details(Guid Id)
        {
            try
            {
                var dto = await _StaticDataDetailsRepository.GetDTOById(Id);

                ModuleSummary moduleSummary = await _StaticDataDetailsRepository.GetModuleBussinesLogicSetup(dto, dto.StaticDataMasterId);

                return View("Details", moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataDetails", Action = CurrentAction.Edit)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Update(FormCollection formCollection)
        {
            try
            {
                StaticDataDetailsDTO staticdatadetailsDTO = new StaticDataDetailsDTO();
                TryUpdateModel<StaticDataDetailsDTO>(staticdatadetailsDTO);

                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<StaticDataDetailsDTO>(staticdatadetailsDTO);

                formCollection.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    await this._StaticDataDetailsRepository.Update(staticdatadetailsDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "StaticDataDetails", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = staticdatadetailsDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataDetails", Action = CurrentAction.Delete)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Delete(FormCollection formCollection)
        {
            try
            {
                StaticDataDetailsDTO staticdatadetailsDTO = new StaticDataDetailsDTO();
                TryUpdateModel<StaticDataDetailsDTO>(staticdatadetailsDTO);

                formCollection.Clear();

                if (staticdatadetailsDTO != null)
                {
                    await this._StaticDataDetailsRepository.Delete(staticdatadetailsDTO.Id, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "StaticDataDetails", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = staticdatadetailsDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataDetails", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Authorise(FormCollection formCollection)
        {
            try
            {
                StaticDataDetailsDTO staticdatadetailsDTO = new StaticDataDetailsDTO();
                TryUpdateModel<StaticDataDetailsDTO>(staticdatadetailsDTO);

                formCollection.Clear();

                if (staticdatadetailsDTO != null)
                {
                    await this._StaticDataDetailsRepository.Authorise(staticdatadetailsDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        IsSuccess = true,
                        Id = staticdatadetailsDTO.Id,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataDetails", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Revert(FormCollection formCollection)
        {
            try
            {
                StaticDataDetailsDTO staticdatadetailsDTO = new StaticDataDetailsDTO();
                TryUpdateModel<StaticDataDetailsDTO>(staticdatadetailsDTO);

                formCollection.Clear();

                if (staticdatadetailsDTO != null)
                {
                    await this._StaticDataDetailsRepository.Revert(staticdatadetailsDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = staticdatadetailsDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "StaticDataDetails", Action = CurrentAction.Discard)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Discard(FormCollection formCollection)
        {
            try
            {
                StaticDataDetailsDTO staticdatadetailsDTO = new StaticDataDetailsDTO();
                TryUpdateModel<StaticDataDetailsDTO>(staticdatadetailsDTO);

                formCollection.Clear();

                if (staticdatadetailsDTO != null)
                {
                    await this._StaticDataDetailsRepository.DiscardChanges(staticdatadetailsDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = staticdatadetailsDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }
    }
}