 using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;
//using GSAssetManagement.AttributeHelper;
using GSAssetManagement.Entity.Validation;

namespace GSAssetManagement.Web.Areas.Setting.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleSetup", Url = "/Setting/ModuleSetup", Parent = true)]
    [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleSetup", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class ModuleSetupController : Controller
    {
        private readonly IModuleSetupRepository _ModuleSetupRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ModuleSetupController(IModuleSetupRepository ModuleSetupRepository,
            IUnitOfWork unitOfWork,
            IExceptionLoggerRepository exceptionLoggerRepository)
        {
            _ModuleSetupRepository = ModuleSetupRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork;
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleSetup", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            try
            {
                ModuleSummary moduleSummary = await _ModuleSetupRepository.GetModuleBussinesLogicSetup();
                moduleSummary.SchemaName = ModuleName.AdminSetting.ToString();
                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                
                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));

                moduleSummary.SummaryRecord = await _ModuleSetupRepository.GetAllByProcedure("Setting.SP_GetModuleSetupList", sqlParameters.ToArray());

                return View(moduleSummary);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleSetup", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {
                ModuleSummary moduleSummary = await _ModuleSetupRepository.GetModuleBussinesLogicSetup();

                var sqlParameters = SearchParameters.GetSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                moduleSummary.SummaryRecord = await _ModuleSetupRepository.GetAllByProcedure("Setting.SP_GetModuleSetupList", sqlParameters.ToArray());

                SearchParameters.Clear();

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleSetup", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Create()
        {
            try
            {
                ModuleSummary moduleSummary = await _ModuleSetupRepository.GetModuleBussinesLogicSetup();
                if (!moduleSummary.IsParent && moduleSummary.DoRecordExists)
                {
                    return View("Details", moduleSummary);
                }
                else
                {
                    return View("Create", moduleSummary);
                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleSetup", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection model)
        {
            try
            {
                ModuleSetupDTO modulesetupDTO = new ModuleSetupDTO();
                TryUpdateModel<ModuleSetupDTO>(modulesetupDTO);

                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ModuleSetupDTO>(modulesetupDTO);

                model.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    Guid Id = this._ModuleSetupRepository.Add(modulesetupDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "ModuleSetup", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();



                    return new JsonHttpStatusResult(new
                    {
                        Id = Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)

                    }, System.Net.HttpStatusCode.OK);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleSetup", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Details(Guid Id)
        {
            try
            {
                var dto = await _ModuleSetupRepository.GetDTOById(Id);
                ModuleSummary moduleSummary = await _ModuleSetupRepository.GetModuleBussinesLogicSetup(dto, null);

                if (Request.IsAjaxRequest())
                {
                    return View("PartialDetails", moduleSummary);
                }
                else
                {
                    return View("Details", moduleSummary);
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleSetup", Action = CurrentAction.Edit)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Update(FormCollection formCollection)
        {
            try
            {
                ModuleSetupDTO modulesetupDTO = new ModuleSetupDTO();
                TryUpdateModel<ModuleSetupDTO>(modulesetupDTO);

                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ModuleSetupDTO>(modulesetupDTO);

                formCollection.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    await this._ModuleSetupRepository.Update(modulesetupDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "ModuleSetup", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = modulesetupDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleSetup", Action = CurrentAction.Delete)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Delete(FormCollection formCollection)
        {
            try
            {
                ModuleSetupDTO modulesetupDTO = new ModuleSetupDTO();
                TryUpdateModel<ModuleSetupDTO>(modulesetupDTO);

                formCollection.Clear();

                if (modulesetupDTO != null)
                {
                    await this._ModuleSetupRepository.Delete(modulesetupDTO.Id, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "ModuleSetup", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = modulesetupDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleSetup", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Authorise(FormCollection formCollection)
        {
            try
            {
                ModuleSetupDTO modulesetupDTO = new ModuleSetupDTO();
                TryUpdateModel<ModuleSetupDTO>(modulesetupDTO);

                formCollection.Clear();

                if (modulesetupDTO != null)
                {
                    await this._ModuleSetupRepository.Authorise(modulesetupDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        IsSuccess = true,
                        Id = modulesetupDTO.Id,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleSetup", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Revert(FormCollection formCollection)
        {
            try
            {
                ModuleSetupDTO modulesetupDTO = new ModuleSetupDTO();
                TryUpdateModel<ModuleSetupDTO>(modulesetupDTO);

                formCollection.Clear();

                if (modulesetupDTO != null)
                {
                    await this._ModuleSetupRepository.Revert(modulesetupDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = modulesetupDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ModuleSetup", Action = CurrentAction.Discard)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Discard(FormCollection formCollection)
        {
            try
            {
                ModuleSetupDTO modulesetupDTO = new ModuleSetupDTO();
                TryUpdateModel<ModuleSetupDTO>(modulesetupDTO);

                formCollection.Clear();

                if (modulesetupDTO != null)
                {
                    await this._ModuleSetupRepository.DiscardChanges(modulesetupDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = modulesetupDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }
    }
}