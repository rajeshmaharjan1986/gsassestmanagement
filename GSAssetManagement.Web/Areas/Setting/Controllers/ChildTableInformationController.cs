using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;
using GSAssetManagement.Entity.Validation;
using System.Linq.Expressions;

namespace GSAssetManagement.Web.Areas.Setting.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.AdminSetting, SubModuleName = "ChildTableInformation", Url = "/Setting/ChildTableInformation", Parent = false)]
    [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ChildTableInformation", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class ChildTableInformationController : Controller
    {
        private readonly IChildTableInformationRepository _ChildTableInformationRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ChildTableInformationController(IChildTableInformationRepository ChildTableInformationRepository,
            IUnitOfWork unitOfWork,
            IExceptionLoggerRepository exceptionLoggerRepository)
        {
            _ChildTableInformationRepository = ChildTableInformationRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork;
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ChildTableInformation", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index(Guid ParentPrimaryRecordId)
        {
            try
            {
                ModuleSummary moduleSummary = await _ChildTableInformationRepository.GetModuleBussinesLogicSetup();
                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ModuleSetupId").ToList().ForEach(c => c.CurrentValue = ParentPrimaryRecordId);

                moduleSummary.SchemaName = ModuleName.AdminSetting.ToString();
                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("ModuleSetupId", ParentPrimaryRecordId));
                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));

                moduleSummary.SummaryRecord = await _ChildTableInformationRepository.GetAllByProcedure("Setting.SP_GetChildTableInformationList", sqlParameters.ToArray());

                return View(moduleSummary);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ChildTableInformation", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {
                ModuleSummary moduleSummary = await _ChildTableInformationRepository.GetModuleBussinesLogicSetup();

                var sqlParameters = SearchParameters.GetSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                moduleSummary.SummaryRecord = await _ChildTableInformationRepository.GetAllByProcedure("Setting.SP_GetChildTableInformationList", sqlParameters.ToArray());
                //moduleSummary.SummaryRecord = await _ChildTableInformationRepository.GetAllByProcedure(ModuleName.AdminSetting.ToString(), moduleSummary.ModuleSummaryName, sqlParameters.ToArray());

                SearchParameters.Clear();

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ChildTableInformation", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Create(Guid ParentPrimaryRecordId)
        {
            try
            {
                ModuleSummary moduleSummary = await _ChildTableInformationRepository.GetModuleBussinesLogicSetup();
                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ModuleSetupId").ToList().ForEach(c => c.CurrentValue = ParentPrimaryRecordId);

                if (!moduleSummary.IsParent && moduleSummary.DoRecordExists)
                {
                    return View("Details", moduleSummary);
                }
                else
                {
                    return View("Create", moduleSummary);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ChildTableInformation", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection model)
        {
            try
            {
                ChildTableInformationDTO childtableinformationDTO = new ChildTableInformationDTO();
                TryUpdateModel<ChildTableInformationDTO>(childtableinformationDTO);

                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ChildTableInformationDTO>(childtableinformationDTO);

                model.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    Guid Id = this._ChildTableInformationRepository.Add(childtableinformationDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "ChildTableInformation", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();



                    return new JsonHttpStatusResult(new
                    {
                        Id = Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)

                    }, System.Net.HttpStatusCode.OK);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ChildTableInformation", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Details(Guid Id)
        {
            try
            {
                var childTableInformationDTO = await this._ChildTableInformationRepository.GetDTOById(Id);
                ModuleSummary moduleSummary = await _ChildTableInformationRepository.GetModuleBussinesLogicSetup(childTableInformationDTO, childTableInformationDTO.ModuleSetupId);

                return View("Details", moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ChildTableInformation", Action = CurrentAction.Edit)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Update(FormCollection formCollection)
        {
            try
            {
                ChildTableInformationDTO childtableinformationDTO = new ChildTableInformationDTO();
                TryUpdateModel<ChildTableInformationDTO>(childtableinformationDTO);

                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ChildTableInformationDTO>(childtableinformationDTO);

                formCollection.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    await this._ChildTableInformationRepository.Update(childtableinformationDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "ChildTableInformation", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = childtableinformationDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)

                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ChildTableInformation", Action = CurrentAction.Delete)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Delete(FormCollection formCollection)
        {
            try
            {
                ChildTableInformationDTO childtableinformationDTO = new ChildTableInformationDTO();
                TryUpdateModel<ChildTableInformationDTO>(childtableinformationDTO);

                formCollection.Clear();

                if (childtableinformationDTO != null)
                {
                    await this._ChildTableInformationRepository.Delete(childtableinformationDTO.Id, AuthorizeViewHelper.IsAuthorize(ModuleName.AdminSetting.ToString(), "ChildTableInformation", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = childtableinformationDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ChildTableInformation", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Authorise(FormCollection formCollection)
        {
            try
            {
                ChildTableInformationDTO childtableinformationDTO = new ChildTableInformationDTO();
                TryUpdateModel<ChildTableInformationDTO>(childtableinformationDTO);

                formCollection.Clear();

                if (childtableinformationDTO != null)
                {
                    await this._ChildTableInformationRepository.Authorise(childtableinformationDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        IsSuccess = true,
                        Id = childtableinformationDTO.Id,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ChildTableInformation", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Revert(FormCollection formCollection)
        {
            try
            {
                ChildTableInformationDTO childtableinformationDTO = new ChildTableInformationDTO();
                TryUpdateModel<ChildTableInformationDTO>(childtableinformationDTO);

                formCollection.Clear();

                if (childtableinformationDTO != null)
                {
                    await this._ChildTableInformationRepository.Revert(childtableinformationDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = childtableinformationDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AdminSetting, SubModuleName = "ChildTableInformation", Action = CurrentAction.Discard)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Discard(FormCollection formCollection)
        {
            try
            {
                ChildTableInformationDTO childtableinformationDTO = new ChildTableInformationDTO();
                TryUpdateModel<ChildTableInformationDTO>(childtableinformationDTO);

                formCollection.Clear();

                if (childtableinformationDTO != null)
                {
                    await this._ChildTableInformationRepository.DiscardChanges(childtableinformationDTO.Id);
                    await this._unitOfWork.CommitAsync();

                    return Json(new
                    {
                        Id = childtableinformationDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)

                    }, JsonRequestBehavior.DenyGet);

                }
                else
                {

                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)

                    }, JsonRequestBehavior.DenyGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}