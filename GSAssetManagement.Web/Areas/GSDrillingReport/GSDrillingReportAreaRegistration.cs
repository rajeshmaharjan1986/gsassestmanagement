﻿using System.Web.Mvc;

namespace GSAssetManagement.Web.Areas.DrillingReport
{
    public class GSDrillingReportAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "GSDrillingReport";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "GSDrillingReport_default",
                "GSDrillingReport/{controller}/{action}",
                new { action = "Index" }
            );
        }
    }
}