using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;
using GSAssetManagement.Entity.Validation;
namespace GSAssetManagement.Web.Areas.DrillingReport.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.GSDrillingReport, SubModuleName = "DrillingReport", Url = "/GSDrillingReport/DrillingReportByNotWorkingStatus", Parent = true)]
    [CRUDAuthorize(ModuleName = ModuleName.GSDrillingReport, SubModuleName = "DrillingReport", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class DrillingReportByNotWorkingStatusController : Controller
    {
        private readonly IBranchDrillingReportRepository _BranchDrillingReportRepository;
        private readonly IStaticDataDetailsRepository _StaticDataDetailsRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;
        public DrillingReportByNotWorkingStatusController(IBranchDrillingReportRepository branchdrillingreportRepository,
            IStaticDataDetailsRepository staticDataDetailsRepository,
        IUnitOfWork unitOfWork,
        IExceptionLoggerRepository exceptionLoggerRepository)
        {
            this._BranchDrillingReportRepository = branchdrillingreportRepository;
            _StaticDataDetailsRepository = staticDataDetailsRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork; ;
        }

        [CRUDAuthorize(ModuleName = ModuleName.GSDrillingReport, SubModuleName = "DrillingReport", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            try
            {
                var deptId = ViewUserInformation.GetStaffDepartment();
                var branchId = ViewUserInformation.GetBranch();

                ModuleSummaryReport moduleSummary = await _BranchDrillingReportRepository.GetModuleSummaryReportSetup();
                moduleSummary.SchemaName = ModuleName.GSDrillingReport.ToString();

                moduleSummary.moduleBussinesLogicSummaries = await _BranchDrillingReportRepository.GetReportFilterParameterNew();
                
                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("FROMReportDate", DateTime.Today));
                sqlParameters.Add(new SqlParameter("ToReportDate", DateTime.Today));

                moduleSummary.listReportByNotWorkingStatusDTOs = await _BranchDrillingReportRepository.GetDrillingReportByNotWorkingStatus(sqlParameters.ToArray());

                return View(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.GSDrillingReport, SubModuleName = "DrillingReport", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {
                ModuleSummaryReport moduleSummary = await _BranchDrillingReportRepository.GetModuleSummaryReportSetup();
                moduleSummary.moduleBussinesLogicSummaries = await _BranchDrillingReportRepository.GetReportFilterParameterNew();

                var sqlParameters = SearchParameters.GetCustomizedSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                moduleSummary.listReportByNotWorkingStatusDTOs = await _BranchDrillingReportRepository.GetDrillingReportByNotWorkingStatus(sqlParameters.ToArray());

                SearchParameters.Clear();

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        
    }
}
