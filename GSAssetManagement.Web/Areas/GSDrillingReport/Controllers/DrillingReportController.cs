using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;
using GSAssetManagement.Entity.Validation;
namespace GSAssetManagement.Web.Areas.DrillingReport.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.GSDrillingReport, SubModuleName = "DrillingReport", Url = "/GSDrillingReport/DrillingReport", Parent = true)]
    [CRUDAuthorize(ModuleName = ModuleName.GSDrillingReport, SubModuleName = "DrillingReport", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class DrillingReportController : Controller
    {
        private readonly IBranchDrillingReportRepository _BranchDrillingReportRepository;
        private readonly IStaticDataDetailsRepository _StaticDataDetailsRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;
        public DrillingReportController(IBranchDrillingReportRepository branchdrillingreportRepository,
            IStaticDataDetailsRepository staticDataDetailsRepository,
        IUnitOfWork unitOfWork,
        IExceptionLoggerRepository exceptionLoggerRepository)
        {
            this._BranchDrillingReportRepository = branchdrillingreportRepository;
            _StaticDataDetailsRepository = staticDataDetailsRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork; ;
        }

        [CRUDAuthorize(ModuleName = ModuleName.GSDrillingReport, SubModuleName = "DrillingReport", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            try
            {
                var deptId = ViewUserInformation.GetStaffDepartment();
                var branchId = ViewUserInformation.GetBranch();

                ModuleSummary moduleSummary = await _BranchDrillingReportRepository.GetModuleBussinesLogicSetup();
                moduleSummary.SchemaName = ModuleName.GSDrillingReport.ToString();

                moduleSummary.moduleBussinesLogicSummaries = await _BranchDrillingReportRepository.GetReportFilterParameter();

                List<SqlParameter> sqlParameters = new List<SqlParameter>();

                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));

                moduleSummary.SummaryRecord = await _BranchDrillingReportRepository.GetAllByProcedure("DrillingReport.SP_GetDrillingReportList", sqlParameters.ToArray());
                return View(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.GSDrillingReport, SubModuleName = "DrillingReport", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {

                ModuleSummary moduleSummary = await _BranchDrillingReportRepository.GetModuleBussinesLogicSetup();
                
                moduleSummary.moduleBussinesLogicSummaries = await _BranchDrillingReportRepository.GetReportFilterParameter();

                var sqlParameters = SearchParameters.GetCustomizedSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));

                moduleSummary.SummaryRecord = await _BranchDrillingReportRepository.GetAllByProcedure("DrillingReport.SP_GetDrillingReportList", sqlParameters.ToArray());

                SearchParameters.Clear();

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        [CRUDAuthorize(ModuleName = ModuleName.GSDrillingReport, SubModuleName = "DrillingReport", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Details(Guid Id)
        {
            try
            {
                var deptId = ViewUserInformation.GetStaffDepartment();
                var branchId = ViewUserInformation.GetBranch();

                var selectList = Enum.GetValues(typeof(WorkingStatus)).Cast<WorkingStatus>().Select(s => new Select() { ColumnName = "WorkingStatus", Title = s.DisplayName(), Value = s.ToString() }).ToList<Select>();

                ViewBag.WorkingStatusSelectList = selectList;

                ModuleSummaryForDetails moduleSummary = await _BranchDrillingReportRepository.GetModuleBussinesLogicSetupWithChild(Id, ViewUserInformation.GetBranch());
                moduleSummary.SchemaName = ModuleName.DrillingReport.ToString();

                if (deptId != "7")
                {
                    moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "BranchCode").ToList().ForEach(c => {
                        c.CurrentValue = branchId;
                        c.Attributes.Add("disabled", "");
                    });

                    var dateInBS = NepaliCalendarBS.NepaliCalendar.Convert_AD2BS(DateTime.Now);

                    moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ReportDateInBS").ToList().ForEach(c => {
                        c.CurrentValue = dateInBS;
                        c.Attributes.Add("readonly", "");
                    });
                }

                return View("Details", moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
