using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;
using GSAssetManagement.Entity.Validation;
using GSAssetManagement.Service;

namespace GSAssetManagement.Web.Areas.DrillingReport.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.GSDrillingReport, SubModuleName = "DrillingReport", Url = "/GSDrillingReport/BranchWiseReportStatus", Parent = true)]
    [CRUDAuthorize(ModuleName = ModuleName.GSDrillingReport, SubModuleName = "DrillingReport", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class BranchWiseReportStatusController : Controller
    {
        private readonly IBranchDrillingReportRepository _BranchDrillingReportRepository;
        private readonly IStaticDataDetailsRepository _StaticDataDetailsRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;
        public BranchWiseReportStatusController(IBranchDrillingReportRepository branchdrillingreportRepository,
            IStaticDataDetailsRepository staticDataDetailsRepository,
        IUnitOfWork unitOfWork,
        IExceptionLoggerRepository exceptionLoggerRepository)
        {
            this._BranchDrillingReportRepository = branchdrillingreportRepository;
            _StaticDataDetailsRepository = staticDataDetailsRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork; ;
        }

        [CRUDAuthorize(ModuleName = ModuleName.GSDrillingReport, SubModuleName = "DrillingReport", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            try
            {
                var deptId = ViewUserInformation.GetStaffDepartment();
                var branchId = ViewUserInformation.GetBranch();

               

                ModuleSummaryReport moduleSummary = await _BranchDrillingReportRepository.GetModuleSummaryReportSetup();
                moduleSummary.SchemaName = ModuleName.GSDrillingReport.ToString();

                moduleSummary.moduleBussinesLogicSummaries = await _BranchDrillingReportRepository.GetReportFilterParameter();

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("FROMReportDate", DateTime.Today));
                sqlParameters.Add(new SqlParameter("ToReportDate", DateTime.Today));

                List<GetBranchWiseReportStatusDTO> getBranchWiseReportStatusDTOs = await _BranchDrillingReportRepository.GetBranchWiseReportStatus(sqlParameters.ToArray());

                APIConsumtionService aPIConsumtionService = new APIConsumtionService();
                List<BranchList> branchLists = aPIConsumtionService.GetBranchList();
                List<GetBranchWiseReportStatusDTO> listBranchReportStatus = new List<GetBranchWiseReportStatusDTO>();
                foreach (var branch in branchLists) {
                    if (branch.BranchID != "0")
                    {
                        GetBranchWiseReportStatusDTO dto = new GetBranchWiseReportStatusDTO();
                        dto.BranchCode = branch.BranchID;
                        dto.BranchName = branch.BranchName;
                        dto.BranchReportStatus = string.Empty;

                        var reportStatusDTO = getBranchWiseReportStatusDTOs.Where(a => a.BranchCode == branch.BranchID).FirstOrDefault();
                        if (reportStatusDTO != null)
                        {
                            dto.BranchReportStatus = reportStatusDTO.BranchReportStatus;
                            dto.ReportDate = reportStatusDTO.ReportDate;
                            dto.ReportDateInBS = reportStatusDTO.ReportDateInBS;
                            dto.ReportMonth = reportStatusDTO.ReportMonth;
                            dto.ReportYear = reportStatusDTO.ReportYear;
                            dto.ReportPeriod = reportStatusDTO.ReportPeriod;
                        }
                        listBranchReportStatus.Add(dto);
                    }
                }

                moduleSummary.listBranchWiseReportStatusDTOs = listBranchReportStatus;
                return View(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.GSDrillingReport, SubModuleName = "DrillingReport", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {
                ModuleSummaryReport moduleSummary = await _BranchDrillingReportRepository.GetModuleSummaryReportSetup();
                moduleSummary.moduleBussinesLogicSummaries = await _BranchDrillingReportRepository.GetReportFilterParameter();

                var sqlParameters = SearchParameters.GetCustomizedSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                List<GetBranchWiseReportStatusDTO> getBranchWiseReportStatusDTOs = await _BranchDrillingReportRepository.GetBranchWiseReportStatus(sqlParameters.ToArray());
                APIConsumtionService aPIConsumtionService = new APIConsumtionService();
                List<BranchList> branchLists = aPIConsumtionService.GetBranchList();

                List<GetBranchWiseReportStatusDTO> listBranchReportStatus = new List<GetBranchWiseReportStatusDTO>();
                foreach (var branch in branchLists)
                {
                    if (branch.BranchID != "0")
                    {
                        GetBranchWiseReportStatusDTO dto = new GetBranchWiseReportStatusDTO();
                        dto.BranchCode = branch.BranchID;
                        dto.BranchName = branch.BranchName;
                        dto.BranchReportStatus = string.Empty;

                        var reportStatusDTO = getBranchWiseReportStatusDTOs.Where(a => a.BranchCode == branch.BranchID).FirstOrDefault();
                        if (reportStatusDTO != null)
                        {
                            dto.BranchReportStatus = reportStatusDTO.BranchReportStatus;
                            dto.ReportDate = reportStatusDTO.ReportDate;
                            dto.ReportDateInBS = reportStatusDTO.ReportDateInBS;
                            dto.ReportMonth = reportStatusDTO.ReportMonth;
                            dto.ReportYear = reportStatusDTO.ReportYear;
                            dto.ReportPeriod = reportStatusDTO.ReportPeriod;
                        }
                        listBranchReportStatus.Add(dto);
                    }
                }

                moduleSummary.listBranchWiseReportStatusDTOs = listBranchReportStatus;

                //moduleSummary.listBranchWiseReportStatusDTOs = await _BranchDrillingReportRepository.GetBranchWiseReportStatus(sqlParameters.ToArray());

                SearchParameters.Clear();

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
