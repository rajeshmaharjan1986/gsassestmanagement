using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;

using GSAssetManagement.Entity.Validation;
namespace GSAssetManagement.Web.Areas.UserManagement.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRoleDetails", Url = "/UserManagement/ApplicationRoleDetails", Parent = false)]
    [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRoleDetails", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class ApplicationRoleDetailsController : Controller
    {
        private readonly IApplicationRoleDetailsRepository _ApplicationRoleDetailsRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;
        public ApplicationRoleDetailsController(IApplicationRoleDetailsRepository applicationroledetailsRepository,
        IUnitOfWork unitOfWork,
        IExceptionLoggerRepository exceptionLoggerRepository)
        {
            this._ApplicationRoleDetailsRepository = applicationroledetailsRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork; ;
        }

        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRoleDetails", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index(Guid ParentPrimaryRecordId)
        {
            try
            {

                ModuleSummary moduleSummary = await _ApplicationRoleDetailsRepository.GetModuleBussinesLogicSetup();
                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ApplicationRoleId").ToList().ForEach(c => c.CurrentValue = ParentPrimaryRecordId);

                moduleSummary.SchemaName = ModuleName.UserManagement.ToString();
                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("ApplicationRoleId", ParentPrimaryRecordId));
                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));
                moduleSummary.SummaryRecord = await _ApplicationRoleDetailsRepository.GetAllByProcedure("Administrator.SP_GetApplicationRoleDetailsList", sqlParameters.ToArray());
                return View(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRoleDetails", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {

                ModuleSummary moduleSummary = await _ApplicationRoleDetailsRepository.GetModuleBussinesLogicSetup();

                var sqlParameters = SearchParameters.GetSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                moduleSummary.SummaryRecord = await _ApplicationRoleDetailsRepository.GetAllByProcedure("Administrator.SP_GetApplicationRoleDetailsList", sqlParameters.ToArray());

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRoleDetails", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Create(Guid ParentPrimaryRecordId)
        {
            try
            {
                ModuleSummary moduleSummary = await _ApplicationRoleDetailsRepository.GetModuleBussinesLogicSetup();
                if (!moduleSummary.IsParent && moduleSummary.DoRecordExists)
                {
                    return View("Details", moduleSummary);
                }
                else
                {
                    return View("Create", moduleSummary);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRoleDetails", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection model)
        {
            try
            {

                ApplicationRoleDetailsDTO applicationroledetailsDTO = new ApplicationRoleDetailsDTO();
                TryUpdateModel<ApplicationRoleDetailsDTO>(applicationroledetailsDTO);
                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ApplicationRoleDetailsDTO>(applicationroledetailsDTO);

                model.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    if (Request.Files.Count > 0)
                    {
                        HttpFileCollectionBase httpFileCollectionBases = Request.Files;
                        FileUploaderHelper.GetPath(httpFileCollectionBases);
                    }

                    Guid Id = this._ApplicationRoleDetailsRepository.Add(applicationroledetailsDTO, true); //AuthorizeViewHelper.IsAuthorize(ModuleName.UserManagement.ToString(), "ApplicationRoleDetails", CurrentAction.AutoAuthorise)
                    await this._unitOfWork.CommitAsync();

                    return new JsonHttpStatusResult(new
                    {
                        Id = Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                    }, System.Net.HttpStatusCode.OK);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRoleDetails", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Details(Guid Id)
        {
            try
            {
                var dto = await _ApplicationRoleDetailsRepository.GetDTOById(Id);
                ModuleSummary moduleSummary = await _ApplicationRoleDetailsRepository.GetModuleBussinesLogicSetup(dto, dto.RoleId);
                if (Request.IsAjaxRequest())
                {
                    return View("PartialDetails", moduleSummary);
                }
                else
                {
                    return View("Details", moduleSummary);
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRoleDetails", Action = CurrentAction.Edit)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Update(FormCollection formCollection)
        {
            try
            {

                ApplicationRoleDetailsDTO applicationroledetailsDTO = new ApplicationRoleDetailsDTO();
                TryUpdateModel<ApplicationRoleDetailsDTO>(applicationroledetailsDTO);
                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ApplicationRoleDetailsDTO>(applicationroledetailsDTO);

                formCollection.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    await this._ApplicationRoleDetailsRepository.Update(applicationroledetailsDTO, true);//AuthorizeViewHelper.IsAuthorize(ModuleName.UserManagement.ToString(), "ApplicationRoleDetails", CurrentAction.AutoAuthorise)
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = applicationroledetailsDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRoleDetails", Action = CurrentAction.Delete)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Delete(FormCollection formCollection)
        {
            try
            {

                ApplicationRoleDetailsDTO applicationroledetailsDTO = new ApplicationRoleDetailsDTO();
                TryUpdateModel<ApplicationRoleDetailsDTO>(applicationroledetailsDTO);

                formCollection.Clear();

                if (applicationroledetailsDTO != null)
                {
                    await this._ApplicationRoleDetailsRepository.Delete(applicationroledetailsDTO.Id, AuthorizeViewHelper.IsAuthorize(ModuleName.UserManagement.ToString(), "ApplicationRoleDetails", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = applicationroledetailsDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRoleDetails", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Authorise(FormCollection formCollection)
        {
            try
            {

                ApplicationRoleDetailsDTO applicationroledetailsDTO = new ApplicationRoleDetailsDTO();
                TryUpdateModel<ApplicationRoleDetailsDTO>(applicationroledetailsDTO);

                formCollection.Clear();

                if (applicationroledetailsDTO != null)
                {
                    await this._ApplicationRoleDetailsRepository.Authorise(applicationroledetailsDTO.Id);
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = applicationroledetailsDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRoleDetails", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Revert(FormCollection formCollection)
        {
            try
            {

                ApplicationRoleDetailsDTO applicationroledetailsDTO = new ApplicationRoleDetailsDTO();
                TryUpdateModel<ApplicationRoleDetailsDTO>(applicationroledetailsDTO);

                formCollection.Clear();

                if (applicationroledetailsDTO != null)
                {
                    await this._ApplicationRoleDetailsRepository.Revert(applicationroledetailsDTO.Id);
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = applicationroledetailsDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRoleDetails", Action = CurrentAction.Discard)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Discard(FormCollection formCollection)
        {
            try
            {

                ApplicationRoleDetailsDTO applicationroledetailsDTO = new ApplicationRoleDetailsDTO();
                TryUpdateModel<ApplicationRoleDetailsDTO>(applicationroledetailsDTO);

                formCollection.Clear();

                if (applicationroledetailsDTO != null)
                {
                    await this._ApplicationRoleDetailsRepository.DiscardChanges(applicationroledetailsDTO.Id);
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = applicationroledetailsDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
