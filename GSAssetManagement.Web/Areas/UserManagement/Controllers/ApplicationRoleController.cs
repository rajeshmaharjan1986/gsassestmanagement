using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;

using GSAssetManagement.Entity.Validation;
namespace GSAssetManagement.Web.Areas.UserManagement.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRole", Url = "/UserManagement/ApplicationRole", Parent = true)]
    [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRole", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class ApplicationRoleController : Controller
    {
        private readonly IApplicationRoleRepository _ApplicationRoleRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;
        public ApplicationRoleController(IApplicationRoleRepository applicationroleRepository,
        IUnitOfWork unitOfWork,
        IExceptionLoggerRepository exceptionLoggerRepository)
        {
            this._ApplicationRoleRepository = applicationroleRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork; ;
        }

        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRole", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            try
            {

                ModuleSummary moduleSummary = await _ApplicationRoleRepository.GetModuleBussinesLogicSetup(null, null, true, true);
                moduleSummary.SchemaName = ModuleName.UserManagement.ToString();
                List<SqlParameter> sqlParameters = new List<SqlParameter>();

                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));
                moduleSummary.SummaryRecord = await _ApplicationRoleRepository.GetAllByProcedure("Administrator", moduleSummary.ModuleSummaryName, sqlParameters.ToArray());
                return View(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRole", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {

                ModuleSummary moduleSummary = await _ApplicationRoleRepository.GetModuleBussinesLogicSetup(null, null, true, false);

                var sqlParameters = SearchParameters.GetSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                moduleSummary.SummaryRecord = await _ApplicationRoleRepository.GetAllByProcedure("Administrator", moduleSummary.ModuleSummaryName, sqlParameters.ToArray());

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRole", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Create()
        {
            try
            {

                ModuleSummary moduleSummary = await _ApplicationRoleRepository.GetModuleBussinesLogicSetup(null, null, false, true);
                if (!moduleSummary.IsParent && moduleSummary.DoRecordExists)
                {
                    return View("Details", moduleSummary);
                }
                else
                {
                    return View("Create", moduleSummary);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRole", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection model)
        {
            try
            {

                ApplicationRoleDTO applicationroleDTO = new ApplicationRoleDTO();
                TryUpdateModel<ApplicationRoleDTO>(applicationroleDTO);
                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ApplicationRoleDTO>(applicationroleDTO);

                model.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    Guid Id = this._ApplicationRoleRepository.InsertRecords(applicationroleDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.UserManagement.ToString(), "ApplicationRole", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    return new JsonHttpStatusResult(new
                    {
                        Id = Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                    }, System.Net.HttpStatusCode.OK);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRole", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Details(Guid Id)
        {
            try
            {

                ModuleSummary moduleSummary = await _ApplicationRoleRepository.GetModuleBussinesLogicSetup(Id, null, false, true);
                if (Request.IsAjaxRequest())
                {
                    return View("PartialDetails", moduleSummary);
                }
                else
                {
                    return View("Details", moduleSummary);
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRole", Action = CurrentAction.Edit)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Update(FormCollection formCollection)
        {
            try
            {

                ApplicationRoleDTO applicationroleDTO = new ApplicationRoleDTO();
                TryUpdateModel<ApplicationRoleDTO>(applicationroleDTO);
                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ApplicationRoleDTO>(applicationroleDTO);

                formCollection.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    await this._ApplicationRoleRepository.UpdateRecords(applicationroleDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.UserManagement.ToString(), "ApplicationRole", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = applicationroleDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRole", Action = CurrentAction.Delete)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Delete(FormCollection formCollection)
        {
            try
            {

                ApplicationRoleDTO applicationroleDTO = new ApplicationRoleDTO();
                TryUpdateModel<ApplicationRoleDTO>(applicationroleDTO);

                formCollection.Clear();

                if (applicationroleDTO != null)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);

                    //await this._ApplicationRoleRepository.Delete(applicationroleDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.UserManagement.ToString(), "ApplicationRole", CurrentAction.AutoAuthorise));
                    //await this._unitOfWork.CommitAsync();
                    //return Json(new
                    //{
                    //    Id = applicationroleDTO.Id,
                    //    IsSuccess = true,
                    //    ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    //}, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRole", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Authorise(FormCollection formCollection)
        {
            try
            {

                ApplicationRoleDTO applicationroleDTO = new ApplicationRoleDTO();
                TryUpdateModel<ApplicationRoleDTO>(applicationroleDTO);

                formCollection.Clear();

                if (applicationroleDTO != null)
                {
                    await this._ApplicationRoleRepository.AuthorizedRecords(applicationroleDTO);
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = applicationroleDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRole", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Revert(FormCollection formCollection)
        {
            try
            {

                ApplicationRoleDTO applicationroleDTO = new ApplicationRoleDTO();
                TryUpdateModel<ApplicationRoleDTO>(applicationroleDTO);

                formCollection.Clear();

                if (applicationroleDTO != null)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);

                    //await this._ApplicationRoleRepository.Revert(applicationroleDTO);
                    //await this._unitOfWork.CommitAsync();
                    //return Json(new
                    //{
                    //    Id = applicationroleDTO.Id,
                    //    IsSuccess = true,
                    //    ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    //}, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationRole", Action = CurrentAction.Discard)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Discard(FormCollection formCollection)
        {
            try
            {

                ApplicationRoleDTO applicationroleDTO = new ApplicationRoleDTO();
                TryUpdateModel<ApplicationRoleDTO>(applicationroleDTO);

                formCollection.Clear();

                if (applicationroleDTO != null)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);

                    //await this._ApplicationRoleRepository.DiscardChanges(applicationroleDTO);
                    //await this._unitOfWork.CommitAsync();
                    //return Json(new
                    //{
                    //    Id = applicationroleDTO.Id,
                    //    IsSuccess = true,
                    //    ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    //}, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
