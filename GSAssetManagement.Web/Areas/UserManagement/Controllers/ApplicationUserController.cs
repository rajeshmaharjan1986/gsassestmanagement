using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;

using GSAssetManagement.Entity.Validation;
using GSAssetManagement.Service;

namespace GSAssetManagement.Web.Areas.UserManagement.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUser", Url = "/UserManagement/ApplicationUser", Parent = true)]
    [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUser", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class ApplicationUserController : Controller
    {
        private readonly IApplicationUserRepository _ApplicationUserRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;
        public ApplicationUserController(IApplicationUserRepository applicationuserRepository,
        IUnitOfWork unitOfWork,
        IExceptionLoggerRepository exceptionLoggerRepository)
        {
            this._ApplicationUserRepository = applicationuserRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork; ;
        }

        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUser", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            try
            {

                ModuleSummary moduleSummary = await _ApplicationUserRepository.GetModuleBussinesLogicSetup(null, null, true, true);
                moduleSummary.SchemaName = ModuleName.UserManagement.ToString();
                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));
                moduleSummary.SummaryRecord = await _ApplicationUserRepository.GetAllByProcedure("Administrator", moduleSummary.ModuleSummaryName, sqlParameters.ToArray());
                return View(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUser", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {

                ModuleSummary moduleSummary = await _ApplicationUserRepository.GetModuleBussinesLogicSetup(null, null, true, false);

                var sqlParameters = SearchParameters.GetSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                moduleSummary.SummaryRecord = await _ApplicationUserRepository.GetAllByProcedure("Administrator", moduleSummary.ModuleSummaryName, sqlParameters.ToArray());

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUser", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Create()
        {
            try
            {

                ModuleSummary moduleSummary = await _ApplicationUserRepository.GetModuleBussinesLogicSetup(null, null, false, true);
                if (!moduleSummary.IsParent && moduleSummary.DoRecordExists)
                {
                    return View("Details", moduleSummary);
                }
                else
                {
                    return View("Create", moduleSummary);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUser", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection model)
        {
            try
            {

                ApplicationUserDTO applicationuserDTO = new ApplicationUserDTO();
                TryUpdateModel<ApplicationUserDTO>(applicationuserDTO);
                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ApplicationUserDTO>(applicationuserDTO);

                model.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    if (Request.Files.Count > 0)
                    {
                        HttpFileCollectionBase httpFileCollectionBases = Request.Files;
                        FileUploaderHelper.GetPath(httpFileCollectionBases);
                    }

                    Guid Id = await this._ApplicationUserRepository.InsertUpdateRecordWithOutPassword(applicationuserDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.UserManagement.ToString(), "ApplicationUser", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    if (Id.Equals(Guid.Empty))
                    {
                        EntityValidationResult entityValidationResult = new EntityValidationResult();
                        entityValidationResult.ColumnName = "User Name";
                        entityValidationResult.Name = "UserName";
                        entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("User Name", "Already exist username"));
                        EntityValidationResults.Add(entityValidationResult);

                        return Json(new
                        {
                            IsSuccess = false,
                            ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)
                        }, JsonRequestBehavior.DenyGet);
                    }
                    else
                    {
                        return new JsonHttpStatusResult(new
                        {
                            Id = Id,
                            IsSuccess = true,
                            ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                        }, System.Net.HttpStatusCode.OK);
                    }
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUser", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Details(Guid Id)
        {
            try
            {

                ModuleSummary moduleSummary = await _ApplicationUserRepository.GetModuleBussinesLogicSetup(Id, null, false, true);
                if (Request.IsAjaxRequest())
                {
                    return View("PartialDetails", moduleSummary);
                }
                else
                {
                    return View("Details", moduleSummary);
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUser", Action = CurrentAction.Edit)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Update(FormCollection formCollection)
        {
            try
            {

                ApplicationUserDTO applicationuserDTO = new ApplicationUserDTO();
                TryUpdateModel<ApplicationUserDTO>(applicationuserDTO);
                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ApplicationUserDTO>(applicationuserDTO);

                formCollection.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);

                    //await this._ApplicationUserRepository.Update(applicationuserDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.UserManagement.ToString(), "ApplicationUser", CurrentAction.AutoAuthorise));
                    //await this._unitOfWork.CommitAsync();
                    //return Json(new
                    //{
                    //    Id = applicationuserDTO.Id,
                    //    IsSuccess = true,
                    //    ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                    //}, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUser", Action = CurrentAction.Delete)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Delete(FormCollection formCollection)
        {
            try
            {

                ApplicationUserDTO applicationuserDTO = new ApplicationUserDTO();
                TryUpdateModel<ApplicationUserDTO>(applicationuserDTO);

                formCollection.Clear();

                if (applicationuserDTO != null)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", null)
                    }, JsonRequestBehavior.DenyGet);

                    //await this._ApplicationUserRepository.Delete(applicationuserDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.UserManagement.ToString(), "ApplicationUser", CurrentAction.AutoAuthorise));
                    //await this._unitOfWork.CommitAsync();
                    //return Json(new
                    //{
                    //    Id = applicationuserDTO.Id,
                    //    IsSuccess = true,
                    //    ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    //}, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUser", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Authorise(FormCollection formCollection)
        {
            try
            {

                ApplicationUserDTO applicationuserDTO = new ApplicationUserDTO();
                TryUpdateModel<ApplicationUserDTO>(applicationuserDTO);

                formCollection.Clear();

                if (applicationuserDTO != null)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", null)
                    }, JsonRequestBehavior.DenyGet);

                    //await this._ApplicationUserRepository.Authorise(applicationuserDTO);
                    //await this._unitOfWork.CommitAsync();
                    //return Json(new
                    //{
                    //    Id = applicationuserDTO.Id,
                    //    IsSuccess = true,
                    //    ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    //}, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUser", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Revert(FormCollection formCollection)
        {
            try
            {

                ApplicationUserDTO applicationuserDTO = new ApplicationUserDTO();
                TryUpdateModel<ApplicationUserDTO>(applicationuserDTO);

                formCollection.Clear();

                if (applicationuserDTO != null)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", null)
                    }, JsonRequestBehavior.DenyGet);

                    //await this._ApplicationUserRepository.Revert(applicationuserDTO);
                    //await this._unitOfWork.CommitAsync();
                    //return Json(new
                    //{
                    //    Id = applicationuserDTO.Id,
                    //    IsSuccess = true,
                    //    ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    //}, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUser", Action = CurrentAction.Discard)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Discard(FormCollection formCollection)
        {
            try
            {

                ApplicationUserDTO applicationuserDTO = new ApplicationUserDTO();
                TryUpdateModel<ApplicationUserDTO>(applicationuserDTO);

                formCollection.Clear();

                if (applicationuserDTO != null)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", null)
                    }, JsonRequestBehavior.DenyGet);

                    //await this._ApplicationUserRepository.DiscardChanges(applicationuserDTO);
                    //await this._unitOfWork.CommitAsync();
                    //return Json(new
                    //{
                    //    Id = applicationuserDTO.Id,
                    //    IsSuccess = true,
                    //    ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    //}, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        public async Task<ActionResult> GetStaffDetail(string staffId)
        {
            try
            {

                APIConsumtionService aPIConsumtionService = new APIConsumtionService();
                var staffDetail = aPIConsumtionService.GetStaff(staffId);

                if (staffDetail != null)
                {
                    //FIDataReaderByAPI fIDataReaderByAPI = new FIDataReaderByAPI();
                    //List<UserRoleInfo> userRoleInfos = fIDataReaderByAPI.GetUserRoleInfoByStaffId(staffDetail.staffId);

                    return Json(new
                    {
                        IsSuccess = true,
                        data = staffDetail
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        data = staffDetail
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
