using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;

using GSAssetManagement.Entity.Validation;
namespace GSAssetManagement.Web.Areas.UserManagement.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUserRole", Url = "/UserManagement/ApplicationUserRole", Parent = false)]
    [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUserRole", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class ApplicationUserRoleController : Controller
    {
        private readonly IApplicationUserRoleRepository _ApplicationUserRoleRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;
        public ApplicationUserRoleController(IApplicationUserRoleRepository applicationuserroleRepository,
        IUnitOfWork unitOfWork,
        IExceptionLoggerRepository exceptionLoggerRepository)
        {
            this._ApplicationUserRoleRepository = applicationuserroleRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork; ;
        }

        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUserRole", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index(Guid ParentPrimaryRecordId)
        {
            try
            {

                ModuleSummary moduleSummary = await _ApplicationUserRoleRepository.GetModuleBussinesLogicSetup();
                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ApplicationUserId").ToList().ForEach(c => c.CurrentValue = ParentPrimaryRecordId);

                moduleSummary.SchemaName = ModuleName.UserManagement.ToString();
                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("ApplicationUserId", ParentPrimaryRecordId));
                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));
                moduleSummary.SummaryRecord = await _ApplicationUserRoleRepository.GetAllByProcedure("Administrator.SP_GetApplicationUserRoleList", sqlParameters.ToArray());
                return View(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUserRole", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {

                ModuleSummary moduleSummary = await _ApplicationUserRoleRepository.GetModuleBussinesLogicSetup();

                var sqlParameters = SearchParameters.GetSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                moduleSummary.SummaryRecord = await _ApplicationUserRoleRepository.GetAllByProcedure("Administrator.SP_GetApplicationUserRoleList", sqlParameters.ToArray());

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUserRole", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Create(Guid ParentPrimaryRecordId)
        {
            try
            {

                ModuleSummary moduleSummary = await _ApplicationUserRoleRepository.GetModuleBussinesLogicSetup();
                if (!moduleSummary.IsParent && moduleSummary.DoRecordExists)
                {
                    return View("Details", moduleSummary);
                }
                else
                {
                    return View("Create", moduleSummary);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUserRole", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection model)
        {
            try
            {

                ApplicationUserRoleDTO applicationuserroleDTO = new ApplicationUserRoleDTO();
                TryUpdateModel<ApplicationUserRoleDTO>(applicationuserroleDTO);
                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ApplicationUserRoleDTO>(applicationuserroleDTO);

                model.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    if (Request.Files.Count > 0)
                    {
                        HttpFileCollectionBase httpFileCollectionBases = Request.Files;
                        FileUploaderHelper.GetPath(httpFileCollectionBases);
                    }

                    this._ApplicationUserRoleRepository.Add(applicationuserroleDTO);
                    await this._unitOfWork.CommitAsync();

                    return new JsonHttpStatusResult(new
                    {
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                    }, System.Net.HttpStatusCode.OK);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUserRole", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Details(Guid Id)
        {
            try
            {

                ModuleSummary moduleSummary = await _ApplicationUserRoleRepository.GetModuleBussinesLogicSetup();
                if (Request.IsAjaxRequest())
                {
                    return View("PartialDetails", moduleSummary);
                }
                else
                {
                    return View("Details", moduleSummary);
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUserRole", Action = CurrentAction.Edit)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Update(FormCollection formCollection)
        {
            try
            {

                ApplicationUserRoleDTO applicationuserroleDTO = new ApplicationUserRoleDTO();
                TryUpdateModel<ApplicationUserRoleDTO>(applicationuserroleDTO);
                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<ApplicationUserRoleDTO>(applicationuserroleDTO);

                formCollection.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);

                    //await this._ApplicationUserRoleRepository.Update(applicationuserroleDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.UserManagement.ToString(), "ApplicationUserRole", CurrentAction.AutoAuthorise));
                    //await this._unitOfWork.CommitAsync();
                    //return Json(new
                    //{
                    //    Id = applicationuserroleDTO.Id,
                    //    IsSuccess = true,
                    //    ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                    //}, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUserRole", Action = CurrentAction.Delete)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Delete(FormCollection formCollection)
        {
            try
            {

                ApplicationUserRoleDTO applicationuserroleDTO = new ApplicationUserRoleDTO();
                TryUpdateModel<ApplicationUserRoleDTO>(applicationuserroleDTO);

                formCollection.Clear();

                if (applicationuserroleDTO != null)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);

                    //await this._ApplicationUserRoleRepository.Delete(applicationuserroleDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.UserManagement.ToString(), "ApplicationUserRole", CurrentAction.AutoAuthorise));
                    //await this._unitOfWork.CommitAsync();
                    //return Json(new
                    //{
                    //    Id = applicationuserroleDTO.Id,
                    //    IsSuccess = true,
                    //    ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    //}, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUserRole", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Authorise(FormCollection formCollection)
        {
            try
            {

                ApplicationUserRoleDTO applicationuserroleDTO = new ApplicationUserRoleDTO();
                TryUpdateModel<ApplicationUserRoleDTO>(applicationuserroleDTO);

                formCollection.Clear();

                if (applicationuserroleDTO != null)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);

                    //await this._ApplicationUserRoleRepository.Authorise(applicationuserroleDTO);
                    //await this._unitOfWork.CommitAsync();
                    //return Json(new
                    //{
                    //    Id = applicationuserroleDTO.Id,
                    //    IsSuccess = true,
                    //    ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    //}, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUserRole", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Revert(FormCollection formCollection)
        {
            try
            {

                ApplicationUserRoleDTO applicationuserroleDTO = new ApplicationUserRoleDTO();
                TryUpdateModel<ApplicationUserRoleDTO>(applicationuserroleDTO);

                formCollection.Clear();

                if (applicationuserroleDTO != null)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);

                    //await this._ApplicationUserRoleRepository.Revert(applicationuserroleDTO);
                    //await this._unitOfWork.CommitAsync();
                    //return Json(new
                    //{
                    //    Id = applicationuserroleDTO.Id,
                    //    IsSuccess = true,
                    //    ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    //}, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.UserManagement, SubModuleName = "ApplicationUserRole", Action = CurrentAction.Discard)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Discard(FormCollection formCollection)
        {
            try
            {

                ApplicationUserRoleDTO applicationuserroleDTO = new ApplicationUserRoleDTO();
                TryUpdateModel<ApplicationUserRoleDTO>(applicationuserroleDTO);

                formCollection.Clear();

                if (applicationuserroleDTO != null)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);

                    //await this._ApplicationUserRoleRepository.DiscardChanges(applicationuserroleDTO);
                    //await this._unitOfWork.CommitAsync();
                    //return Json(new
                    //{
                    //    Id = applicationuserroleDTO.Id,
                    //    IsSuccess = true,
                    //    ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    //}, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
