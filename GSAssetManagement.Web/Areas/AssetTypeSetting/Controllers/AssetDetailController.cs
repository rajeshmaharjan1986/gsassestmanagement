using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;
using GSAssetManagement.Entity.Validation;
using GSAssetManagement.Service;

namespace GSAssetManagement.Web.Areas.AssetTypeSetting.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetDetail", Url = "/AssetTypeSetting/AssetDetail", Parent = true)]
    [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetDetail", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class AssetDetailController : Controller
    {
        private readonly IAssetDetailRepository _AssetDetailRepository;
        private readonly IStaticDataDetailsRepository _StaticDataDetailsRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;
        public AssetDetailController(IAssetDetailRepository assetdetailRepository,
            IStaticDataDetailsRepository staticDataDetailsRepository,
        IUnitOfWork unitOfWork,
        IExceptionLoggerRepository exceptionLoggerRepository)
        {
            this._AssetDetailRepository = assetdetailRepository;
            this._StaticDataDetailsRepository = staticDataDetailsRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork; ;
        }

        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetDetail", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            try
            {
                var deptId = ViewUserInformation.GetStaffDepartment();
                var branchId = ViewUserInformation.GetBranch() == "000" ? "1" : ViewUserInformation.GetBranch();

                ModuleSummary moduleSummary = await _AssetDetailRepository.GetModuleBussinesLogicSetup();
                moduleSummary.SchemaName = ModuleName.AssetTypeSetting.ToString();
                List<SqlParameter> sqlParameters = new List<SqlParameter>();

                if (deptId != "7" && deptId != "5")
                {
                    var branchSelectList = moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "BranchCode").FirstOrDefault().SelectList;

                    branchSelectList.ForEach(b =>
                    {
                        b.Selected = b.Value == branchId ? true : false;
                    });

                    moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "BranchCode").ToList().ForEach(c =>
                    {
                        c.CurrentValue = branchId;
                        c.HtmlDataType = "hidden";
                        c.SelectList = branchSelectList;
                    });
                    sqlParameters.Add(new SqlParameter("BranchCode", branchId));
                }


                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));
                moduleSummary.SummaryRecord = await _AssetDetailRepository.GetAllByProcedure("AssetTypeSetting.SP_GetAssetDetailList", sqlParameters.ToArray());
                return View(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetDetail", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {

                ModuleSummary moduleSummary = await _AssetDetailRepository.GetModuleBussinesLogicSetup();

                var sqlParameters = SearchParameters.GetSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                var deptId = ViewUserInformation.GetStaffDepartment();
                var branchId = ViewUserInformation.GetBranch() == "000" ? "1" : ViewUserInformation.GetBranch();
                if (deptId != "7" && deptId != "5")
                {
                    var branchCode = sqlParameters.Where(x => x.ParameterName == "@BranchCode").FirstOrDefault();
                    if (branchCode == null)
                    {
                        sqlParameters.Add(new SqlParameter("BranchCode", branchId));
                    }
                }

                moduleSummary.SummaryRecord = await _AssetDetailRepository.GetAllByProcedure("AssetTypeSetting.SP_GetAssetDetailList", sqlParameters.ToArray());

                SearchParameters.Clear();

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetDetail", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Create()
        {
            try
            {
                var deptId = ViewUserInformation.GetStaffDepartment();
                var branchId = ViewUserInformation.GetBranch() == "000" ? "1" : ViewUserInformation.GetBranch();

                ModuleSummary moduleSummary = await _AssetDetailRepository.GetModuleBussinesLogicSetup();
                moduleSummary.SchemaName = ModuleName.AssetTypeSetting.ToString();

                if (deptId != "7" && deptId != "5")
                {
                    var branchSelectList = moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "BranchCode").FirstOrDefault().SelectList;


                    branchSelectList.ForEach(b =>
                    {
                        b.Selected = b.Value == branchId ? true : false;
                    });

                    moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "BranchCode").ToList().ForEach(c =>
                    {
                        c.CurrentValue = branchId;
                        c.Attributes.Add("disabled", "");
                        c.SelectList = branchSelectList;
                    });
                }

                moduleSummary.ListStaticDataDetailsDTO = await _StaticDataDetailsRepository.GetStaticDataList("Location", "");

                if (!moduleSummary.IsParent && moduleSummary.DoRecordExists)
                {
                    return View("Details", moduleSummary);
                }
                else
                {
                    return View("Create", moduleSummary);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> NumberChange(Nullable<Guid> Id, int number, string assetCategory,Guid assetCategoryId)
        {
            try
            {
                var deptId = ViewUserInformation.GetStaffDepartment();
                var branchId = ViewUserInformation.GetBranch() == "000" ? "1" : ViewUserInformation.GetBranch();

                ModuleSummary moduleSummary = await _AssetDetailRepository.GetModuleBussinesLogicSetup();
                moduleSummary.SchemaName = ModuleName.AssetTypeSetting.ToString();

                if ((assetCategoryId == Guid.Parse("0C264D45-9790-40AE-A899-32DB10A6611F") && number > 10)//Panic Button
                || (assetCategoryId == Guid.Parse("5876A566-80FD-4853-9E1D-4533FFB9BAD4") && number > 10)//Door Access Control (Electronic)
                || (assetCategoryId == Guid.Parse("6C120E17-EC7F-446F-8B9F-4768A0B4858E") && number > 10)//Smoke Detector
                || (assetCategoryId == Guid.Parse("055BC4E8-2327-4434-A263-4A2B048A7890") && number > 20)//CCTV and Camera
                || (assetCategoryId == Guid.Parse("2D6A8477-C5B6-4977-913E-5445C9A6B763") && number > 20)//Fire Extinguisher
                || (assetCategoryId == Guid.Parse("15DB2277-8D33-40FB-A50E-D8B14D2FC3CA") && number > 5)//Metal Detector
                    )
                {
                    string numberLimit = "5";
                    if (assetCategoryId == Guid.Parse("0C264D45-9790-40AE-A899-32DB10A6611F"))
                    {
                        numberLimit = "10";
                    }
                    else if (assetCategoryId == Guid.Parse("5876A566-80FD-4853-9E1D-4533FFB9BAD4"))
                    {
                        numberLimit = "10";
                    }
                    else if (assetCategoryId == Guid.Parse("6C120E17-EC7F-446F-8B9F-4768A0B4858E"))
                    {
                        numberLimit = "10";
                    }
                    else if (assetCategoryId == Guid.Parse("055BC4E8-2327-4434-A263-4A2B048A7890"))
                    {
                        numberLimit = "20";
                    }
                    else if (assetCategoryId == Guid.Parse("2D6A8477-C5B6-4977-913E-5445C9A6B763"))
                    {
                        numberLimit = "20";
                    }
                    else if (assetCategoryId == Guid.Parse("15DB2277-8D33-40FB-A50E-D8B14D2FC3CA"))
                    {
                        numberLimit = "5";
                    }

                    return new JsonHttpStatusResult(new
                    {
                        Id = Id,
                        IsSuccess = false,
                        ResponseView = "Number Limit for selected category is " + numberLimit
                    }, System.Net.HttpStatusCode.OK);
                }




                if (Id == null)
                {
                    List<AssetExtraDetailDTO> array = new List<AssetExtraDetailDTO>();
                    for (int i = 1; i <= number; i++)
                    {
                        AssetExtraDetailDTO assetExtraDetailDTO = new AssetExtraDetailDTO();
                        array.Add(assetExtraDetailDTO);
                    }

                    moduleSummary.ListAssetExtraDetailDTO.AddRange(array);
                }
                else
                {
                    var dto = await _AssetDetailRepository.GetAssetDetailDTOById(Id.Value);
                    if (number > dto.assetExtraDetailDTOs.Count())
                    {
                        List<AssetExtraDetailDTO> array = new List<AssetExtraDetailDTO>();
                        for (int i = 1; i <= number - dto.assetExtraDetailDTOs.Count(); i++)
                        {
                            AssetExtraDetailDTO assetExtraDetailDTO = new AssetExtraDetailDTO();
                            array.Add(assetExtraDetailDTO);
                        }
                        moduleSummary.ListAssetExtraDetailDTO.AddRange(array);
                        moduleSummary.ListAssetExtraDetailDTO.AddRange(dto.assetExtraDetailDTOs);
                    }
                    else if (dto.assetExtraDetailDTOs.Count() == number)
                    {
                        moduleSummary.ListAssetExtraDetailDTO.AddRange(dto.assetExtraDetailDTOs);
                    }
                    else
                    {
                        dto.assetExtraDetailDTOs.RemoveRange(number + 1, dto.assetExtraDetailDTOs.Count());
                        moduleSummary.ListAssetExtraDetailDTO.AddRange(dto.assetExtraDetailDTOs);
                    }
                }

                moduleSummary.ListStaticDataDetailsDTO = await _StaticDataDetailsRepository.GetStaticDataList("Location", assetCategory);

                return PartialView("AssetExtraCreateView", moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetDetail", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection model)
        {
            try
            {

                AssetDetailDTO assetdetailDTO = new AssetDetailDTO();
                TryUpdateModel<AssetDetailDTO>(assetdetailDTO);
                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<AssetDetailDTO>(assetdetailDTO);

                if ((assetdetailDTO.AssetCategoryId == Guid.Parse("0C264D45-9790-40AE-A899-32DB10A6611F") && assetdetailDTO.AssetNumber > 10)//Panic Button
                || (assetdetailDTO.AssetCategoryId == Guid.Parse("5876A566-80FD-4853-9E1D-4533FFB9BAD4") && assetdetailDTO.AssetNumber > 10)//Door Access Control (Electronic)
                || (assetdetailDTO.AssetCategoryId == Guid.Parse("6C120E17-EC7F-446F-8B9F-4768A0B4858E") && assetdetailDTO.AssetNumber > 10)//Smoke Detector
                || (assetdetailDTO.AssetCategoryId == Guid.Parse("055BC4E8-2327-4434-A263-4A2B048A7890") && assetdetailDTO.AssetNumber > 20)//CCTV and Camera
                || (assetdetailDTO.AssetCategoryId == Guid.Parse("2D6A8477-C5B6-4977-913E-5445C9A6B763") && assetdetailDTO.AssetNumber > 20)//Fire Extinguisher
                || (assetdetailDTO.AssetCategoryId == Guid.Parse("15DB2277-8D33-40FB-A50E-D8B14D2FC3CA") && assetdetailDTO.AssetNumber > 5)//Metal Detector
                    )
                {
                    string numberLimit = "5";
                    if (assetdetailDTO.AssetCategoryId == Guid.Parse("0C264D45-9790-40AE-A899-32DB10A6611F")) {
                        numberLimit = "10";
                    }
                    else if (assetdetailDTO.AssetCategoryId == Guid.Parse("5876A566-80FD-4853-9E1D-4533FFB9BAD4"))
                    {
                        numberLimit = "10";
                    }
                    else if (assetdetailDTO.AssetCategoryId == Guid.Parse("6C120E17-EC7F-446F-8B9F-4768A0B4858E"))
                    {
                        numberLimit = "10";
                    }
                    else if (assetdetailDTO.AssetCategoryId == Guid.Parse("055BC4E8-2327-4434-A263-4A2B048A7890"))
                    {
                        numberLimit = "20";
                    }
                    else if (assetdetailDTO.AssetCategoryId == Guid.Parse("2D6A8477-C5B6-4977-913E-5445C9A6B763"))
                    {
                        numberLimit = "20";
                    }
                    else if (assetdetailDTO.AssetCategoryId == Guid.Parse("15DB2277-8D33-40FB-A50E-D8B14D2FC3CA"))
                    {
                        numberLimit = "5";
                    }

                    EntityValidationResult entityValidationResult = new EntityValidationResult();
                    entityValidationResult.ColumnName = "Number of Assets";
                    entityValidationResult.Name = "Number of Assets";
                    entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Number of Assets", "Number of Assets should not excess than " + numberLimit));

                    EntityValidationResults.Add(entityValidationResult);
                }

                List<string> listString = new List<string>();

                for (int i = 1; i <= assetdetailDTO.AssetNumber; i++)
                {
                    AssetExtraDetailDTO assetExtraDetailDTO = new AssetExtraDetailDTO();
                    assetExtraDetailDTO.Location = model["Location" + i.ToString()];
                    assetdetailDTO.assetExtraDetailDTOs.Add(assetExtraDetailDTO);

                    if (!listString.Contains(model["Location" + i.ToString()]))
                    {
                        listString.Add(model["Location" + i.ToString()]);
                    }

                    if (string.IsNullOrEmpty(assetExtraDetailDTO.Location))
                    {
                        EntityValidationResult entityValidationResult = new EntityValidationResult();
                        entityValidationResult.ColumnName = "Location";
                        entityValidationResult.Name = "Location";
                        entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Location", "Location is required."));

                        EntityValidationResults.Add(entityValidationResult);
                    }
                }

                if (listString.Count() < assetdetailDTO.assetExtraDetailDTOs.Count()) {
                    EntityValidationResult entityValidationResult = new EntityValidationResult();
                    entityValidationResult.ColumnName = "Duplicate Location";
                    entityValidationResult.Name = "Duplicate Location";
                    entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Duplicate Location", "Location should not be select more than one."));

                    EntityValidationResults.Add(entityValidationResult);
                }

                model.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    if (Request.Files.Count > 0)
                    {
                        HttpFileCollectionBase httpFileCollectionBases = Request.Files;
                        FileUploaderHelper.GetPath(httpFileCollectionBases);
                    }

                    APIConsumtionService aPIConsumtionService = new APIConsumtionService();
                    List<BranchList> branchLists = aPIConsumtionService.GetBranchList();
                    BranchList branch = branchLists.Where(x => x.BranchID == assetdetailDTO.BranchCode).FirstOrDefault();
                    if (branch != null)
                    {
                        assetdetailDTO.BranchName = branch.BranchName;
                    }

                    Guid Id = this._AssetDetailRepository.AddAssetDetail(assetdetailDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AssetTypeSetting.ToString(), "AssetDetail", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    return new JsonHttpStatusResult(new
                    {
                        Id = Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                    }, System.Net.HttpStatusCode.OK);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetDetail", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Details(Guid Id)
        {
            try
            {
                var deptId = ViewUserInformation.GetStaffDepartment();
                //var branchId = ViewUserInformation.GetBranch() == "000" ? "1" : ViewUserInformation.GetBranch();

                var dto = await _AssetDetailRepository.GetAssetDetailDTOById(Id);
                ModuleSummary moduleSummary = await _AssetDetailRepository.GetModuleBussinesLogicSetup(dto, null);
                moduleSummary.SchemaName = ModuleName.AssetTypeSetting.ToString();
                moduleSummary.ListAssetExtraDetailDTO = dto.assetExtraDetailDTOs;

                if (deptId != "7" && deptId != "5")
                {
                    moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "BranchCode").ToList().ForEach(c =>
                    {
                        c.Attributes.Add("disabled", "");
                    });
                }

                moduleSummary.ListStaticDataDetailsDTO = await _StaticDataDetailsRepository.GetStaticDataList("Location", "");

                if (Request.IsAjaxRequest())
                {
                    return View("PartialDetails", moduleSummary);
                }
                else
                {
                    return View("Details", moduleSummary);
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetDetail", Action = CurrentAction.Edit)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Update(FormCollection formCollection)
        {
            try
            {

                AssetDetailDTO assetdetailDTO = new AssetDetailDTO();
                TryUpdateModel<AssetDetailDTO>(assetdetailDTO);
                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<AssetDetailDTO>(assetdetailDTO);

                if ((assetdetailDTO.AssetCategoryId == Guid.Parse("0C264D45-9790-40AE-A899-32DB10A6611F") && assetdetailDTO.AssetNumber > 10)//Panic Button
                || (assetdetailDTO.AssetCategoryId == Guid.Parse("5876A566-80FD-4853-9E1D-4533FFB9BAD4") && assetdetailDTO.AssetNumber > 10)//Door Access Control (Electronic)
                || (assetdetailDTO.AssetCategoryId == Guid.Parse("6C120E17-EC7F-446F-8B9F-4768A0B4858E") && assetdetailDTO.AssetNumber > 10)//Smoke Detector
                || (assetdetailDTO.AssetCategoryId == Guid.Parse("055BC4E8-2327-4434-A263-4A2B048A7890") && assetdetailDTO.AssetNumber > 20)//CCTV and Camera
                || (assetdetailDTO.AssetCategoryId == Guid.Parse("2D6A8477-C5B6-4977-913E-5445C9A6B763") && assetdetailDTO.AssetNumber > 20)//Fire Extinguisher
                || (assetdetailDTO.AssetCategoryId == Guid.Parse("15DB2277-8D33-40FB-A50E-D8B14D2FC3CA") && assetdetailDTO.AssetNumber > 5)//Metal Detector
                    )
                {
                    string numberLimit = "5";
                    if (assetdetailDTO.AssetCategoryId == Guid.Parse("0C264D45-9790-40AE-A899-32DB10A6611F"))
                    {
                        numberLimit = "10";
                    }
                    else if (assetdetailDTO.AssetCategoryId == Guid.Parse("5876A566-80FD-4853-9E1D-4533FFB9BAD4"))
                    {
                        numberLimit = "10";
                    }
                    else if (assetdetailDTO.AssetCategoryId == Guid.Parse("6C120E17-EC7F-446F-8B9F-4768A0B4858E"))
                    {
                        numberLimit = "10";
                    }
                    else if (assetdetailDTO.AssetCategoryId == Guid.Parse("055BC4E8-2327-4434-A263-4A2B048A7890"))
                    {
                        numberLimit = "20";
                    }
                    else if (assetdetailDTO.AssetCategoryId == Guid.Parse("2D6A8477-C5B6-4977-913E-5445C9A6B763"))
                    {
                        numberLimit = "20";
                    }
                    else if (assetdetailDTO.AssetCategoryId == Guid.Parse("15DB2277-8D33-40FB-A50E-D8B14D2FC3CA"))
                    {
                        numberLimit = "5";
                    }

                    EntityValidationResult entityValidationResult = new EntityValidationResult();
                    entityValidationResult.ColumnName = "Number of Assets";
                    entityValidationResult.Name = "Number of Assets";
                    entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Number of Assets", "Number of Assets should not excess than " + numberLimit));

                    EntityValidationResults.Add(entityValidationResult);
                }

                List<string> listString = new List<string>();

                for (int i = 1; i <= assetdetailDTO.AssetNumber; i++)
                {
                    AssetExtraDetailDTO assetExtraDetailDTO = new AssetExtraDetailDTO();
                    assetExtraDetailDTO.Location = formCollection["Location" + i.ToString()];
                    assetdetailDTO.assetExtraDetailDTOs.Add(assetExtraDetailDTO);


                    if (!listString.Contains(formCollection["Location" + i.ToString()]))
                    {
                        listString.Add(formCollection["Location" + i.ToString()]);
                    }

                    if (string.IsNullOrEmpty(assetExtraDetailDTO.Location))
                    {
                        EntityValidationResult entityValidationResult = new EntityValidationResult();
                        entityValidationResult.ColumnName = "Location";
                        entityValidationResult.Name = "Location";
                        entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Location", "Location is required."));

                        EntityValidationResults.Add(entityValidationResult);
                    }
                }

                if (listString.Count() < assetdetailDTO.assetExtraDetailDTOs.Count())
                {
                    EntityValidationResult entityValidationResult = new EntityValidationResult();
                    entityValidationResult.ColumnName = "Duplicate Location";
                    entityValidationResult.Name = "Duplicate Location";
                    entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Duplicate Location", "Location should not be select more than one."));

                    EntityValidationResults.Add(entityValidationResult);
                }

                formCollection.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    APIConsumtionService aPIConsumtionService = new APIConsumtionService();
                    List<BranchList> branchLists = aPIConsumtionService.GetBranchList();
                    BranchList branch = branchLists.Where(x => x.BranchID == assetdetailDTO.BranchCode).FirstOrDefault();
                    if (branch != null)
                    {
                        assetdetailDTO.BranchName = branch.BranchName;
                    }

                    await this._AssetDetailRepository.UpdateAssetDetail(assetdetailDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AssetTypeSetting.ToString(), "AssetDetail", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = assetdetailDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetDetail", Action = CurrentAction.Delete)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Delete(FormCollection formCollection)
        {
            try
            {

                AssetDetailDTO assetdetailDTO = new AssetDetailDTO();
                TryUpdateModel<AssetDetailDTO>(assetdetailDTO);

                formCollection.Clear();

                if (assetdetailDTO != null)
                {
                    await this._AssetDetailRepository.Delete(assetdetailDTO.Id, AuthorizeViewHelper.IsAuthorize(ModuleName.AssetTypeSetting.ToString(), "AssetDetail", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = assetdetailDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetDetail", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Authorise(FormCollection formCollection)
        {
            try
            {

                AssetDetailDTO assetdetailDTO = new AssetDetailDTO();
                TryUpdateModel<AssetDetailDTO>(assetdetailDTO);

                formCollection.Clear();

                if (assetdetailDTO != null)
                {
                    await this._AssetDetailRepository.Authorise(assetdetailDTO.Id);
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = assetdetailDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetDetail", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Revert(FormCollection formCollection)
        {
            try
            {

                AssetDetailDTO assetdetailDTO = new AssetDetailDTO();
                TryUpdateModel<AssetDetailDTO>(assetdetailDTO);

                formCollection.Clear();

                if (assetdetailDTO != null)
                {
                    await this._AssetDetailRepository.Revert(assetdetailDTO.Id);
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = assetdetailDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetDetail", Action = CurrentAction.Discard)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Discard(FormCollection formCollection)
        {
            try
            {

                AssetDetailDTO assetdetailDTO = new AssetDetailDTO();
                TryUpdateModel<AssetDetailDTO>(assetdetailDTO);

                formCollection.Clear();

                if (assetdetailDTO != null)
                {
                    await this._AssetDetailRepository.DiscardChanges(assetdetailDTO.Id);
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = assetdetailDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
