using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;
using GSAssetManagement.Entity.Validation;
namespace GSAssetManagement.Web.Areas.AssetTypeSetting.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetCategory", Url = "/AssetTypeSetting/AssetCategory", Parent = true)]
    [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetCategory", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class AssetCategoryController : Controller
    {
        private readonly IAssetCategoryRepository _AssetCategoryRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;
        public AssetCategoryController(IAssetCategoryRepository assetcategoryRepository,
        IUnitOfWork unitOfWork,
        IExceptionLoggerRepository exceptionLoggerRepository)
        {
            this._AssetCategoryRepository = assetcategoryRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork; ;
        }

        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetCategory", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            try
            {

                ModuleSummary moduleSummary = await _AssetCategoryRepository.GetModuleBussinesLogicSetup();
                moduleSummary.SchemaName = ModuleName.AssetTypeSetting.ToString();
                List<SqlParameter> sqlParameters = new List<SqlParameter>();

                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));
                moduleSummary.SummaryRecord = await _AssetCategoryRepository.GetAllByProcedure("AssetTypeSetting.SP_GetAssetCategoryList", sqlParameters.ToArray());
                return View(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetCategory", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {

                ModuleSummary moduleSummary = await _AssetCategoryRepository.GetModuleBussinesLogicSetup();

                var sqlParameters = SearchParameters.GetSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                moduleSummary.SummaryRecord = await _AssetCategoryRepository.GetAllByProcedure("AssetTypeSetting.SP_GetAssetCategoryList", sqlParameters.ToArray());

                SearchParameters.Clear();

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetCategory", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Create()
        {
            try
            {

                ModuleSummary moduleSummary = await _AssetCategoryRepository.GetModuleBussinesLogicSetup();
                moduleSummary.SchemaName = ModuleName.AssetTypeSetting.ToString();
                if (!moduleSummary.IsParent && moduleSummary.DoRecordExists)
                {
                    return View("Details", moduleSummary);
                }
                else
                {
                    return View("Create", moduleSummary);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetCategory", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection model)
        {
            try
            {

                AssetCategoryDTO assetcategoryDTO = new AssetCategoryDTO();
                TryUpdateModel<AssetCategoryDTO>(assetcategoryDTO);
                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<AssetCategoryDTO>(assetcategoryDTO);

                model.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    if (Request.Files.Count > 0)
                    {
                        HttpFileCollectionBase httpFileCollectionBases = Request.Files;
                        FileUploaderHelper.GetPath(httpFileCollectionBases);
                    }

                    Guid Id = this._AssetCategoryRepository.Add(assetcategoryDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AssetTypeSetting.ToString(), "AssetCategory", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();

                    return new JsonHttpStatusResult(new
                    {
                        Id = Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                    }, System.Net.HttpStatusCode.OK);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetCategory", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Details(Guid Id)
        {
            try
            {

                var dto = await _AssetCategoryRepository.GetDTOById(Id);
                ModuleSummary moduleSummary = await _AssetCategoryRepository.GetModuleBussinesLogicSetup(dto, null);
                moduleSummary.SchemaName = ModuleName.AssetTypeSetting.ToString();
                if (Request.IsAjaxRequest())
                {
                    return View("PartialDetails", moduleSummary);
                }
                else
                {
                    return View("Details", moduleSummary);
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetCategory", Action = CurrentAction.Edit)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Update(FormCollection formCollection)
        {
            try
            {

                AssetCategoryDTO assetcategoryDTO = new AssetCategoryDTO();
                TryUpdateModel<AssetCategoryDTO>(assetcategoryDTO);
                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<AssetCategoryDTO>(assetcategoryDTO);

                formCollection.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    await this._AssetCategoryRepository.Update(assetcategoryDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.AssetTypeSetting.ToString(), "AssetCategory", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = assetcategoryDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetCategory", Action = CurrentAction.Delete)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Delete(FormCollection formCollection)
        {
            try
            {

                AssetCategoryDTO assetcategoryDTO = new AssetCategoryDTO();
                TryUpdateModel<AssetCategoryDTO>(assetcategoryDTO);

                formCollection.Clear();

                if (assetcategoryDTO != null)
                {
                    await this._AssetCategoryRepository.Delete(assetcategoryDTO.Id, AuthorizeViewHelper.IsAuthorize(ModuleName.AssetTypeSetting.ToString(), "AssetCategory", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = assetcategoryDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetCategory", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Authorise(FormCollection formCollection)
        {
            try
            {

                AssetCategoryDTO assetcategoryDTO = new AssetCategoryDTO();
                TryUpdateModel<AssetCategoryDTO>(assetcategoryDTO);

                formCollection.Clear();

                if (assetcategoryDTO != null)
                {
                    await this._AssetCategoryRepository.Authorise(assetcategoryDTO.Id);
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = assetcategoryDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetCategory", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Revert(FormCollection formCollection)
        {
            try
            {

                AssetCategoryDTO assetcategoryDTO = new AssetCategoryDTO();
                TryUpdateModel<AssetCategoryDTO>(assetcategoryDTO);

                formCollection.Clear();

                if (assetcategoryDTO != null)
                {
                    await this._AssetCategoryRepository.Revert(assetcategoryDTO.Id);
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = assetcategoryDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.AssetTypeSetting, SubModuleName = "AssetCategory", Action = CurrentAction.Discard)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Discard(FormCollection formCollection)
        {
            try
            {

                AssetCategoryDTO assetcategoryDTO = new AssetCategoryDTO();
                TryUpdateModel<AssetCategoryDTO>(assetcategoryDTO);

                formCollection.Clear();

                if (assetcategoryDTO != null)
                {
                    await this._AssetCategoryRepository.DiscardChanges(assetcategoryDTO.Id);
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = assetcategoryDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
