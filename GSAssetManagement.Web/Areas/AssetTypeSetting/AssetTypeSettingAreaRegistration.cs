﻿using System.Web.Mvc;

namespace GSAssetManagement.Web.Areas.AssetTypeSetting
{
    public class AssetTypeSettingAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "AssetTypeSetting";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "AssetTypeSetting_default",
                "AssetTypeSetting/{controller}/{action}",
                new { action = "Index" }
            );
        }
    }
}