﻿using System.Web.Mvc;

namespace GSAssetManagement.Web.Areas.DrillingReport
{
    public class DrillingReportAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "DrillingReport";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "DrillingReport_default",
                "DrillingReport/{controller}/{action}",
                new { action = "Index" }
            );
        }
    }
}