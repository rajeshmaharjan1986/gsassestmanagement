using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;
using GSAssetManagement.Entity.Validation;
namespace GSAssetManagement.Web.Areas.DrillingReport.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.DrillingReport, SubModuleName = "AssetWorkingStatus", Url = "/DrillingReport/AssetWorkingStatus", Parent = false)]
    [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "AssetWorkingStatus", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class AssetWorkingStatusController : Controller
    {
        private readonly IAssetWorkingStatusRepository _AssetWorkingStatusRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;
        public AssetWorkingStatusController(IAssetWorkingStatusRepository assetworkingstatusRepository,
        IUnitOfWork unitOfWork,
        IExceptionLoggerRepository exceptionLoggerRepository)
        {
            this._AssetWorkingStatusRepository = assetworkingstatusRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork; ;
        }

        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "AssetWorkingStatus", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index(Guid ParentPrimaryRecordId)
        {
            try
            {
                
                ModuleSummary moduleSummary = await _AssetWorkingStatusRepository.GetModuleBussinesLogicSetup();
                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "BranchDrillingReportId").ToList().ForEach(c => c.CurrentValue = ParentPrimaryRecordId);
                moduleSummary.SchemaName = ModuleName.DrillingReport.ToString();
                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                sqlParameters.Add(new SqlParameter("BranchDrillingReportId", ParentPrimaryRecordId));
                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));
                moduleSummary.SummaryRecord = await _AssetWorkingStatusRepository.GetAllByProcedure("DrillingReport.SP_GetAssetWorkingStatusList", sqlParameters.ToArray());
                return View(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "AssetWorkingStatus", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {

                ModuleSummary moduleSummary = await _AssetWorkingStatusRepository.GetModuleBussinesLogicSetup();

                var sqlParameters = SearchParameters.GetSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                moduleSummary.SummaryRecord = await _AssetWorkingStatusRepository.GetAllByProcedure("DrillingReport.SP_GetAssetWorkingStatusList", sqlParameters.ToArray());

                SearchParameters.Clear();

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "AssetWorkingStatus", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Create(Guid ParentPrimaryRecordId)
        {
            try
            {

                ModuleSummary moduleSummary = await _AssetWorkingStatusRepository.GetModuleBussinesLogicSetup();
                moduleSummary.SchemaName = ModuleName.DrillingReport.ToString();
                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "BranchDrillingReportId").ToList().ForEach(c => c.CurrentValue = ParentPrimaryRecordId);
                if (!moduleSummary.IsParent && moduleSummary.DoRecordExists)
                {
                    return View("Details", moduleSummary);
                }
                else
                {
                    return View("Create", moduleSummary);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "AssetWorkingStatus", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection model)
        {
            try
            {

                AssetWorkingStatusDTO assetworkingstatusDTO = new AssetWorkingStatusDTO();
                TryUpdateModel<AssetWorkingStatusDTO>(assetworkingstatusDTO);
                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<AssetWorkingStatusDTO>(assetworkingstatusDTO);

                model.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    if (Request.Files.Count > 0)
                    {
                        HttpFileCollectionBase httpFileCollectionBases = Request.Files;
                        FileUploaderHelper.GetPath(httpFileCollectionBases);
                    }

                    Guid Id = this._AssetWorkingStatusRepository.Add(assetworkingstatusDTO);
                    await this._unitOfWork.CommitAsync();

                    return new JsonHttpStatusResult(new
                    {
                        Id = Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                    }, System.Net.HttpStatusCode.OK);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "AssetWorkingStatus", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Details(Guid Id)
        {
            try
            {

                var dto = await _AssetWorkingStatusRepository.GetDTOById(Id);
                ModuleSummary moduleSummary = await _AssetWorkingStatusRepository.GetModuleBussinesLogicSetup(dto, dto.BranchDrillingReportId);
                moduleSummary.SchemaName = ModuleName.DrillingReport.ToString();
                if (Request.IsAjaxRequest())
                {
                    return View("PartialDetails", moduleSummary);
                }
                else
                {
                    return View("Details", moduleSummary);
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "AssetWorkingStatus", Action = CurrentAction.Edit)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Update(FormCollection formCollection)
        {
            try
            {

                AssetWorkingStatusDTO assetworkingstatusDTO = new AssetWorkingStatusDTO();
                TryUpdateModel<AssetWorkingStatusDTO>(assetworkingstatusDTO);
                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<AssetWorkingStatusDTO>(assetworkingstatusDTO);

                formCollection.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    await this._AssetWorkingStatusRepository.Update(assetworkingstatusDTO);
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = assetworkingstatusDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "AssetWorkingStatus", Action = CurrentAction.Delete)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Delete(FormCollection formCollection)
        {
            try
            {

                AssetWorkingStatusDTO assetworkingstatusDTO = new AssetWorkingStatusDTO();
                TryUpdateModel<AssetWorkingStatusDTO>(assetworkingstatusDTO);

                formCollection.Clear();

                if (assetworkingstatusDTO != null)
                {
                    //await this._AssetWorkingStatusRepository.Delete(assetworkingstatusDTO.Id);
                    //await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = assetworkingstatusDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "AssetWorkingStatus", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Authorise(FormCollection formCollection)
        {
            try
            {

                AssetWorkingStatusDTO assetworkingstatusDTO = new AssetWorkingStatusDTO();
                TryUpdateModel<AssetWorkingStatusDTO>(assetworkingstatusDTO);

                formCollection.Clear();

                if (assetworkingstatusDTO != null)
                {
                    //await this._AssetWorkingStatusRepository.Authorise(assetworkingstatusDTO.Id);
                    //await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = assetworkingstatusDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "AssetWorkingStatus", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Revert(FormCollection formCollection)
        {
            try
            {

                AssetWorkingStatusDTO assetworkingstatusDTO = new AssetWorkingStatusDTO();
                TryUpdateModel<AssetWorkingStatusDTO>(assetworkingstatusDTO);

                formCollection.Clear();

                if (assetworkingstatusDTO != null)
                {
                    //await this._AssetWorkingStatusRepository.Revert(assetworkingstatusDTO.Id);
                    //await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = assetworkingstatusDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "AssetWorkingStatus", Action = CurrentAction.Discard)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Discard(FormCollection formCollection)
        {
            try
            {

                AssetWorkingStatusDTO assetworkingstatusDTO = new AssetWorkingStatusDTO();
                TryUpdateModel<AssetWorkingStatusDTO>(assetworkingstatusDTO);

                formCollection.Clear();

                if (assetworkingstatusDTO != null)
                {
                    //await this._AssetWorkingStatusRepository.DiscardChanges(assetworkingstatusDTO.Id);
                    //await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = assetworkingstatusDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
