using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GSAssetManagement.Entity;
using GSAssetManagement.Entity.DTO;
using GSAssetManagement.Infrastructure;
using GSAssetManagement.Repository;
using GSAssetManagement.Web;
using GSAssetManagement.Web.Utility;
using GSAssetManagement.Entity.Validation;
namespace GSAssetManagement.Web.Areas.DrillingReport.Controllers
{
    [ModuleInfo(ModuleName = ModuleName.DrillingReport, SubModuleName = "BranchDrillingReport", Url = "/DrillingReport/BranchDrillingReport", Parent = true)]
    [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "BranchDrillingReport", Action = CurrentAction.View)]
    [ExceptionHandler]
    public class BranchDrillingReportController : Controller
    {
        private readonly IBranchDrillingReportRepository _BranchDrillingReportRepository;
        private readonly IStaticDataDetailsRepository _StaticDataDetailsRepository;
        private IExceptionLoggerRepository _exceptionLoggerRepository;
        private readonly IUnitOfWork _unitOfWork;
        public BranchDrillingReportController(IBranchDrillingReportRepository branchdrillingreportRepository,
            IStaticDataDetailsRepository staticDataDetailsRepository,
        IUnitOfWork unitOfWork,
        IExceptionLoggerRepository exceptionLoggerRepository)
        {
            this._BranchDrillingReportRepository = branchdrillingreportRepository;
            _StaticDataDetailsRepository = staticDataDetailsRepository;
            _exceptionLoggerRepository = exceptionLoggerRepository;
            _unitOfWork = unitOfWork; ;
        }

        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "BranchDrillingReport", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            try
            {
                var deptId = ViewUserInformation.GetStaffDepartment();
                var branchId = ViewUserInformation.GetBranch();

                ModuleSummary moduleSummary = await _BranchDrillingReportRepository.GetModuleBussinesLogicSetup();
                moduleSummary.SchemaName = ModuleName.DrillingReport.ToString();

                if (deptId != "7" && deptId != "5")
                {
                    moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "BranchCode").ToList().ForEach(c =>
                    {
                        c.CurrentValue = branchId;
                        c.HtmlDataType = "hidden";
                        c.Attributes.Add("disabled", "");
                    });

                    moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ReportDateInBS" || x.ColumnName == "ReportMonth").ToList().ForEach(c =>
                    {
                        c.HtmlDataType = "hidden";
                    });
                }

                List<SqlParameter> sqlParameters = new List<SqlParameter>();

                sqlParameters.Add(new SqlParameter("BranchCode", branchId));
                sqlParameters.Add(new SqlParameter("PageNumber", 1));
                sqlParameters.Add(new SqlParameter("PageSize", 20));
                moduleSummary.SummaryRecord = await _BranchDrillingReportRepository.GetAllByProcedure("DrillingReport.SP_GetBranchDrillingReportList", sqlParameters.ToArray());
                return View(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "BranchDrillingReport", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SearchIndex(FormCollection SearchParameters)
        {
            try
            {
                ModuleSummary moduleSummary = await _BranchDrillingReportRepository.GetModuleBussinesLogicSetup();

                var sqlParameters = SearchParameters.GetSearchParameters(moduleSummary.moduleBussinesLogicSummaries);

                var deptId = ViewUserInformation.GetStaffDepartment();
                var branchId = ViewUserInformation.GetBranch() == "000" ? "1" : ViewUserInformation.GetBranch();
                if (deptId != "7" && deptId != "5")
                {
                    var branchCode = sqlParameters.Where(x => x.ParameterName == "BranchCode").FirstOrDefault();
                    if (branchCode == null)
                    {
                        sqlParameters.Add(new SqlParameter("BranchCode", branchId));
                    }
                }

                moduleSummary.SummaryRecord = await _BranchDrillingReportRepository.GetAllByProcedure("DrillingReport.SP_GetBranchDrillingReportList", sqlParameters.ToArray());

                SearchParameters.Clear();

                return PartialView(moduleSummary);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "BranchDrillingReport", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Create()
        {
            try
            {
                var deptId = ViewUserInformation.GetStaffDepartment();
                var branchId = ViewUserInformation.GetBranch() == "000" ? "1" : ViewUserInformation.GetBranch();

                var selectList = Enum.GetValues(typeof(WorkingStatus)).Cast<WorkingStatus>().Select(s => new Select() { ColumnName = "WorkingStatus", Title = s.DisplayName(), Value = s.ToString() }).ToList<Select>();


                //.Select(k => k.DisplayName(), v => (int)v);
                ViewBag.WorkingStatusSelectList = selectList;

                ModuleSummaryForDetails moduleSummary = await _BranchDrillingReportRepository.GetModuleBussinesLogicSetupWithChild(null, ViewUserInformation.GetBranch());
                moduleSummary.SchemaName = ModuleName.DrillingReport.ToString();

                //if (deptId != "7" && deptId != "5")
                //{
                var branchSelectList = moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "BranchCode").FirstOrDefault().SelectList;

                branchSelectList.ForEach(b =>
                {
                    b.Selected = b.Value == branchId ? true : false;
                });

                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "BranchCode").ToList().ForEach(c =>
                {
                    c.CurrentValue = branchId;
                    c.Attributes.Add("disabled", "");
                    c.SelectList = branchSelectList;
                });

                var dateInBS = NepaliCalendarBS.NepaliCalendar.Convert_AD2BS(DateTime.Now);
                int prevMonth = dateInBS.Month - 1;
                if (dateInBS.Month == 1)
                {
                    prevMonth = 12;
                }

                var pre_month = NepaliCalendarBS.NepaliCalendar.Get_Month_Name_English(prevMonth);
                var month = NepaliCalendarBS.NepaliCalendar.Get_Month_Name_English(dateInBS.Month);

                StaticDataDetailsDTO dto = await _StaticDataDetailsRepository.GetStaticDataByValue("ReportPeriod", month);
                string ReportPeriodData = "";
                if (dto != null)
                {
                    ReportPeriodData = dto.Value;
                }

                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ReportDateInBS").ToList().ForEach(c =>
                {
                    c.CurrentValue = dateInBS;
                    c.Attributes.Add("readonly", "");
                    c.Attributes.Remove("class");
                    c.Attributes.Add("class", "form-control");
                });

                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ReportDate").ToList().ForEach(c =>
                {
                    c.CurrentValue = DateTime.Today;
                    c.Attributes.Add("readonly", "");
                });

                var periodSelectList = moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ReportPeriod").FirstOrDefault().SelectList;
                periodSelectList.ForEach(m =>
                {
                    m.Selected = m.Value == ReportPeriodData ? true : false;
                });

                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ReportPeriod").ToList().ForEach(c =>
                {
                    c.CurrentValue = ReportPeriodData;
                    //c.Attributes.Add("readonly", "");
                    c.SelectList = periodSelectList;
                });

                var monthSelectList = moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ReportMonth").FirstOrDefault().SelectList;
                if (monthSelectList != null)
                {
                    monthSelectList.ForEach(m =>
                    {
                        m.Selected = m.Value == month ? true : false;
                    });
                }

                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ReportMonth").ToList().ForEach(c =>
                {
                    c.CurrentValue = month;
                    c.Attributes.Add("disabled", "");
                    c.SelectList = monthSelectList;
                });

                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ReportYear").ToList().ForEach(c =>
                {
                    c.CurrentValue = dateInBS.Year;
                    c.Attributes.Add("readonly", "");
                });
                //}

                if (!moduleSummary.IsParent && moduleSummary.DoRecordExists)
                {
                    return View("Details", moduleSummary);
                }
                else
                {
                    return View("Create", moduleSummary);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "BranchDrillingReport", Action = CurrentAction.Create)]
        [ExceptionHandler]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FormCollection model)
        {
            try
            {
                BranchDrillingReportDTO branchdrillingreportDTO = new BranchDrillingReportDTO();
                TryUpdateModel<BranchDrillingReportDTO>(branchdrillingreportDTO);
                branchdrillingreportDTO.CCTVBackupRemarks = model["CCTVBackup"];
                branchdrillingreportDTO.CCTVBackupStatus = model["CCTVStatus"];

                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<BranchDrillingReportDTO>(branchdrillingreportDTO);

                if (string.IsNullOrEmpty(branchdrillingreportDTO.ReportYear))
                {
                    EntityValidationResult entityValidationResult = new EntityValidationResult();
                    entityValidationResult.ColumnName = "Calendar Year";
                    entityValidationResult.Name = "Calendar Year";
                    entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Calendar Year", "Calendar Year is required."));

                    EntityValidationResults.Add(entityValidationResult);
                }

                if (string.IsNullOrEmpty(branchdrillingreportDTO.ReportPeriod))
                {
                    EntityValidationResult entityValidationResult = new EntityValidationResult();
                    entityValidationResult.ColumnName = "Report Period";
                    entityValidationResult.Name = "Report Period";
                    entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Report Period", "Report Period is required."));

                    EntityValidationResults.Add(entityValidationResult);
                }

                var count = model["AssetDetaillistCount"];

                for (int i = 1; i < int.Parse(count); i++)
                {
                    try
                    {
                        var categoryName = model["CategoryName-" + i.ToString()];

                        AssetWorkingStatusDTO assetWorkingStatusDTO = new AssetWorkingStatusDTO();
                        assetWorkingStatusDTO.AssetDetailId = Guid.Parse(model["AssetDetailId-" + i.ToString()]);
                        assetWorkingStatusDTO.AssetExtraDetailId = Guid.Parse(model["AssetExtraDetailId-" + i.ToString()]);
                        assetWorkingStatusDTO.Status = model["Status-" + i.ToString()];
                        assetWorkingStatusDTO.Remarks = model["Remarks-" + i.ToString()];

                        if (model.AllKeys.Contains("ExpireDate-" + i.ToString()))
                        {
                            var expireDateString = model["ExpireDate-" + i.ToString()];
                            if (string.IsNullOrEmpty(expireDateString))
                            {
                                EntityValidationResult entityValidationResult = new EntityValidationResult();
                                entityValidationResult.ColumnName = "Expire Date";
                                entityValidationResult.Name = "Expire Date";
                                entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Expire Date", "Expire Date is required."));

                                EntityValidationResults.Add(entityValidationResult);
                            }
                            else
                            {
                                DateTime dateTime;
                                if (DateTime.TryParse(model["ExpireDate-" + i.ToString()], out dateTime))
                                {
                                    assetWorkingStatusDTO.ExpireDate = dateTime;

                                    if (assetWorkingStatusDTO.ExpireDate <= DateTime.Today && assetWorkingStatusDTO.Status == "Working")
                                    {
                                        EntityValidationResult entityValidationResult = new EntityValidationResult();
                                        entityValidationResult.ColumnName = "Expire Date";
                                        entityValidationResult.Name = "Expire Date";
                                        entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Expire Date", "Expire Date should be greater than today."));

                                        EntityValidationResults.Add(entityValidationResult);
                                    }
                                }
                                else
                                {
                                    EntityValidationResult entityValidationResult = new EntityValidationResult();
                                    entityValidationResult.ColumnName = "Expire Date";
                                    entityValidationResult.Name = "Expire Date";
                                    entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Expire Date", "Expire Date is not correct in format."));

                                    EntityValidationResults.Add(entityValidationResult);
                                }
                            }
                        }


                        if (string.IsNullOrEmpty(assetWorkingStatusDTO.Status))
                        {
                            EntityValidationResult entityValidationResult = new EntityValidationResult();
                            entityValidationResult.ColumnName = "Status";
                            entityValidationResult.Name = "Status";
                            entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Status of " + categoryName, "Status is required."));

                            EntityValidationResults.Add(entityValidationResult);
                        }

                        if (assetWorkingStatusDTO.Status == "NotWorking" && string.IsNullOrEmpty(assetWorkingStatusDTO.Remarks))
                        {
                            EntityValidationResult entityValidationResult = new EntityValidationResult();
                            entityValidationResult.ColumnName = "Remarks";
                            entityValidationResult.Name = "Remarks";
                            entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Remarks of " + categoryName, "Remarks is required for Not working status"));

                            EntityValidationResults.Add(entityValidationResult);
                        }

                        branchdrillingreportDTO.AssetWorkingStatusDTOs.Add(assetWorkingStatusDTO);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                if (string.IsNullOrEmpty(branchdrillingreportDTO.CCTVBackupRemarks))
                {

                    EntityValidationResult entityValidationResult = new EntityValidationResult();
                    entityValidationResult.ColumnName = "CCTVBackupRemarks";
                    entityValidationResult.Name = "CCTVBackupRemarks";
                    entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("CCTVBackupRemarks", "CCTVBackupRemarks is required"));

                    EntityValidationResults.Add(entityValidationResult);
                }

                model.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    if (Request.Files.Count > 0)
                    {
                        HttpFileCollectionBase httpFileCollectionBases = Request.Files;
                        FileUploaderHelper.GetPath(httpFileCollectionBases);
                    }

                    branchdrillingreportDTO.ReportStatus = "Saved Draft";
                    var dateInAD = NepaliCalendarBS.NepaliCalendar.Convert_BS2AD(branchdrillingreportDTO.ReportDateInBS);
                    branchdrillingreportDTO.ReportDate = dateInAD;

                    var list = await this._BranchDrillingReportRepository.CheckBranchDrillingReportDTOs(branchdrillingreportDTO.BranchCode, branchdrillingreportDTO.ReportYear, branchdrillingreportDTO.ReportPeriod);
                    if (list.Count() == 0)
                    {
                        if (!string.IsNullOrEmpty(branchdrillingreportDTO.CCTVBackupRemarks))
                        {
                            if (int.Parse(branchdrillingreportDTO.CCTVBackupRemarks) < 90 && string.IsNullOrEmpty(branchdrillingreportDTO.CCTVBackupStatus))
                            {
                                branchdrillingreportDTO.CCTVBackupStatus = "Not properly working";
                            }
                        }

                        Guid Id = this._BranchDrillingReportRepository.AddBranchDrillingReport(branchdrillingreportDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.DrillingReport.ToString(), "BranchDrillingReport", CurrentAction.AutoAuthorise));
                        await this._unitOfWork.CommitAsync();

                        return new JsonHttpStatusResult(new
                        {
                            Id = Id,
                            IsSuccess = true,
                            ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                        }, System.Net.HttpStatusCode.OK);
                    }
                    else
                    {
                        return Json(new
                        {
                            IsSuccess = false,
                            ResponseView = "Drilling report for selected period and year already entry!"
                        }, JsonRequestBehavior.DenyGet);
                    }
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("ValidationResultView", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "BranchDrillingReport", Action = CurrentAction.View)]
        [ExceptionHandler]
        [HttpGet]
        public async Task<ActionResult> Details(Guid Id)
        {
            try
            {
                var deptId = ViewUserInformation.GetStaffDepartment();
                var branchId = ViewUserInformation.GetBranch();

                var selectList = Enum.GetValues(typeof(WorkingStatus)).Cast<WorkingStatus>().Select(s => new Select() { ColumnName = "WorkingStatus", Title = s.DisplayName(), Value = s.ToString() }).ToList<Select>();


                //.Select(k => k.DisplayName(), v => (int)v);
                ViewBag.WorkingStatusSelectList = selectList;

                ModuleSummaryForDetails moduleSummary = await _BranchDrillingReportRepository.GetModuleBussinesLogicSetupWithChild(Id, ViewUserInformation.GetBranch());
                moduleSummary.SchemaName = ModuleName.DrillingReport.ToString();

                //if (deptId != "7" && deptId != "5")
                //{
                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "BranchCode").ToList().ForEach(c =>
                {
                    c.Attributes.Add("disabled", "");
                });

                //var dateInBS = NepaliCalendarBS.NepaliCalendar.Convert_AD2BS(DateTime.Now);

                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ReportDateInBS").ToList().ForEach(c =>
                {
                    c.Attributes.Add("readonly", "");
                });

                moduleSummary.moduleBussinesLogicSummaries.Where(x => x.ColumnName == "ReportMonth").ToList().ForEach(c =>
                {
                    c.Attributes.Add("disabled", "");
                });
                //}

                //var selectList = new SelectList(Enum.GetValues(typeof(WorkingStatus))
                //                        .Cast<WorkingStatus>()
                //                        .ToDictionary(k => k.DisplayName(), v => (int)v));

                //ViewBag.WorkingStatusSelectList = selectList;

                //var dto = await _BranchDrillingReportRepository.GetDTOById(Id);
                //ModuleSummary moduleSummary = await _BranchDrillingReportRepository.GetModuleBussinesLogicSetup(dto, null);
                if (Request.IsAjaxRequest())
                {
                    return View("PartialDetails", moduleSummary);
                }
                else
                {
                    return View("Details", moduleSummary);
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "BranchDrillingReport", Action = CurrentAction.Edit)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Update(FormCollection formCollection)
        {
            try
            {

                BranchDrillingReportDTO branchdrillingreportDTO = new BranchDrillingReportDTO();
                TryUpdateModel<BranchDrillingReportDTO>(branchdrillingreportDTO);
                branchdrillingreportDTO.CCTVBackupRemarks = formCollection["CCTVBackup"];
                branchdrillingreportDTO.CCTVBackupStatus = formCollection["CCTVStatus"];

                List<EntityValidationResult> EntityValidationResults = ValidationAttribute.IsValid<BranchDrillingReportDTO>(branchdrillingreportDTO);

                var count = formCollection["AssetDetaillistCount"];

                if (string.IsNullOrEmpty(branchdrillingreportDTO.ReportYear))
                {
                    EntityValidationResult entityValidationResult = new EntityValidationResult();
                    entityValidationResult.ColumnName = "Calendar Year";
                    entityValidationResult.Name = "Calendar Year";
                    entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Calendar Year", "Calendar Year is required."));

                    EntityValidationResults.Add(entityValidationResult);
                }

                if (string.IsNullOrEmpty(branchdrillingreportDTO.ReportPeriod))
                {
                    EntityValidationResult entityValidationResult = new EntityValidationResult();
                    entityValidationResult.ColumnName = "Report Period";
                    entityValidationResult.Name = "Report Period";
                    entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Report Period", "Report Period is required."));

                    EntityValidationResults.Add(entityValidationResult);
                }

                for (int i = 1; i < int.Parse(count); i++)
                {
                    var categoryName = formCollection["CategoryName-" + i.ToString()];

                    AssetWorkingStatusDTO assetWorkingStatusDTO = new AssetWorkingStatusDTO();
                    assetWorkingStatusDTO.BranchDrillingReportId = branchdrillingreportDTO.Id;
                    assetWorkingStatusDTO.Id = Guid.Parse(formCollection["AssetWorkingStatusId-" + i.ToString()]);
                    assetWorkingStatusDTO.AssetDetailId = Guid.Parse(formCollection["AssetDetailId-" + i.ToString()]);
                    assetWorkingStatusDTO.AssetExtraDetailId = Guid.Parse(formCollection["AssetExtraDetailId-" + i.ToString()]);
                    assetWorkingStatusDTO.Status = formCollection["Status-" + i.ToString()];
                    assetWorkingStatusDTO.Remarks = formCollection["Remarks-" + i.ToString()];

                    if (formCollection.AllKeys.Contains("ExpireDate-" + i.ToString()))
                    {
                        var expireDateString = formCollection["ExpireDate-" + i.ToString()];
                        if (string.IsNullOrEmpty(expireDateString))
                        {
                            EntityValidationResult entityValidationResult = new EntityValidationResult();
                            entityValidationResult.ColumnName = "Expire Date";
                            entityValidationResult.Name = "Expire Date";
                            entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Expire Date", "Expire Date is required."));

                            EntityValidationResults.Add(entityValidationResult);
                        }
                        else
                        {
                            DateTime dateTime;
                            if (DateTime.TryParse(formCollection["ExpireDate-" + i.ToString()], out dateTime))
                            {
                                assetWorkingStatusDTO.ExpireDate = dateTime;

                                if (assetWorkingStatusDTO.ExpireDate <= DateTime.Today && assetWorkingStatusDTO.Status == "Working")
                                {
                                    EntityValidationResult entityValidationResult = new EntityValidationResult();
                                    entityValidationResult.ColumnName = "Expire Date";
                                    entityValidationResult.Name = "Expire Date";
                                    entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Expire Date", "Expire Date should be greater than today."));

                                    EntityValidationResults.Add(entityValidationResult);
                                }
                            }
                            else
                            {
                                EntityValidationResult entityValidationResult = new EntityValidationResult();
                                entityValidationResult.ColumnName = "Expire Date";
                                entityValidationResult.Name = "Expire Date";
                                entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Expire Date", "Expire Date is not correct in format."));

                                EntityValidationResults.Add(entityValidationResult);
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(assetWorkingStatusDTO.Status))
                    {
                        EntityValidationResult entityValidationResult = new EntityValidationResult();
                        entityValidationResult.ColumnName = "Status";
                        entityValidationResult.Name = "Status";
                        entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Status of " + categoryName, "Status is required."));

                        EntityValidationResults.Add(entityValidationResult);
                    }

                    if (assetWorkingStatusDTO.Status == "NotWorking" && string.IsNullOrEmpty(assetWorkingStatusDTO.Remarks))
                    {
                        EntityValidationResult entityValidationResult = new EntityValidationResult();
                        entityValidationResult.ColumnName = "Remarks";
                        entityValidationResult.Name = "Remarks";
                        entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("Remarks of " + categoryName, "Remarks is required for Not working status"));

                        EntityValidationResults.Add(entityValidationResult);
                    }

                    branchdrillingreportDTO.AssetWorkingStatusDTOs.Add(assetWorkingStatusDTO);
                }

                if (string.IsNullOrEmpty(branchdrillingreportDTO.CCTVBackupRemarks))
                {

                    EntityValidationResult entityValidationResult = new EntityValidationResult();
                    entityValidationResult.ColumnName = "CCTVBackupRemarks";
                    entityValidationResult.Name = "CCTVBackupRemarks";
                    entityValidationResult.ValidationResultDetails.Add(new ValidationResultDetails("CCTVBackupRemarks", "CCTVBackupRemarks is required"));

                    EntityValidationResults.Add(entityValidationResult);
                }

                formCollection.Clear();

                if (EntityValidationResults.Count() == 0)
                {
                    //branchdrillingreportDTO.ReportStatus = "Approved";
                    var list = await this._BranchDrillingReportRepository.CheckBranchDrillingReportDTOs(branchdrillingreportDTO.BranchCode, branchdrillingreportDTO.ReportYear, branchdrillingreportDTO.ReportPeriod);
                    var newList = list.Where(x => x.Id != branchdrillingreportDTO.Id).ToList();
                    if (newList.Count() == 0)
                    {
                        if (!string.IsNullOrEmpty(branchdrillingreportDTO.CCTVBackupRemarks))
                        {
                            if (int.Parse(branchdrillingreportDTO.CCTVBackupRemarks) < 90 && string.IsNullOrEmpty(branchdrillingreportDTO.CCTVBackupStatus))
                            {
                                branchdrillingreportDTO.CCTVBackupStatus = "Not properly working";
                            }
                        }

                        await this._BranchDrillingReportRepository.UpdateBranchDrillingReport(branchdrillingreportDTO, AuthorizeViewHelper.IsAuthorize(ModuleName.DrillingReport.ToString(), "BranchDrillingReport", CurrentAction.AutoAuthorise));
                        await this._unitOfWork.CommitAsync();
                        return Json(new
                        {
                            Id = branchdrillingreportDTO.Id,
                            IsSuccess = true,
                            ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", EntityValidationResults)
                        }, JsonRequestBehavior.DenyGet);
                    }
                    else
                    {
                        return Json(new
                        {
                            IsSuccess = false,
                            ResponseView = "Drilling report for selected period and year already entry!"
                        }, JsonRequestBehavior.DenyGet);
                    }
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", EntityValidationResults)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "BranchDrillingReport", Action = CurrentAction.Delete)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Delete(FormCollection formCollection)
        {
            try
            {

                BranchDrillingReportDTO branchdrillingreportDTO = new BranchDrillingReportDTO();
                TryUpdateModel<BranchDrillingReportDTO>(branchdrillingreportDTO);

                formCollection.Clear();

                if (branchdrillingreportDTO != null)
                {
                    await this._BranchDrillingReportRepository.Delete(branchdrillingreportDTO.Id, AuthorizeViewHelper.IsAuthorize(ModuleName.DrillingReport.ToString(), "BranchDrillingReport", CurrentAction.AutoAuthorise));
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = branchdrillingreportDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "BranchDrillingReport", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Authorise(FormCollection formCollection)
        {
            try
            {

                BranchDrillingReportDTO branchdrillingreportDTO = new BranchDrillingReportDTO();
                TryUpdateModel<BranchDrillingReportDTO>(branchdrillingreportDTO);

                formCollection.Clear();

                if (branchdrillingreportDTO != null)
                {
                    await this._BranchDrillingReportRepository.Authorise(branchdrillingreportDTO.Id);
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = branchdrillingreportDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "BranchDrillingReport", Action = CurrentAction.Authorise)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Revert(FormCollection formCollection)
        {
            try
            {

                BranchDrillingReportDTO branchdrillingreportDTO = new BranchDrillingReportDTO();
                TryUpdateModel<BranchDrillingReportDTO>(branchdrillingreportDTO);

                formCollection.Clear();

                if (branchdrillingreportDTO != null)
                {
                    await this._BranchDrillingReportRepository.Revert(branchdrillingreportDTO.Id);
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = branchdrillingreportDTO.Id,
                        IsSuccess = true,
                        ResponseView = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [CRUDAuthorize(ModuleName = ModuleName.DrillingReport, SubModuleName = "BranchDrillingReport", Action = CurrentAction.Discard)]
        [ExceptionHandler]
        [HttpPost]
        public async Task<ActionResult> Discard(FormCollection formCollection)
        {
            try
            {

                BranchDrillingReportDTO branchdrillingreportDTO = new BranchDrillingReportDTO();
                TryUpdateModel<BranchDrillingReportDTO>(branchdrillingreportDTO);

                formCollection.Clear();

                if (branchdrillingreportDTO != null)
                {
                    await this._BranchDrillingReportRepository.DiscardChanges(branchdrillingreportDTO.Id);
                    await this._unitOfWork.CommitAsync();
                    return Json(new
                    {
                        Id = branchdrillingreportDTO.Id,
                        IsSuccess = true,
                        ResponseMessage = this.RenderRazorViewToString("SuccessfulResponseView", null)
                    }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResponseView = this.RenderRazorViewToString("RecordNotFound", null)
                    }, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
