﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GSAssetManagement.Web.Startup))]
namespace GSAssetManagement.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
