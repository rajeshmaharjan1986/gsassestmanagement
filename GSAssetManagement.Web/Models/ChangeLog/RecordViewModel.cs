﻿using GSAssetManagement.Entity.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GSAssetManagement.Web.Models
{
    public class RecordViewModel
    {
        public RecordViewModel()
        {
            RecordsList = new List<Records>();
            FieldRuleList = new List<FieldRule>();
        }

        public string SectionTitle { get; set; }
        public string Section { get; set; }
        public List<Records> RecordsList { get; set; }
        public List<FieldRule> FieldRuleList { get; set; }

    }

    public class Records
    {
        public Records()
        {
            FieldDetailsList = new List<FieldDetails>();
            ArrayOfChangeLogDTO = new List<ChangeLogDTO>();
        }

        public List<FieldDetails> FieldDetailsList { get; set; }
        public List<ChangeLogDTO> ArrayOfChangeLogDTO { get; set; }
    }

    public class FieldDetails
    {
        public FieldDetails()
        {
            ParameterList = new List<Parameter>();
        }
        public string PropertyTitle{ get; set; }
        public string PropertyHelper { get; set; }
        public string PropertyName { get; set; }
        public object PropertyValue { get; set; }
        public string HtmlDataType { get; set; }
        public string DataType { get; set; }
        public string _paramter { get; set; }
        public int Order { get; set; }
        public List<Parameter> ParameterList { get; set; }
        public bool ParameterisedSource { get; set; }
        public string TableName { get; set; }
        public SelectList selectListItems { get; set; }
    }
}