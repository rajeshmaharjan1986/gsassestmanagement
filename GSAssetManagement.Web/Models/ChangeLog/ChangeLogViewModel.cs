﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GSAssetManagement.Web.Models
{
    public class ChangeLogViewModel
    {
        public ChangeLogViewModel()
        {
            LogList = new List<Logs>();
        }

        public string SectionName { get; set; }
        public List<Logs> LogList { get; set; }
    }

    public class Logs
    {
        public string PropertyName { get; set; }
        public object Originalvalue { get; set; }
        public object ModifiedValue { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string LogStatus { get; set; }
    }
}