﻿using GSAssetManagement.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GSAssetManagement.Web.Models
{
    public class MenuModel
    {
        public string MenuHeadName { get; set; }
        public string IconName { get; set; }
        public ModuleName ModuleName { get; set; }
        public List<SubMenu> SubmenuList { get; set; }
    }

    public class SubMenu
    {
        public string SubModuleName { get; set; }
        public string IconName { get; set; }
        public string Url { get; set; }

    }

    public class XMLMenuModel
    {
        public string Position { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string IconName { get; set; }
        public List<XMLSubMenu> SubmenuList { get; set; }
    }
    public class XMLSubMenu
    {
        public string Position { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string IconName { get; set; }
        public string Url { get; set; }
        public string AuthViewModule { get; set; }
    }
}