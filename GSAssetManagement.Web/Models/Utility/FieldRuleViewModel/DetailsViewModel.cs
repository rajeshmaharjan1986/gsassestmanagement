﻿using GSAssetManagement.Entity.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GSAssetManagement.Web.Models.Utility
{
    public class DetailsViewModel
    {
        public DetailsViewModel()
        {
            FieldRuleList = new List<Utility.FieldRuleViewModel>();
            ChangeLogList = new List<ChangeLogDTO>();
            
        }
        public List<FieldRuleViewModel> FieldRuleList { get; set; }
        public List<ChangeLogDTO> ChangeLogList { get; set; }
        

    }
}