﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GSAssetManagement.Web.Models.Utility
{
    public class FieldRuleViewModel
    {
        public FieldRuleViewModel()
        {
            Attributes = new Dictionary<string, object>();
        }
        public string ColumnName { get; set; }
        public string FieldTitle { get; set; }
        public string DataType { get; set; }
        public string HtmlDataType { get; set; }
        public bool IsRequired { get; set; }
        public string Regex { get; set; }
        public string Length { get; set; }
        public string ErrorMessage { get; set; }
        public string HelpMessage { get; set; }
        public Nullable<decimal> RangeForm { get; set; }
        public Nullable<decimal> RangeTo { get; set; }
        public bool IsUpdateable { get; set; }
        public int Order { get; set; }
        public bool SummaryHeader { get; set; }
        public bool Enable { get; set; }
        public bool SearchField { get; set; }
        public bool Hidden { get; set; }
        public string HtmlClass { get; set; }
        public bool ReadOnly { get; set; }
        public bool EnableRemoteValidation { get; set; }
        public string RemoteValidationService { get; set; }
        public string RemoteParameters { get; set; }
        public string Datasource { get; set; }
        public bool ParameterisedSource { get; set; }
        public string Parameter { get; set; }
        public object Value { get; set; }
        public object DefaultValue { get; set; }
        public string FieldString1 { get; set; }
        public string FieldString2 { get; set; }
        public string FieldString3 { get; set; }
        public string FieldString4 { get; set; }
        public string FieldString5 { get; set; }
        public IDictionary<string, object> Attributes { get; set; }
        public SelectList DropdownMasterList { get; set; }
        public MultiSelectList MultiSelectDropdownMasterList { get; set; }
    }
}