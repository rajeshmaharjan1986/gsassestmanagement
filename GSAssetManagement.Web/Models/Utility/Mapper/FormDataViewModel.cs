﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GSAssetManagement.Web.Models.Utility
{
    public class FormDataViewModel
    {
        public string FormFieldName { get; set; }
        public object FormFieldValue { get; set; }
        public object Originalvalue { get; set; }
        public object Modifiedvalue { get; set; }
        public bool IsUpdated { get; set; }
        public bool IsValid { get; set; }
        public bool IsSearch { get; set; }
        public EntityState RecordState { get; set; }
        public string ErrorMessage { get; set; }
    }


}