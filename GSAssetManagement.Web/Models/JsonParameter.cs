﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GSAssetManagement.Web.Models
{
    public class JsonParameter
    {
        public string Name { get; set; }
        public object Value { get; set; }
    }

    public class ParameterList
    {
        public ParameterList()
        {
            Parameters = new List<JsonParameter>();
        }

        public string EntityName { get; set; }
        public string FieldName { get; set; }
        public List<JsonParameter> Parameters { get; set; }
    }
}